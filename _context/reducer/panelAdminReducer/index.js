import { Provider as optionReducerProvider, Context as optionReducerContext } from "./optionReducer";

const panelAdminReducer = {
  optionReducerProvider,
  optionReducerContext,
};
export default panelAdminReducer;
