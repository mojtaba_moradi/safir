import React from "react";
import PanelScreen from "../../panelAdmin/screen/PanelScreen";
const routingHandle = ({ Component, pageProps, router }) => {
  if (Component.panelAdminLayout || router?.asPath === "/panelAdmin/")
    return (
      <PanelScreen>
        <Component {...pageProps} />
      </PanelScreen>
    );
  else return <Component {...pageProps} />;
};

export default routingHandle;
