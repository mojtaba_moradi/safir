import React from "react";

import PanelScreen from "../../panelAdmin/screen/PanelScreen";

const panelStatic = (props) => {
  <PanelScreen>
    {/* <Component {...pageProps} /> */}
    {props.children}
  </PanelScreen>;
};

export default panelStatic;
