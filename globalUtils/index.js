import Loading from "./Loading";
import axiosBase from "./axiosBase";
import globalHoc from "./globalHoc";
const globalUtils = {
  Loading,
  axiosBase,
  globalHoc,
};

export default globalUtils;
