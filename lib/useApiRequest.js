import useSWR from "swr";
import axios from "../globalUtils/axiosBase";

export default function useApiRequest(request, { initialData, ...config } = {}) {
  return useSWR(
    request && JSON.stringify(request),
    () =>
      axios(request || {}).then((response) => {
        // //console.log({ response });

        return response.data;
      }),
    {
      ...config,
      initialData,
    }
  );
}
