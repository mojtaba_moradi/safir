import React, { useEffect } from "react";
import axios from "../globalUtils/axiosBase";
import useSWR, { mutate, trigger } from "swr";
import panelAdmin from "../panelAdmin";
import { useRouter } from "next/router";
const home = ({ resData }) => {
  // //console.log({ resData });
  const router = useRouter();
  // ======================================================== SWR
  // const { data } = useSWR("/homescreen", { initialData: resData });
  useEffect(() => {
    console.log("home : ", { router });

    router.push("/panelAdmin");
  }, []);
  // ======================================================== COMPONENT
  return <div>go to admin ...</div>;
};

// ======================================================== getInitialProps
// home.getInitialProps = async () => {
//   const resData = await axios
//     .get("/homescreen")
//     .then((r) => r.data)
//     .catch((e) => e);
//   return { resData };
// };

export default home;
