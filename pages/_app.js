import App from "next/app";
import Head from "next/head";
import "../public/styles/index.scss";
import { SWRConfig } from "swr";
import axios from "../globalUtils/axiosBase";
import PanelScreen from "../panelAdmin/screen/PanelScreen";
import routingHandle from "../globalUtils/globalHoc/routingHandle";
import globalUtils from "../globalUtils";
// const PanelScreen = dynamic(() => import("../panelAdmin/screen/PanelScreen"), { ssr: false });
class safirApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ctx });
    }

    return { pageProps };
  }
  render() {
    const { Component, pageProps, router } = this.props;

    return (
      <div>
        <Head>
          <title>{"سفیر "}</title>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.3.1/leaflet.css" />
          <link href="https://unpkg.com/leaflet-geosearch@latest/assets/css/leaflet.css" rel="stylesheet" />
          <meta name="description" content="    ." />
          <meta name="keywords" content=" ,,IPTV," />
          <meta content="width=device-width, initial-scale=1" name="viewport" />
          <link href={"/styles/css/styles.css"} rel={"stylesheet"} />
        </Head>

        <div className="base-page">
          {/* {body} */}
          {/* <SWRConfig value={{ refreshInterval: 0, fetcher: (...args) => axios(...args).then((r) => r.data) }}> */}
          {globalUtils.globalHoc.routingHandle({ Component, pageProps, router })}

          {/* <Component {...pageProps} /> */}
          {/* </SWRConfig> */}
        </div>
      </div>
    );
  }
}

export default safirApp;

// import Layout from ""
// const Products = ()=>{

// }

// Products.Layout = Layout
// ================

// const App = ({Component})=>{
//   const Layout  = Component.Layout;
//   return Layout ? <Layout><Component/></Layout> : <Component/>
// }
