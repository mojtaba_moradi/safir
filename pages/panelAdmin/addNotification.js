import React, { useEffect, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import AddNotification from "../../panelAdmin/screen/Notification/AddNotification";
import reducer from "../../_context/reducer";
const addNotification = (props) => {
  const Context = reducer.panelAdminReducer.optionReducerContext;

  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;

  useEffect(() => {
    dispatch.changePageName(panelAdmin.values.strings.ADD_NOTIFICATION);
  }, []);
  return <AddNotification />;
};
addNotification.panelAdminLayout = true;

export default addNotification;
