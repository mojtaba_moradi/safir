import React, { useEffect, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import reducer from "../../_context/reducer";
import AddOwner from "../../panelAdmin/screen/Owner/AddOwner";

const addOwner = (props) => {
  const Context = reducer.panelAdminReducer.optionReducerContext;

  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;

  useEffect(() => {
    dispatch.changePageName(panelAdmin.values.strings.ADD_OWNER);
  }, []);
  return <AddOwner />;
};
addOwner.panelAdminLayout = true;

// addOwner.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   return { isServer };
// };

export default addOwner;
