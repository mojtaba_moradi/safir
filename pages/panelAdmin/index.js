import React, { useEffect } from "react";
import { useRouter } from "next/router";
import Cookie from "js-cookie";
import panelAdmin from "../../panelAdmin";

const PanelAdmin = (props) => {
  let router = useRouter();

  useEffect(() => {
    console.log("panelAdmin/index");
    if (!panelAdmin.utils.authorization()) router.push("/panelAdmin/login");
    else router.push("/panelAdmin/dashboard");
  });

  // useEffect(() => {
  //   // The counter changed!
  // }, [router.query.counter]);
  return <div></div>;
};

export default PanelAdmin;
