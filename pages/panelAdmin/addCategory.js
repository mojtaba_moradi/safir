import React, { useEffect, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import AddCategory from "../../panelAdmin/screen/Category/AddCategory";
import reducer from "../../_context/reducer";
const addCategory = (props) => {
  const Context = reducer.panelAdminReducer.optionReducerContext;

  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;

  useEffect(() => {
    dispatch.changePageName(panelAdmin.values.strings.ADD_CATEGORY);
  }, []);
  return <AddCategory />;
};
addCategory.panelAdminLayout = true;

export default addCategory;
