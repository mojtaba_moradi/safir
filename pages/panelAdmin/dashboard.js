import React, { useEffect, useContext } from "react";
import DashboardScreen from "../../panelAdmin/screen/DashboardScreen";
import reducer from "../../_context/reducer";

// =============================== import context

const dashBoard = (props) => {
  const Context = reducer.panelAdminReducer.optionReducerContext;

  const giveContextData = useContext(Context);
  const dispatch = giveContextData?.dispatch;

  useEffect(() => {
    dispatch.changePageName("داشبورد");
  }, []);

  return <DashboardScreen />;
};
dashBoard.panelAdminLayout = true;
// =============================== getInitialProps
// dashBoard.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   // store.dispatch(sagaActions.getHomeData());
//   return { isServer };
// };
export default dashBoard;
