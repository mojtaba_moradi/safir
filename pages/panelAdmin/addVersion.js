import React, { useEffect, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import reducer from "../../_context/reducer";
import AddVersion from "../../panelAdmin/screen/Version/AddVersion";
const addVersion = (props) => {
  const Context = reducer.panelAdminReducer.optionReducerContext;

  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;

  useEffect(() => {
    dispatch.changePageName(panelAdmin.values.strings.ADD_VERSION);
  }, []);
  return <AddVersion />;
};
addVersion.panelAdminLayout = true;

export default addVersion;
