import React, { useEffect, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import reducer from "../../_context/reducer";
import AddStore from "../../panelAdmin/screen/Store/AddStore";

const addStore = (props) => {
  const Context = reducer.panelAdminReducer.optionReducerContext;

  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;

  useEffect(() => {
    dispatch.changePageName(panelAdmin.values.strings.ADD_STORE);
  }, []);
  return <AddStore />;
};
addStore.panelAdminLayout = true;

// addStore.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   return { isServer };
// };

export default addStore;
