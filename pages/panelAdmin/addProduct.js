import React, { useEffect, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import AddProduct from "../../panelAdmin/screen/Product/AddProduct";
import reducer from "../../_context/reducer";

const addProduct = (props) => {
  const Context = reducer.panelAdminReducer.optionReducerContext;

  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;

  useEffect(() => {
    dispatch.changePageName(panelAdmin.values.strings.ADD_PRODUCT);
  }, []);
  return <AddProduct />;
};
addProduct.panelAdminLayout = true;

// addProduct.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   return { isServer };
// };

export default addProduct;
