import React, { useEffect, useState, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import useSWR, { mutate, trigger } from "swr";
import reducer from "../../_context/reducer";
import useApiRequest from "../../lib/useApiRequest";
import { useRouter } from "next/router";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";
import VersionScreen from "../../panelAdmin/screen/Version/VersionScreen";

const version = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const { resData, isServer } = props;
  const giveContextData = useContext(Context);
  const [currentPage, setCurrentPage] = useState(1);
  const [loadingApi, setLoadingApi] = useState(true);
  const [state, setState] = useState(false);
  // const { query } = useRouter();
  // const [serverQuery] = useState(query);
  // //console.log({ serverQuery, query });

  const { dispatch } = giveContextData;
  useEffect(() => {
    dispatch.changePageName("ورژن");
    apiPageFetch();
  }, []);
  // ======================================================== SWR
  // const { data: image } = useApiRequest(strings.version + "/" + currentPage, { initialData: resData });
  const apiPageFetch = async (page = 1) => {
    if (!page) return;
    setLoadingApi(true);
    const res = await panelAdmin.api.get.versions();
    const resData = res.data;
    //console.log({ res });

    setState(res.data);
    setLoadingApi(false);
  };
  const onDataSearch = ({ title, page = 1 }) => {
    dispatch(sagaActions.getSearchversionData({ title, page }));
  };

  return (
    <>
      {" "}
      <VersionScreen requestData={state} apiPageFetch={apiPageFetch} data={resData} />
      {loadingApi ? (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      ) : (
        ""
      )}
    </>
  );
  // return true;
};
// version.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.categories({ page: "1" });
//   const resData = res.data;

//   return { resData, isServer };
// };
version.panelAdminLayout = true;

export default version;
