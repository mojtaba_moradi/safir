import React, { useEffect, useContext, useState, useRef } from "react";
import reducer from "../../_context/reducer";
import panelAdmin from "../../panelAdmin";
import useSWR, { mutate, trigger } from "swr";
import useApiRequest from "../../lib/useApiRequest";
import globalUtils from "../../globalUtils";
// import UserScreen from "../../panelAdmin/screen/Store/StoreScreen";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";
import UserScreen from "../../panelAdmin/screen/User/UserScreen";

// const axios = globalUtils.axiosBase;
// const fetcher = (url) => axios(url).then((r) => r.json());

const user = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue, isServer } = props;
  const [state, setState] = useState(false);
  const [searchData, setSearchData] = useState(false);

  const CurrentPage = state?.page || "1";
  const [loadingApi, setLoadingApi] = useState(true);
  let searchTitle = useRef(null);
  // ======================================================== SWR
  // const { data } = useSWR(strings.IMAGE, fetcher, { initialData: resData });
  // const { data: image } = useApiRequest(strings.IMAGE + "/" + CurrentPage, { initialData: resData }, { refreshInterval: 0 });
  useEffect(() => {
    dispatch.changePageName("کاربران");
    apiPageFetch();
  }, []);
  // //console.log({ data, resData });

  const apiPageFetch = async (page) => {
    // if (!page) return;
    setLoadingApi(true);
    const res = await panelAdmin.api.get.users(page || state?.page || "1");
    //console.log({ res });
    setState(res);
    setLoadingApi(false);
  };
  const onDataSearch = async (page, value) => {
    //console.log({ page, value });
    if (value && !page) searchTitle.current = value;
    else if (!page) searchTitle.current = "";

    const resDataSearch = await panelAdmin.api.get.userSearch(value || searchTitle.current, page || searchData?.page || "1");
    //console.log({ resDataSearch });
    setSearchData(resDataSearch);
  };
  return (
    <>
      <UserScreen requestData={searchData || state} onDataSearch={onDataSearch} acceptedCardInfo={acceptedCardInfo} apiPageFetch={searchData ? onDataSearch : apiPageFetch} />
    </>
  );
  return true;
};
// ========================================= getInitialProps
// user.getInitialProps = async (props) => {
//   const { user, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.user({ page: "1" });
//   const resData = res.data;

//   return { resData, isServer };
// };
user.panelAdminLayout = true;

export default user;
