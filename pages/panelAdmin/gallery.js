import React, { useEffect, useContext, useState, Fragment } from "react";
import GalleryScreen from "../../panelAdmin/screen/Gallery/GalleryScreen";
import reducer from "../../_context/reducer";
import panelAdmin from "../../panelAdmin";
import useSWR, { mutate, trigger } from "swr";
import useApiRequest from "../../lib/useApiRequest";
import globalUtils from "../../globalUtils";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";

// const axios = globalUtils.axiosBase;
// const fetcher = (url) => axios(url).then((r) => r.json());

const gallery = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue, isServer } = props;
  const [loadingApi, setLoadingApi] = useState(true);
  const [state, setState] = useState(false);
  const CurrentPage = state?.page || "1";
  // //console.log({ resData });

  // ======================================================== SWR
  // const { data } = useSWR(strings.IMAGE, fetcher, { initialData: resData });
  // const { data: resData } = useApiRequest(strings.IMAGE + "/" + CurrentPage, { initialData: state }, { refreshInterval: 0 });
  useEffect(() => {
    !acceptedCardInfo && dispatch.changePageName("گالری");
    apiPageFetch("1");
  }, []);
  // //console.log({ resData });

  const apiPageFetch = async (page = CurrentPage) => {
    if (!page) return;
    setLoadingApi(true);
    const res = await panelAdmin.api.get.gallery({ page });
    // //console.log({ res });
    setState(res?.data);
    setLoadingApi(false);
  };

  return (
    <Fragment>
      <GalleryScreen requestData={state} acceptedCardInfo={acceptedCardInfo} apiPageFetch={apiPageFetch} />
      {loadingApi ? (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      ) : (
        ""
      )}
    </Fragment>
  );
  // return true;
};
gallery.panelAdminLayout = true;
// ========================================= getInitialProps
// gallery.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.gallery({ page: "1" });
//   const resData = res.data;

//   return { resData, isServer };
// };

export default gallery;
