import React, { useEffect, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import reducer from "../../_context/reducer";
import AddSlider from "../../panelAdmin/screen/Slider/AddSlider";

const addSlider = (props) => {
  const Context = reducer.panelAdminReducer.optionReducerContext;

  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;

  useEffect(() => {
    dispatch.changePageName(panelAdmin.values.strings.SLIDER);
  }, []);
  return <AddSlider />;
};
addSlider.panelAdminLayout = true;

// addSlider.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   return { isServer };
// };

export default addSlider;
