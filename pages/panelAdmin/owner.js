import React, { useEffect, useContext, useState, useRef } from "react";
import OwnerScreen from "../../panelAdmin/screen/Owner/OwnerScreen";
import reducer from "../../_context/reducer";
import panelAdmin from "../../panelAdmin";
import useSWR, { mutate, trigger } from "swr";
import useApiRequest from "../../lib/useApiRequest";
import globalUtils from "../../globalUtils";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";

// const axios = globalUtils.axiosBase;
// const fetcher = (url) => axios(url).then((r) => r.json());

const owner = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue } = props;
  const [state, setState] = useState(false);
  const [searchData, setSearchData] = useState(false);
  const [loadingApi, setLoadingApi] = useState(true);
  const CurrentPage = state?.page || "1";
  // const isSearchData = useRef(false);
  const [isSearchData, setIsSearchData] = useState(false);
  let searchTitle = useRef(null);
  // ======================================================== SWR
  // const { data } = useSWR(strings.IMAGE, fetcher, { initialData: resData });
  // const { data: image } = useApiRequest(strings.IMAGE + "/" + CurrentPage, { initialData: resData }, { refreshInterval: 0 });
  useEffect(() => {
    dispatch.changePageName("فروشنده");
    apiPageFetch();
  }, []);
  // //console.log({ data, resData });

  const apiPageFetch = async (page) => {
    // if (!page) return;
    setLoadingApi(true);
    const res = await panelAdmin.api.get.owners(page || state?.page || "1");
    console.log({ res });
    setState(res?.data);
    setLoadingApi(false);
  };
  const onDataSearch = async (page, value) => {
    // //console.log({ value, page }, value && !page);
    if (value && !page) searchTitle.current = value;
    else if (!page) searchTitle.current = "";
    const resDataSearch = await panelAdmin.api.get.ownersSearch(value || searchTitle.current, page || searchData?.page || "1");
    setSearchData(resDataSearch);
  };
  // //console.log({ isSearchData: isSearchData.current });

  return (
    <>
      <OwnerScreen requestData={searchData || state} onDataSearch={onDataSearch} acceptedCardInfo={acceptedCardInfo} apiPageFetch={searchData ? onDataSearch : apiPageFetch} />
    </>
  );
  // return true;
};
// ========================================= getInitialProps
// owner.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.owners({ page: "1" });
//   const resData = res.data;

//   return { resData, isServer };
// };
owner.panelAdminLayout = true;

export default owner;
