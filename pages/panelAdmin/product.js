import React, { useEffect, useState, useContext } from "react";
import panelAdmin from "../../panelAdmin";
import ProductScreen from "../../panelAdmin/screen/Product/ProductScreen";
import reducer from "../../_context/reducer";

const product = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const { resData, isServer } = props;
  const giveContextData = useContext(Context);
  const [state, setState] = useState(false);

  const { dispatch } = giveContextData;
  useEffect(() => {
    dispatch.changePageName("محصولات");
  }, []);

  const apiPageFetch = async (page = 1) => {
    const res = await panelAdmin.api.get.products({ page });
    setState(res.data);
  };

  const onDataSearch = ({ title = "", page = 1 }) => {
    dispatch(sagaActions.getSearchCategoryData({ title, page }));
  };

  return <ProductScreen ApiData={state || resData} apiPageFetch={apiPageFetch} />;
  // return true;
};
// product.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.products({ page: "1" });
//   const resData = res.data;
//   return { resData, isServer };
// };
product.panelAdminLayout = true;

export default product;
