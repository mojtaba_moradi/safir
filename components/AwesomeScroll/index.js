import React, { useEffect, useRef } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import { WithUserAgentProps, withUserAgent } from "next-useragent";
const AwesomeScroll = (props) => {
  const { data, Row, ua, scrollBar } = props;

  // //console.log({ data, Row });
  // //console.log({ ua });

  const scrollRef = useRef(null);
  // //console.log({ AwesomeScroll: data });

  useEffect(() => {
    if (scrollRef.current) {
      const slider = scrollRef.current.children[0];
      let isDown = false;
      let startX;
      let scrollLeft;

      slider.addEventListener("mousedown", (e) => {
        isDown = true;
        // slider.classList.add("active");
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });

      slider.addEventListener("mouseleave", (e) => {
        isDown = false;
        // slider.classList.remove("active");
        // //console.log("mouseleave");
      });

      slider.addEventListener("mouseup", () => {
        isDown = false;
        // slider.classList.remove("active");
      });

      slider.addEventListener("mousemove", (e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = x - startX; //scroll-fast
        // //console.log({ x, walk }, (slider.scrollLeft = scrollLeft - walk));

        slider.scrollLeft = scrollLeft - walk;
      });
    }
  }, []);

  return (
    <div>
      {!scrollBar ? (
        <div ref={scrollRef} className="awesome-scroll-container">
          {props.children}
        </div>
      ) : ua && ua.isMobile ? (
        <div className="awesome-scroll-container">{props.children}</div>
      ) : (
        <div ref={scrollRef} className="awesome-scroll-container">
          <PerfectScrollbar>{props.children}</PerfectScrollbar>
        </div>
      )}
    </div>
  );
};
AwesomeScroll.getInitialProps = async (ctx) => {
  return { useragent: ctx.ua.source };
};
export default withUserAgent(AwesomeScroll);
