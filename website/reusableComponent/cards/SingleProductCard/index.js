import React from "react";
import _ from "lodash";
import { Col } from "react-bootstrap";
import formatMoney from "../../../../panelAdmin/utils/formatMoney";

const SingleProductCard = (props) => {
  const { info, onSelectedProduct } = props;

  const { id, percent, alt, category, productName, weight, price, image, unit } = props.info;

  return (
    <Col as="li" xs={6} xl={2} lg={3} md={3} sm={4} className=" single-card-container px-0 mx-2">
      <figure className="single-card-wrapper ">
        {info?.discount ? <span className="percent">{parseInt(info?.discount).toFixed()} %</span> : ""}
        <img src={info?.image} alt={alt} />
        <figcaption>
          <h2>{info?.name}</h2>
          <h3>{info?.category?.name}</h3>
          <h2>
            {info?.weight} {info?.unit}
          </h2>

          <h2 className="price">
            {formatMoney(info?.realPrice)}
            <span>تومان</span>
          </h2>
        </figcaption>
      </figure>
    </Col>
  );
};
export default SingleProductCard;
