import React from "react";
import { Container } from "react-bootstrap";
const Section = (props) => {
  const { title, btnTitle, Container: Containers, data } = props;

  return (
    // <div className="">
    <div className="">
      <Container className="section-header-container">
        <div className="d-flex">
          <span className="radius "></span>
          <h3>{title}</h3>
        </div>

        <a href="#">{btnTitle}</a>
      </Container>
      <Containers data={data} />
    </div>
    // </div>
  );
};

export default Section;
