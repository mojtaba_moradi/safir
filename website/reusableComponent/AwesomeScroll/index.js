import React, { useEffect, useRef } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";

const AwesomeScroll = (props) => {
  const { data, Row, svg } = props;
  //console.log({ data });
  const scrollRef = useRef(null);

  useEffect(() => {
    const slider = scrollRef.current.children[0];
    let isDown = false;
    let startX;
    let scrollLeft;
    let moving = false;

    slider.addEventListener("mousedown", (e) => {
      isDown = true;
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener("mouseleave", (e) => {
      isDown = false;
      // slider.classList.remove("active");
    });
    slider.addEventListener("click", (e) => {
      if (moving) e.preventDefault();
      else //console.log({ e });
      // slider.classList.remove("active");
    });
    slider.addEventListener("mouseup", () => {
      isDown = false;
      // slider.classList.remove("active");
    });

    slider.addEventListener("mousemove", (e) => {
      if (!isDown) return;
      e.preventDefault();
      moving = true;
      const x = e.pageX - slider.offsetLeft;
      const walk = x - startX; //scroll-fast

      slider.scrollLeft = scrollLeft - walk;
    });
  }, []);

  return (
    <div>
      <div ref={scrollRef} className={" awesome-scroll-container"}>
        <PerfectScrollbar>
          <ul onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper">
            {data.map((item, index) => {
              return <Row key={"item-" + index} info={item} {...props} />;
            })}
          </ul>
        </PerfectScrollbar>
      </div>
    </div>
  );
};
export default AwesomeScroll;
