import React, { useEffect } from "react";
import _ from "lodash";
import { Col } from "react-bootstrap";
import formatMoney from "../../../panelAdmin/utils/formatMoney";

const DoubleProductCard = (props) => {
  const { info, onSelectedProduct } = props;

  return (
    <Col as="li" xs={6} xl={2} lg={3} md={3} sm={4} className="single-card-container px-0 mx-2">
      {info?.map((inf, index) => {
        //console.log({ inf });

        return (
          <figure key={"DoubleProductCard-" + index} className="single-card-wrapper">
            {inf?.discount ? <span className="percent">{parseInt(inf?.discount).toFixed()} %</span> : ""}
            <img src={inf?.image} alt={inf?.productName} />
            <figcaption>
              <h2>{info?.name}</h2>
              <h3>{info?.category?.name}</h3>
              <h2>
                {info?.weight} {info?.unit}
              </h2>

              <h2 className="price">
                {formatMoney(info?.realPrice)}
                <span>تومان</span>
              </h2>
            </figcaption>
          </figure>
        );
      })}
    </Col>
  );
};
export default DoubleProductCard;
