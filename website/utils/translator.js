const translator = (localStorage, obj, faTitle, enTitle) => {
  const direction = localStorage.getItem("DIR");

  if (direction == "ltr") {
    return obj[enTitle];
  } else {
    return obj[faTitle];
  }
};

export default translator;
