const moodCard = (data, direction) => {
  let convertData = [];

  // //console.log({ mood: data });

  let dir = false;
  if (direction === "rtl") dir = true;
  for (const index in data) {
    convertData.push({
      title: data[index].title,
      images: data[index].images,
      location: { href: "/slider", as: `/slider#` },
    });
  }
  return convertData;
};
export default moodCard;
