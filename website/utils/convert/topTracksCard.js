import chunkArray from "../chunkArray";
const topTracksCard = (data, chunkNumber, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;
    for (let index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleBottom: [data[index].artist, ", ", data[index].artist],
        image: data[index].cover,
        location: { href: "#", as: `#` },
      });
    }
    if (chunkNumber) convertData = chunkArray(convertData, chunkNumber);
    // convertData = chunkArray(convertData, Math.ceil(data.length / 2));
    return convertData;
  }
};
export default topTracksCard;
