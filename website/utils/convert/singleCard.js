import chunkArray from "../chunkArray";
const singleCard = (data, direction) => {
  if (data) {
    // //console.log({ data });

    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (const index in data) {
      convertData.push({
        titleTop: data[index].title,
        titleMiddle: data[index].artist,
        titleBottom: [data[index].releaseDate, data[index].genres[0]],
        images: data[index].images,
        location: { href: "#", as: `#` },
      });
    }
    convertData = chunkArray(convertData, 2);
    // convertData = chunkArray(convertData, Math.ceil(data.length / 2));
    // //console.log({ convertData });

    return convertData;
  }
};
export default singleCard;
