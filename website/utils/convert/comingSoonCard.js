import website from "../..";

const comingSoonCard = (data, direction) => {
  if (data) {
    let convertData = [];
    let dir = false;
    if (direction === "rtl") dir = true;

    for (const index in data) {
      let noEntries = website.values.strings.NO_ENTRIED;
      let releaseDate = data[index].releaseDate;
      let year =
        releaseDate.year + "/" + releaseDate.month + "/" + releaseDate.day;
      let title = data[index].title ? data[index].title : noEntries;
      let artist = data[index].artist ? data[index].artist : noEntries;
      let genres = data[index].genres.length
        ? data[index].genres[0]
        : noEntries;
      convertData.push({
        titleTop: title,
        titleMiddle: artist,
        titleBottom: [year, genres],
        images: data[index].images,
        location: { href: "#", as: `#` },
      });
    }
    return convertData;
  }
};
export default comingSoonCard;
