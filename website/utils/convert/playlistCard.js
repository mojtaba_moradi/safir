import website from "../..";

const playlistCard = (data, direction) => {
  let convertData = [];
  // //console.log({PAPPAAPA:data});
  let noEntries = website.values.strings.NO_ENTRIED;

  for (const index in data) {
    let title = data[index].title ? data[index].title : noEntries;
    let publisher = data[index].publisher ? data[index].publisher : noEntries;
    let tracksCount = data[index].tracksCount ? data[index].tracksCount : "0";
    let genres = data[index].genres ? data[index].genres : "0";

    convertData.push({
      titleTop: title,
      titleMiddle: publisher,
      titleBottom: [tracksCount + " " + website.values.strings.TRACKS, genres[0] + " " + website.values.strings.FOLLOWERS],
      images: data[index].images,
      location: { href: "/playlist", as: `/playlist#` },
    });
  }
  return convertData;
};
export default playlistCard;
