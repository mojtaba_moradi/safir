import React from "react";
import BannerCard from "../BannerCard";
import { Container } from "react-bootstrap";

const BannerContainer = () => {
  const bannerCard = [
    {
      image: "https://www.verywellfit.com/thmb/aEBWlcb7dEtuZxT18fvqBtHsgJQ=/3000x2002/filters:no_upscale():max_bytes(150000):strip_icc()/dairy-cacff1ed02c741f38d87b5d9c0043b9f.jpg",
      title: "رمضان،ماه پر از برکت",
      desc: "تخفیفات ویژه به مناسبت ماه مبارک رمضان",
    },
  ];
  const renderBannerCard = () => bannerCard.map((card, index) => <BannerCard key={"BannerCard-" + index} data={card} />);

  return (
    <Container fluid>
      <Container>
        <div className="banner-container mb-5">{renderBannerCard()}</div>
      </Container>
    </Container>
  );
};
export default BannerContainer;
