import React from "react";
import DoubleBannerCard from "../DoubleBannerCard";
import { Container, Row } from "react-bootstrap";

const DoubleBannerContainer = () => {
  const cardData = [
    {
      image: "https://www.verywellfit.com/thmb/aEBWlcb7dEtuZxT18fvqBtHsgJQ=/3000x2002/filters:no_upscale():max_bytes(150000):strip_icc()/dairy-cacff1ed02c741f38d87b5d9c0043b9f.jpg",
      title: "محصولات جایگزین لبنیات مخصوص گیاهخوار ها",
    },
    {
      image: "https://www.verywellfit.com/thmb/aEBWlcb7dEtuZxT18fvqBtHsgJQ=/3000x2002/filters:no_upscale():max_bytes(150000):strip_icc()/dairy-cacff1ed02c741f38d87b5d9c0043b9f.jpg",
      title: "محصولات لبنی کاملا ارگانیک",
    },
  ];

  const renderCardData = () => {
    return cardData.map((data, index) => <DoubleBannerCard key={"DoubleBannerCard-" + index} props={data} />);
  };
  return (
    <Container fluid>
      <Container className="container">
        <Row className="double-banner-container mx-0">{renderCardData()}</Row>
      </Container>
    </Container>
  );
};
export default DoubleBannerContainer;
