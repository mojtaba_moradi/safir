import React from "react";
import Link from "next/link";
import { Container, Col, Row } from "react-bootstrap";

const Footer = () => {
  return (
    <div className="f">
      <div className="">
        {/* <footer> */}
        <Container className="d-flex footer-wrapper">
          <Col lg={7} className="right-side offset-1">
            <h2>فروشگاه اینترنتی پرنی</h2>
            <p>
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
              استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله
              در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد
              نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد،
              کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان
              جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای
              طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان
              فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری
              موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد
              نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل
              دنیای موجود طراحی اساسا مورد استفاده قرار گیرد
            </p>
          </Col>
          <Col as="ul" lg={4} className="left-side">
            <li>
              <Link href="#">
                <a>درباره ما</a>
              </Link>
            </li>
            <li>
              <Link href="#">
                <a>تماس با ما</a>
              </Link>
            </li>
            <li>
              <Link href="#">
                <a>سوالات متداول</a>
              </Link>
            </li>
            <li>
              <Link href="#">
                <a>قوانین و مقررات</a>
              </Link>
            </li>
            <li>
              <Link href="#">
                <a>شماره تماس پشتیبانی : 01333548</a>
              </Link>
            </li>
          </Col>
        </Container>
        <div className="social-wrapper ">
          <Container className="social-wrapper">
            <h6>ما را در شبکه های اجتماعی دنبال کنید</h6>

            <Row as='ul' className="social">
              <li>
                <Link href="#">
                  <a>
                    <i className="fab fa-instagram"></i>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="#">
                  <a>
                    <i className="fab fa-telegram-plane"></i>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="#">
                  <a>
                    <i className="fab fa-twitter"></i>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="#">
                  <a>
                    <i className="far fa-envelope"></i>
                  </a>
                </Link>
              </li>
            </Row>
          </Container>
          {/* </footer> */}
        </div>
      </div>
    </div>
  );
};
export default Footer;
