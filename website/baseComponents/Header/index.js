import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
// import logo from "../../../public/assets/images/logo/pernyLogo.jpeg";
const Header = () => {
  const [state, setState] = useState({
    showModal: false,
    clickedComponent: false,
    menuData: [],
  });
  const wrapperRef = useRef(null);
  const hamburgerRef = useRef(null);
  const showModalprofile = (data) => {
    // let noting = ;
    setState((prev) => ({
      ...prev,
      showModal: !state.showModal,
      clickedComponent: true,
      menuData: data,
    }));
  };
  const memuNavSm = [
    {
      title: "تخفیف ها",
      iconClass: "",
      href: "#",
      onClick: null,
    },
    {
      title: "پیشنهادات ویژه ",
      iconClass: "",
      href: "#",
      onClick: null,
    },
    {
      title: "جدید ترین ها",
      iconClass: "",
      href: "#",
      onClick: null,
    },
  ];

  // //console.log({ state: state.showModal });

  // const logOut = () => Cookies.remove("SafirAdminToken");
  const adminTitleModal = [
    {
      title: "ورود",
      iconClass: "far fa-sign-in-alt",
      href: "#",
      onClick: null,
    },
    {
      title: "عضویت",
      iconClass: "fad fa-user-plus",
      href: "#",
      value: "",
      onClick: null,
    },
  ];

  const handleClickOutside = (event) => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      showModalprofile([]);
    }
  };
  useEffect(() => {
    if (state.showModal) {
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
  });
  const adminTitleModal_map = (
    <ul className={`profile-modal-container ${state.showModal ? "actived" : state.clickedComponent ? "deActive" : "showOutStatic"}`}>
      {state.menuData.map((admin, index) => {
        return (
          <li onClick={admin.onClick} key={index}>
            <i className={admin.iconClass}></i>

            <Link href={admin.href} as={admin.href}>
              <a>{admin.title}</a>
            </Link>
            {admin.value ? <span className="show-modal-icon-value">{admin.value}</span> : ""}
          </li>
        );
      })}
    </ul>
  );
  //============================= UI COMPONENT
  // Header Logo
  const Logo = () => {
    return (
      <div className="d-flex ">
        <Link href="/">
          <a className="navbar-brand col-4">{/* <img src={logo} alt="logo" /> */}</a>
        </Link>
      </div>
    );
  };

  // Header menu
  const Menus = () => {
    return (
      <div className="d-flex">
        <ul className="header-menu d-none d-sm-flex ">
          <li>
            {/* <i className="fal fa-bars" /> */}
            <h3>تخفیف ها</h3>
          </li>
          <li>
            {/* <i className="fal fa-bars" /> */}
            <h3>پیشنهادات ویژه</h3>
          </li>
          <li>
            {/* <i className="fal fa-bars" /> */}
            <h3>جدیدترین ها</h3>
          </li>
        </ul>
        <div className="header-login-sm   d-lg-none d-md-none d-flex" onClick={() => showModalprofile(memuNavSm)} ref={hamburgerRef}>
          <i className="fal fa-bars " />
          {/* <i className="fas fa-angle-down" /> */}
        </div>
      </div>
    );
  };

  //Header search
  const SearchBar = () => {
    return (
      <div className="header-search">
        <i className="fal fa-search" />
        {/* <i className="fas fa-angle-down" /> */}
      </div>
    );
  };

  //Login Button
  const LoginButton = () => {
    return (
      <div className="header-login">
        {/* sm */}
        <div className="header-login-sm d-lg-none d-md-none d-flex" onClick={() => showModalprofile(adminTitleModal)} ref={wrapperRef}>
          <i className="fal fa-user" />
          <i className="fas fa-angle-down" />
        </div>
        {/* lg */}
        <div className="header-login-lg d-none d-sm-flex">
          <i className="fal fa-user" />
          <h4 className="">ورود به حساب کاربری</h4>
        </div>
      </div>
    );
  };

  //Header Cart
  const Cart = () => {
    return (
      <div className="header-cart">
        <i className="fal fa-shopping-cart" />
      </div>
    );
  };

  return (
    <nav className="nav-bar">
      <section className="container nav-bar-holder">
        <div className="header-right">
          <Logo />
          <Menus />
        </div>
        <div className="header-left">
          <SearchBar />
          <LoginButton />
          <Cart />
        </div>
      </section>
      {adminTitleModal_map}
    </nav>
  );
};

export default Header;
