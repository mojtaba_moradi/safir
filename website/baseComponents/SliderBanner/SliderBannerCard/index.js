import React from "react";
import Link from "next/link";
// import { Col } from "reactstrap";
import { Col } from "react-bootstrap";

const SliderBannerCard = (props) => {
  const { info, parentClass } = props;
  return (
    <Col as="figure" xs={6} xl={2} lg={3} md={3} sm={4} {...parentClass} className="card-wrapper mx-2">
      <Link href="#">
        <a>
          <img src={info?.image} alt="photo" />
          <figcaption>
            <h5>{info?.name}</h5>
          </figcaption>
        </a>
      </Link>
    </Col>
  );
};
export default SliderBannerCard;
