import React from "react";
import SliderBannerCard from "../SliderBannerCard";
import AwesomeScroll from "../../../../components/AwesomeScroll";
import { Container, Row } from "react-bootstrap";

const CategoryContainer = ({ data }) => {
  const bannerCards = [
    // { image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6Xd4S45pPIHZpwe3PpUFehUpFcPaRr4KpvyrLy8kLe3E-IjWX&s", title: "ماست" },
    // { image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUUOAjDmNP80-jo1U7ixMM5Lsn0iXZga5b2fY-r-5sEzCdirVBoQ&s", title: "سر شیر" },
    // {
    //   image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqFzYXFeFHqvOhyr_LdN5pIlbK1PDkV-oDD7C2K8yGjD63BBvS&s",
    //   title: "بستنی",
    // },
    // { image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTueT0BLLOw_XCQL-bNfpM5D8s9K6Lvj9j8OVUpmxKEw9DEN-b30w&s", title: "کره" },
    // { image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQAEtlENwQGGLtrpWcoNgrCSx9aIvu2bPKrImq-BBIyadDDCm2d3A&s", title: "شیر" },
    // { image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkNjpZoMQEMHCYDla6rLRxIdFBV1U9DQ_12cXyNr2_hQRO4XfOcA&s", title: "تخم مرغ" },
    // { image: "https://picsum.photos/200/100", title: "خامه" },
    // { image: "https://picsum.photos/200/100", title: "پنیر" },
    // { image: "https://picsum.photos/200/100", title: "دوغ" },
  ];
  return (
    <div className="slider-banner-container element-container-col">
      <Container onDragOverCapture={(e) => e.preventDefault()}>
        <AwesomeScroll scrollBar>
          <Row as="ul" onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper mx-0">
            {data.map((info, index) => {
              return <SliderBannerCard key={"DoubleProductCard-" + index} info={info} />;
            })}
          </Row>
        </AwesomeScroll>
      </Container>
    </div>
  );
};
export default CategoryContainer;
{
  /* return <div className="banner-container row">{renderBannerCards()}</div>; */
}
