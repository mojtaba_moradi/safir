import React from "react";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import DoubleProductCard from "../../reusableComponent/DoubleProductCard";
import cartActions from "../../../store/actions/redux/cartActions";
import AwesomeScroll from "../../../components/AwesomeScroll";
import { Container, Row } from "react-bootstrap";
// import AwesomeScroll from "../../reusableComponent/AwesomeScroll";
// import Section from "../../../reusableComponent/Section";

const DiscountsContainer = ({ data }) => {
  const svgGreen = "svg-green";

  const onSelectedProduct = (id, count) => {
    const foundedPop = _.find(store, { id: id });
    if (foundedPop) {
      return dispatch(cartActions.changeCount({ id, count }));
    } else {
      if (count != -1) return dispatch(cartActions.addToCart({ id }));
    }
  };

  // //increment
  // const onSelectedProduct = (id, count) => {
  //   const foundedPop = _.find(selectedNewest, ["id", id]);
  //   if (foundedPop) {
  //     if (count === -1 && foundedPop.count === 0) return true;
  //     var newArr = _.map(selectedNewest, function (pop) {
  //       return pop.id === id ? { id: pop.id, count: pop.count + count } : pop;
  //     });
  //     setSelectedNewest(newArr);
  //   } else setSelectedNewest((prev) => [...prev, { id, count }]);
  // };
  const productsList = _.chunk(data, 2);
  // //console.log({ productsList });

  //TODO conncet to databas with saga to get popular data
  return (
    <div className="element-container-col" style={{ backgroundColor: "rgba(255, 212, 1,0.5)" }}>
      <div className={svgGreen}></div>
      <Container>
        {/* <AwesomeScroll onSelectedProduct={onSelectedProduct} data={productsList} Row={DoubleProductCard} /> */}
        <AwesomeScroll scrollBar>
          <Row as="ul" onDragStart={(e) => e.preventDefault()} className="awesome-scroll-wrapper mx-0">
            {productsList.map((data, index) => {
              return <DoubleProductCard key={"DoubleProductCard-" + index} info={data} />;
            })}
          </Row>
        </AwesomeScroll>
      </Container>
    </div>
  );
};
export default DiscountsContainer;
