import React from "react";
import { Carousel } from "react-responsive-carousel";
import SliderCard from "../SliderCard";
import SliderArrow from "../SliderArrow";
import SliderArrowP from "../SliderArrowP";
import { Container } from "react-bootstrap";

// TODO => we should connect to api and get images
const images = [
  "https://image.freepik.com/free-vector/banner-with-milk-bottle-full-glass-fresh-dairy-drink-with-splash_1441-1799.jpg",
  "https://i.pinimg.com/originals/8d/62/64/8d6264ce57e8dab6881863951c33116e.jpg",
  "https://lh5.googleusercontent.com/proxy/KL4CrgNIFB9jXDywrlfthQrGPA5ELX2nLF1M1pt8zon1oUXqvB1VMkvnkmewGeNL85I5C2cb_Q3a6XfJZPEtoRBo848",
  "https://www.theorganicmilk.com/wp-content/uploads/2015/02/banner-4-1200x500.jpg",
];

const SliderContainer = ({ data }) => {
  return (
    <Container className="slider-container">
      <Carousel renderArrowNext={(onClickHandler, hasNext) => hasNext && <SliderArrow onClick={onClickHandler} />} renderArrowPrev={(onClickHandler, hasPrev) => hasPrev && <SliderArrowP onClick={onClickHandler} />} swipeable={true} transitionTime={1000} interval={5000} infiniteLoop={true} autoPlay={true} showThumbs={false} showStatus={false}>
        {data.map((info, index) => {
          return <SliderCard key={"SliderCard-" + index} image={info?.image} />;
        })}
      </Carousel>
    </Container>
  );
};

export default SliderContainer;
