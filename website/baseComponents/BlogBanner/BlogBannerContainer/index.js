import React from "react";
import BannerCard from "../BlogBannerCard";
import { Container } from "react-bootstrap";

const BlogBannerContainer = () => {
  const bannerCard = [
    {
      image: "https://picsum.photos/810/360",
      title: "# در_خانه_بمان و با خیال راحت خرید کن !",
      desc: "بدون نیاز به خارج شدن از خانه و به خطر انداختن سلامتیت محصولات لبنی مورد نیازت رو تامین کن ",
    },
  ];
  const renderBannerCard = () => bannerCard.map((card, index) => <BannerCard key={"BannerCard-" + index} data={card} />);

  return (
    <Container fluid>
      <Container>
        <div className="blog-banner-container p-0 my-2 ">{renderBannerCard()}</div>
      </Container>
    </Container>
  );
};
export default BlogBannerContainer;
