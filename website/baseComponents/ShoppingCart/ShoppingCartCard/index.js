import React from "react";

const ShoppingCartCard = (props) => {
  const { itmes } = props;
  const { clicked } = props;

  return (
    <div>
      <div className="header-wrapper">
        <h6>سبد خرید</h6>
        <span onClick={clicked}>
          <i className="fal fa-times-circle"></i>
        </span>
      </div>
      <ul>{itmes}</ul>
      <div className="total">
        <h5>جمع کل</h5>
        <span>456,000 تومان</span>
      </div>
      <div className="final">
        <a className="final-btn" href="#">
          نهایی کردن خرید
        </a>
        <a className="return-to-shopping" href="#">
          رفتن به سبد خرید
        </a>
      </div>
    </div>
  );
};
export default ShoppingCartCard;
