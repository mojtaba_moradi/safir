import axios from "../../../utils/axiosBase";

export const getAlbumsFilter = () => {
  return axios
    .get("/")
    .then((res) => {
      return { data: res.data };
    })
    .catch((e) => {
      return { error: e };
    });
};
