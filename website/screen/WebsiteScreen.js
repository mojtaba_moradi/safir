import React from "react";
import Header from "../baseComponents/Header";
import Footer from "../baseComponents/Footer/FooterCard";

const WebsiteScreen = (props) => {
  return (
    <div className="base-page  px-0">
      <div className=" px-0">
        <Header />
        <main className="main-wrapper">{props.children}</main>
        <Footer />
      </div>
    </div>
  );
};

export default WebsiteScreen;
