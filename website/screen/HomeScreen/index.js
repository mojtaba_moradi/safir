import React from "react";
import SliderContainer from "../../baseComponents/Slider/SliderContainer";
import PopularsContainer from "../../baseComponents/Populars/PopularsContainer";
import NewestContainer from "../../baseComponents/NewestContainer";
import DiscountsContainer from "../../baseComponents/DiscountsContainer";
import Section from "../../reusableComponent/Section";
import CategoryContainer from "../../baseComponents/SliderBanner/CategoryContainer";
// import Layer from "../../../reusableComponent/Layers/HomeLayer";
import DoubleBannerContainer from "../../baseComponents/DoubleBanner/DoubleBannerContainer";
import BannerContainer from "../../baseComponents/Banner/BannerContainer";
import BlogBannerContainer from "../../baseComponents/BlogBanner/BlogBannerContainer";
import SpecialOffersContainer from "../../baseComponents/SpecialOffersContainer";
import productsJason from "../../values/jason/productsJason.json";
const HomeScreen = ({ homeData }) => {
  return (
    <div>
      {/* <div className="base-page container-fluid px-0"> */}
      {/* <Layer> */}
      <SliderContainer data={homeData.sliders} />
      <CategoryContainer data={homeData.categories} />

      <Section data={homeData.mostSoldProducts} btnTitle={"  بیشتر "} title={"محبوب ترین ها"} Container={PopularsContainer} />
      <Section data={homeData.latestProducts} btnTitle={" بیشتر"} title={"جدیدترین کالاها"} Container={NewestContainer} />
      <DoubleBannerContainer />
      <Section data={homeData.highestDiscount} btnTitle={" بیشتر"} title={"بیشترین تخفیف ها"} Container={DiscountsContainer} />
      <BannerContainer />
      <Section data={homeData.specialOffers} btnTitle={" بیشتر"} title={"پیشنهاد های ویژه"} Container={SpecialOffersContainer} />
      <BlogBannerContainer />
      {/* </Layer> */}
    </div>
    // </div>
  );
};

export default HomeScreen;
