webpackHotUpdate("static/development/pages/panelAdmin/addOwner.js",{

/***/ "./panelAdmin/utils/checkValidity/index.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/checkValidity/index.js ***!
  \*************************************************/
/*! exports provided: checkValidity */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkValidity", function() { return checkValidity; });
/* harmony import */ var _minLengthValidity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./minLengthValidity */ "./panelAdmin/utils/checkValidity/minLengthValidity.js");
/* harmony import */ var _maxLengthValidity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./maxLengthValidity */ "./panelAdmin/utils/checkValidity/maxLengthValidity.js");
/* harmony import */ var _numberValidity__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./numberValidity */ "./panelAdmin/utils/checkValidity/numberValidity.js");



var checkValidity = function checkValidity(value, rules, array, beforeValue) {
  var isValid = true;
  var errorTitle = false;
  var object = true;
  var checkValid = true; // console.log({ moojValid: { value, rules, array, beforeValue } });
  // console.log({ moooojValidtypeof: typeof value });

  if (!rules.required) return {
    isValid: isValid,
    errorTitle: errorTitle
  };

  if (!rules) {
    return {
      isValid: isValid,
      errorTitle: errorTitle
    };
  }

  if (array) {
    isValid = beforeValue.length ? true : false; // console.log({ mooj: beforeValue.length ? true : false });

    if (!isValid) errorTitle = "خالی بودن فیلد مجاز نمی باشد";
  } else {
    if (rules.required) {
      if (typeof value === "object") {
        for (var key in value) {
          var _value$key, _value$key$toString;

          object = ((_value$key = value[key]) === null || _value$key === void 0 ? void 0 : (_value$key$toString = _value$key.toString()) === null || _value$key$toString === void 0 ? void 0 : _value$key$toString.trim()) !== "" && object;
        }

        if (!object) errorTitle = "خالی بودن فیلد مجاز نمی باشد";
        isValid = object && isValid;
      } else {
        isValid = value.trim() !== "" && isValid;
        errorTitle = value.trim() === "" && ".خالی بودن فیلد مجاز نمی باشد";
      }
    }
  }

  if (isValid) {
    if (rules.minLength) {
      checkValid = Object(_minLengthValidity__WEBPACK_IMPORTED_MODULE_0__["default"])({
        array: array,
        beforeValue: beforeValue,
        value: value,
        rules: rules
      });
      console.log({
        checkValid: checkValid
      });
      if (!checkValid) errorTitle = " \u06A9\u0645\u062A\u0631\u06CC\u0646 \u0645\u0642\u062F\u0627\u0631 \u0645\u062C\u0627\u0632 \u0641\u06CC\u0644\u062F ".concat(rules.minLength, " \u06A9\u0644\u0645\u0647 \u06CC\u0627 \u0631\u0642\u0645 \u0645\u06CC \u0628\u0627\u0634\u062F ");
      isValid = isValid && checkValid;
    }

    if (rules.maxLength) {
      checkValid = Object(_maxLengthValidity__WEBPACK_IMPORTED_MODULE_1__["default"])({
        array: array,
        beforeValue: beforeValue,
        value: value,
        rules: rules
      });
      if (!checkValid) errorTitle = " \u0628\u06CC\u0634\u062A\u0631\u06CC\u0646 \u0645\u0642\u062F\u0627\u0631 \u0645\u062C\u0627\u0632 \u0641\u06CC\u0644\u062F ".concat(rules.maxLength, " \u06A9\u0644\u0645\u0647 \u06CC\u0627 \u0631\u0642\u0645 \u0645\u06CC \u0628\u0627\u0634\u062F ");
      isValid = isValid && checkValid;
    }

    if (rules.isNumeric) {
      checkValid = Object(_numberValidity__WEBPACK_IMPORTED_MODULE_2__["default"])({
        array: array,
        beforeValue: beforeValue,
        value: value
      });
      if (!checkValid) errorTitle = "فقط شماره مجاز می باشد";
      isValid = isValid && checkValid;
    }

    if (rules.isEmail) {
      var pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    if (rules.isPhone) {
      var _pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im; // /^0\d{2,3}-\d{8}|\d{8}$/ regex(/^0\d{2,3}-\d{8}|\d{8}$/)

      isValid = _pattern.test(value) && isValid;
    }

    if (rules.isMobile) {
      var _pattern2 = /^09[\w]{9}$/;
      if (array) beforeValue.map(function (val) {
        return isValid = _pattern2.test(val) && isValid;
      }); // const pattern = /[0,9]{2}\d{9}/g;
      // const pattern = /^(+98|0)?9\d{9}$/;
      else isValid = _pattern2.test(value) && isValid;
    }

    if (rules.isEn) {
      var _pattern3 = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
      if (array) beforeValue.map(function (val) {
        return isValid = _pattern3.test(val) && isValid;
      });else isValid = _pattern3.test(value) && isValid;
    }

    if (rules.isFa) {
      var _pattern4 = /^[\u0600-\u06FF\s]+$/;
      if (array) beforeValue.map(function (val) {
        return isValid = _pattern4.test(val) && isValid;
      });else isValid = _pattern4.test(value) && isValid;
    }
  } // //console.log({ mooojisValid: isValid });


  return {
    isValid: isValid,
    errorTitle: errorTitle
  };
};

/***/ })

})
//# sourceMappingURL=addOwner.js.fa1b235e628d4dd07aae.hot-update.js.map