webpackHotUpdate("static/development/pages/panelAdmin/addOwner.js",{

/***/ "./panelAdmin/utils/onChanges/arrayOnchange.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/onChanges/arrayOnchange.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var arrayOnchange = function arrayOnchange(props) {
  var e, data, setData, setState, setLoading, imageType, validName, checkValidity, updateObject, uploadChange, changeValid, updatedForm, updatedFormElement;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function arrayOnchange$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          e = props.event, data = props.data, setData = props.setData, setState = props.setState, setLoading = props.setLoading, imageType = props.imageType, validName = props.validName, checkValidity = props.checkValidity, updateObject = props.updateObject, uploadChange = props.uploadChange; // //console.log({ validName });

          updatedFormElement = {};
          e.map(function _callee(event) {
            var value, update, eventValue, typeofData, isArray, isObject, isString, remove, push, checkValidValue, formIsValid, name;
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    value = data.Form[event.name].value;
                    update = _objectSpread({}, value);
                    eventValue = event.value;
                    typeofData = typeof value;
                    typeofData === "object" && value != null ? value.length >= 0 ? isArray = true : isObject = true : isString = true;

                    remove = function remove(index) {
                      return value.splice(index, 1)[0];
                    };

                    push = function push(val, newVal) {
                      return newVal != undefined ? val.push(newVal) : "";
                    }; // //console.log({ eventValue, value });
                    // if (event.type === "file") {
                    //   const uploadFile = await uploadChange(event, setLoading, imageType, setState);
                    //   if (uploadFile) {
                    //     if (isArray) value.includes(uploadFile) ? remove(value.findIndex((d) => d === uploadFile)) : push(value, uploadFile);
                    //     else value = uploadFile;
                    //   } else return;
                    // } else if (isArray) value.includes(eventValue) ? remove(value.findIndex((d) => d === eventValue)) : push(value, eventValue);
                    // else if (isObject) {
                    //   value = eventValue;
                    //   if (event.child) value = update[event.child] = eventValue;
                    // } else if (isString)


                    value = eventValue;
                    if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value;
                    updatedFormElement[event.name] = updateObject(data.Form[event.name], {
                      value: value,
                      valid: data.Form[event.name].validation.required ? typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).isValid : true,
                      touched: true,
                      titleValidity: checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).errorTitle
                    });

                    if (validName) {
                      changeValid = updateObject(data.Form[validName], {
                        valid: true
                      });
                      updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement, Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, validName, changeValid)));
                    } else updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement)); // //console.log({ updatedFormElement, updatedForm });


                    formIsValid = false;

                    for (name in updatedForm) {
                      formIsValid = updatedForm[name].valid && formIsValid || updatedForm[name].block && formIsValid;
                    }

                    return _context.abrupt("return", setData({
                      Form: updatedForm,
                      formIsValid: formIsValid
                    }));

                  case 14:
                  case "end":
                    return _context.stop();
                }
              }
            }, null, null, null, Promise);
          });

        case 3:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, null, Promise);
};

/* harmony default export */ __webpack_exports__["default"] = (arrayOnchange);

/***/ })

})
//# sourceMappingURL=addOwner.js.fec6301e58f7e8bf367f.hot-update.js.map