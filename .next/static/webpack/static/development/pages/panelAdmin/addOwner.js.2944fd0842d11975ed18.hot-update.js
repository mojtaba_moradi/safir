webpackHotUpdate("static/development/pages/panelAdmin/addOwner.js",{

/***/ "./panelAdmin/utils/onChanges/handelOnchange.js":
/*!******************************************************!*\
  !*** ./panelAdmin/utils/onChanges/handelOnchange.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var handelOnchange = function handelOnchange(_ref) {
  var event, data, setData, setState, setLoading, imageType, validName, checkValidity, updateObject, uploadChange, fileName, changeValid, updatedForm, updatedFormElement, formIsValid, value, update, eventValue, typeofData, isArray, isObject, isString, remove, push, uploadFile, checkValidValue, name;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function handelOnchange$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          event = _ref.event, data = _ref.data, setData = _ref.setData, setState = _ref.setState, setLoading = _ref.setLoading, imageType = _ref.imageType, validName = _ref.validName, checkValidity = _ref.checkValidity, updateObject = _ref.updateObject, uploadChange = _ref.uploadChange, fileName = _ref.fileName;
          updatedFormElement = {};
          formIsValid = true;
          value = data.Form[event.name].value;
          update = _objectSpread({}, value);
          eventValue = event.value;
          typeofData = typeof value;
          // //console.log({ length: value.length });
          typeofData === "object" && value != undefined ? value && value.length >= 0 ? isArray = true : isObject = true : isString = true; // //console.log({ typeofData, value, isArray, isObject, isString });

          remove = function remove(index) {
            return value.splice(index, 1)[0];
          };

          push = function push(val, newVal) {
            return newVal != undefined ? val.push(newVal) : "";
          };

          if (!(event.type === "file")) {
            _context.next = 21;
            break;
          }

          _context.next = 13;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(uploadChange({
            event: event,
            setLoading: setLoading,
            imageType: imageType,
            setState: setState,
            valid: data.Form[event.name].kindOf,
            fileName: fileName
          }));

        case 13:
          uploadFile = _context.sent;

          if (!uploadFile) {
            _context.next = 18;
            break;
          }

          if (isArray) value.includes(uploadFile) ? remove(value.findIndex(function (d) {
            return d === uploadFile;
          })) : push(value, uploadFile);else value = uploadFile;
          _context.next = 19;
          break;

        case 18:
          return _context.abrupt("return");

        case 19:
          _context.next = 22;
          break;

        case 21:
          if (isArray) if (eventValue !== "") {
            // console.log({ eventValue, value });
            value.includes(eventValue) ? remove(value.findIndex(function (d) {
              return d === eventValue;
            })) : push(value, eventValue);
          } else if (isObject) {
            // value = eventValue;
            // //console.log({ event });
            if (event.child) {
              // //console.log({ eventValue, value });
              value[event.child] = eventValue;
            } else {
              value = eventValue;
            }
          } else if (isString) value = eventValue;

        case 22:
          if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value; // //console.log({ moooooj: checkValidity(checkValidValue, data.Form[event.name].validation, isArray).isValid });

          console.log({
            checkValidValue: checkValidValue,
            eventValue: eventValue,
            value: value,
            typeofData: typeofData
          });
          updatedFormElement[event.name] = updateObject(data.Form[event.name], {
            value: value,
            valid: data.Form[event.name].validation.required ? typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).isValid : true,
            touched: true,
            block: false,
            titleValidity: checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).errorTitle
          });

          if (validName) {
            changeValid = updateObject(data.Form[validName], {
              block: true
            });
            updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement, Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__["default"])({}, validName, changeValid)));
          } else updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement));

          console.log({
            updatedFormElement: updatedFormElement,
            updatedForm: updatedForm
          });

          for (name in updatedForm) {
            formIsValid = updatedForm[name].valid || updatedForm[name].block && formIsValid;
          }

          return _context.abrupt("return", setData({
            Form: updatedForm,
            formIsValid: formIsValid
          }));

        case 29:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, null, Promise);
};

/* harmony default export */ __webpack_exports__["default"] = (handelOnchange);

/***/ })

})
//# sourceMappingURL=addOwner.js.2944fd0842d11975ed18.hot-update.js.map