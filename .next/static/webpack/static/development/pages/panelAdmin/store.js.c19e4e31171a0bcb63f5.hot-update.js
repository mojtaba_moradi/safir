webpackHotUpdate("static/development/pages/panelAdmin/store.js",{

/***/ "./panelAdmin/component/UI/PaginationM/index.js":
/*!******************************************************!*\
  !*** ./panelAdmin/component/UI/PaginationM/index.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
var _this = undefined,
    _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/PaginationM/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



var PaginationM = function PaginationM(props) {
  var limited = props.limited,
      pages = props.pages,
      activePage = props.activePage,
      _onClick = props.onClick;
  var sort = [];
  Number(activePage); // console.log({ limited, pages, activePage });

  var viewNext = +limited + +activePage;
  var viewPrev = activePage - limited; // console.log({ viewNext, viewPrev });

  for (var index = 1; index <= pages; index++) {
    if (pages >= 9) {
      // console.log({ check: pages - limited }, limited - pages === index, { index });
      if (2 == index && viewPrev == 2) sort.unshift(1);
      if (viewPrev <= index && viewNext >= index) sort.push(index);
    } else if (pages <= 9) sort.push(index);
  }

  if (pages - limited - 1 == activePage && pages >= 9) sort.push(pages); // console.log({ active: pages - limited - 1 == activePage, sort });

  var prevFirst = Number(activePage) < Number(limited);
  var prevSecond = Number(activePage) === Number(1);
  var nextFirst = Number(activePage) === Number(pages);
  var nextSecond = Number(activePage) > Number(pages - limited); // console.log({ prevFirst });
  // console.log({ prevSecond });
  // console.log({ nextFirst });
  // console.log({ nextSecond });
  // console.log({ sort });

  var Pagination_Sort = __jsx("div", {
    className: "Pagination-wrapper",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 5
    }
  }, " ", __jsx("div", {
    className: "paginationIcon centerAll translateR transition0-2",
    disabled: prevFirst,
    onClick: function onClick() {
      return prevFirst ? "" : _onClick(1);
    },
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 7
    }
  }, __jsx("i", {
    className: "fa fa-angle-double-right",
    "aria-hidden": "true",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 9
    }
  })), __jsx("div", {
    className: "paginationIcon centerAll translateR transition0-2",
    disabled: prevSecond,
    onClick: function onClick() {
      return prevSecond ? "" : _onClick(activePage - 1);
    },
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 7
    }
  }, __jsx("i", {
    className: "fa fa-angle-right",
    "aria-hidden": "true",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 9
    }
  })), Number(activePage) > 2 + Number(limited) && pages >= 9 && __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "paginationIcon centerAll translateT transition0-2",
    onClick: function onClick() {
      return _onClick(1);
    },
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 11
    }
  }, 1), __jsx("div", {
    className: "paginationIcon centerAll",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 11
    }
  }, __jsx("i", {
    className: "fa fa-ellipsis-h",
    "aria-hidden": "true",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 13
    }
  }))), sort.map(function (number, index) {
    if (number == 1 && +activePage > +limited + 2 && +pages >= 9) return;else if (number === pages && activePage < pages - limited && pages >= 9) return;else return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, __jsx("div", {
      className: "paginationIcon centerAll translateT transition0-2 ".concat(number === Number(activePage) && "actived"),
      onClick: function onClick() {
        return _onClick(number === Number(activePage) ? false : number);
      },
      key: index + "m",
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58,
        columnNumber: 15
      }
    }, number));
  }), pages >= 9 && pages - limited - 1 == activePage && __jsx("div", {
    className: "paginationIcon centerAll translateT transition0-2 ",
    onClick: function onClick() {
      return _onClick(number === Number(activePage) ? false : pages);
    },
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 9
    }
  }, pages), activePage < pages - limited - 1 && pages >= 9 && __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "paginationIcon centerAll",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 11
    }
  }, __jsx("i", {
    className: "fa fa-ellipsis-h",
    "aria-hidden": "true",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 13
    }
  })), __jsx("div", {
    className: "paginationIcon centerAll translateT transition0-2",
    onClick: function onClick() {
      return _onClick(pages);
    },
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 11
    }
  }, pages)), __jsx("div", {
    className: "paginationIcon centerAll translateL transition0-2",
    disabled: nextFirst,
    onClick: function onClick() {
      return nextFirst ? "" : _onClick(+activePage + +1);
    },
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80,
      columnNumber: 7
    }
  }, __jsx("i", {
    className: "fa fa-angle-left",
    "aria-hidden": "true",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81,
      columnNumber: 9
    }
  })), __jsx("div", {
    className: "paginationIcon centerAll translateL transition0-2",
    disabled: nextSecond,
    onClick: function onClick() {
      return nextSecond ? "" : _onClick(Number(pages));
    },
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 7
    }
  }, __jsx("i", {
    className: "fa fa-angle-double-left",
    "aria-hidden": "true",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 9
    }
  })));

  return Pagination_Sort;
};

/* harmony default export */ __webpack_exports__["default"] = (PaginationM);

/***/ })

})
//# sourceMappingURL=store.js.c19e4e31171a0bcb63f5.hot-update.js.map