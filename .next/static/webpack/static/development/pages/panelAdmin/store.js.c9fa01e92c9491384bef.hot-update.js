webpackHotUpdate("static/development/pages/panelAdmin/store.js",{

/***/ "./panelAdmin/screen/Gallery/GalleryScreen/index.js":
/*!**********************************************************!*\
  !*** ./panelAdmin/screen/Gallery/GalleryScreen/index.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _AddGallery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../AddGallery */ "./panelAdmin/screen/Gallery/AddGallery/index.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../.. */ "./panelAdmin/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _component_UI_Modals_MyVerticallyCenteredModal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../component/UI/Modals/MyVerticallyCenteredModal */ "./panelAdmin/component/UI/Modals/MyVerticallyCenteredModal/index.js");
/* harmony import */ var _component_cards_ShowCardInformation__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../component/cards/ShowCardInformation */ "./panelAdmin/component/cards/ShowCardInformation/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var _component_UI_PaginationM__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../component/UI/PaginationM */ "./panelAdmin/component/UI/PaginationM/index.js");
/* harmony import */ var _lib_useApiRequest__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../lib/useApiRequest */ "./lib/useApiRequest.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../utils */ "./panelAdmin/utils/index.js");
/* harmony import */ var _DependentComponent_Submitted__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./DependentComponent/Submitted */ "./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/Submitted/index.js");
/* harmony import */ var _DependentComponent_ModalOption__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./DependentComponent/ModalOption */ "./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/ModalOption/index.js");
/* harmony import */ var _DependentComponent_EditData__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./DependentComponent/EditData */ "./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/EditData/index.js");
/* harmony import */ var _component_UI_Modals_ModalStructure__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../../component/UI/Modals/ModalStructure */ "./panelAdmin/component/UI/Modals/ModalStructure/index.js");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! swr */ "./node_modules/swr/esm/index.js");




var _this = undefined,
    _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/Gallery/GalleryScreen/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_2__["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }











 // const axios = globalUtils.axiosBase;







var GalleryScreen = function GalleryScreen(props) {
  var states = ___WEBPACK_IMPORTED_MODULE_6__["default"].utils.consts.states;
  var card = ___WEBPACK_IMPORTED_MODULE_6__["default"].utils.consts.card;
  var apiPageFetch = props.apiPageFetch,
      filters = props.filters,
      acceptedCardInfo = props.acceptedCardInfo,
      requestData = props.requestData,
      loadingApi = props.loadingApi;

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      showUploadModal = _useState[0],
      setUploadModal = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(_objectSpread({}, states.addGallery)),
      data = _useState2[0],
      setData = _useState2[1];

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])({
    show: false,
    kindOf: null,
    data: {
      src: false,
      type: false,
      name: false
    },
    name: null,
    removeId: null,
    editId: null,
    editData: []
  }),
      modalDetails = _useState3[0],
      setModalDetails = _useState3[1];

  var _useState4 = Object(react__WEBPACK_IMPORTED_MODULE_3__["useState"])(false),
      removeLoading = _useState4[0],
      setRemoveLoading = _useState4[1]; // const CurrentPage = info?.page || "1";


  var strings = ___WEBPACK_IMPORTED_MODULE_6__["default"].values.apiString; // const URL = strings.IMAGE + "/" + CurrentPage;
  // ========================================================= remove data with data id

  var reqApiRemove = function reqApiRemove(id) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function reqApiRemove$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            setRemoveLoading(true);
            _context.next = 3;
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(___WEBPACK_IMPORTED_MODULE_6__["default"].api.deletes.gallery(id));

          case 3:
            if (!_context.sent) {
              _context.next = 6;
              break;
            }

            apiPageFetch(requestData.page);
            onHideModal();

          case 6:
            setRemoveLoading(false);

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, null, Promise);
  }; // ========================================================= modal


  var _onSubmit = function _onSubmit(e) {
    var formData, formElementIdentifier;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function _onSubmit$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            formData = {};

            for (formElementIdentifier in data.Form) {
              formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
            } // trigger(URL);


            apiPageFetch();
            setData(_objectSpread({}, states.addGallery));

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    }, null, null, null, Promise);
  }; // ================================== modal close


  var onHideModal = function onHideModal() {
    setModalDetails(_objectSpread({}, modalDetails, {
      show: false,
      kindOf: false,
      removeId: null
    }));
  }; // ================================== modal open


  var onShowModal = function onShowModal(event) {
    setModalDetails(_objectSpread({}, modalDetails, {
      show: true,
      kindOf: event.kindOf,
      name: event === null || event === void 0 ? void 0 : event.name,
      editData: event === null || event === void 0 ? void 0 : event.editData,
      removeId: event === null || event === void 0 ? void 0 : event.removeId
    }));
  }; // ================================== handel modal end work


  var modalRequest = function modalRequest(bool) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function modalRequest$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            if (bool) reqApiRemove(modalDetails.removeId);else onHideModal();

          case 1:
          case "end":
            return _context3.stop();
        }
      }
    }, null, null, null, Promise);
  }; // ========================================================= END modal


  var modalData = {
    size: "lg",
    status: showUploadModal,
    setStatus: setUploadModal,
    title: "آپلود عکس",
    acceptedBtn: data.formIsValid ? "ثبت" : "اطلاعات کامل نمی باشد",
    acceptedDisabled: !data.formIsValid,
    onSubmit: _onSubmit
  };

  var optionClick = function optionClick(_ref) {
    var _id = _ref._id,
        mission = _ref.mission,
        index = _ref.index;

    //console.log({ _id, mission });
    switch (mission) {
      case "remove":
        onShowModal({
          kindOf: "question",
          removeId: _id
        });
        break;

      case "edit":
        onShowModal({
          kindOf: "component",
          editData: requestData.docs[index]
        });
        break;

      default:
        break;
    }
  }; // ========================================  CARD ELEMENT


  var showDataElement = __jsx(_component_cards_ShowCardInformation__WEBPACK_IMPORTED_MODULE_9__["default"], {
    optionClick: optionClick,
    options: {
      remove: true
    },
    data: card.gallery(requestData === null || requestData === void 0 ? void 0 : requestData.docs, acceptedCardInfo === null || acceptedCardInfo === void 0 ? void 0 : acceptedCardInfo.acceptedCard),
    onClick: null,
    acceptedCardInfo: acceptedCardInfo,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95,
      columnNumber: 27
    }
  });

  return __jsx("div", {
    className: "gallery",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98,
      columnNumber: 5
    }
  }, __jsx(_component_UI_Modals_ModalStructure__WEBPACK_IMPORTED_MODULE_17__["default"], {
    modalRequest: modalRequest,
    reqApiRemove: reqApiRemove,
    onHideModal: onHideModal,
    modalDetails: modalDetails,
    loading: removeLoading,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 99,
      columnNumber: 7
    }
  }), __jsx(_component_UI_Modals_MyVerticallyCenteredModal__WEBPACK_IMPORTED_MODULE_8__["default"], Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({}, modalData, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 101,
      columnNumber: 7
    }
  }), __jsx(_AddGallery__WEBPACK_IMPORTED_MODULE_4__["default"], {
    data: data,
    setData: setData,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 102,
      columnNumber: 9
    }
  })), __jsx("div", {
    className: "gallery-header-wrapper",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "gallery-header",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 105,
      columnNumber: 9
    }
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
    onClick: function onClick() {
      return setUploadModal(true);
    },
    className: "",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 106,
      columnNumber: 11
    }
  }, "افزودن عکس")), showDataElement, __jsx(_component_UI_PaginationM__WEBPACK_IMPORTED_MODULE_11__["default"], {
    limited: "3",
    pages: requestData === null || requestData === void 0 ? void 0 : requestData.pages,
    activePage: requestData === null || requestData === void 0 ? void 0 : requestData.page,
    onClick: apiPageFetch,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 9
    }
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (GalleryScreen);

/***/ })

})
//# sourceMappingURL=store.js.c9fa01e92c9491384bef.hot-update.js.map