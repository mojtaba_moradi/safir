exports.ids = [28];
exports.modules = {

/***/ "V84h":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MyMap; });
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("HJQg");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("AuoD");
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_leaflet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var leaflet_geosearch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("6LTc");
/* harmony import */ var leaflet_geosearch__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet_geosearch__WEBPACK_IMPORTED_MODULE_3__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





class SearchBox extends react_leaflet__WEBPACK_IMPORTED_MODULE_2__["MapControl"] {
  constructor(props) {
    super(props);
    props.leaflet.map.on("geosearch/showlocation", e => props.updateMarker(e));
  }

  createLeafletElement() {
    const searchEl = Object(leaflet_geosearch__WEBPACK_IMPORTED_MODULE_3__["GeoSearchControl"])({
      provider: new leaflet_geosearch__WEBPACK_IMPORTED_MODULE_3__["OpenStreetMapProvider"](),
      style: "bar",
      showMarker: true,
      showPopup: false,
      autoClose: true,
      retainZoomLevel: false,
      animateZoom: true,
      keepResult: false,
      searchLabel: "search"
    });
    return searchEl;
  }

}

class MyMap extends react__WEBPACK_IMPORTED_MODULE_1__["Component"] {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      center: {
        lat: 31.698956,
        lng: 76.732407
      },
      marker: {
        lat: 31.698956,
        lng: 76.732407
      },
      zoom: 13,
      draggable: true
    });

    _defineProperty(this, "refmarker", Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])(this.state.marker));

    _defineProperty(this, "toggleDraggable", () => {
      this.setState({
        draggable: !this.state.draggable
      });
    });

    _defineProperty(this, "updateMarker", e => {
      // const marker = e.marker;
      this.setState({
        marker: e.marker.getLatLng()
      }); //console.log(e.marker.getLatLng());
    });

    _defineProperty(this, "updatePosition", () => {
      const marker = this.refmarker.current;

      if (marker != null) {
        this.setState({
          marker: marker.leafletElement.getLatLng()
        });
      } //console.log(marker.leafletElement.getLatLng());

    });
  }

  render() {
    const position = [this.state.center.lat, this.state.center.lng];
    const markerPosition = [this.state.marker.lat, this.state.marker.lng];
    const SearchBar = Object(react_leaflet__WEBPACK_IMPORTED_MODULE_2__["withLeaflet"])(SearchBox);
    return __jsx("div", {
      className: "jsx-744391653" + " " + "map-root"
    }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_2__["Map"], {
      center: position,
      zoom: this.state.zoom,
      style: {
        height: "700px"
      }
    }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_2__["TileLayer"], {
      attribution: "&copy <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors",
      url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    }), __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_2__["Marker"], {
      draggable: true,
      onDragend: this.updatePosition,
      position: markerPosition,
      animate: true,
      ref: this.refmarker
    }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_2__["Popup"], {
      minWidth: 90
    }, __jsx("span", {
      onClick: this.toggleDraggable,
      className: "jsx-744391653"
    }, this.state.draggable ? "DRAG MARKER" : "MARKER FIXED"))), __jsx(SearchBar, {
      updateMarker: this.updateMarker,
      className: "jsx-744391653"
    })), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
      id: "744391653"
    }, [".map-root.jsx-744391653{height:100%;}", ".leaflet-container.jsx-744391653{height:400px !important;width:80%;margin:0 auto;}"]));
  }

}

/***/ })

};;