module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 16);
/******/ })
/************************************************************************/
/******/ ({

/***/ "/jkW":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string

const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ "0Bsm":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.default = withRouter;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _router = __webpack_require__("nOHt");

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return _react.default.createElement(ComposedComponent, Object.assign({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (false) { var name; }

  return WithRouterWrapper;
}

/***/ }),

/***/ 16:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("u+oT");


/***/ }),

/***/ "1Awo":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const SpinnerRotate = () => {
  return __jsx("div", {
    className: "spinner"
  }, __jsx("div", {
    className: "cube1"
  }), __jsx("div", {
    className: "cube2"
  }));
};

/* harmony default export */ __webpack_exports__["a"] = (SpinnerRotate);

/***/ }),

/***/ "284h":
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__("cDf5");

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "4Q3z":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "7KhZ":
/***/ (function(module) {

module.exports = JSON.parse("{\"headers\":[\"ردیف\",\"عکس\",\"عنوان\"],\"body\":[\"url\",\"name\"]}");

/***/ }),

/***/ "7NiY":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const BackgrandCover = props => {
  return __jsx("div", {
    id: "coverContainer",
    onClick: props.onClick,
    className: props.fadeIn ? " fadeIn" : " fadeOut"
  });
};

/* harmony default export */ __webpack_exports__["a"] = (BackgrandCover);

/***/ }),

/***/ "E6+O":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("gIEs");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("vmXh");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_1__);

 // const instance = axios.create({ baseURL: "https://rimtal.com/api/v1" });
// const instance = create({ baseURL: "https://pernymarket.ir/api/v1" });

_globalUtils__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].axiosBase.defaults.headers.common["Authorization"] = "Bearer " + js_cookie__WEBPACK_IMPORTED_MODULE_1___default.a.get("SafirAdminToken");
/* harmony default export */ __webpack_exports__["a"] = (_globalUtils__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].axiosBase);

/***/ }),

/***/ "FRaV":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "get", function() { return /* reexport */ Get; });
__webpack_require__.d(__webpack_exports__, "post", function() { return /* reexport */ Post; });
__webpack_require__.d(__webpack_exports__, "put", function() { return /* reexport */ Put; });
__webpack_require__.d(__webpack_exports__, "patch", function() { return /* reexport */ api_Patch; });
__webpack_require__.d(__webpack_exports__, "deletes", function() { return /* reexport */ api_Delete; });

// EXTERNAL MODULE: ./panelAdmin/api/axios-orders.js
var axios_orders = __webpack_require__("E6+O");

// EXTERNAL MODULE: ./panelAdmin/index.js + 16 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/api/Get/categories.js

 // import axios from "../axios-orders";

const categories = async page => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let getUrl = page ? strings.CATEGORY + "/" + page : strings.CATEGORY;
  return axios_orders["a" /* default */].get(getUrl).then(res => {
    console.log({
      categories: res
    });
    return res;
  }).catch(error => {
    console.log({
      error
    }); // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

    return false;
  });
};

/* harmony default export */ var Get_categories = (categories);
// CONCATENATED MODULE: ./panelAdmin/api/Get/products.js



const products = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.PRODUCT;
  return axios_orders["a" /* default */].get(strings + "/" + page); // .then((products) => {
  //   //console.log({ products });
  //   returnData(products.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   //console.log({ error });
  //  // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_products = (products);
// CONCATENATED MODULE: ./panelAdmin/api/Get/gallery.js

 // //console.log(axios);

const gallery = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  return axios_orders["a" /* default */].get(strings.IMAGE + "/" + page).then(gallery => {
    //console.log({ gallery });
    return gallery; // loading(false);
  }).catch(error => {//console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ var Get_gallery = (gallery);
// CONCATENATED MODULE: ./panelAdmin/api/Get/sliders.js



const sliders = async page => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const axiosData = page ? strings.SLIDER + "/" + page : strings.SLIDER;
  return axios_orders["a" /* default */].get(axiosData).then(sliders => {
    //console.log({ sliders });
    return sliders.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Get_sliders = (sliders);
// EXTERNAL MODULE: ./globalUtils/index.js + 2 modules
var globalUtils = __webpack_require__("gIEs");

// CONCATENATED MODULE: ./panelAdmin/api/Get/owners.js



const owners = async page => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.OWNERS;
  let url = page ? strings + "/" + page : strings;
  return axios.get(url).then(owners => {
    //console.log({ owners });
    return owners;
  }).catch(error => {//console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ var Get_owners = (owners);
// CONCATENATED MODULE: ./panelAdmin/api/Get/store.js



const store = async page => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.STORE;
  let url = page ? strings + "/" + page : strings;
  return axios.get(url).then(store => {
    //console.log({ store });
    return store;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Get_store = (store);
// CONCATENATED MODULE: ./panelAdmin/api/Get/ownersSearch.js




const ownersSearch = async (param, page = 1) => {
  // const axios = globalUtils.axiosBase;
  //console.log({ param, page });
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.OWNERS + "/s/" + param + "/" + page; //console.log(strings);

  return axios_orders["a" /* default */].get(strings).then(ownersSearch => {
    return ownersSearch.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Get_ownersSearch = (ownersSearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/storeSearch.js



const storeSearch = async (param, page = 1) => {
  //console.log({ param, page });
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.STORE + "/s/" + param + "/" + page; //console.log({ strings });

  return axios.get(strings).then(storeSearch => {
    //console.log({ storeSearch });
    return storeSearch.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Get_storeSearch = (storeSearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/banners.js



const banners = async page => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const axiosData = page ? strings.BANNER + "/" + page : strings.BANNER;
  return axios_orders["a" /* default */].get(axiosData).then(banners => {
    //console.log({ banners });
    return banners.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Get_banners = (banners);
// CONCATENATED MODULE: ./panelAdmin/api/Get/notifications.js



const notifications = async page => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let getUrl = page ? strings.NOTIFICATION + "/" + page : strings.NOTIFICATION;
  return axios_orders["a" /* default */].get(getUrl); // .then((res) => {
  //   //console.log({ notifications: res });
  //   return res;
  // })
  // .catch((error) => {
  //   //console.log({ error });
  //  // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  //   return error;
  // });
};

/* harmony default export */ var Get_notifications = (notifications);
// CONCATENATED MODULE: ./panelAdmin/api/Get/versions.js



const versions = async page => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.VERSION;
  let getUrl = page ? strings + "/" + page : strings;
  return axios.get(getUrl).then(res => {
    //console.log({ versions: res });
    return res;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return error;
  });
};

/* harmony default export */ var Get_versions = (versions);
// CONCATENATED MODULE: ./panelAdmin/api/Get/users.js



const users = async page => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.USER;
  let url = page ? strings + "/" + page : strings;
  return axios.get(url).then(users => {
    console.log({
      users
    });
    return users === null || users === void 0 ? void 0 : users.data;
  }).catch(error => {
    console.log({
      error
    }); // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

    return false;
  });
};

/* harmony default export */ var Get_users = (users);
// CONCATENATED MODULE: ./panelAdmin/api/Get/index.js












const get = {
  categories: Get_categories,
  products: Get_products,
  gallery: Get_gallery,
  sliders: Get_sliders,
  owners: Get_owners,
  store: Get_store,
  ownersSearch: Get_ownersSearch,
  storeSearch: Get_storeSearch,
  banners: Get_banners,
  notifications: Get_notifications,
  versions: Get_versions,
  users: Get_users
};
/* harmony default export */ var Get = (get);
// CONCATENATED MODULE: ./panelAdmin/api/Put/owner.js



const owner = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.OWNERS; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Put_owner = (owner);
// CONCATENATED MODULE: ./panelAdmin/api/Put/store.js



const store_store = async param => {
  //console.log({ param });
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.STORE; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Put_store = (store_store);
// CONCATENATED MODULE: ./panelAdmin/api/Put/category.js



const category = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.CATEGORY; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Put_category = (category);
// CONCATENATED MODULE: ./panelAdmin/api/Put/banner.js



const banner = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.BANNER; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Put_banner = (banner);
// CONCATENATED MODULE: ./panelAdmin/api/Put/slider.js



const slider = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.SLIDER; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Put_slider = (slider);
// CONCATENATED MODULE: ./panelAdmin/api/Put/index.js
// import editSection from "./editSection";





const put = {
  owner: Put_owner,
  store: Put_store,
  category: Put_category,
  banner: Put_banner,
  slider: Put_slider
};
/* harmony default export */ var Put = (put);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// CONCATENATED MODULE: ./panelAdmin/api/Post/imageUpload.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const imageUpload = async (files, setLoading, setState, imageName) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString; //console.log({ files, setLoading, setState, imageName });

  setLoading(true); // ============================================= const

  const CancelToken = external_axios_default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => _objectSpread({}, prev, {
        progressPercentImage: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const URL = strings.UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName); // formData.append("imageType", type);

  formData.append("image", files); //=============================================== axios

  return axios_orders["a" /* default */].post(URL, formData, settings).then(Response => {
    //console.log({ Response });
    setLoading(false);
    return Response.data;
  }).catch(error => {
    //console.log({ error });
    setLoading(false); // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

    return false;
  });
};

/* harmony default export */ var Post_imageUpload = (imageUpload);
// CONCATENATED MODULE: ./panelAdmin/api/Post/category.js



const category_category = async (param, setLoading) => {
  // setLoading(true);
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.CATEGORY;
  return axios_orders["a" /* default */].post(URL, param).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // setLoading(false);
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // if (error.response.data)
    //   switch (error.response.data.Error) {
    //     case 1016:
    //       toastify("وزن تکراری می باشد", "error");
    //       break;
    //     case 1017:
    //       toastify("عنوان انگلیسی تکراری می باشد", "error");
    //       break;
    //     case 1018:
    //       toastify("عنوان فارسی تکراری می باشد", "error");
    //       break;
    //     default:
    //       toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    //       break;
    //   }
    return false;
  });
};

/* harmony default export */ var Post_category = (category_category);
// CONCATENATED MODULE: ./panelAdmin/api/Post/product.js



const product = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.PRODUCT;
  return axios_orders["a" /* default */].post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_product = (product);
// CONCATENATED MODULE: ./panelAdmin/api/Post/owner.js



const owner_owner = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const axios = globalUtils["a" /* default */].axiosBase;
  let URL = strings.OWNERS;
  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_owner = (owner_owner);
// CONCATENATED MODULE: ./panelAdmin/api/Post/store.js



const Post_store_store = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const axios = globalUtils["a" /* default */].axiosBase;
  let URL = strings.STORE;
  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_store = (Post_store_store);
// CONCATENATED MODULE: ./panelAdmin/api/Post/slider.js



const slider_slider = async (param, setLoading) => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.SLIDER; //console.log({ param });

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_slider = (slider_slider);
// CONCATENATED MODULE: ./panelAdmin/api/Post/banner.js



const banner_banner = async (param, setLoading) => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.BANNER; //console.log({ param });

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_banner = (banner_banner);
// CONCATENATED MODULE: ./panelAdmin/api/Post/notification.js



const notification = async (param, setLoading) => {
  // setLoading(true);
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.NOTIFICATION;
  return axios_orders["a" /* default */].post(URL, param).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    }); // setLoading(false);
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //  if (error.response.data)
    //   switch (error.response.data.Error) {
    //     case 1016:
    //       toastify("وزن تکراری می باشد", "error");
    //       break;
    //     case 1017:
    //       toastify("عنوان انگلیسی تکراری می باشد", "error");
    //       break;
    //     case 1018:
    //       toastify("عنوان فارسی تکراری می باشد", "error");
    //       break;
    //     default:
    //       toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    //       break;
    //   }

    return false;
  });
};

/* harmony default export */ var Post_notification = (notification);
// CONCATENATED MODULE: ./panelAdmin/api/Post/version.js



const version = async (param, setLoading) => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.VERSION; //console.log({ param });

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_version = (version);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__("vmXh");
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);

// CONCATENATED MODULE: ./panelAdmin/api/Post/login.js




const login = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const pageRoutes = panelAdmin["a" /* default */].values.routes.GS_ADMIN_DASHBOARD;
  const axios = globalUtils["a" /* default */].axiosBase;
  console.log({
    param
  });
  let URL = strings.LOGIN; // setLoading(true);

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    external_js_cookie_default.a.set("SafirAdminToken", Response.data.token, {
      expires: 7
    }); // // window.location = pageRoutes.GS_PANEL_ADMIN_TITLE;

    toastify("شما تایید شده اید", "success");
    setTimeout(() => {
      window.location = pageRoutes;
    }, 1000);
    return true;
  }).catch(error => {
    console.log({
      error
    });
    setLoading(false);
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    if (error.response.data.Error === 1019) toastify("این شماره ثبت نشده است", "error");else if (error.response.data.Error === 1099) toastify("این شماره ثبت نشده است", "error");else if (error.response.data.Error === 1098) toastify("پسورد شما نامعتبر است", "error");else toastify("خطایی در سرور . لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_login = (login);
// CONCATENATED MODULE: ./panelAdmin/api/Post/index.js










const post = {
  imageUpload: Post_imageUpload,
  category: Post_category,
  product: Post_product,
  owner: Post_owner,
  store: Post_store,
  slider: Post_slider,
  banner: Post_banner,
  notification: Post_notification,
  version: Post_version,
  login: Post_login
};
/* harmony default export */ var Post = (post);
// CONCATENATED MODULE: ./panelAdmin/api/Patch/index.js
// import category from "./category";
// import editDiscount from "./discount";
// import club from "./club";
// import owner from "./owner";
// import slider from "./slider";
// import banner from "./banner";
const Patch = {// category,
  // editDiscount,
  // club,
  // owner,
  // slider,
  // banner,
};
/* harmony default export */ var api_Patch = (Patch);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/product.js

 // import Axios from "axios";

const product_product = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  let URL = panelAdmin["a" /* default */].values.apiString.PRODUCT;
  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_product = (product_product);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/gallery.js

 // import Axios from "axios";

const gallery_gallery = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.IMAGE; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_gallery = (gallery_gallery);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/category.js

 // import Axios from "axios";

const Delete_category_category = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.CATEGORY; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_category = (Delete_category_category);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/owner.js

 // import Axios from "axios";

const Delete_owner_owner = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.OWNERS; // //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_owner = (Delete_owner_owner);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/store.js

 // import Axios from "axios";

const store_owner = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.STORE; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_store = (store_owner);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/slider.js

 // import Axios from "axios";

const Delete_slider_slider = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.SLIDER; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_slider = (Delete_slider_slider);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/banner.js

 // import Axios from "axios";

const Delete_banner_banner = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.BANNER; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_banner = (Delete_banner_banner);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/index.js
// import owner from "./owner";
// import discount from "./discount";
// import club from "./club";
// import slider from "./slider";
// import banner from "./banner";
// import category from "./category";







const Delete = {
  product: Delete_product,
  gallery: Delete_gallery,
  category: Delete_category,
  owner: Delete_owner,
  store: Delete_store,
  slider: Delete_slider,
  banner: Delete_banner
};
/* harmony default export */ var api_Delete = (Delete);
// CONCATENATED MODULE: ./panelAdmin/api/index.js







/***/ }),

/***/ "Go1v":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export default */
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("aYjl");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _globalUtils_axiosBase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("OZZ0");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



function useApiRequest(request, _ref = {}) {
  let {
    initialData
  } = _ref,
      config = _objectWithoutProperties(_ref, ["initialData"]);

  return swr__WEBPACK_IMPORTED_MODULE_0___default()(request && JSON.stringify(request), () => Object(_globalUtils_axiosBase__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(request || {}).then(response => {
    // //console.log({ response });
    return response.data;
  }), _objectSpread({}, config, {
    initialData
  }));
}

/***/ }),

/***/ "H8dW":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__("YFqc");
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);

// EXTERNAL MODULE: external "react-dom"
var external_react_dom_ = __webpack_require__("faye");

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__("4Q3z");

// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/MenuTitle/index.js
var __jsx = external_react_default.a.createElement;


const MenuTitle = ({
  title
}) => {
  return __jsx("li", {
    className: "side-header change-position"
  }, __jsx("h6", null, title));
};

/* harmony default export */ var SideMenu_MenuTitle = (MenuTitle);
// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/SubMenu/index.js
var SubMenu_jsx = external_react_default.a.createElement;



const SubMenu = ({
  menu,
  setMenuTitle,
  showLi,
  selectedMenu,
  windowLocation
}) => {
  const location = windowLocation;
  const sideMenuLi = Object(external_react_["useRef"])(null);

  const onSubMenuClicked = (subMenu, menu) => {
    setMenuTitle(menu.menuTitle);
  };

  let liWidth = 0;
  if (sideMenuLi.current) liWidth = sideMenuLi.current.clientHeight;
  return SubMenu_jsx("ul", {
    style: {
      height: showLi === menu.menuTitle ? menu.subMenu.length * liWidth + "px" : ""
    },
    className: `side-child-navigation transition0-3 ${showLi === menu.menuTitle && menu.subMenu.length ? "showIn" : "showOut"}`
  }, menu.subMenu.map((child, i) => SubMenu_jsx("li", {
    ref: sideMenuLi,
    className: `side-iteme`,
    key: "subMenu-" + i,
    onClick: () => onSubMenuClicked(child, menu)
  }, SubMenu_jsx(link_default.a, {
    href: child.route,
    as: child.route
  }, SubMenu_jsx("a", {
    className: `side-link side-child-ling ${child.route === selectedMenu || location.includes(child.route) ? "activedSideChild" : ""} `,
    id: "sideChildTitle"
  }, child.title)))));
};

/* harmony default export */ var SideMenu_SubMenu = (SubMenu);
// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/Menu/index.js
var Menu_jsx = external_react_default.a.createElement;




const Menu = ({
  menus,
  showLi,
  setShowLi,
  selectedMenuTitle,
  setMenuTitle,
  selectedMenu,
  windowLocation
}) => {
  const location = windowLocation;

  const activedSideTitle = name => {
    let newName = name;

    if (showLi === name) {
      newName = "";
    } else {
      newName = name;
    }

    setShowLi(newName);
  };

  const classNameForMenu = (length, title) => {
    let classes;
    classes = [length ? "icon-right-open " : "", // selectedMenuTitle === title ? (showLi === title ? "" : "activeMenu") : "",
    showLi === title ? "arrowRotate" : "unsetRotate"].join(" ");
    return classes;
  };

  const _handelStateNull = () => {
    setShowLi("");
    setMenuTitle("");
  };

  return menus.map((menu, index) => {
    return Menu_jsx("li", {
      key: "menus-" + index,
      className: "side-iteme"
    }, Menu_jsx(link_default.a, {
      href: !menu.subMenu.length > 0 ? menu.route : "#"
    }, Menu_jsx("a", {
      onClick: menu.route ? _handelStateNull : () => activedSideTitle(menu.subMenu && menu.menuTitle),
      id: location.includes(menu.route) ? "activedSide" : selectedMenuTitle === menu.menuTitle ? showLi === menu.menuTitle ? "" : "activedSide" : "",
      className: `side-link ${classNameForMenu(menu.subMenu.length, menu.menuTitle)}`
    }, menu.menuIconImg ? Menu_jsx("img", {
      src: menu.menuIconImg,
      alt: "icon menu"
    }) : Menu_jsx("i", {
      className: menu.menuIconClass
    }), Menu_jsx("span", null, menu.menuTitle), menu.subMenu.length ? Menu_jsx("div", {
      className: "menu-arrow-icon"
    }, Menu_jsx("i", {
      className: "fas fa-angle-left"
    })) : "")), Menu_jsx(SideMenu_SubMenu, {
      windowLocation: windowLocation,
      menu: menu,
      setMenuTitle: setMenuTitle,
      showLi: showLi,
      selectedMenu: selectedMenu
    }));
  });
};

/* harmony default export */ var SideMenu_Menu = (Menu);
// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/MainMenu/index.js
var MainMenu_jsx = external_react_default.a.createElement;




const MainMenu = ({
  mainMenus,
  windowLocation
}) => {
  const {
    0: showLi,
    1: setShowLi
  } = Object(external_react_["useState"])("");
  const {
    0: selectedMenuTitle,
    1: setMenuTitle
  } = Object(external_react_["useState"])(); // //console.log({ windowLocation });

  Object(external_react_["useEffect"])(() => {
    checkMenu();
  }, []);

  const checkMenu = () => {
    mainMenus.map(menu => {
      return menu.menus.map(menu => {
        return menu.subMenu.map(subMenu => {
          if (subMenu.route === "/" + windowLocation.substr(windowLocation.indexOf("panelAdmin"))) {
            setMenuTitle(menu.menuTitle);
            setShowLi(menu.menuTitle);
            return true;
          }
        });
      });
    });
  };

  Object(external_react_["useEffect"])(() => {
    checkMenu();
  }, [windowLocation]); // //console.log({ showLi, selectedMenuTitle });

  return mainMenus.map((mainMenu, index) => {
    return MainMenu_jsx("ul", {
      key: index + "m"
    }, MainMenu_jsx(SideMenu_MenuTitle, {
      title: mainMenu.title
    }), MainMenu_jsx(SideMenu_Menu, {
      windowLocation: windowLocation,
      menus: mainMenu.menus,
      showLi: showLi,
      setShowLi: setShowLi,
      selectedMenuTitle: selectedMenuTitle,
      setMenuTitle: setMenuTitle
    }));
  });
};

/* harmony default export */ var SideMenu_MainMenu = (MainMenu);
// EXTERNAL MODULE: external "react-scrollbars-custom"
var external_react_scrollbars_custom_ = __webpack_require__("YPTf");

// EXTERNAL MODULE: external "react-perfect-scrollbar"
var external_react_perfect_scrollbar_ = __webpack_require__("RfFk");
var external_react_perfect_scrollbar_default = /*#__PURE__*/__webpack_require__.n(external_react_perfect_scrollbar_);

// EXTERNAL MODULE: ./panelAdmin/index.js + 16 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/index.js
var SideMenu_jsx = external_react_default.a.createElement;



 // import logo from "../../assets/img/Rimtal.png";

 // import "./index.scss";
// import "react-perfect-scrollbar/dist/css/styles.css";

 // import ScrollArea from "react-scrollbar";

 // const ScrollArea = require("react-scrollbar");



const SideMenu = ({
  windowLocation,
  prefetch
}) => {
  // return useMemo(() => {
  const router = Object(router_["useRouter"])(); // useEffect(() => {
  //   if (prefetch) router.prefetch();
  // });
  //console.log({ router });

  return SideMenu_jsx("div", {
    className: `panelAdmin-sideBar-container `
  }, SideMenu_jsx(external_react_perfect_scrollbar_default.a, null, SideMenu_jsx(SideMenu_MainMenu, {
    mainMenus: panelAdmin["a" /* default */].menuFormat,
    windowLocation: router.route
  }))); // }, []);
};

/* harmony default export */ var component_SideMenu = (SideMenu);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__("vmXh");
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);

// CONCATENATED MODULE: ./panelAdmin/component/Header/HeaderProfile/index.js
var HeaderProfile_jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import profileImage from "../../../assets/Images/icons/user.png";
// import "./index.scss";





const HeaderProfile = () => {
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    showModal: false,
    clickedComponent: false
  });
  const wrapperRef = Object(external_react_["useRef"])(null);
  const routers = Object(router_["useRouter"])();

  const showModalprofile = () => {
    // let noting = ;
    setState(prev => _objectSpread({}, prev, {
      showModal: !state.showModal,
      clickedComponent: true
    }));
  }; // //console.log({ state: state.showModal });


  const logOut = () => {
    external_js_cookie_default.a.remove("SafirAdminToken");
    routers.push("/panelAdmin/login");
  };

  const adminTitleModal = [// {
  //   title: "پروفایل",
  //   iconClass: "fas fa-user",
  //   href: "#",
  //   onClick: null,
  // },
  // {
  //   title: "پیام ها",
  //   iconClass: "fas fa-envelope",
  //   href: "#",
  //   value: "",
  //   onClick: null,
  // },
  // {
  //   title: "تنظیمات",
  //   iconClass: "fas fa-cog",
  //   href: "#",
  //   onClick: null,
  // },
  {
    title: "خروج",
    iconClass: " fas fa-sign-out-alt",
    href: "#",
    onClick: logOut
  }];

  const handleClickOutside = event => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      showModalprofile();
    }
  };

  Object(external_react_["useEffect"])(() => {
    if (state.showModal) {
      document.addEventListener("click", handleClickOutside);
      return () => {
        document.removeEventListener("click", handleClickOutside);
      };
    }
  });

  const adminTitleModal_map = HeaderProfile_jsx("ul", {
    className: `profile-modal-container ${state.showModal ? "actived" : state.clickedComponent ? "deActive" : "showOutStatic"}`
  }, adminTitleModal.map((admin, index) => {
    return HeaderProfile_jsx("li", {
      onClick: admin.onClick,
      key: index
    }, HeaderProfile_jsx("i", {
      className: admin.iconClass
    }), HeaderProfile_jsx(link_default.a, {
      href: admin.href,
      as: admin.href
    }, HeaderProfile_jsx("a", null, admin.title)), admin.value ? HeaderProfile_jsx("span", {
      className: "show-modal-icon-value"
    }, admin.value) : "");
  }));

  return HeaderProfile_jsx("ul", {
    onClick: showModalprofile,
    ref: wrapperRef,
    className: "panel-navbar-profile "
  }, HeaderProfile_jsx("li", {
    className: "pointer hoverColorblack normalTransition"
  }, HeaderProfile_jsx("div", {
    className: "centerAll"
  }, HeaderProfile_jsx("i", {
    className: "fas fa-angle-down"
  })), HeaderProfile_jsx("div", {
    className: "admin-profile-name  icon-up-dir "
  }, HeaderProfile_jsx("span", null, "\u0627\u062F\u0645\u06CC\u0646"), " "), HeaderProfile_jsx("div", {
    className: "admin-profile-image"
  })), adminTitleModal_map);
};

/* harmony default export */ var Header_HeaderProfile = (HeaderProfile);
// EXTERNAL MODULE: ./_context/reducer/index.js + 4 modules
var reducer = __webpack_require__("uqW6");

// CONCATENATED MODULE: ./panelAdmin/component/Header/index.js
var Header_jsx = external_react_default.a.createElement;



const Context = reducer["a" /* default */].panelAdminReducer.optionReducerContext;

const Header = ({
  _handelSidebarToggle
}) => {
  const giveContextData = Object(external_react_["useContext"])(Context);
  const {
    state
  } = giveContextData;
  return Header_jsx("nav", {
    className: "panelAdmin-navbar-container"
  }, Header_jsx("div", {
    className: "panel-navbar-box"
  }, Header_jsx("div", {
    className: "panel-navbar-side-element smallDisplay"
  }, Header_jsx("i", {
    onClick: _handelSidebarToggle,
    className: "fas fa-bars"
  }), Header_jsx("span", {
    className: "page-accepted-name"
  }, state.pageName)), Header_jsx("div", {
    className: "panel-navbar-side-element"
  }, Header_jsx("ul", {
    className: "panel-navbar-notifications"
  }, Header_jsx("li", {
    className: "pointer hoverColorblack normalTransition"
  }, Header_jsx("i", {
    className: "icon-search"
  })), Header_jsx("li", {
    className: "navbar-icon-massege pointer hoverColorblack normalTransition"
  })), Header_jsx(Header_HeaderProfile, null))));
};

/* harmony default export */ var component_Header = (Header);
// EXTERNAL MODULE: ./panelAdmin/component/UI/BackgrandCover/index.js
var BackgrandCover = __webpack_require__("7NiY");

// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__("oAEb");

// EXTERNAL MODULE: ./panelAdmin/utils/adminHoc/WithErrorHandler/index.js + 1 modules
var WithErrorHandler = __webpack_require__("HaeA");

// CONCATENATED MODULE: ./panelAdmin/screen/PanelScreen.js
var PanelScreen_jsx = external_react_default.a.createElement;









const OptionReducer = reducer["a" /* default */].panelAdminReducer.optionReducerProvider;

const PanelScreen = props => {
  const {
    0: sidebarToggle,
    1: setSidebarToggle
  } = Object(external_react_["useState"])(false);
  const router = Object(router_["useRouter"])();

  const _handelSidebarToggle = () => {
    setSidebarToggle(!sidebarToggle);
  };

  Object(external_react_["useEffect"])(() => {
    if (!panelAdmin["a" /* default */].utils.authorization()) router.push("/panelAdmin/login");else if (router.asPath === "/panelAdmin" || router.asPath === "/panelAdmin/") router.push("/panelAdmin/dashboard");
  });
  return panelAdmin["a" /* default */].utils.authorization() ? PanelScreen_jsx(OptionReducer, null, PanelScreen_jsx("div", {
    className: `panelAdmin-wrapper  ${sidebarToggle ? "fadeIn" : "fadeOut"}`
  }, PanelScreen_jsx("div", {
    style: {
      direction: "rtl",
      display: "flex"
    }
  }, PanelScreen_jsx(component_SideMenu, {
    windowLocation: router.asPath
  }), PanelScreen_jsx("div", {
    className: "panelAdmin-container"
  }, PanelScreen_jsx(component_Header, {
    _handelSidebarToggle: _handelSidebarToggle
  }), PanelScreen_jsx("div", {
    className: "panelAdmin-content"
  }, props.children))), PanelScreen_jsx(BackgrandCover["a" /* default */], {
    fadeIn: sidebarToggle,
    onClick: _handelSidebarToggle
  }), PanelScreen_jsx(external_react_toastify_["ToastContainer"], {
    rtl: true
  }))) : "";
};

/* harmony default export */ var screen_PanelScreen = __webpack_exports__["a"] = (Object(WithErrorHandler["a" /* default */])(PanelScreen));

/***/ }),

/***/ "HaeA":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./panelAdmin/api/axios-orders.js
var axios_orders = __webpack_require__("E6+O");

// CONCATENATED MODULE: ./panelAdmin/utils/adminHoc/WithErrorHandler/checkError.js
const checkError = ({
  errorCode
}) => {
  let errorTitle;

  switch (errorCode) {
    case 1096:
      errorTitle = "آیدی اشتباه است";
      break;

    case 1098:
      errorTitle = "پسورد شما نامعتبر است";
      break;

    case 1099:
      errorTitle = "خطایی در سرور . لطفا دوباره تلاش کنید";
      break;

    case 1019:
      errorTitle = "ارسال موفقیت آمیز نبود";
      break;

    case 1011:
      errorTitle = "اطلاعات ارسالی نامعتبر است";
      break;

    case 1017:
      errorTitle = "نام انگلیسی تکراری می باشد";
      break;

    case 1018:
      errorTitle = "نام فارسی تکراری می باشد";
      break;

    case 1020:
      errorTitle = "شماره تلفن ثبت نشده است";
      break;

    case 1021:
      errorTitle = "عکس هایی ک در حال استفاده می باشند را نمیتوان حذف کرد";
      break;

    default:
      errorTitle = "خطا در سرور . لطفا دوباره تلاش کنید";
      break;
  }

  return errorTitle;
};

/* harmony default export */ var WithErrorHandler_checkError = (checkError);
// EXTERNAL MODULE: ./panelAdmin/utils/toastify.js
var toastify = __webpack_require__("o6Tf");

// CONCATENATED MODULE: ./panelAdmin/utils/adminHoc/WithErrorHandler/index.js
var __jsx = external_react_default.a.createElement;





const WithErrorHandler = WrappedComponent => {
  return props => {
    var _error$config, _error$config2;

    const {
      0: error,
      1: setError
    } = Object(external_react_["useState"])(null);
    const reqInterceptor = axios_orders["a" /* default */].interceptors.request.use(req => {
      // console.log({ req });
      setError(null);
      return req;
    });
    const resInterceptor = axios_orders["a" /* default */].interceptors.response.use(res => {
      // console.log({ res });
      setError(null);
      return res;
    }, err => {
      setError(err);
    });
    Object(external_react_["useEffect"])(() => {
      return () => {
        axios_orders["a" /* default */].interceptors.request.eject(reqInterceptor);
        axios_orders["a" /* default */].interceptors.response.eject(resInterceptor);
      };
    }, [reqInterceptor, resInterceptor]); // useEffect(() => {
    //   setTimeout(() => {
    //     if (error) setError(null);
    //   }, 1000);
    // }, [error]);

    console.log({
      error
    });
    const errorDisable = "/admin/owner/s//";
    console.log(errorDisable.search(error === null || error === void 0 ? void 0 : (_error$config = error.config) === null || _error$config === void 0 ? void 0 : _error$config.url));
    const test = error === null || error === void 0 ? void 0 : (_error$config2 = error.config) === null || _error$config2 === void 0 ? void 0 : _error$config2.url.split("/");
    if (test) console.log(test[3], test[4], test[5]);
    Object(external_react_["useEffect"])(() => {
      var _error$response;

      if (error) if (test[3] !== "s" && test[4] !== "" && !test[5]) if (error.message === "Network Error") Object(toastify["a" /* default */])("دسترسی به اینترنت را بررسی کنید", "error");else Object(toastify["a" /* default */])(WithErrorHandler_checkError({
        errorCode: error === null || error === void 0 ? void 0 : (_error$response = error.response) === null || _error$response === void 0 ? void 0 : _error$response.data.Error
      }), "error");
    }, [error]);
    return __jsx(external_react_default.a.Fragment, null, __jsx(WrappedComponent, props));
  };
};

/* harmony default export */ var adminHoc_WithErrorHandler = __webpack_exports__["a"] = (WithErrorHandler);

/***/ }),

/***/ "J27u":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _panelAdmin_screen_PanelScreen__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("H8dW");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const routingHandle = ({
  Component,
  pageProps,
  router
}) => {
  if (Component.panelAdminLayout || (router === null || router === void 0 ? void 0 : router.asPath) === "/panelAdmin/") return __jsx(_panelAdmin_screen_PanelScreen__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], null, __jsx(Component, pageProps));else return __jsx(Component, pageProps);
};

/* harmony default export */ __webpack_exports__["a"] = (routingHandle);

/***/ }),

/***/ "OZZ0":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("zr5I");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
 // import Cookie from "js-cookie";

const instance = axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: "https://safirccard.ir/api/v1"
}); // instance.defaults.headers.common["Authorization"] = "Bearer ";

/* harmony default export */ __webpack_exports__["a"] = (instance);

/***/ }),

/***/ "Osoz":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/router-context.js");

/***/ }),

/***/ "RfFk":
/***/ (function(module, exports) {

module.exports = require("react-perfect-scrollbar");

/***/ }),

/***/ "TqRt":
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "VGcP":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./panelAdmin/values/strings/en/navbar.js
const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ var en_navbar = (navbar);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/sideMenu.js
const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";
const CATEGORIES = "دسته بندی ها";
const SEE_ARTIST = "مشاهده هنرمندان";
const ARTIST = "هنرمند";
const ADD_ARTIST = "افزودن هنرمند";
const SEE_ALBUM = "مشاهده آلبوم ها";
const ALBUM = "آلبوم";
const ADD_ALBUM = "افزودن آلبوم";
const SEE_GENRES = "مشاهده ژانر ها";
const GENRES = "ژانر";
const ADD_GENRE = "افزودن ژانر";
const SEE_COUNTRY = "مشاهده کشور ها";
const COUNTRY = "کشور";
const ADD_COUNTRY = "افزودن کشور";
const SEE_INSTRUMENT = "مشاهده ساز ها";
const INSTRUMENT = "ساز";
const ADD_INSTRUMENT = "افزودن ساز";
const SEE_GALLERY = "مشاهده گالری ها";
const GALLERY = "گالری";
const SEE_SONG = "مشاهده آهنگ ها";
const SONG = "آهنگ";
const SEE_MOOD = "مشاهده حالت ها";
const MOOD = "حالت";
const sideMenu = {
  SEE_GALLERY,
  GALLERY,
  DASHBOARD,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  SETTING_WEB,
  CATEGORIES,
  SEE_ARTIST,
  ARTIST,
  ADD_ARTIST,
  ALBUM,
  SEE_ALBUM,
  ADD_ALBUM,
  SEE_GENRES,
  GENRES,
  ADD_GENRE,
  SEE_COUNTRY,
  COUNTRY,
  ADD_COUNTRY,
  SEE_INSTRUMENT,
  INSTRUMENT,
  ADD_INSTRUMENT,
  SEE_SONG,
  SONG,
  SEE_MOOD,
  MOOD
};
/* harmony default export */ var en_sideMenu = (sideMenu);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/global.js
const global_TRACKS = "Tracks";
const NO_ENTRIES = "no entries";
const FOLLOWERS = "followers";
const global = {
  TRACKS: global_TRACKS,
  NO_ENTRIES: NO_ENTRIES,
  FOLLOWERS
};
/* harmony default export */ var en_global = (global);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/constants.js
const ALBUM_CONSTANTS = "album";
const PLAY_LIST_CONSTANTS = "playlist";
const SONG_CONSTANTS = "song";
const FLAG_CONSTANTS = "flag";
const ARTIST_CONSTANTS = "artist";
const INSTRUMENT_CONSTANTS = "instrument";
const MUSIC_VIDEO_CONSTANTS = "musicvideo";
const MOOD_CONSTANTS = "mood";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS
};
/* harmony default export */ var en_constants = (constants);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/index.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const en = _objectSpread({}, en_navbar, {}, en_sideMenu, {}, en_global, {}, en_constants);

/* harmony default export */ var strings_en = (en);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/navbar.js
const navbar_APP_NAME = "ریمتال";
const navbar_EXPLORE = "کاوش کردن";
const navbar_TRACKS = "آهنگ ها";
const navbar_PLAYLISTS = "لیست های پخش";
const navbar_ALBUMS = "آلبوم ها";
const navbar_ARTISTS = "هنرمندان";
const navbar_VIDEOS = "ویدیو ها";
const navbar_SIGN_IN = "ورود";
const navbar_navbar = {
  APP_NAME: navbar_APP_NAME,
  EXPLORE: navbar_EXPLORE,
  TRACKS: navbar_TRACKS,
  PLAYLISTS: navbar_PLAYLISTS,
  ALBUMS: navbar_ALBUMS,
  ARTISTS: navbar_ARTISTS,
  VIDEOS: navbar_VIDEOS,
  SIGN_IN: navbar_SIGN_IN
};
/* harmony default export */ var fa_navbar = (navbar_navbar);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/sideMenu.js
const sideMenu_DASHBOARD = "داشبورد";
const sideMenu_SIDEBAR_ONE_TITLE = "عمومی";
const sideMenu_SIDEBAR_TWO_TITLE = "کاربردی";
const sideMenu_SETTING_WEB = "تنظیمات سایت";
const SEE_VARIABLE = "مشاهده متغییر";
const VARIABLE = "متغییر";
const ADD_VARIABLE = "افزودن متغییر";
const sideMenu_GALLERY = "گالری";
const GALLERIES = "گالری ها";
const ADD_GALLERY = "افزودن گالری";
const SEE_GALLERIES = "مشاهده گالری ها";
const CATEGORY = "دسته بندی";
const sideMenu_CATEGORIES = "دسته بندی ها";
const ADD_CATEGORY = "افزودن دسته بندی";
const SEE_CATEGORIES = "مشاهده دسته بندی ها";
const PRODUCT = "محصول ";
const ADD_PRODUCT = "افزودن محصول";
const SEE_PRODUCTS = "مشاهده محصولات ";
const OWNER = "فروشنده ";
const ADD_OWNER = "افزودن فروشنده";
const SEE_OWNERS = "مشاهده فروشندگان ";
const STORE = "فروشگاه ";
const ADD_STORE = "افزودن فروشگاه";
const SEE_STORES = "مشاهده فروشگاه ها ";
const SLIDER = "اسلایدر ";
const ADD_SLIDER = "افزودن اسلایدر";
const SEE_SLIDERS = "مشاهده اسلایدر ها ";
const BANNER = "بنر ";
const ADD_BANNER = "افزودن بنر";
const SEE_BANNERS = "مشاهده بنر ها ";
const NOTIFICATION = "اعلان ";
const ADD_NOTIFICATION = "افزودن اعلان";
const SEE_NOTIFICATIONS = "مشاهده اعلان ها ";
const VERSION = "ورژن ";
const ADD_VERSION = "افزودن ورژن";
const SEE_VERSIONS = "مشاهده ورژن ها ";
const USERS = "کاربران ";
const sideMenu_sideMenu = {
  DASHBOARD: sideMenu_DASHBOARD,
  SIDEBAR_ONE_TITLE: sideMenu_SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE: sideMenu_SIDEBAR_TWO_TITLE,
  SETTING_WEB: sideMenu_SETTING_WEB,
  CATEGORIES: sideMenu_CATEGORIES,
  SEE_VARIABLE,
  VARIABLE,
  ADD_VARIABLE,
  GALLERY: sideMenu_GALLERY,
  GALLERIES,
  ADD_GALLERY,
  SEE_GALLERIES,
  CATEGORY,
  ADD_CATEGORY,
  SEE_CATEGORIES,
  PRODUCT,
  ADD_PRODUCT,
  SEE_PRODUCTS,
  OWNER,
  ADD_OWNER,
  SEE_OWNERS,
  STORE,
  ADD_STORE,
  SEE_STORES,
  STORE,
  ADD_STORE,
  SEE_STORES,
  SLIDER,
  ADD_SLIDER,
  SEE_SLIDERS,
  BANNER,
  ADD_BANNER,
  SEE_BANNERS,
  NOTIFICATION,
  ADD_NOTIFICATION,
  SEE_NOTIFICATIONS,
  VERSION,
  ADD_VERSION,
  SEE_VERSIONS,
  USERS
};
/* harmony default export */ var fa_sideMenu = (sideMenu_sideMenu);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/global.js
const fa_global_TRACKS = "آهنگ ها ";
const global_NO_ENTRIES = "وارد نشده";
const global_FOLLOWERS = "دنبال کنندگان";
const global_global = {
  TRACKS: fa_global_TRACKS,
  NO_ENTRIES: global_NO_ENTRIES,
  FOLLOWERS: global_FOLLOWERS
};
/* harmony default export */ var fa_global = (global_global);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/constants.js
const constants_ALBUM_CONSTANTS = "آلبوم";
const constants_PLAY_LIST_CONSTANTS = "لیست پخش";
const constants_SONG_CONSTANTS = "موزیک";
const constants_FLAG_CONSTANTS = "پرچم";
const constants_ARTIST_CONSTANTS = "هنرمند";
const constants_INSTRUMENT_CONSTANTS = "ساز";
const constants_MUSIC_VIDEO_CONSTANTS = "موزیک ویدئو";
const constants_MOOD_CONSTANTS = "حالت";
const constants_constants = {
  ALBUM_CONSTANTS: constants_ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS: constants_PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS: constants_SONG_CONSTANTS,
  FLAG_CONSTANTS: constants_FLAG_CONSTANTS,
  ARTIST_CONSTANTS: constants_ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS: constants_INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS: constants_MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS: constants_MOOD_CONSTANTS
};
/* harmony default export */ var fa_constants = (constants_constants);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/index.js
function fa_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function fa_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { fa_ownKeys(Object(source), true).forEach(function (key) { fa_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { fa_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function fa_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const fa = fa_objectSpread({}, fa_navbar, {}, fa_sideMenu, {}, fa_global, {}, fa_constants);

/* harmony default export */ var strings_fa = (fa);
// CONCATENATED MODULE: ./panelAdmin/values/strings/index.js


const Lang = "rtl";
let strings;
if (Lang === "rtl") strings = strings_fa;else strings = strings_en;
/* harmony default export */ var values_strings = (strings);
// CONCATENATED MODULE: ./panelAdmin/values/routes/index.js
const GS_PANEL_ADMIN_TITLE = "/panelAdmin";
const GS_ADMIN_DASHBOARD = GS_PANEL_ADMIN_TITLE + "/dashboard";
const GS_ADMIN_GALLERY = GS_PANEL_ADMIN_TITLE + "/gallery";
const GS_ADMIN_CATEGORY = GS_PANEL_ADMIN_TITLE + "/category";
const GS_ADMIN_ADD_CATEGORY = GS_PANEL_ADMIN_TITLE + "/addCategory";
const GS_ADMIN_PRODUCT = GS_PANEL_ADMIN_TITLE + "/product";
const GS_ADMIN_ADD_PRODUCT = GS_PANEL_ADMIN_TITLE + "/addProduct";
const GS_ADMIN_VARIABLE = GS_PANEL_ADMIN_TITLE + "/variable";
const GS_ADMIN_ADD_VARIABLE = GS_PANEL_ADMIN_TITLE + "/addVariable";
const GS_ADMIN_OWNER = GS_PANEL_ADMIN_TITLE + "/owner";
const GS_ADMIN_ADD_OWNER = GS_PANEL_ADMIN_TITLE + "/addOwner";
const GS_ADMIN_STORE = GS_PANEL_ADMIN_TITLE + "/store";
const GS_ADMIN_ADD_STORE = GS_PANEL_ADMIN_TITLE + "/addStore";
const GS_ADMIN_SLIDER = GS_PANEL_ADMIN_TITLE + "/slider";
const GS_ADMIN_ADD_SLIDER = GS_PANEL_ADMIN_TITLE + "/addSlider";
const GS_ADMIN_BANNER = GS_PANEL_ADMIN_TITLE + "/banner";
const GS_ADMIN_ADD_BANNER = GS_PANEL_ADMIN_TITLE + "/addBanner";
const GS_ADMIN_NOTIFICATION = GS_PANEL_ADMIN_TITLE + "/notification";
const GS_ADMIN_ADD_NOTIFICATION = GS_PANEL_ADMIN_TITLE + "/addNotification";
const GS_ADMIN_VERSION = GS_PANEL_ADMIN_TITLE + "/version";
const GS_ADMIN_ADD_VERSION = GS_PANEL_ADMIN_TITLE + "/addVersion";
const GS_ADMIN_USER = GS_PANEL_ADMIN_TITLE + "/user";
const routes = {
  GS_ADMIN_DASHBOARD,
  GS_PANEL_ADMIN_TITLE,
  GS_ADMIN_GALLERY,
  GS_ADMIN_VARIABLE,
  GS_ADMIN_ADD_VARIABLE,
  GS_ADMIN_CATEGORY,
  GS_ADMIN_ADD_CATEGORY,
  GS_ADMIN_PRODUCT,
  GS_ADMIN_ADD_PRODUCT,
  GS_ADMIN_OWNER,
  GS_ADMIN_ADD_OWNER,
  GS_ADMIN_STORE,
  GS_ADMIN_ADD_STORE,
  GS_ADMIN_SLIDER,
  GS_ADMIN_ADD_SLIDER,
  GS_ADMIN_BANNER,
  GS_ADMIN_ADD_BANNER,
  GS_ADMIN_NOTIFICATION,
  GS_ADMIN_ADD_NOTIFICATION,
  GS_ADMIN_VERSION,
  GS_ADMIN_ADD_VERSION,
  GS_ADMIN_USER
};
/* harmony default export */ var values_routes = (routes);
// CONCATENATED MODULE: ./panelAdmin/values/apiString.js
const ADMIN = "/admin";
const apiString_CATEGORY = ADMIN + "/category";
const apiString_PRODUCT = ADMIN + "/product";
const IMAGE = ADMIN + "/images";
const UPLOAD = ADMIN + "/upload";
const OWNERS = ADMIN + "/owner";
const apiString_STORE = ADMIN + "/store";
const apiString_SLIDER = ADMIN + "/slider";
const apiString_BANNER = ADMIN + "/banner";
const apiString_NOTIFICATION = ADMIN + "/notification";
const apiString_VERSION = ADMIN + "/version";
const LOGIN = ADMIN + "/login";
const USER = ADMIN + "/users";
const apiString = {
  CATEGORY: apiString_CATEGORY,
  PRODUCT: apiString_PRODUCT,
  IMAGE,
  UPLOAD,
  OWNERS,
  STORE: apiString_STORE,
  SLIDER: apiString_SLIDER,
  BANNER: apiString_BANNER,
  NOTIFICATION: apiString_NOTIFICATION,
  VERSION: apiString_VERSION,
  LOGIN,
  USER
};
/* harmony default export */ var values_apiString = (apiString);
// CONCATENATED MODULE: ./panelAdmin/values/strings/constants.js
const strings_constants_ALBUM_CONSTANTS = "album";
const strings_constants_PLAY_LIST_CONSTANTS = "playlist";
const strings_constants_SONG_CONSTANTS = "song";
const strings_constants_FLAG_CONSTANTS = "flag";
const strings_constants_ARTIST_CONSTANTS = "artist";
const strings_constants_INSTRUMENT_CONSTANTS = "instrument";
const strings_constants_MUSIC_VIDEO_CONSTANTS = "musicvideo";
const strings_constants_MOOD_CONSTANTS = "mood";
const strings_constants_constants = {
  ALBUM_CONSTANTS: strings_constants_ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS: strings_constants_PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS: strings_constants_SONG_CONSTANTS,
  FLAG_CONSTANTS: strings_constants_FLAG_CONSTANTS,
  ARTIST_CONSTANTS: strings_constants_ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS: strings_constants_INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS: strings_constants_MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS: strings_constants_MOOD_CONSTANTS
};
/* harmony default export */ var strings_constants = (strings_constants_constants);
// CONCATENATED MODULE: ./panelAdmin/values/index.js






const values = {
  routes: values_routes,
  strings: values_strings,
  apiString: values_apiString,
  constants: strings_constants,
  fa: strings_fa,
  en: strings_en
};
/* harmony default export */ var panelAdmin_values = (values);
// CONCATENATED MODULE: ./panelAdmin/utils/menuFormat.js


const menuFormat = [{
  title: "عمومی",
  menus: [{
    route: panelAdmin_values.routes.GS_ADMIN_DASHBOARD,
    menuTitle: panelAdmin_values.strings.DASHBOARD,
    menuIconImg: false,
    menuIconClass: "fas fa-tachometer-slowest",
    subMenu: [// { title: "SubDashboard", route: "/dashboard1" },
      // { title: "SubDashboard", route: "/dashboard2" }
    ]
  }]
}, {
  title: "کاربردی",
  menus: [{
    route: false,
    menuTitle: panelAdmin_values.strings.GALLERY,
    menuIconImg: false,
    menuIconClass: "fad fa-images",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_GALLERIES,
      route: panelAdmin_values.routes.GS_ADMIN_GALLERY
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.CATEGORY,
    menuIconImg: false,
    menuIconClass: "fad fa-cubes",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_CATEGORIES,
      route: panelAdmin_values.routes.GS_ADMIN_CATEGORY
    }, {
      title: panelAdmin_values.strings.ADD_CATEGORY,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_CATEGORY
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.OWNER,
    menuIconImg: false,
    menuIconClass: "far fa-street-view",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_OWNERS,
      route: panelAdmin_values.routes.GS_ADMIN_OWNER
    }, {
      title: panelAdmin_values.strings.ADD_OWNER,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_OWNER
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.STORE,
    menuIconImg: false,
    menuIconClass: "fad fa-store",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_STORES,
      route: panelAdmin_values.routes.GS_ADMIN_STORE
    }, {
      title: panelAdmin_values.strings.ADD_STORE,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_STORE
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.SLIDER,
    menuIconImg: false,
    menuIconClass: "fas fa-presentation",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_SLIDERS,
      route: panelAdmin_values.routes.GS_ADMIN_SLIDER
    }, {
      title: panelAdmin_values.strings.ADD_SLIDER,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_SLIDER
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.BANNER,
    menuIconImg: false,
    menuIconClass: "far fa-archive",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_BANNERS,
      route: panelAdmin_values.routes.GS_ADMIN_BANNER
    }, {
      title: panelAdmin_values.strings.ADD_BANNER,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_BANNER
    }]
  }, {
    route: panelAdmin_values.routes.GS_ADMIN_ADD_NOTIFICATION,
    menuTitle: panelAdmin_values.strings.NOTIFICATION,
    menuIconImg: false,
    menuIconClass: "fad fa-bells",
    subMenu: [// {
      //   title: values.strings.SEE_NOTIFICATIONS,
      //   route: values.routes.GS_ADMIN_NOTIFICATION,
      // },
      // {
      //   title: values.strings.ADD_NOTIFICATION,
      //   route: values.routes.GS_ADMIN_ADD_NOTIFICATION,
      // },
    ]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.VERSION,
    menuIconImg: false,
    menuIconClass: "fab fa-vimeo-v",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_VERSIONS,
      route: panelAdmin_values.routes.GS_ADMIN_VERSION
    }, {
      title: panelAdmin_values.strings.ADD_VERSION,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_VERSION
    }]
  }, {
    route: panelAdmin_values.routes.GS_ADMIN_USER,
    menuTitle: panelAdmin_values.strings.USERS,
    menuIconImg: false,
    menuIconClass: "fad fa-users",
    subMenu: []
  }]
}];
/* harmony default export */ var utils_menuFormat = (menuFormat);
// EXTERNAL MODULE: ./panelAdmin/utils/index.js + 62 modules
var utils = __webpack_require__("xb3y");

// EXTERNAL MODULE: ./panelAdmin/api/index.js + 39 modules
var api = __webpack_require__("FRaV");

// CONCATENATED MODULE: ./panelAdmin/index.js




const panelAdmin = {
  menuFormat: utils_menuFormat,
  values: panelAdmin_values,
  utils: utils["a" /* default */],
  api: api
};
/* harmony default export */ var panelAdmin_0 = __webpack_exports__["a"] = (panelAdmin);

/***/ }),

/***/ "YFqc":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("cTJO")


/***/ }),

/***/ "YPTf":
/***/ (function(module, exports) {

module.exports = require("react-scrollbars-custom");

/***/ }),

/***/ "YTqd":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = (normalizedRoute.replace(/\/$/, '') || '/').replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => {
    const isCatchAll = /^(\\\.){3}/.test($1);
    groups[$1 // Un-escape key
    .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '') // eslint-disable-next-line no-sequences
    ] = {
      pos: groupIndex++,
      repeat: isCatchAll
    };
    return isCatchAll ? '/(.+?)' : '/([^/]+?)';
  });
  return {
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  };
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "aYjl":
/***/ (function(module, exports) {

module.exports = require("swr");

/***/ }),

/***/ "bFXO":
/***/ (function(module) {

module.exports = JSON.parse("{\"headers\":[\"ردیف\",\"عکس\",\"عنوان\"],\"body\":[\"image\",\"name\"]}");

/***/ }),

/***/ "bzos":
/***/ (function(module, exports) {

module.exports = require("url");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "cDf5":
/***/ (function(module, exports) {

function _typeof2(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "cTJO":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

var _interopRequireWildcard = __webpack_require__("284h");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__("cDcd"));

var _url = __webpack_require__("bzos");

var _utils = __webpack_require__("kYf9");

var _router = _interopRequireDefault(__webpack_require__("nOHt"));

var _router2 = __webpack_require__("elyg");

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new Map();
var IntersectionObserver = false ? undefined : null;
var prefetched = {};

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: (0, _router2.addBasePath)(formatUrl(href)),
        as: asHref ? (0, _router2.addBasePath)(formatUrl(asHref)) : asHref
      };
    });

    this.linkClicked = e => {
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (false) {}

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  getPaths() {
    var {
      pathname
    } = window.location;
    var {
      href: parsedHref,
      as: parsedAs
    } = this.formatUrls(this.props.href, this.props.as);
    var resolvedHref = (0, _url.resolve)(pathname, parsedHref);
    return [resolvedHref, parsedAs ? (0, _url.resolve)(pathname, parsedAs) : resolvedHref];
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      var isPrefetched = prefetched[this.getPaths().join( // Join on an invalid URI character
      '%')];

      if (!isPrefetched) {
        this.cleanUpListeners = listenToIntersections(ref, () => {
          this.prefetch();
        });
      }
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch(options) {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var paths = this.getPaths(); // We need to handle a prefetch error here since we may be
    // loading with priority which can reject but we don't
    // want to force navigation since this is only a prefetch

    _router.default.prefetch(paths[
    /* href */
    0], paths[
    /* asPath */
    1], options).catch(err => {
      if (false) {}
    });

    prefetched[paths.join( // Join on an invalid URI character
    '%')] = true;
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch({
          priority: true
        });
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) { var rewriteUrlForNextExport; }

    return _react.default.cloneElement(child, props);
  }

}

if (false) { var exact, PropTypes, warn; }

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "dZ6Y":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

Object.defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = Object.create(null);
  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "elyg":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");

const mitt_1 = __importDefault(__webpack_require__("dZ6Y"));

const utils_1 = __webpack_require__("g/15");

const is_dynamic_1 = __webpack_require__("/jkW");

const route_matcher_1 = __webpack_require__("gguc");

const route_regex_1 = __webpack_require__("YTqd");

const basePath =  false || '';

function addBasePath(path) {
  return path.indexOf(basePath) !== 0 ? basePath + path : path;
}

exports.addBasePath = addBasePath;

function delBasePath(path) {
  return path.indexOf(basePath) === 0 ? path.substr(basePath.length) || '/' : path;
}

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

const prepareRoute = path => toRoute(!path || path === '/' ? '/index' : path);

function fetchNextData(pathname, query, isServerRender, cb) {
  let attempts = isServerRender ? 3 : 1;

  function getResponse() {
    return fetch(utils_1.formatWithValidation({
      // @ts-ignore __NEXT_DATA__
      pathname: `/_next/data/${__NEXT_DATA__.buildId}${pathname}.json`,
      query
    }), {
      // Cookies are required to be present for Next.js' SSG "Preview Mode".
      // Cookies may also be required for `getServerSideProps`.
      //
      // > `fetch` won’t send cookies, unless you set the credentials init
      // > option.
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      //
      // > For maximum browser compatibility when it comes to sending &
      // > receiving cookies, always supply the `credentials: 'same-origin'`
      // > option instead of relying on the default.
      // https://github.com/github/fetch#caveats
      credentials: 'same-origin'
    }).then(res => {
      if (!res.ok) {
        if (--attempts > 0 && res.status >= 500) {
          return getResponse();
        }

        throw new Error(`Failed to load static props`);
      }

      return res.json();
    });
  }

  return getResponse().then(data => {
    return cb ? cb(data) : data;
  }).catch(err => {
    // We should only trigger a server-side transition if this was caused
    // on a client-side transition. Otherwise, we'd get into an infinite
    // loop.
    if (!isServerRender) {
      ;
      err.code = 'PAGE_LOAD_ERROR';
    }

    throw err;
  });
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription,
    isFallback
  }) {
    // Static Data Cache
    this.sdc = {};

    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state && this.isSsr && e.state.as === this.asPath && url_1.parse(e.state.url).pathname === this.pathname) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (false) {}

      this.replace(url, as, options);
    };

    this._getStaticData = asPath => {
      const pathname = prepareRoute(url_1.parse(asPath).pathname);
      return  true && this.sdc[pathname] ? Promise.resolve(this.sdc[pathname]) : fetchNextData(pathname, null, this.isSsr, data => this.sdc[pathname] = data);
    };

    this._getServerData = asPath => {
      let {
        pathname,
        query
      } = url_1.parse(asPath, true);
      pathname = prepareRoute(pathname);
      return fetchNextData(pathname, query, this.isSsr);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err,
        __N_SSG: initialProps && initialProps.__N_SSG,
        __N_SSP: initialProps && initialProps.__N_SSP
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.autoExport ? pathname : as;
    this.basePath = basePath;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;
    this.isFallback = isFallback;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    if (false) {} else {
      return url;
    }
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = Object.assign(Object.assign({}, data), {
      Component,
      __N_SSG: mod.__N_SSG,
      __N_SSP: mod.__N_SSP
    });
    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new Promise((resolve, reject) => {
      if (!options._h) {
        this.isSsr = false;
      } // marking route changes as a navigation start entry


      if (utils_1.ST) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      let url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as;
      url = addBasePath(url);
      as = addBasePath(as); // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as, options);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (false) {}

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      }

      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const routeRegex = route_regex_1.getRouteRegex(route);
        const routeMatch = route_matcher_1.getRouteMatcher(routeRegex)(asPathname);

        if (!routeMatch) {
          const missingParams = Object.keys(routeRegex.groups).filter(param => !query[param]);

          if (missingParams.length > 0) {
            if (false) {}

            return reject(new Error(`The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). ` + `Read more: https://err.sh/zeit/next.js/incompatible-href-as`));
          }
        } else {
          // Merge params into `query`, overwriting any specified in search
          Object.assign(query, routeMatch);
        }
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);

        if (false) {}

        this.set(route, pathname, query, as, routeInfo);

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (false) {}

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      window.history[method]({
        url,
        as,
        options
      }, // Most browsers currently ignores this parameter, although they may use it in the future.
      // Passing the empty string here should be safe against future changes to the method.
      // https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
      '', as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return Promise.resolve(cachedRouteInfo);
    }

    const handleError = (err, loadErrorFail) => {
      return new Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR' || loadErrorFail) {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(res => {
          const {
            page: Component
          } = res;
          const routeInfo = {
            Component,
            err
          };
          return new Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }).catch(err => handleError(err, true)));
      });
    };

    return new Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(res => resolve({
        Component: res.page,
        __N_SSG: res.mod.__N_SSG,
        __N_SSP: res.mod.__N_SSP
      }), reject);
    }).then(routeInfo => {
      const {
        Component,
        __N_SSG,
        __N_SSP
      } = routeInfo;

      if (false) {}

      return this._getData(() => __N_SSG ? this._getStaticData(as) : __N_SSP ? this._getServerData(as) : this.getInitialProps(Component, // we provide AppTree later so this needs to be `any`
      {
        pathname,
        query,
        asPath: as
      })).then(props => {
        routeInfo.props = props;
        this.components[route] = routeInfo;
        return routeInfo;
      });
    }).catch(handleError);
  }

  set(route, pathname, query, as, data) {
    this.isFallback = false;
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch page code, you may wait for the data during page rendering.
   * This feature only works in production!
   * @param url the href of prefetched page
   * @param asPath the as path of the prefetched page
   */


  prefetch(url, asPath = url, options = {}) {
    return new Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (false) {}

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (false) {}

      const route = delBasePath(toRoute(pathname));
      Promise.all([this.pageLoader.prefetchData(url, delBasePath(asPath)), this.pageLoader[options.priority ? 'loadPage' : 'prefetch'](route)]).then(() => resolve(), reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    route = delBasePath(route);
    const componentResult = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return componentResult;
  }

  _getData(fn) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    return fn().then(data => {
      if (cancel === this.clc) {
        this.clc = null;
      }

      if (cancelled) {
        const err = new Error('Loading initial props cancelled');
        err.cancelled = true;
        throw err;
      }

      return data;
    });
  }

  getInitialProps(Component, ctx) {
    const {
      Component: App
    } = this.components['/_app'];

    const AppTree = this._wrapApp(App);

    ctx.AppTree = AppTree;
    return utils_1.loadGetInitialProps(App, {
      AppTree,
      Component,
      router: this,
      ctx
    });
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

exports.default = Router;
Router.events = mitt_1.default();

/***/ }),

/***/ "faye":
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),

/***/ "g/15":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn(...args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  var _a;

  if (false) {} // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (false) {}

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (false) {}

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SP = typeof performance !== 'undefined';
exports.ST = exports.SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "gIEs":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./globalUtils/Loading/index.js
var __jsx = external_react_default.a.createElement;

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import { CoffeeLoading } from "react-loadingg";

function Loading(Component) {
  return function LoadingComponent(_ref) {
    let {
      isLoading
    } = _ref,
        props = _objectWithoutProperties(_ref, ["isLoading"]);

    if (!isLoading) return __jsx(Component, props);else return __jsx("div", {
      style: {
        display: "flex",
        width: "100vw",
        height: "100vh",
        justifyContent: "center",
        alignItem: "center"
      }
    });
  };
}

/* harmony default export */ var globalUtils_Loading = (Loading);
// EXTERNAL MODULE: ./globalUtils/axiosBase.js
var axiosBase = __webpack_require__("OZZ0");

// EXTERNAL MODULE: ./globalUtils/globalHoc/routingHandle.js
var routingHandle = __webpack_require__("J27u");

// CONCATENATED MODULE: ./globalUtils/globalHoc/index.js

const globalHoc = {
  routingHandle: routingHandle["a" /* default */]
};
/* harmony default export */ var globalUtils_globalHoc = (globalHoc);
// CONCATENATED MODULE: ./globalUtils/index.js



const globalUtils = {
  Loading: globalUtils_Loading,
  axiosBase: axiosBase["a" /* default */],
  globalHoc: globalUtils_globalHoc
};
/* harmony default export */ var globalUtils_0 = __webpack_exports__["a"] = (globalUtils);

/***/ }),

/***/ "ge5p":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const updateObject = (oldObject, updatedProperties) => {
  return _objectSpread({}, oldObject, {}, updatedProperties);
};

/* harmony default export */ __webpack_exports__["a"] = (updateObject);

/***/ }),

/***/ "gguc":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = param => {
      try {
        return decodeURIComponent(param);
      } catch (_) {
        const err = new Error('failed to decode param');
        err.code = 'DECODE_FAILED';
        throw err;
      }
    };

    const params = {};
    Object.keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });
    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "kYf9":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "nOHt":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__("284h");

var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _router2 = _interopRequireWildcard(__webpack_require__("elyg"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__("Osoz");

var _withRouter = _interopRequireDefault(__webpack_require__("0Bsm"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components', 'isFallback', 'basePath'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

Object.defineProperty(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  Object.defineProperty(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = Object.assign({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "o6Tf":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("oAEb");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_1__);



const toastify = async (text, type) => {
  react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].configure();

  if (type === "error") {
    // alert("error");
    await react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].error(text, {
      position: "top-right",
      autoClose: 5000
    });
  } else if (type === "success") {
    // alert("success");
    await react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].success(text, {
      position: "top-right",
      autoClose: 5000
    });
  }
};

/* harmony default export */ __webpack_exports__["a"] = (toastify);

/***/ }),

/***/ "oAEb":
/***/ (function(module, exports) {

module.exports = require("react-toastify");

/***/ }),

/***/ "tMBA":
/***/ (function(module, exports) {



/***/ }),

/***/ "u+oT":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _panelAdmin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("VGcP");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("aYjl");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _context_reducer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("uqW6");
/* harmony import */ var _lib_useApiRequest__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("Go1v");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("4Q3z");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _panelAdmin_component_UI_Loadings_SpinnerRotate__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("1Awo");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;








const notification = props => {
  const strings = _panelAdmin__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"].values.apiString;
  const Context = _context_reducer__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"].panelAdminReducer.optionReducerContext;
  const {
    resData,
    isServer
  } = props;
  const giveContextData = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(Context);
  const {
    0: currentPage,
    1: setCurrentPage
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(1);
  const {
    0: loadingApi,
    1: setLoadingApi
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true);
  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false); // const { query } = useRouter();
  // const [serverQuery] = useState(query);
  // //console.log({ serverQuery, query });

  const {
    dispatch
  } = giveContextData;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    dispatch.changePageName("دسته بندی");
    apiPageFetch();
  }, []); // ======================================================== SWR
  // const { data: image } = useApiRequest(strings.notification + "/" + currentPage, { initialData: resData });

  const apiPageFetch = async (page = 1) => {
    if (!page) return;
    setLoadingApi(true);
    const res = await _panelAdmin__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"].api.get.notifications(page);
    const resData = res.data;
    setState(res.data);
    setLoadingApi(false);
  };

  const onDataSearch = ({
    title,
    page = 1
  }) => {
    dispatch(sagaActions.getSearchnotificationData({
      title,
      page
    }));
  };

  return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " ", loadingApi ? __jsx("div", {
    className: "staticStyle bgWhite"
  }, __jsx(_panelAdmin_component_UI_Loadings_SpinnerRotate__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"], null)) : ""); // return true;
}; // notification.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.categories({ page: "1" });
//   const resData = res.data;
//   return { resData, isServer };
// };


notification.panelAdminLayout = true;
/* harmony default export */ __webpack_exports__["default"] = (notification);

/***/ }),

/***/ "uqW6":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./_context/index.js
var __jsx = external_react_default.a.createElement;

/* harmony default export */ var _context = ((reducer, actions, defaultValue) => {
  const Context = external_react_default.a.createContext();

  const Provider = ({
    children
  }) => {
    const {
      0: state,
      1: dispatch
    } = Object(external_react_["useReducer"])(reducer, defaultValue);
    const boundActions = {};

    for (let key in actions) {
      boundActions[key] = actions[key](dispatch);
    }

    return __jsx(Context.Provider, {
      value: {
        state,
        dispatch: boundActions
      }
    }, children);
  };

  return {
    Context,
    Provider
  };
});
// CONCATENATED MODULE: ./_context/reducer/panelAdminReducer/optionReducer.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // ========================================================  REDUCER

const optionReducer = (state, action) => {
  switch (action.type) {
    case "SET_PAGE_NAME":
      return _objectSpread({}, state, {
        pageName: action.payload
      });

    default:
      return state;
  }
}; // ========================================================  DISPATCH

const changePageName = dispatch => title => {
  // //console.log({ dispatch, title });
  dispatch({
    type: "SET_PAGE_NAME",
    payload: title
  });
};

const {
  Provider: optionReducer_Provider,
  Context: optionReducer_Context
} = _context(optionReducer, {
  changePageName
}, {
  pageName: ""
});
// CONCATENATED MODULE: ./_context/reducer/panelAdminReducer/index.js

const panelAdminReducer = {
  optionReducerProvider: optionReducer_Provider,
  optionReducerContext: optionReducer_Context
};
/* harmony default export */ var reducer_panelAdminReducer = (panelAdminReducer);
// CONCATENATED MODULE: ./_context/reducer/webSiteReducer/index.js
const webSiteReducer = {};
/* harmony default export */ var reducer_webSiteReducer = (webSiteReducer);
// CONCATENATED MODULE: ./_context/reducer/index.js


const reducer_reducer = {
  panelAdminReducer: reducer_panelAdminReducer,
  webSiteReducer: reducer_webSiteReducer
};
/* harmony default export */ var _context_reducer = __webpack_exports__["a"] = (reducer_reducer);

/***/ }),

/***/ "vmXh":
/***/ (function(module, exports) {

module.exports = require("js-cookie");

/***/ }),

/***/ "xb3y":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/genres.js


const genres = data => {
  const thead = ["#", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_genres = (genres);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenario.js
var __jsx = external_react_default.a.createElement;


const ShowScenario = scenario => {
  const thead = ["#", "نام", "تاریخ شروع ", " تاریخ پایان", "تعداد برندگان ", "شركت كنندگان", "برندگان", "جوایز", "پوچ ها", "زمان چرخش مجدد (روز)", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < scenario.length; index++) {
    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    tbody.push({
      data: [scenario[index].name, scenario[index].startDate, scenario[index].endDate, scenario[index].winnersCount, {
        option: {
          eye: true,
          name: "participants"
        }
      }, {
        option: {
          eye: true,
          name: "winners"
        }
      }, {
        option: {
          eye: true,
          name: "gifts"
        }
      }, {
        option: {
          eye: true,
          name: "empty"
        }
      }, scenario[index].spinRepeatTime, scenario[index].isActive ? active : deActive, {
        option: {
          edit: true
        }
      }],
      style: {
        background: scenario[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenario = (ShowScenario);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenarioDataInModal.js


const ShowScenarioDataInModal = (data, name) => {
  const NotEntered = "وارد نشده";
  let thead = null;
  const headGift = ["شماره", "نام", "شماره همراه", "جایزه", " تاریخ "];
  const headNotGift = ["شماره", "نام", "شماره همراه", " تاریخ "];
  name === "participants" ? thead = headNotGift : thead = headGift;
  let tbody = [];
  let body = null;

  for (let index = 0; index < data.length; index++) {
    let fullName = data[index].fullName ? data[index].fullName : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let gift = data[index].gift ? data[index].gift : NotEntered;
    let date = data[index].date ? data[index].date : NotEntered;
    const bodyGift = [fullName, phoneNumber, gift, date];
    const bodyNotGift = [fullName, phoneNumber, date];
    name === "participants" ? body = bodyNotGift : body = bodyGift;
    tbody.push({
      data: body,
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenarioDataInModal = (ShowScenarioDataInModal);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenarioDataInModalTwo.js


const ShowScenarioDataInModalTwo = (data, name) => {
  const NotEntered = "وارد نشده";
  const thead = ["شماره", name];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    tbody.push({
      data: [data[index] ? data[index] : NotEntered],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenarioDataInModalTwo = (ShowScenarioDataInModalTwo);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/owner.js
var owner_jsx = external_react_default.a.createElement;


const owner = data => {
  //console.log({ ownerdata: data });
  const thead = ["#", "عکس", "نام ", "محدوده  ", " شماره همراه", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = owner_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = owner_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : NotEntered;
    let district = data[index].district ? data[index].district : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let balance = data[index].balance ? data[index].balance : "0";
    let isActive = data[index].isActive ? active : deActive; // , { option: { star: true, value: rating } }

    tbody.push({
      data: [thumbnail, title, district, phoneNumber, isActive, {
        option: {
          edit: true,
          remove: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_owner = (owner);
// CONCATENATED MODULE: ./panelAdmin/utils/formatMoney.js
const formatMoney = number => {
  return number === null || number === void 0 ? void 0 : number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

/* harmony default export */ var utils_formatMoney = (formatMoney);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowDiscount.js
var ShowDiscount_jsx = external_react_default.a.createElement;



const ShowDiscount = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = ShowDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = ShowDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let realPrice = data[index].realPrice ? utils_formatMoney(data[index].realPrice) : NotEntered;
    let newPrice = data[index].newPrice ? utils_formatMoney(data[index].newPrice) : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? "%" + data[index].percent : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowDiscount = (ShowDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowClub.js
var ShowClub_jsx = external_react_default.a.createElement;


const ShowClub = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "عضویت ", "دسته بندی ", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let cardElement = ShowClub_jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-credit-card"
    });

    let applicationElement = ShowClub_jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-mobile-android"
    });

    let active = ShowClub_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = ShowClub_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let NotEntered = "وارد نشده";
    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? data[index].percent + " %" : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : "";
    let membership = data[index].membership.length ? data[index].membership : NotEntered;
    let membershipData = membership.includes("CARD") && membership.includes("APPLICATION") ? ShowClub_jsx("div", {
      style: {
        fontSize: "1.2em",
        fontWeight: "900",
        display: "flex",
        justifyContent: "space-around"
      }
    }, " ", ShowClub_jsx("div", null, cardElement, " "), ShowClub_jsx("div", null, " ", applicationElement)) : membership.includes("APPLICATION") ? applicationElement : membership.includes("CARD") ? cardElement : "";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, membershipData, categoryTitleFa, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowClub = (ShowClub);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowTransactions.js
var ShowTransactions_jsx = external_react_default.a.createElement;



const ShowTransactions = data => {
  const thead = ["#", "نام", " شماره همراه ", " تاریخ (زمان) ", " کل مبلغ", "تخفیف", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let NotEntered = "وارد نشده";
    let user = data[index].user ? data[index].user : NotEntered;
    let userName = user ? user.name : NotEntered;
    let userPhoneNumber = user ? user.phoneNumber : NotEntered;
    let date = dateSplit ? ShowTransactions_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = data[index].totalPrice ? utils_formatMoney(data[index].totalPrice) : "0";
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let paymentStatus = data[index].paymentStatus ? ShowTransactions_jsx("span", {
      style: {
        color: "green"
      }
    }, "پرداخت شده") : ShowTransactions_jsx("span", {
      style: {
        color: "red"
      }
    }, " ", "پرداخت نشده");
    tbody.push({
      data: [userName, userPhoneNumber, date, totalPrice, showDiscount, paymentStatus],
      style: {
        background: data[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowTransactions = (ShowTransactions);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/transactionDiscount.js
var transactionDiscount_jsx = external_react_default.a.createElement;



const transactionDiscount = data => {
  // let data =datas.discount
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = transactionDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = transactionDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let dataIndex = data[index].discount;
    let thumbnail = dataIndex.thumbnail ? dataIndex.thumbnail : NotEntered;
    let title = dataIndex.title ? dataIndex.title : NotEntered;
    let realPrice = dataIndex.realPrice ? utils_formatMoney(dataIndex.realPrice) : NotEntered;
    let newPrice = dataIndex.newPrice ? utils_formatMoney(dataIndex.newPrice) : NotEntered;
    let rating = dataIndex.rating ? dataIndex.rating : 0;
    let percent = dataIndex.percent ? "%" + dataIndex.percent : NotEntered;
    let boughtCount = dataIndex.boughtCount ? dataIndex.boughtCount : "0";
    let category = dataIndex.category ? dataIndex.category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = dataIndex.viewCount ? dataIndex.viewCount : "0";
    let isActive = dataIndex.isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_transactionDiscount = (transactionDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/members.js
var members_jsx = external_react_default.a.createElement;


const members = data => {
  const thead = ["#", "نام", "شماره همراه", "تراکنش ها", "تخفیف ها", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = members_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = members_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    }); // let avatar = data[index].avatar ? data[index].avatar : NotEntered;


    let name = data[index].name ? data[index].name : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let showTransaction = {
      option: {
        eye: true,
        name: "transactions"
      }
    };
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [name, phoneNumber, showTransaction, showDiscount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_members = (members);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/memberTransaction.js
var memberTransaction_jsx = external_react_default.a.createElement;



const memberTransaction = data => {
  //console.log({ data });
  // let data =datas.discount
  const thead = ["#", "تاریخ", "قیمت ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let paymentStatus = data[index].paymentStatus ? memberTransaction_jsx("span", {
      style: {
        color: "green"
      }
    }, "پرداخت شده") : memberTransaction_jsx("span", {
      style: {
        color: "red"
      }
    }, " ", "پرداخت نشده");
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let date = dateSplit ? memberTransaction_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = dataIndex.totalPrice ? utils_formatMoney(dataIndex.totalPrice) : "0";
    tbody.push({
      data: [date, totalPrice, paymentStatus],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_memberTransaction = (memberTransaction);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/memberDiscount.js
var memberDiscount_jsx = external_react_default.a.createElement;



const memberDiscount = data => {
  //console.log({ data });
  // let data =datas.discount
  const thead = ["#", "نام", "قیمت ", "تاریخ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let discount = data[index].discount;
    let discountName = discount.title;
    let dateSplit = data[index].usedDate ? data[index].usedDate.split(" ") : "";
    let date = dateSplit ? memberDiscount_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : "استفاده نشده";
    let price = dataIndex.price ? utils_formatMoney(dataIndex.price) : "0";
    let isUsed = dataIndex.isUsed ? "استفاده شده" : "استفاده نشده";
    tbody.push({
      data: [discountName, price, date, isUsed],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_memberDiscount = (memberDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/country.js


const country = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let flag = data[index].flag ? data[index].flag : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [flag, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_country = (country);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/instrument.js
const instrument = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [thumbnail, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_instrument = (instrument);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/song.js


const song = data => {
  const thead = ["#", "نام ", "پخش "];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let title = data[index].title ? data[index].title : NotEntered;
    tbody.push({
      data: [title, {
        option: {
          play: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_song = (song);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/products.js


const products = data => {
  //console.log({ data });
  const thead = ["#", "عکس", "عنوان", "قیمت اصلی ", " قیمت جدید", "وزن", "تخفیف", "بازدید", "تنظیمات"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let image = data[index].image ? data[index].image : NotEntered;
    let name = data[index].name ? data[index].name : NotEntered;
    let realPrice = data[index].realPrice ? data[index].realPrice : NotEntered;
    let newPrice = data[index].newPrice ? data[index].newPrice : NotEntered;
    let weight = data[index].weight ? data[index].weight : "0";
    let discount = data[index].discount ? data[index].discount : "0";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    tbody.push({
      data: [image, name, realPrice, newPrice, weight, viewCount, discount, {
        option: {
          edit: true,
          remove: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_products = (products);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/store.js
var store_jsx = external_react_default.a.createElement;


const store = data => {
  //console.log({ storedata: data });
  const thead = ["#", "عکس", "نام ", "کمیسیون  ", "اقساط", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = store_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = store_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : NotEntered;
    let commission = data[index].commission ? data[index].commission : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let installments = data[index].installments ? data[index].installments : "0";
    let isActive = data[index].isActive ? active : deActive; // , { option: { star: true, value: rating } }

    tbody.push({
      data: [thumbnail, title, commission, installments, isActive, {
        option: {
          edit: true,
          remove: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_store = (store);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/user.js
var user_jsx = external_react_default.a.createElement;


const user_user = data => {
  //console.log({ userdata: data });
  const thead = ["#", "نام", "شماره همراه  "];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    var _data$index, _data$index2;

    let NotEntered = "وارد نشده";

    let active = user_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = user_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let name = ((_data$index = data[index]) === null || _data$index === void 0 ? void 0 : _data$index.name) || "";
    let familyName = ((_data$index2 = data[index]) === null || _data$index2 === void 0 ? void 0 : _data$index2.familyName) || "";
    let fullName = name + " " + familyName;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    if (!name && !familyName) fullName = NotEntered; // , { option: { star: true, value: rating } }

    tbody.push({
      data: [fullName, phoneNumber],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_user = (user_user);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/index.js


















const table = {
  instrument: table_instrument,
  country: table_country,
  memberDiscount: table_memberDiscount,
  memberTransaction: table_memberTransaction,
  members: table_members,
  transactionDiscount: table_transactionDiscount,
  showTransaction: table_ShowTransactions,
  ShowClub: table_ShowClub,
  ShowDiscount: table_ShowDiscount,
  owner: table_owner,
  showScenario: table_ShowScenario,
  genres: table_genres,
  ShowScenarioDataInModal: table_ShowScenarioDataInModal,
  ShowScenarioDataInModalTwo: table_ShowScenarioDataInModalTwo,
  song: table_song,
  products: table_products,
  store: table_store,
  user: table_user
};
/* harmony default export */ var consts_table = (table);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addCategory.js
const addCategory_owner = {
  Form: {
    titleFa: {
      label: "نام دسته فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام دسته فارسی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "نام دسته انگلیسی  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام دسته انگلیسی "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var addCategory = (addCategory_owner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addProduct.js
const addArtist = {
  Form: {
    name: {
      label: "نام  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    realPrice: {
      label: "قیمت اصلی :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت اصلی"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    newPrice: {
      label: "قیمت جدید :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت جدید"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    weight: {
      label: "وزن :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "وزن"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    unit: {
      label: "واحد :",
      elementType: "input",
      elementConfig: {
        placeholder: "واحد"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var addProduct = (addArtist);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addGallery.js
const addGallery = {
  Form: {
    imageName: {
      label: "نام عکس :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام عکس"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addGallery = (addGallery);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addOwner.js
const addOwner = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // subTitle: {
    //   label: "توضیحات :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "توضیحات ",
    //   },
    //   value: "",
    //   validation: {
    //     required: false,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    district: {
      label: "محدوده :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "محدوده"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    address: {
      label: "آدرس :",
      elementType: "textarea",
      elementConfig: {
        placeholder: "آدرس"
      },
      value: "",
      validation: {
        required: true,
        minLength: 3
      },
      valid: false,
      touched: false
    },
    phoneNumber: {
      label: "تلفن همراه :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "09xxxxxxxxx"
      },
      value: "",
      validation: {
        required: false // minLength: 11,
        // maxLength: 11,
        // isNumeric: true,
        // isMobile: true,

      },
      valid: true,
      touched: false
    },
    phone: {
      label: "تلفن ثابت :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "013-33333333"
      },
      validation: {
        required: false // minLength: 4,
        // isNumeric: false,

      },
      value: [],
      valid: true,
      touched: false
    },
    // rating: {
    //   label: "امتیاز :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "number",
    //     placeholder: "امتیاز"
    //   },
    //   value: "",
    //   validation: {
    //     minLength: 1,
    //     maxLength: 1,
    //     isNumeric: true,
    //     required: false
    //   },
    //   valid: false,
    //   touched: false
    // },
    thumbnail: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    coordinate: {
      label: "مختصات :",
      elementType: "coordinate",
      kindOf: "image",
      value: {
        lat: "",
        lng: ""
      },
      elementConfig: {
        type: "number",
        placeholder: "مختصات"
      },
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addOwner = (addOwner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addStore.js
const addStore = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    installments: {
      label: "اقساط :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "اقساط "
      },
      value: "",
      validation: {
        required: false,
        isNumeric: true
      },
      valid: true,
      touched: false
    },
    commission: {
      label: "کمیسیون :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "کمیسیون "
      },
      value: "",
      validation: {
        required: false,
        isNumeric: true
      },
      valid: true,
      touched: false
    },
    owner: {
      label: "فروشنده :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشنده"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    areaCode: {
      label: "کد فروشگاه :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "کد فروشگاه "
      },
      value: "",
      validation: {
        minLength: 5,
        required: true // isNumeric: true,

      },
      valid: false,
      touched: false
    },
    // phoneNumber: {
    //   label: "تلفن همراه :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "number",
    //     placeholder: "تلفن همراه",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //     minLength: 11,
    //     maxLength: 11,
    //     isNumeric: true,
    //     isMobile: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    phone: {
      label: "تلفن ثابت :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "013-33333333"
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true
      },
      value: [],
      valid: false,
      touched: false
    },
    aboutStore: {
      label: "درباره فروشگاه :",
      elementType: "textarea",
      elementConfig: {
        placeholder: "درباره فروشگاه"
      },
      value: "",
      validation: {// required: true,
      },
      valid: true,
      touched: true
    },
    features: {
      label: "ویژگی استفاده :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "ویژگی استفاده"
      },
      value: [],
      validation: {// required: true,
      },
      valid: true,
      touched: true
    },
    conditions: {
      label: "شرایط استفاده  :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "شرایط استفاده "
      },
      value: [],
      validation: {// required: true,
      },
      valid: true,
      touched: true
    },
    district: {
      label: "محدوده :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "محدوده"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    slides: {
      label: "اسلاید :",
      elementType: "InputFileArray",
      kindOf: "image",
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    location: {
      label: "مختصات :",
      elementType: "coordinate",
      kindOf: "image",
      value: {
        lat: "",
        lng: ""
      },
      elementConfig: {
        type: "number",
        placeholder: "مختصات :"
      },
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addStore = (addStore);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addSlider.js
const addSlider = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [{
        name: "دسته بندی",
        value: "Category"
      }, {
        name: "فروشگاه",
        value: "Store"
      }],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    store: {
      label: "فروشگاه :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // parent: {
    //   label: "فروشگاه :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "فروشگاه",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    // type: {
    //   label: "ت :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "ت",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addSlider = (addSlider);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addBanner.js
const addBanner = {
  Form: {
    store: {
      label: "فروشگاه :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // type: {
    //   label: "ت :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "ت",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addBanner = (addBanner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addNotification.js
const addNotification_owner = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    content: {
      label: "محتوی  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "محتوی "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var addNotification = (addNotification_owner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addVersion.js
const version = {
  Form: {
    version: {
      label: "ورژن :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "ورژن"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // link: {
    //   label: "لینک  :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "لینک ",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    isRequired: {
      label: "آیا ضروری میباشد  :",
      elementType: "twoCheckBox",
      elementConfig: {
        type: "checkBox",
        placeholder: "آیا ضروری میباشد "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var addVersion = (version);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/index.js









const states = {
  addCategory: addCategory,
  addProduct: addProduct,
  addGallery: states_addGallery,
  addOwner: states_addOwner,
  addStore: states_addStore,
  addSlider: states_addSlider,
  addBanner: states_addBanner,
  addNotification: addNotification,
  addVersion: addVersion
};
/* harmony default export */ var consts_states = (states);
// EXTERNAL MODULE: ./panelAdmin/index.js + 16 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/utils/consts/galleryConstants.js



const galleryConstants = () => {
  const ConstantsEn = panelAdmin["a" /* default */].values.en;
  const string = panelAdmin["a" /* default */].values.strings;
  return [{
    value: ConstantsEn.ALBUM_CONSTANTS,
    title: string.ALBUM_CONSTANTS
  }, {
    value: ConstantsEn.PLAY_LIST_CONSTANTS,
    title: string.PLAY_LIST_CONSTANTS
  }, {
    value: ConstantsEn.FLAG_CONSTANTS,
    title: string.FLAG_CONSTANTS
  }, {
    value: ConstantsEn.SONG_CONSTANTS,
    title: string.SONG_CONSTANTS
  }, {
    value: ConstantsEn.ARTIST_CONSTANTS,
    title: string.ARTIST_CONSTANTS
  }, {
    value: ConstantsEn.INSTRUMENT_CONSTANTS,
    title: string.INSTRUMENT_CONSTANTS
  }, {
    value: ConstantsEn.MUSIC_VIDEO_CONSTANTS,
    title: string.MUSIC_VIDEO_CONSTANTS
  }, {
    value: ConstantsEn.MOOD_CONSTANTS,
    title: string.MOOD_CONSTANTS
  }];
};

/* harmony default export */ var consts_galleryConstants = (galleryConstants);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/gallery.js
const gallery_instrument = (data, acceptedCard) => {
  const cardFormat = []; //console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let title = dataIndex.title ? dataIndex.title : "";
    let href = dataIndex.href ? dataIndex.href : "";
    let images = href; // //console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard === href ? "activeImage" : "" : "",
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var gallery = (gallery_instrument);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/product.js


const product = data => {
  //console.log({ productCard: data });
  const cardFormat = [];

  for (let index in data) {
    let NO_ENTERED = "وارد نشده";
    let dataIndex = data[index];
    let category = dataIndex.category ? dataIndex.category : "";
    let categoryName = category ? category.name : NO_ENTERED;
    let name = dataIndex.name ? dataIndex.name : NO_ENTERED;
    let unit = dataIndex.unit ? dataIndex.unit : NO_ENTERED;
    let weight = dataIndex.weight ? dataIndex.weight : NO_ENTERED;
    let realPrice = dataIndex.realPrice ? dataIndex.realPrice : NO_ENTERED;
    let newPrice = dataIndex.newPrice ? dataIndex.newPrice : NO_ENTERED;
    let image = dataIndex.image ? dataIndex.image : NO_ENTERED;
    cardFormat.push({
      _id: dataIndex._id,
      // isActive: dataIndex.isActive,
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: name,
          style: {
            color: "black",
            fontSize: "1.3em",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: categoryName,
          title: categoryName,
          style: {
            color: "",
            fontSize: "0.9em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: realPrice,
          direction: "ltr",
          style: {
            color: "red",
            fontSize: "",
            fontWeight: ""
          }
        }],
        right: [{
          elementType: "price",
          value: newPrice,
          direction: "ltr",
          style: {
            color: "green",
            fontSize: "",
            fontWeight: ""
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: unit
        }],
        left: [{
          elementType: "icon",
          value: weight,
          className: "",
          direction: "ltr",
          style: {
            fontSize: "1em",
            fontWeight: "500"
          },
          iconStyle: {
            fontSize: "1.4em"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_product = (product);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/category.js


const category_category = data => {
  const cardFormat = []; //console.log({ data });

  for (let index in data) {
    let dataIndex = data[index];
    let titleFa = dataIndex.titleFa ? dataIndex.titleFa : "";
    let titleEn = dataIndex.titleEn ? dataIndex.titleEn : "";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: titleFa,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        left: [{
          elementType: "text",
          value: titleEn,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_category = (category_category);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/slider.js


const slider = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({
    insss: data
  });

  for (let index in data) {
    let noEntries = "وارد نشده است";
    let dataIndex = data[index];
    let parent = (dataIndex === null || dataIndex === void 0 ? void 0 : dataIndex.parent) || false;
    let parentName = (parent === null || parent === void 0 ? void 0 : parent.titleFa) || (parent === null || parent === void 0 ? void 0 : parent.title) || noEntries;
    let parentType = (dataIndex === null || dataIndex === void 0 ? void 0 : dataIndex.parentType) || "وارد نشده";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard === image ? "activeImage" : "" : "",
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: parentName,
          style: {
            color: "#828181",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        left: [{
          elementType: "text",
          value: panelAdmin["a" /* default */].utils.dictionary(parentType),
          style: {
            color: "#147971",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_slider = (slider);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/banner.js
const banner = (data, acceptedCard) => {
  const cardFormat = []; //console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let NO_ENTERED = "وارد نشده";
    let type = dataIndex.type ? dataIndex.type : NO_ENTERED;
    let store = dataIndex.store ? dataIndex.store : false;
    let title = store.title ? store.title : NO_ENTERED;
    let image = dataIndex.image ? dataIndex.image : NO_ENTERED; // //console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard === image ? "activeImage" : "" : "",
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_banner = (banner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/version.js


const version_version = data => {
  const cardFormat = []; //console.log({ data });

  for (let index in data) {
    let dataIndex = data[index];
    let link = dataIndex.link ? dataIndex.link : "";
    let version = dataIndex.version ? dataIndex.version : "";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: false,
      body: [{
        right: [{
          elementType: "text",
          value: link,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        left: [{
          elementType: "text",
          value: version,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_version = (version_version);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/index.js
// import instrument from "./instrument";
 // import country from "./country";
// import mood from "./mood";
// import hashtag from "./hashtag";
// import genre from "./genre";






const card = {
  category: card_category,
  // instrument,
  gallery: gallery,
  // country,
  // mood,
  // hashtag,
  product: card_product,
  slider: card_slider,
  banner: card_banner,
  version: card_version // genre

};
/* harmony default export */ var consts_card = (card);
// EXTERNAL MODULE: ./panelAdmin/utils/consts/modal/index.js
var modal = __webpack_require__("tMBA");
var modal_default = /*#__PURE__*/__webpack_require__.n(modal);

// CONCATENATED MODULE: ./panelAdmin/utils/consts/index.js





const consts = {
  table: consts_table,
  states: consts_states,
  galleryConstants: consts_galleryConstants,
  card: consts_card,
  modal: modal_default.a
};
/* harmony default export */ var utils_consts = (consts);
// EXTERNAL MODULE: ./panelAdmin/utils/toastify.js
var toastify = __webpack_require__("o6Tf");

// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/convertToCelander.js
const convertToCelander = value => {
  let valDate,
      day,
      month,
      year,
      selectedDay = null;

  if (value) {
    valDate = value.split("/");
    day = Number(valDate[2]);
    month = Number(valDate[1]);
    year = Number(valDate[0]);
    selectedDay = {
      day,
      month,
      year
    };
  }

  return selectedDay;
};

/* harmony default export */ var CelanderConvert_convertToCelander = (convertToCelander);
// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/convertToDate.js
const convertToDate = selectedDay => {
  let day = selectedDay.day;
  let month = selectedDay.month;
  let year = selectedDay.year;
  let date = year + "/" + month + "/" + day;
  return date;
};

/* harmony default export */ var CelanderConvert_convertToDate = (convertToDate);
// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/index.js


const CelanderConvert = {
  convertToCelander: CelanderConvert_convertToCelander,
  convertToDate: CelanderConvert_convertToDate
};
/* harmony default export */ var utils_CelanderConvert = (CelanderConvert);
// CONCATENATED MODULE: ./panelAdmin/utils/checkValidity/minLengthValidity.js
const minLengthValidity = ({
  array,
  beforeValue,
  value,
  rules
}) => {
  let minLength = true;

  if (array) {
    beforeValue.map(val => {
      console.log({
        minLengthVal: val
      });
      return minLength = val.length >= rules.minLength && minLength;
    });
  } else minLength = value.length >= rules.minLength && minLength;

  console.log({
    minLength,
    array,
    beforeValue,
    value,
    rules
  });
  return minLength;
};

/* harmony default export */ var checkValidity_minLengthValidity = (minLengthValidity);
// CONCATENATED MODULE: ./panelAdmin/utils/checkValidity/maxLengthValidity.js
const maxLengthValidity = ({
  array,
  beforeValue,
  value,
  rules
}) => {
  let maxLength = true;
  if (array) beforeValue.map(val => maxLength = val.length >= rules.maxLength && maxLength);else maxLength = value.length <= rules.maxLength && maxLength;
  return maxLength;
};

/* harmony default export */ var checkValidity_maxLengthValidity = (maxLengthValidity);
// CONCATENATED MODULE: ./panelAdmin/utils/checkValidity/numberValidity.js
const numberValidity = ({
  array,
  beforeValue,
  value
}) => {
  let isNumeric = true;
  const pattern = /^\d+$/;
  if (array) beforeValue.map(val => {
    // console.log("number:", { val });
    return isNumeric = pattern.test(val) && isNumeric;
  });else isNumeric = pattern.test(value) && isNumeric;
  return isNumeric;
};

/* harmony default export */ var checkValidity_numberValidity = (numberValidity);
// CONCATENATED MODULE: ./panelAdmin/utils/checkValidity/index.js



const checkValidity_checkValidity = (value, rules, array, beforeValue) => {
  let isValid = true;
  let errorTitle = false;
  let object = true;
  let checkValid = true;
  console.log({
    moojValid: {
      value,
      rules,
      array,
      beforeValue
    }
  }); // console.log({ moooojValidtypeof: typeof value });

  if (!rules.required) return {
    isValid,
    errorTitle
  };

  if (!rules) {
    return {
      isValid,
      errorTitle
    };
  }

  if (array) {
    isValid = beforeValue.length ? true : false; // console.log({ mooj: beforeValue.length ? true : false });

    if (!isValid) errorTitle = "خالی بودن فیلد مجاز نمی باشد";
  } else {
    if (rules.required) {
      if (typeof value === "object") {
        for (const key in value) {
          var _value$key, _value$key$toString;

          object = ((_value$key = value[key]) === null || _value$key === void 0 ? void 0 : (_value$key$toString = _value$key.toString()) === null || _value$key$toString === void 0 ? void 0 : _value$key$toString.trim()) !== "" && object;
        }

        if (!object) errorTitle = "خالی بودن فیلد مجاز نمی باشد";
        isValid = object && isValid;
      } else {
        isValid = value.trim() !== "" && isValid;
        errorTitle = value.trim() === "" && ".خالی بودن فیلد مجاز نمی باشد";
      }
    }
  }

  if (isValid) {
    if (rules.minLength) {
      checkValid = checkValidity_minLengthValidity({
        array,
        beforeValue,
        value,
        rules
      });
      console.log({
        checkValid
      });
      if (!checkValid) errorTitle = ` کمترین مقدار مجاز فیلد ${rules.minLength} کلمه یا رقم می باشد `;
      isValid = isValid && checkValid;
    }

    if (rules.maxLength) {
      checkValid = checkValidity_maxLengthValidity({
        array,
        beforeValue,
        value,
        rules
      });
      if (!checkValid) errorTitle = ` بیشترین مقدار مجاز فیلد ${rules.maxLength} کلمه یا رقم می باشد `;
      isValid = isValid && checkValid;
    }

    if (rules.isNumeric) {
      checkValid = checkValidity_numberValidity({
        array,
        beforeValue,
        value
      });
      if (!checkValid) errorTitle = "فقط شماره مجاز می باشد";
      isValid = isValid && checkValid;
    }

    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    if (rules.isPhone) {
      const pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im; // /^0\d{2,3}-\d{8}|\d{8}$/ regex(/^0\d{2,3}-\d{8}|\d{8}$/)

      isValid = pattern.test(value) && isValid;
    }

    if (rules.isMobile) {
      const pattern = /^09[\w]{9}$/;
      if (array) beforeValue.map(val => isValid = pattern.test(val) && isValid); // const pattern = /[0,9]{2}\d{9}/g;
      // const pattern = /^(+98|0)?9\d{9}$/;
      else isValid = pattern.test(value) && isValid;
    }

    if (rules.isEn) {
      const pattern = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
      if (array) beforeValue.map(val => isValid = pattern.test(val) && isValid);else isValid = pattern.test(value) && isValid;
    }

    if (rules.isFa) {
      const pattern = /^[\u0600-\u06FF\s]+$/;
      if (array) beforeValue.map(val => isValid = pattern.test(val) && isValid);else isValid = pattern.test(value) && isValid;
    }
  } // //console.log({ mooojisValid: isValid });


  return {
    isValid,
    errorTitle
  };
};
// EXTERNAL MODULE: ./panelAdmin/utils/updateObject.js
var utils_updateObject = __webpack_require__("ge5p");

// EXTERNAL MODULE: ./panelAdmin/api/index.js + 39 modules
var api = __webpack_require__("FRaV");

// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/validUpload/voicevalid.js
let formats = ["mp3", "voice", "audio"];

const voiceValid = type => {
  let valid = formats.map(format => {
    if (type.includes(format)) return true;else return false;
  });
  let finalValid = valid.includes(true) ? true : false;
  return finalValid;
};

/* harmony default export */ var voicevalid = (voiceValid);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/uploadChange.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const uploadChange = async props => {
  const {
    event,
    setLoading,
    imageType,
    setState,
    valid,
    fileName,
    dispatch
  } = props; //console.log({ eventUpload: event });

  let files = event.files[0]; //console.log({ imageType, fileName });

  let returnData = false;

  if (files) {
    switch (valid) {
      case "image":
        if (files.type.includes("image")) {
          if (fileName) {
            if ( // dispatch(sagaActions.uploadImageData({ data: files, imageType, fileName }))
            await api["post"].imageUpload(files, setLoading, setState, fileName)) returnData = fileName;
          } else {
            Object(toastify["a" /* default */])("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      case "video":
        if (files.type.includes("video")) returnData = await api["post"].videoUpload(files, setLoading, imageType, setState);
        break;

      case "voice":
        if (voicevalid(files.type)) {
          if (fileName) {
            if (await api["post"].voiceUpload(files, setLoading, imageType, setState, fileName)) returnData = fileName;
          } else {
            Object(toastify["a" /* default */])("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      default:
        Object(toastify["a" /* default */])("فایل شما نباید " + files.type + " باشد", "error");
        break;
    }
  } //console.log({ files: files, returnData, fileName });


  if (!returnData && files && (valid === "image" ? fileName : true)) Object(toastify["a" /* default */])("فایل شما نباید " + files.type + " باشد", "error");
  setState(prev => _objectSpread({}, prev, {
    progressPercentImage: null,
    progressPercentVideo: null,
    progressPercentSongs: null
  }));
  return returnData;
};

/* harmony default export */ var onChanges_uploadChange = (uploadChange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/handelOnchange.js
function handelOnchange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function handelOnchange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { handelOnchange_ownKeys(Object(source), true).forEach(function (key) { handelOnchange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { handelOnchange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function handelOnchange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const handelOnchange = async ({
  event,
  data,
  setData,
  setState,
  setLoading,
  imageType,
  validName,
  checkValidity,
  updateObject,
  uploadChange,
  fileName
}) => {
  let changeValid,
      updatedForm,
      updatedFormElement = {};
  let formIsValid = true;
  let value = data.Form[event.name].value;

  let update = handelOnchange_objectSpread({}, value);

  let eventValue = event.value;
  let typeofData = typeof value;
  let isArray, isObject, isString;
  typeofData === "object" && value != undefined ? value && value.length >= 0 ? isArray = true : isObject = true : isString = true;

  const remove = index => value.splice(index, 1)[0];

  const push = (val, newVal) => newVal != undefined ? val.push(newVal) : "";

  if (event.type === "file") {
    const uploadFile = await uploadChange({
      event,
      setLoading,
      imageType,
      setState,
      valid: data.Form[event.name].kindOf,
      fileName
    });

    if (uploadFile) {
      if (isArray) value.includes(uploadFile) ? remove(value.findIndex(d => d === uploadFile)) : push(value, uploadFile);else value = uploadFile;
    } else return;
  } else if (isArray) value.includes(eventValue) ? remove(value.findIndex(d => d === eventValue)) : push(value, eventValue);else if (isObject) {
    if (event.child) {
      value[event.child] = eventValue;
    } else value = eventValue;
  } else if (isString) value = eventValue;

  let checkValidValue;
  if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value; // ////console.log({ moooooj: checkValidity(checkValidValue, data.Form[event.name].validation, isArray).isValid });
  // console.log({ checkValidValue, eventValue, value, typeofData: typeofData, checkEvent: data.Form[event.name] });

  updatedFormElement[event.name] = updateObject(data.Form[event.name], {
    value: value,
    valid: data.Form[event.name].validation.required ? typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).isValid : true,
    touched: true,
    block: false,
    titleValidity: checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).errorTitle
  });

  if (validName) {
    changeValid = updateObject(data.Form[validName], {
      block: true
    });
    updatedForm = updateObject(data.Form, handelOnchange_objectSpread({}, updatedFormElement, {
      [validName]: changeValid
    }));
  } else updatedForm = updateObject(data.Form, handelOnchange_objectSpread({}, updatedFormElement));

  console.log({
    updatedFormElement,
    updatedForm
  });

  for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid || updatedForm[name].block && formIsValid;

  return setData({
    Form: updatedForm,
    formIsValid: formIsValid
  }); // setData({ Form: updatedForm, formIsValid: formIsValid });
};

/* harmony default export */ var onChanges_handelOnchange = (handelOnchange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/arrayOnchange.js
function arrayOnchange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function arrayOnchange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { arrayOnchange_ownKeys(Object(source), true).forEach(function (key) { arrayOnchange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { arrayOnchange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function arrayOnchange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const arrayOnchange = async props => {
  const {
    event: e,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity,
    updateObject,
    uploadChange
  } = props; // //console.log({ validName });

  let changeValid,
      updatedForm,
      updatedFormElement = {};
  e.map(async event => {
    let value = data.Form[event.name].value;

    let update = arrayOnchange_objectSpread({}, value);

    let eventValue = event.value;
    let typeofData = typeof value;
    let isArray, isObject, isString;
    typeofData === "object" && value != null ? value.length >= 0 ? isArray = true : isObject = true : isString = true;

    const remove = index => value.splice(index, 1)[0];

    const push = (val, newVal) => newVal != undefined ? val.push(newVal) : ""; // //console.log({ eventValue, value });
    // if (event.type === "file") {
    //   const uploadFile = await uploadChange(event, setLoading, imageType, setState);
    //   if (uploadFile) {
    //     if (isArray) value.includes(uploadFile) ? remove(value.findIndex((d) => d === uploadFile)) : push(value, uploadFile);
    //     else value = uploadFile;
    //   } else return;
    // } else if (isArray) value.includes(eventValue) ? remove(value.findIndex((d) => d === eventValue)) : push(value, eventValue);
    // else if (isObject) {
    //   value = eventValue;
    //   if (event.child) value = update[event.child] = eventValue;
    // } else if (isString)


    value = eventValue;
    let checkValidValue;
    if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value;
    updatedFormElement[event.name] = updateObject(data.Form[event.name], {
      value: value,
      valid: data.Form[event.name].validation.required ? typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).isValid : true,
      touched: true,
      titleValidity: checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).errorTitle
    });

    if (validName) {
      changeValid = updateObject(data.Form[validName], {
        valid: true
      });
      updatedForm = updateObject(data.Form, arrayOnchange_objectSpread({}, updatedFormElement, {
        [validName]: changeValid
      }));
    } else updatedForm = updateObject(data.Form, arrayOnchange_objectSpread({}, updatedFormElement)); // //console.log({ updatedFormElement, updatedForm });


    let formIsValid = false;

    for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid || updatedForm[name].block && formIsValid;

    return setData({
      Form: updatedForm,
      formIsValid: formIsValid
    });
  });
};

/* harmony default export */ var onChanges_arrayOnchange = (arrayOnchange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/globalChange.js






const globalChange = async props => {
  const {
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    fileName,
    dispatch
  } = props;
  let typeCheck = typeof event; // //console.log(typeCheck === "object" && event.length > 0);
  //console.log({ event });

  if (typeCheck === "object" && event.length > 0) return onChanges_arrayOnchange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: checkValidity_checkValidity,
    updateObject: utils_updateObject["a" /* default */],
    uploadChange: onChanges_uploadChange,
    fileName,
    dispatch
  });else if (typeCheck === "object") return onChanges_handelOnchange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: checkValidity_checkValidity,
    updateObject: utils_updateObject["a" /* default */],
    uploadChange: onChanges_uploadChange,
    fileName,
    dispatch
  });
};

/* harmony default export */ var onChanges_globalChange = (globalChange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/index.js


const onChanges = {
  globalChange: onChanges_globalChange,
  arrayOnchange: onChanges_arrayOnchange
};
/* harmony default export */ var utils_onChanges = (onChanges);
// CONCATENATED MODULE: ./panelAdmin/utils/handleKey.js
const handleKey = event => {
  const form = event.target.form;
  const index = Array.prototype.indexOf.call(form, event.target);
  let keyCode = event.keyCode;
  let numberAccepted = [8, 13];

  if (numberAccepted.includes(keyCode)) {
    if (keyCode === 13) {
      if (form.elements[index + 1]) return form.elements[index + 1].focus();
    } else if (keyCode === 8) if (form.elements[index].value) return form.elements[index].value.length - 1; // else if (form.elements[index - 1]) form.elements[index - 1].focus();


    event.preventDefault();
  }
};

/* harmony default export */ var utils_handleKey = (handleKey);
// EXTERNAL MODULE: ./panelAdmin/utils/json/table/category.json
var table_category = __webpack_require__("bFXO");

// EXTERNAL MODULE: ./panelAdmin/utils/json/table/gallery.json
var table_gallery = __webpack_require__("7KhZ");

// CONCATENATED MODULE: ./panelAdmin/utils/json/table/index.js


const table_table = {
  category: table_category,
  gallery: table_gallery
};
/* harmony default export */ var json_table = (table_table);
// CONCATENATED MODULE: ./panelAdmin/utils/json/index.js

const json = {
  table: json_table
};
/* harmony default export */ var utils_json = (json);
// CONCATENATED MODULE: ./panelAdmin/utils/operation/submitted.js
function submitted_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function submitted_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { submitted_ownKeys(Object(source), true).forEach(function (key) { submitted_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { submitted_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function submitted_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const submitted = async props => {
  const {
    removeKey,
    editKey,
    setSubmitLoading,
    data,
    editData,
    put,
    post,
    setData,
    states,
    setEdit,
    propsHideModal,
    setCheckSubmitted,
    checkSubmitted
  } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};

  for (let formElementIdentifier in data.Form) {
    if (formElementIdentifier === "isRequired") {
      formData[formElementIdentifier] = data.Form[formElementIdentifier].value === "true" ? true : false;
    } else if (formElementIdentifier === (editKey === null || editKey === void 0 ? void 0 : editKey.beforeKey)) formData[editKey === null || editKey === void 0 ? void 0 : editKey.afterKey] = data.Form[formElementIdentifier].value;else if (formElementIdentifier !== removeKey) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
  }

  console.log({
    formData
  });

  if (editData) {
    if (await put({
      id: editData._id,
      data: formData
    })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post(formData)) setData(submitted_objectSpread({}, states));

  if (states.Form["phone"]) {
    states.Form["phone"].value = [];
  }

  if (states.Form["coordinate"]) {
    states.Form["coordinate"].value = {
      lat: "",
      lng: ""
    };
  }

  if (states.Form["location"]) {
    states.Form["location"].value = {
      lat: "",
      lng: ""
    };
  }

  if (states.Form["slides"]) {
    states.Form["slides"].value = [];
  }

  setSubmitLoading(false);
};

/* harmony default export */ var operation_submitted = (submitted);
// CONCATENATED MODULE: ./panelAdmin/utils/operation/FormManagement.js
function FormManagement_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function FormManagement_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { FormManagement_ownKeys(Object(source), true).forEach(function (key) { FormManagement_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { FormManagement_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function FormManagement_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const FormManagement = ({
  categoryData,
  formElement,
  showModal,
  inputChangedHandler,
  accept,
  OwnerData,
  removeHandel,
  staticTitle,
  storeData
}) => {
  let disabled, staticTitleValue;
  let value = formElement.config.value;
  if (formElement.id === (staticTitle === null || staticTitle === void 0 ? void 0 : staticTitle.name)) staticTitleValue = staticTitle === null || staticTitle === void 0 ? void 0 : staticTitle.value;
  let key = formElement.id;
  let elementType = formElement.config.elementType;
  let elementConfig = formElement.config.elementConfig;

  let remove = index => removeHandel(index, formElement.id);

  let label = formElement.config.label;
  let titleValidity = formElement.config.titleValidity;
  let dataChunk = {
    titleValidity,
    value,
    key,
    elementType,
    elementConfig,
    removeHandel: remove,
    label,
    disabled,
    staticTitle: staticTitleValue
  };

  switch (formElement.id) {
    case "slides":
      disabled = true;
      return FormManagement_objectSpread({}, dataChunk, {
        disabled: true,
        accepted: () => showModal({
          kindOf: "showGallery",
          name: formElement.id
        })
      });
    // break;

    case "image":
      disabled = true;
      return FormManagement_objectSpread({}, dataChunk, {
        disabled: true,
        accepted: () => showModal({
          kindOf: "showGallery",
          name: formElement.id
        })
      });
    // break;

    case "thumbnail":
      disabled = true;
      return FormManagement_objectSpread({}, dataChunk, {
        disabled: true,
        accepted: () => showModal({
          kindOf: "showGallery",
          name: formElement.id
        })
      });

    case "isRequire":
      accepted = value => inputChangedHandler({
        value: value || "",
        name: formElement.id
      });

    case "coordinate":
      return FormManagement_objectSpread({}, dataChunk, {
        changed: value => {
          //console.log({ value });
          inputChangedHandler({
            value: value,
            name: formElement.id
          });
        }
      });

    case "location":
      return FormManagement_objectSpread({}, dataChunk, {
        changed: value => {
          //console.log({ value });
          inputChangedHandler({
            value: value,
            name: formElement.id
          });
        }
      });
    // break;

    case "category":
      return FormManagement_objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: categoryData
      });
    // break;

    case "owner":
      return FormManagement_objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: OwnerData
      });
    // break;

    case "store":
      return FormManagement_objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: storeData
      });
    // break;

    case "parent":
      return FormManagement_objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: storeData
      });
    // break;

    default:
      return FormManagement_objectSpread({}, dataChunk, {
        accepted: value => inputChangedHandler({
          value: value,
          name: formElement.id
        }),
        changed: e => {
          var _e$currentTarget, _e$currentTarget2, _e$currentTarget3;

          return inputChangedHandler({
            value: (_e$currentTarget = e.currentTarget) === null || _e$currentTarget === void 0 ? void 0 : _e$currentTarget.value,
            name: formElement.id,
            type: (_e$currentTarget2 = e.currentTarget) === null || _e$currentTarget2 === void 0 ? void 0 : _e$currentTarget2.type,
            files: (_e$currentTarget3 = e.currentTarget) === null || _e$currentTarget3 === void 0 ? void 0 : _e$currentTarget3.files
          });
        }
      });
  }
};

/* harmony default export */ var operation_FormManagement = (FormManagement);
// CONCATENATED MODULE: ./panelAdmin/utils/operation/formTouchChange.js
function formTouchChange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function formTouchChange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { formTouchChange_ownKeys(Object(source), true).forEach(function (key) { formTouchChange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { formTouchChange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function formTouchChange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const formTouchChange = ({
  data,
  setData
}) => {
  const updateObject = panelAdmin["a" /* default */].utils.updateObject;
  const checkValidity = panelAdmin["a" /* default */].utils.checkValidity;
  let changeValid,
      updatedForm,
      updatedFormElement = {};

  for (const key in data.Form) {
    let value = data.Form[key].value;
    let typeofData = typeof value;
    let isArray, isObject, isString;
    typeofData === "object" && value != undefined ? value && value.length >= 0 ? isArray = true : isObject = true : isString = true; // console.log({ data }, isArray, isObject, isString);

    updatedFormElement[key] = updateObject(data.Form[key], {
      touched: true,
      titleValidity: checkValidity(data.Form[key].value, data.Form[key].validation, isArray, data.Form[key].value).errorTitle
    });
  }

  updatedForm = updateObject(data.Form, formTouchChange_objectSpread({}, updatedFormElement));
  return setData({
    Form: updatedForm,
    formIsValid: false
  });
};

/* harmony default export */ var operation_formTouchChange = (formTouchChange);
// CONCATENATED MODULE: ./panelAdmin/utils/operation/index.js



const operation = {
  submitted: operation_submitted,
  FormManagement: operation_FormManagement,
  formTouchChange: operation_formTouchChange
};
/* harmony default export */ var utils_operation = (operation);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__("vmXh");
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);

// CONCATENATED MODULE: ./panelAdmin/utils/authorization.js


const authorization = () => {
  return external_js_cookie_default.a.get("SafirAdminToken") !== undefined;
};

/* harmony default export */ var utils_authorization = (authorization);
// EXTERNAL MODULE: ./panelAdmin/utils/adminHoc/WithErrorHandler/index.js + 1 modules
var WithErrorHandler = __webpack_require__("HaeA");

// CONCATENATED MODULE: ./panelAdmin/utils/adminHoc/index.js

const adminHoc = {
  WithErrorHandler: WithErrorHandler["a" /* default */]
};
/* harmony default export */ var utils_adminHoc = (adminHoc);
// CONCATENATED MODULE: ./panelAdmin/utils/dictionary/index.js
const dictionary = text => {
  console.log({
    text
  });
  let translated;
  let lowerText = (text === null || text === void 0 ? void 0 : text.toLowerCase()) || "";

  switch (lowerText) {
    case "product":
      translated = "محصول";
      break;

    case "category":
      translated = "دسته بندی";
      break;

    case "store":
      translated = "فروشگاه";
      break;

    default:
      translated = text;
      break;
  }

  return translated;
};

/* harmony default export */ var utils_dictionary = (dictionary);
// CONCATENATED MODULE: ./panelAdmin/utils/index.js













const utils = {
  dictionary: utils_dictionary,
  adminHoc: utils_adminHoc,
  checkValidity: checkValidity_checkValidity,
  authorization: utils_authorization,
  operation: utils_operation,
  consts: utils_consts,
  toastify: toastify["a" /* default */],
  CelanderConvert: utils_CelanderConvert,
  onChanges: utils_onChanges,
  handleKey: utils_handleKey,
  formatMoney: utils_formatMoney,
  json: utils_json,
  updateObject: utils_updateObject["a" /* default */]
};
/* harmony default export */ var panelAdmin_utils = __webpack_exports__["a"] = (utils);

/***/ }),

/***/ "zr5I":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })

/******/ });