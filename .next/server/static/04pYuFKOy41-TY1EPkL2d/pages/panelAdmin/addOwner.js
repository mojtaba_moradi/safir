module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../ssr-module-cache.js');
/******/
/******/ 	// object to store loaded chunks
/******/ 	// "0" means "already loaded"
/******/ 	var installedChunks = {
/******/ 		11: 0
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// require() chunk loading for javascript
/******/
/******/ 		// "0" is the signal for "already loaded"
/******/ 		if(installedChunks[chunkId] !== 0) {
/******/ 			var chunk = require("../../../../" + ({}[chunkId]||chunkId) + "." + {"0":"8aa5d05c26a1d43dcf50","28":"aab3f766c1b421fc5a9d"}[chunkId] + ".js");
/******/ 			var moreModules = chunk.modules, chunkIds = chunk.ids;
/******/ 			for(var moduleId in moreModules) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 			for(var i = 0; i < chunkIds.length; i++)
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// uncaught error handler for webpack runtime
/******/ 	__webpack_require__.oe = function(err) {
/******/ 		process.nextTick(function() {
/******/ 			throw err; // catch this error by using import().catch()
/******/ 		});
/******/ 	};
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ({

/***/ "+3UN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_lab_Rating__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("n9sB");
/* harmony import */ var _material_ui_lab_Rating__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_lab_Rating__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("9Pu4");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const Star = props => {
  const {
    rating,
    fixed,
    size
  } = props;
  const {
    0: Value,
    1: setValue
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])();
  const {
    0: Hover,
    1: setHover
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])();
  const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__["makeStyles"])(theme => ({
    root: {
      display: "flex",
      flexDirection: "column",
      "& > * + *": {
        marginTop: theme.spacing(1)
      }
    }
  }));
  const classes = useStyles();

  let element = __jsx(_material_ui_lab_Rating__WEBPACK_IMPORTED_MODULE_1___default.a, {
    name: "half-rating",
    defaultValue: Value ? Value : 2.5,
    precision: 0.5,
    onChange: (event, newValue) => {
      setValue(newValue);
    },
    onChangeActive: (event, newHover) => {
      setHover(newHover);
    }
  });

  if (fixed) {
    element = __jsx(_material_ui_lab_Rating__WEBPACK_IMPORTED_MODULE_1___default.a, {
      name: "half-rating-read",
      defaultValue: rating,
      precision: 0.5,
      readOnly: true,
      size: size
    });
  }

  return __jsx("div", {
    style: {
      direction: "ltr",
      marginRight: "auto",
      textAlign: "center",
      justifyContent: "center",
      alignItems: "center"
    },
    className: classes.root,
    size: size
  }, element);
};

/* harmony default export */ __webpack_exports__["a"] = (Star);

/***/ }),

/***/ "/T1H":
/***/ (function(module, exports) {

module.exports = require("next/dynamic");

/***/ }),

/***/ "/jkW":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string

const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ "0Bsm":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.default = withRouter;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _router = __webpack_require__("nOHt");

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return _react.default.createElement(ComposedComponent, Object.assign({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (false) { var name; }

  return WithRouterWrapper;
}

/***/ }),

/***/ "1Awo":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const SpinnerRotate = () => {
  return __jsx("div", {
    className: "spinner"
  }, __jsx("div", {
    className: "cube1"
  }), __jsx("div", {
    className: "cube2"
  }));
};

/* harmony default export */ __webpack_exports__["a"] = (SpinnerRotate);

/***/ }),

/***/ "1WZq":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("IZS3");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const PaginationM = props => {
  const {
    limited,
    pages,
    activePage,
    onClick
  } = props;
  let sort = [];
  if (pages === undefined || pages === 1) return __jsx("div", null);
  Number(activePage); // console.log({ limited, pages, activePage });

  let viewNext = +limited + +activePage;
  let viewPrev = activePage - limited; // console.log({ viewNext, viewPrev });

  for (let index = 1; index <= pages; index++) {
    if (pages >= 9) {
      // console.log({ check: pages - limited }, limited - pages === index, { index });
      if (2 == index && viewPrev == 2) sort.unshift(1);
      if (viewPrev <= index && viewNext >= index) sort.push(index);
    } else if (pages <= 9) sort.push(index);
  }

  if (pages - limited - 1 == activePage && pages >= 9) sort.push(pages); // console.log({ active: pages - limited - 1 == activePage, sort });

  let prevFirst = Number(activePage) < Number(limited);
  let prevSecond = Number(activePage) === Number(1);
  let nextFirst = Number(activePage) === Number(pages);
  let nextSecond = Number(activePage) > Number(pages - limited); // console.log({ prevFirst });
  // console.log({ prevSecond });
  // console.log({ nextFirst });
  // console.log({ nextSecond });
  // console.log({ sort });

  let Pagination_Sort = __jsx("div", {
    className: "Pagination-wrapper"
  }, " ", __jsx("div", {
    className: "paginationIcon centerAll translateR transition0-2",
    disabled: prevFirst,
    onClick: () => prevFirst ? "" : onClick(1)
  }, __jsx("i", {
    className: "fa fa-angle-double-right",
    "aria-hidden": "true"
  })), __jsx("div", {
    className: "paginationIcon centerAll translateR transition0-2",
    disabled: prevSecond,
    onClick: () => prevSecond ? "" : onClick(activePage - 1)
  }, __jsx("i", {
    className: "fa fa-angle-right",
    "aria-hidden": "true"
  })), Number(activePage) > 2 + Number(limited) && pages >= 9 && __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, __jsx("div", {
    className: "paginationIcon centerAll translateT transition0-2",
    onClick: () => onClick(1)
  }, 1), __jsx("div", {
    className: "paginationIcon centerAll"
  }, __jsx("i", {
    className: "fa fa-ellipsis-h",
    "aria-hidden": "true"
  }))), sort.map((number, index) => {
    if (number == 1 && +activePage > +limited + 2 && +pages >= 9) return;else if (number === pages && activePage < pages - limited && pages >= 9) return;else return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, __jsx("div", {
      className: `paginationIcon centerAll translateT transition0-2 ${number === Number(activePage) && "actived"}`,
      onClick: () => onClick(number === Number(activePage) ? false : number),
      key: index + "m"
    }, number));
  }), pages >= 9 && pages - limited - 1 == activePage && __jsx("div", {
    className: `paginationIcon centerAll translateT transition0-2 `,
    onClick: () => onClick(number === Number(activePage) ? false : pages)
  }, pages), activePage < pages - limited - 1 && pages >= 9 && __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, __jsx("div", {
    className: "paginationIcon centerAll"
  }, __jsx("i", {
    className: "fa fa-ellipsis-h",
    "aria-hidden": "true"
  })), __jsx("div", {
    className: "paginationIcon centerAll translateT transition0-2",
    onClick: () => onClick(pages)
  }, pages)), __jsx("div", {
    className: "paginationIcon centerAll translateL transition0-2",
    disabled: nextFirst,
    onClick: () => nextFirst ? "" : onClick(+activePage + +1)
  }, __jsx("i", {
    className: "fa fa-angle-left",
    "aria-hidden": "true"
  })), __jsx("div", {
    className: "paginationIcon centerAll translateL transition0-2",
    disabled: nextSecond,
    onClick: () => nextSecond ? "" : onClick(Number(pages))
  }, __jsx("i", {
    className: "fa fa-angle-double-left",
    "aria-hidden": "true"
  })));

  return Pagination_Sort;
};

/* harmony default export */ __webpack_exports__["a"] = (PaginationM);

/***/ }),

/***/ "284h":
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__("cDf5");

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "4Q3z":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("kCdF");


/***/ }),

/***/ "6LTc":
/***/ (function(module, exports) {

module.exports = require("leaflet-geosearch");

/***/ }),

/***/ "7KhZ":
/***/ (function(module) {

module.exports = JSON.parse("{\"headers\":[\"ردیف\",\"عکس\",\"عنوان\"],\"body\":[\"url\",\"name\"]}");

/***/ }),

/***/ "7NiY":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const BackgrandCover = props => {
  return __jsx("div", {
    id: "coverContainer",
    onClick: props.onClick,
    className: props.fadeIn ? " fadeIn" : " fadeOut"
  });
};

/* harmony default export */ __webpack_exports__["a"] = (BackgrandCover);

/***/ }),

/***/ "9Pu4":
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/styles");

/***/ }),

/***/ "AuoD":
/***/ (function(module, exports) {

module.exports = require("react-leaflet");

/***/ }),

/***/ "E6+O":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("gIEs");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("vmXh");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_1__);

 // const instance = axios.create({ baseURL: "https://rimtal.com/api/v1" });
// const instance = create({ baseURL: "https://pernymarket.ir/api/v1" });

_globalUtils__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].axiosBase.defaults.headers.common["Authorization"] = "Bearer " + js_cookie__WEBPACK_IMPORTED_MODULE_1___default.a.get("SafirAdminToken");
/* harmony default export */ __webpack_exports__["a"] = (_globalUtils__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].axiosBase);

/***/ }),

/***/ "FRaV":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "get", function() { return /* reexport */ Get; });
__webpack_require__.d(__webpack_exports__, "post", function() { return /* reexport */ Post; });
__webpack_require__.d(__webpack_exports__, "put", function() { return /* reexport */ Put; });
__webpack_require__.d(__webpack_exports__, "patch", function() { return /* reexport */ api_Patch; });
__webpack_require__.d(__webpack_exports__, "deletes", function() { return /* reexport */ api_Delete; });

// EXTERNAL MODULE: ./panelAdmin/api/axios-orders.js
var axios_orders = __webpack_require__("E6+O");

// EXTERNAL MODULE: ./panelAdmin/index.js + 16 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/api/Get/categories.js

 // import axios from "../axios-orders";

const categories = async page => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let getUrl = page ? strings.CATEGORY + "/" + page : strings.CATEGORY;
  return axios_orders["a" /* default */].get(getUrl).then(res => {
    console.log({
      categories: res
    });
    return res;
  }).catch(error => {
    console.log({
      error
    }); // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

    return false;
  });
};

/* harmony default export */ var Get_categories = (categories);
// CONCATENATED MODULE: ./panelAdmin/api/Get/products.js



const products = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.PRODUCT;
  return axios_orders["a" /* default */].get(strings + "/" + page); // .then((products) => {
  //   //console.log({ products });
  //   returnData(products.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   //console.log({ error });
  //  // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ var Get_products = (products);
// CONCATENATED MODULE: ./panelAdmin/api/Get/gallery.js

 // //console.log(axios);

const gallery = async ({
  page
}) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  return axios_orders["a" /* default */].get(strings.IMAGE + "/" + page).then(gallery => {
    //console.log({ gallery });
    return gallery; // loading(false);
  }).catch(error => {//console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ var Get_gallery = (gallery);
// CONCATENATED MODULE: ./panelAdmin/api/Get/sliders.js



const sliders = async page => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const axiosData = page ? strings.SLIDER + "/" + page : strings.SLIDER;
  return axios_orders["a" /* default */].get(axiosData).then(sliders => {
    //console.log({ sliders });
    return sliders.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Get_sliders = (sliders);
// EXTERNAL MODULE: ./globalUtils/index.js + 2 modules
var globalUtils = __webpack_require__("gIEs");

// CONCATENATED MODULE: ./panelAdmin/api/Get/owners.js



const owners = async page => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.OWNERS;
  let url = page ? strings + "/" + page : strings;
  return axios.get(url).then(owners => {
    //console.log({ owners });
    return owners;
  }).catch(error => {//console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ var Get_owners = (owners);
// CONCATENATED MODULE: ./panelAdmin/api/Get/store.js



const store = async page => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.STORE;
  let url = page ? strings + "/" + page : strings;
  return axios.get(url).then(store => {
    //console.log({ store });
    return store;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Get_store = (store);
// CONCATENATED MODULE: ./panelAdmin/api/Get/ownersSearch.js




const ownersSearch = async (param, page = 1) => {
  // const axios = globalUtils.axiosBase;
  //console.log({ param, page });
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.OWNERS + "/s/" + param + "/" + page; //console.log(strings);

  return axios_orders["a" /* default */].get(strings).then(ownersSearch => {
    return ownersSearch.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Get_ownersSearch = (ownersSearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/storeSearch.js



const storeSearch = async (param, page = 1) => {
  //console.log({ param, page });
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.STORE + "/s/" + param + "/" + page; //console.log({ strings });

  return axios.get(strings).then(storeSearch => {
    //console.log({ storeSearch });
    return storeSearch.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Get_storeSearch = (storeSearch);
// CONCATENATED MODULE: ./panelAdmin/api/Get/banners.js



const banners = async page => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const axiosData = page ? strings.BANNER + "/" + page : strings.BANNER;
  return axios_orders["a" /* default */].get(axiosData).then(banners => {
    //console.log({ banners });
    return banners.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Get_banners = (banners);
// CONCATENATED MODULE: ./panelAdmin/api/Get/notifications.js



const notifications = async page => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let getUrl = page ? strings.NOTIFICATION + "/" + page : strings.NOTIFICATION;
  return axios_orders["a" /* default */].get(getUrl); // .then((res) => {
  //   //console.log({ notifications: res });
  //   return res;
  // })
  // .catch((error) => {
  //   //console.log({ error });
  //  // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  //   return error;
  // });
};

/* harmony default export */ var Get_notifications = (notifications);
// CONCATENATED MODULE: ./panelAdmin/api/Get/versions.js



const versions = async page => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.VERSION;
  let getUrl = page ? strings + "/" + page : strings;
  return axios.get(getUrl).then(res => {
    //console.log({ versions: res });
    return res;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return error;
  });
};

/* harmony default export */ var Get_versions = (versions);
// CONCATENATED MODULE: ./panelAdmin/api/Get/users.js



const users = async page => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString.USER;
  let url = page ? strings + "/" + page : strings;
  return axios.get(url).then(users => {
    console.log({
      users
    });
    return users === null || users === void 0 ? void 0 : users.data;
  }).catch(error => {
    console.log({
      error
    }); // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

    return false;
  });
};

/* harmony default export */ var Get_users = (users);
// CONCATENATED MODULE: ./panelAdmin/api/Get/index.js












const get = {
  categories: Get_categories,
  products: Get_products,
  gallery: Get_gallery,
  sliders: Get_sliders,
  owners: Get_owners,
  store: Get_store,
  ownersSearch: Get_ownersSearch,
  storeSearch: Get_storeSearch,
  banners: Get_banners,
  notifications: Get_notifications,
  versions: Get_versions,
  users: Get_users
};
/* harmony default export */ var Get = (get);
// CONCATENATED MODULE: ./panelAdmin/api/Put/owner.js



const owner = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.OWNERS; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Put_owner = (owner);
// CONCATENATED MODULE: ./panelAdmin/api/Put/store.js



const store_store = async param => {
  //console.log({ param });
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.STORE; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Put_store = (store_store);
// CONCATENATED MODULE: ./panelAdmin/api/Put/category.js



const category = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.CATEGORY; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Put_category = (category);
// CONCATENATED MODULE: ./panelAdmin/api/Put/banner.js



const banner = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.BANNER; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Put_banner = (banner);
// CONCATENATED MODULE: ./panelAdmin/api/Put/slider.js



const slider = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.SLIDER; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Put_slider = (slider);
// CONCATENATED MODULE: ./panelAdmin/api/Put/index.js
// import editSection from "./editSection";





const put = {
  owner: Put_owner,
  store: Put_store,
  category: Put_category,
  banner: Put_banner,
  slider: Put_slider
};
/* harmony default export */ var Put = (put);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// CONCATENATED MODULE: ./panelAdmin/api/Post/imageUpload.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const imageUpload = async (files, setLoading, setState, imageName) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString; //console.log({ files, setLoading, setState, imageName });

  setLoading(true); // ============================================= const

  const CancelToken = external_axios_default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => _objectSpread({}, prev, {
        progressPercentImage: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const URL = strings.UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName); // formData.append("imageType", type);

  formData.append("image", files); //=============================================== axios

  return axios_orders["a" /* default */].post(URL, formData, settings).then(Response => {
    //console.log({ Response });
    setLoading(false);
    return Response.data;
  }).catch(error => {
    //console.log({ error });
    setLoading(false); // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

    return false;
  });
};

/* harmony default export */ var Post_imageUpload = (imageUpload);
// CONCATENATED MODULE: ./panelAdmin/api/Post/category.js



const category_category = async (param, setLoading) => {
  // setLoading(true);
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.CATEGORY;
  return axios_orders["a" /* default */].post(URL, param).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // setLoading(false);
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // if (error.response.data)
    //   switch (error.response.data.Error) {
    //     case 1016:
    //       toastify("وزن تکراری می باشد", "error");
    //       break;
    //     case 1017:
    //       toastify("عنوان انگلیسی تکراری می باشد", "error");
    //       break;
    //     case 1018:
    //       toastify("عنوان فارسی تکراری می باشد", "error");
    //       break;
    //     default:
    //       toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    //       break;
    //   }
    return false;
  });
};

/* harmony default export */ var Post_category = (category_category);
// CONCATENATED MODULE: ./panelAdmin/api/Post/product.js



const product = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.PRODUCT;
  return axios_orders["a" /* default */].post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_product = (product);
// CONCATENATED MODULE: ./panelAdmin/api/Post/owner.js



const owner_owner = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const axios = globalUtils["a" /* default */].axiosBase;
  let URL = strings.OWNERS;
  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_owner = (owner_owner);
// CONCATENATED MODULE: ./panelAdmin/api/Post/store.js



const Post_store_store = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const axios = globalUtils["a" /* default */].axiosBase;
  let URL = strings.STORE;
  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_store = (Post_store_store);
// CONCATENATED MODULE: ./panelAdmin/api/Post/slider.js



const slider_slider = async (param, setLoading) => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.SLIDER; //console.log({ param });

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_slider = (slider_slider);
// CONCATENATED MODULE: ./panelAdmin/api/Post/banner.js



const banner_banner = async (param, setLoading) => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.BANNER; //console.log({ param });

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_banner = (banner_banner);
// CONCATENATED MODULE: ./panelAdmin/api/Post/notification.js



const notification = async (param, setLoading) => {
  // setLoading(true);
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.NOTIFICATION;
  return axios_orders["a" /* default */].post(URL, param).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    }); // setLoading(false);
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //  if (error.response.data)
    //   switch (error.response.data.Error) {
    //     case 1016:
    //       toastify("وزن تکراری می باشد", "error");
    //       break;
    //     case 1017:
    //       toastify("عنوان انگلیسی تکراری می باشد", "error");
    //       break;
    //     case 1018:
    //       toastify("عنوان فارسی تکراری می باشد", "error");
    //       break;
    //     default:
    //       toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    //       break;
    //   }

    return false;
  });
};

/* harmony default export */ var Post_notification = (notification);
// CONCATENATED MODULE: ./panelAdmin/api/Post/version.js



const version = async (param, setLoading) => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  let URL = strings.VERSION; //console.log({ param });

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_version = (version);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__("vmXh");
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);

// CONCATENATED MODULE: ./panelAdmin/api/Post/login.js




const login = async (param, setLoading) => {
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const strings = panelAdmin["a" /* default */].values.apiString;
  const pageRoutes = panelAdmin["a" /* default */].values.routes.GS_ADMIN_DASHBOARD;
  const axios = globalUtils["a" /* default */].axiosBase;
  console.log({
    param
  });
  let URL = strings.LOGIN; // setLoading(true);

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    external_js_cookie_default.a.set("SafirAdminToken", Response.data.token, {
      expires: 7
    }); // // window.location = pageRoutes.GS_PANEL_ADMIN_TITLE;

    toastify("شما تایید شده اید", "success");
    setTimeout(() => {
      window.location = pageRoutes;
    }, 1000);
    return true;
  }).catch(error => {
    console.log({
      error
    });
    setLoading(false);
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    if (error.response.data.Error === 1019) toastify("این شماره ثبت نشده است", "error");else if (error.response.data.Error === 1099) toastify("این شماره ثبت نشده است", "error");else if (error.response.data.Error === 1098) toastify("پسورد شما نامعتبر است", "error");else toastify("خطایی در سرور . لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Post_login = (login);
// CONCATENATED MODULE: ./panelAdmin/api/Post/index.js










const post = {
  imageUpload: Post_imageUpload,
  category: Post_category,
  product: Post_product,
  owner: Post_owner,
  store: Post_store,
  slider: Post_slider,
  banner: Post_banner,
  notification: Post_notification,
  version: Post_version,
  login: Post_login
};
/* harmony default export */ var Post = (post);
// CONCATENATED MODULE: ./panelAdmin/api/Patch/index.js
// import category from "./category";
// import editDiscount from "./discount";
// import club from "./club";
// import owner from "./owner";
// import slider from "./slider";
// import banner from "./banner";
const Patch = {// category,
  // editDiscount,
  // club,
  // owner,
  // slider,
  // banner,
};
/* harmony default export */ var api_Patch = (Patch);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/product.js

 // import Axios from "axios";

const product_product = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  let URL = panelAdmin["a" /* default */].values.apiString.PRODUCT;
  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_product = (product_product);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/gallery.js

 // import Axios from "axios";

const gallery_gallery = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.IMAGE; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_gallery = (gallery_gallery);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/category.js

 // import Axios from "axios";

const Delete_category_category = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.CATEGORY; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_category = (Delete_category_category);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/owner.js

 // import Axios from "axios";

const Delete_owner_owner = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.OWNERS; // //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_owner = (Delete_owner_owner);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/store.js

 // import Axios from "axios";

const store_owner = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.STORE; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_store = (store_owner);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/slider.js

 // import Axios from "axios";

const Delete_slider_slider = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.SLIDER; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_slider = (Delete_slider_slider);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/banner.js

 // import Axios from "axios";

const Delete_banner_banner = async param => {
  const axios = globalUtils["a" /* default */].axiosBase;
  const toastify = panelAdmin["a" /* default */].utils.toastify;
  const URL = panelAdmin["a" /* default */].values.apiString.BANNER; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ var Delete_banner = (Delete_banner_banner);
// CONCATENATED MODULE: ./panelAdmin/api/Delete/index.js
// import owner from "./owner";
// import discount from "./discount";
// import club from "./club";
// import slider from "./slider";
// import banner from "./banner";
// import category from "./category";







const Delete = {
  product: Delete_product,
  gallery: Delete_gallery,
  category: Delete_category,
  owner: Delete_owner,
  store: Delete_store,
  slider: Delete_slider,
  banner: Delete_banner
};
/* harmony default export */ var api_Delete = (Delete);
// CONCATENATED MODULE: ./panelAdmin/api/index.js







/***/ }),

/***/ "Go1v":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export default */
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("aYjl");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _globalUtils_axiosBase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("OZZ0");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



function useApiRequest(request, _ref = {}) {
  let {
    initialData
  } = _ref,
      config = _objectWithoutProperties(_ref, ["initialData"]);

  return swr__WEBPACK_IMPORTED_MODULE_0___default()(request && JSON.stringify(request), () => Object(_globalUtils_axiosBase__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"])(request || {}).then(response => {
    // //console.log({ response });
    return response.data;
  }), _objectSpread({}, config, {
    initialData
  }));
}

/***/ }),

/***/ "H8dW":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__("YFqc");
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);

// EXTERNAL MODULE: external "react-dom"
var external_react_dom_ = __webpack_require__("faye");

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__("4Q3z");

// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/MenuTitle/index.js
var __jsx = external_react_default.a.createElement;


const MenuTitle = ({
  title
}) => {
  return __jsx("li", {
    className: "side-header change-position"
  }, __jsx("h6", null, title));
};

/* harmony default export */ var SideMenu_MenuTitle = (MenuTitle);
// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/SubMenu/index.js
var SubMenu_jsx = external_react_default.a.createElement;



const SubMenu = ({
  menu,
  setMenuTitle,
  showLi,
  selectedMenu,
  windowLocation
}) => {
  const location = windowLocation;
  const sideMenuLi = Object(external_react_["useRef"])(null);

  const onSubMenuClicked = (subMenu, menu) => {
    setMenuTitle(menu.menuTitle);
  };

  let liWidth = 0;
  if (sideMenuLi.current) liWidth = sideMenuLi.current.clientHeight;
  return SubMenu_jsx("ul", {
    style: {
      height: showLi === menu.menuTitle ? menu.subMenu.length * liWidth + "px" : ""
    },
    className: `side-child-navigation transition0-3 ${showLi === menu.menuTitle && menu.subMenu.length ? "showIn" : "showOut"}`
  }, menu.subMenu.map((child, i) => SubMenu_jsx("li", {
    ref: sideMenuLi,
    className: `side-iteme`,
    key: "subMenu-" + i,
    onClick: () => onSubMenuClicked(child, menu)
  }, SubMenu_jsx(link_default.a, {
    href: child.route,
    as: child.route
  }, SubMenu_jsx("a", {
    className: `side-link side-child-ling ${child.route === selectedMenu || location.includes(child.route) ? "activedSideChild" : ""} `,
    id: "sideChildTitle"
  }, child.title)))));
};

/* harmony default export */ var SideMenu_SubMenu = (SubMenu);
// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/Menu/index.js
var Menu_jsx = external_react_default.a.createElement;




const Menu = ({
  menus,
  showLi,
  setShowLi,
  selectedMenuTitle,
  setMenuTitle,
  selectedMenu,
  windowLocation
}) => {
  const location = windowLocation;

  const activedSideTitle = name => {
    let newName = name;

    if (showLi === name) {
      newName = "";
    } else {
      newName = name;
    }

    setShowLi(newName);
  };

  const classNameForMenu = (length, title) => {
    let classes;
    classes = [length ? "icon-right-open " : "", // selectedMenuTitle === title ? (showLi === title ? "" : "activeMenu") : "",
    showLi === title ? "arrowRotate" : "unsetRotate"].join(" ");
    return classes;
  };

  const _handelStateNull = () => {
    setShowLi("");
    setMenuTitle("");
  };

  return menus.map((menu, index) => {
    return Menu_jsx("li", {
      key: "menus-" + index,
      className: "side-iteme"
    }, Menu_jsx(link_default.a, {
      href: !menu.subMenu.length > 0 ? menu.route : "#"
    }, Menu_jsx("a", {
      onClick: menu.route ? _handelStateNull : () => activedSideTitle(menu.subMenu && menu.menuTitle),
      id: location.includes(menu.route) ? "activedSide" : selectedMenuTitle === menu.menuTitle ? showLi === menu.menuTitle ? "" : "activedSide" : "",
      className: `side-link ${classNameForMenu(menu.subMenu.length, menu.menuTitle)}`
    }, menu.menuIconImg ? Menu_jsx("img", {
      src: menu.menuIconImg,
      alt: "icon menu"
    }) : Menu_jsx("i", {
      className: menu.menuIconClass
    }), Menu_jsx("span", null, menu.menuTitle), menu.subMenu.length ? Menu_jsx("div", {
      className: "menu-arrow-icon"
    }, Menu_jsx("i", {
      className: "fas fa-angle-left"
    })) : "")), Menu_jsx(SideMenu_SubMenu, {
      windowLocation: windowLocation,
      menu: menu,
      setMenuTitle: setMenuTitle,
      showLi: showLi,
      selectedMenu: selectedMenu
    }));
  });
};

/* harmony default export */ var SideMenu_Menu = (Menu);
// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/MainMenu/index.js
var MainMenu_jsx = external_react_default.a.createElement;




const MainMenu = ({
  mainMenus,
  windowLocation
}) => {
  const {
    0: showLi,
    1: setShowLi
  } = Object(external_react_["useState"])("");
  const {
    0: selectedMenuTitle,
    1: setMenuTitle
  } = Object(external_react_["useState"])(); // //console.log({ windowLocation });

  Object(external_react_["useEffect"])(() => {
    checkMenu();
  }, []);

  const checkMenu = () => {
    mainMenus.map(menu => {
      return menu.menus.map(menu => {
        return menu.subMenu.map(subMenu => {
          if (subMenu.route === "/" + windowLocation.substr(windowLocation.indexOf("panelAdmin"))) {
            setMenuTitle(menu.menuTitle);
            setShowLi(menu.menuTitle);
            return true;
          }
        });
      });
    });
  };

  Object(external_react_["useEffect"])(() => {
    checkMenu();
  }, [windowLocation]); // //console.log({ showLi, selectedMenuTitle });

  return mainMenus.map((mainMenu, index) => {
    return MainMenu_jsx("ul", {
      key: index + "m"
    }, MainMenu_jsx(SideMenu_MenuTitle, {
      title: mainMenu.title
    }), MainMenu_jsx(SideMenu_Menu, {
      windowLocation: windowLocation,
      menus: mainMenu.menus,
      showLi: showLi,
      setShowLi: setShowLi,
      selectedMenuTitle: selectedMenuTitle,
      setMenuTitle: setMenuTitle
    }));
  });
};

/* harmony default export */ var SideMenu_MainMenu = (MainMenu);
// EXTERNAL MODULE: external "react-scrollbars-custom"
var external_react_scrollbars_custom_ = __webpack_require__("YPTf");

// EXTERNAL MODULE: external "react-perfect-scrollbar"
var external_react_perfect_scrollbar_ = __webpack_require__("RfFk");
var external_react_perfect_scrollbar_default = /*#__PURE__*/__webpack_require__.n(external_react_perfect_scrollbar_);

// EXTERNAL MODULE: ./panelAdmin/index.js + 16 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/component/SideMenu/index.js
var SideMenu_jsx = external_react_default.a.createElement;



 // import logo from "../../assets/img/Rimtal.png";

 // import "./index.scss";
// import "react-perfect-scrollbar/dist/css/styles.css";

 // import ScrollArea from "react-scrollbar";

 // const ScrollArea = require("react-scrollbar");



const SideMenu = ({
  windowLocation,
  prefetch
}) => {
  // return useMemo(() => {
  const router = Object(router_["useRouter"])(); // useEffect(() => {
  //   if (prefetch) router.prefetch();
  // });
  //console.log({ router });

  return SideMenu_jsx("div", {
    className: `panelAdmin-sideBar-container `
  }, SideMenu_jsx(external_react_perfect_scrollbar_default.a, null, SideMenu_jsx(SideMenu_MainMenu, {
    mainMenus: panelAdmin["a" /* default */].menuFormat,
    windowLocation: router.route
  }))); // }, []);
};

/* harmony default export */ var component_SideMenu = (SideMenu);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__("vmXh");
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);

// CONCATENATED MODULE: ./panelAdmin/component/Header/HeaderProfile/index.js
var HeaderProfile_jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import profileImage from "../../../assets/Images/icons/user.png";
// import "./index.scss";





const HeaderProfile = () => {
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    showModal: false,
    clickedComponent: false
  });
  const wrapperRef = Object(external_react_["useRef"])(null);
  const routers = Object(router_["useRouter"])();

  const showModalprofile = () => {
    // let noting = ;
    setState(prev => _objectSpread({}, prev, {
      showModal: !state.showModal,
      clickedComponent: true
    }));
  }; // //console.log({ state: state.showModal });


  const logOut = () => {
    external_js_cookie_default.a.remove("SafirAdminToken");
    routers.push("/panelAdmin/login");
  };

  const adminTitleModal = [// {
  //   title: "پروفایل",
  //   iconClass: "fas fa-user",
  //   href: "#",
  //   onClick: null,
  // },
  // {
  //   title: "پیام ها",
  //   iconClass: "fas fa-envelope",
  //   href: "#",
  //   value: "",
  //   onClick: null,
  // },
  // {
  //   title: "تنظیمات",
  //   iconClass: "fas fa-cog",
  //   href: "#",
  //   onClick: null,
  // },
  {
    title: "خروج",
    iconClass: " fas fa-sign-out-alt",
    href: "#",
    onClick: logOut
  }];

  const handleClickOutside = event => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      showModalprofile();
    }
  };

  Object(external_react_["useEffect"])(() => {
    if (state.showModal) {
      document.addEventListener("click", handleClickOutside);
      return () => {
        document.removeEventListener("click", handleClickOutside);
      };
    }
  });

  const adminTitleModal_map = HeaderProfile_jsx("ul", {
    className: `profile-modal-container ${state.showModal ? "actived" : state.clickedComponent ? "deActive" : "showOutStatic"}`
  }, adminTitleModal.map((admin, index) => {
    return HeaderProfile_jsx("li", {
      onClick: admin.onClick,
      key: index
    }, HeaderProfile_jsx("i", {
      className: admin.iconClass
    }), HeaderProfile_jsx(link_default.a, {
      href: admin.href,
      as: admin.href
    }, HeaderProfile_jsx("a", null, admin.title)), admin.value ? HeaderProfile_jsx("span", {
      className: "show-modal-icon-value"
    }, admin.value) : "");
  }));

  return HeaderProfile_jsx("ul", {
    onClick: showModalprofile,
    ref: wrapperRef,
    className: "panel-navbar-profile "
  }, HeaderProfile_jsx("li", {
    className: "pointer hoverColorblack normalTransition"
  }, HeaderProfile_jsx("div", {
    className: "centerAll"
  }, HeaderProfile_jsx("i", {
    className: "fas fa-angle-down"
  })), HeaderProfile_jsx("div", {
    className: "admin-profile-name  icon-up-dir "
  }, HeaderProfile_jsx("span", null, "\u0627\u062F\u0645\u06CC\u0646"), " "), HeaderProfile_jsx("div", {
    className: "admin-profile-image"
  })), adminTitleModal_map);
};

/* harmony default export */ var Header_HeaderProfile = (HeaderProfile);
// EXTERNAL MODULE: ./_context/reducer/index.js + 4 modules
var reducer = __webpack_require__("uqW6");

// CONCATENATED MODULE: ./panelAdmin/component/Header/index.js
var Header_jsx = external_react_default.a.createElement;



const Context = reducer["a" /* default */].panelAdminReducer.optionReducerContext;

const Header = ({
  _handelSidebarToggle
}) => {
  const giveContextData = Object(external_react_["useContext"])(Context);
  const {
    state
  } = giveContextData;
  return Header_jsx("nav", {
    className: "panelAdmin-navbar-container"
  }, Header_jsx("div", {
    className: "panel-navbar-box"
  }, Header_jsx("div", {
    className: "panel-navbar-side-element smallDisplay"
  }, Header_jsx("i", {
    onClick: _handelSidebarToggle,
    className: "fas fa-bars"
  }), Header_jsx("span", {
    className: "page-accepted-name"
  }, state.pageName)), Header_jsx("div", {
    className: "panel-navbar-side-element"
  }, Header_jsx("ul", {
    className: "panel-navbar-notifications"
  }, Header_jsx("li", {
    className: "pointer hoverColorblack normalTransition"
  }, Header_jsx("i", {
    className: "icon-search"
  })), Header_jsx("li", {
    className: "navbar-icon-massege pointer hoverColorblack normalTransition"
  })), Header_jsx(Header_HeaderProfile, null))));
};

/* harmony default export */ var component_Header = (Header);
// EXTERNAL MODULE: ./panelAdmin/component/UI/BackgrandCover/index.js
var BackgrandCover = __webpack_require__("7NiY");

// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__("oAEb");

// EXTERNAL MODULE: ./panelAdmin/utils/adminHoc/WithErrorHandler/index.js + 1 modules
var WithErrorHandler = __webpack_require__("HaeA");

// CONCATENATED MODULE: ./panelAdmin/screen/PanelScreen.js
var PanelScreen_jsx = external_react_default.a.createElement;









const OptionReducer = reducer["a" /* default */].panelAdminReducer.optionReducerProvider;

const PanelScreen = props => {
  const {
    0: sidebarToggle,
    1: setSidebarToggle
  } = Object(external_react_["useState"])(false);
  const router = Object(router_["useRouter"])();

  const _handelSidebarToggle = () => {
    setSidebarToggle(!sidebarToggle);
  };

  Object(external_react_["useEffect"])(() => {
    if (!panelAdmin["a" /* default */].utils.authorization()) router.push("/panelAdmin/login");else if (router.asPath === "/panelAdmin" || router.asPath === "/panelAdmin/") router.push("/panelAdmin/dashboard");
  });
  return panelAdmin["a" /* default */].utils.authorization() ? PanelScreen_jsx(OptionReducer, null, PanelScreen_jsx("div", {
    className: `panelAdmin-wrapper  ${sidebarToggle ? "fadeIn" : "fadeOut"}`
  }, PanelScreen_jsx("div", {
    style: {
      direction: "rtl",
      display: "flex"
    }
  }, PanelScreen_jsx(component_SideMenu, {
    windowLocation: router.asPath
  }), PanelScreen_jsx("div", {
    className: "panelAdmin-container"
  }, PanelScreen_jsx(component_Header, {
    _handelSidebarToggle: _handelSidebarToggle
  }), PanelScreen_jsx("div", {
    className: "panelAdmin-content"
  }, props.children))), PanelScreen_jsx(BackgrandCover["a" /* default */], {
    fadeIn: sidebarToggle,
    onClick: _handelSidebarToggle
  }), PanelScreen_jsx(external_react_toastify_["ToastContainer"], {
    rtl: true
  }))) : "";
};

/* harmony default export */ var screen_PanelScreen = __webpack_exports__["a"] = (Object(WithErrorHandler["a" /* default */])(PanelScreen));

/***/ }),

/***/ "HJQg":
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),

/***/ "HaeA":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./panelAdmin/api/axios-orders.js
var axios_orders = __webpack_require__("E6+O");

// CONCATENATED MODULE: ./panelAdmin/utils/adminHoc/WithErrorHandler/checkError.js
const checkError = ({
  errorCode
}) => {
  let errorTitle;

  switch (errorCode) {
    case 1096:
      errorTitle = "آیدی اشتباه است";
      break;

    case 1098:
      errorTitle = "پسورد شما نامعتبر است";
      break;

    case 1099:
      errorTitle = "خطایی در سرور . لطفا دوباره تلاش کنید";
      break;

    case 1019:
      errorTitle = "ارسال موفقیت آمیز نبود";
      break;

    case 1011:
      errorTitle = "اطلاعات ارسالی نامعتبر است";
      break;

    case 1017:
      errorTitle = "نام انگلیسی تکراری می باشد";
      break;

    case 1018:
      errorTitle = "نام فارسی تکراری می باشد";
      break;

    case 1020:
      errorTitle = "شماره تلفن ثبت نشده است";
      break;

    case 1021:
      errorTitle = "عکس هایی ک در حال استفاده می باشند را نمیتوان حذف کرد";
      break;

    default:
      errorTitle = "خطا در سرور . لطفا دوباره تلاش کنید";
      break;
  }

  return errorTitle;
};

/* harmony default export */ var WithErrorHandler_checkError = (checkError);
// EXTERNAL MODULE: ./panelAdmin/utils/toastify.js
var toastify = __webpack_require__("o6Tf");

// CONCATENATED MODULE: ./panelAdmin/utils/adminHoc/WithErrorHandler/index.js
var __jsx = external_react_default.a.createElement;





const WithErrorHandler = WrappedComponent => {
  return props => {
    var _error$config, _error$config2;

    const {
      0: error,
      1: setError
    } = Object(external_react_["useState"])(null);
    const reqInterceptor = axios_orders["a" /* default */].interceptors.request.use(req => {
      // console.log({ req });
      setError(null);
      return req;
    });
    const resInterceptor = axios_orders["a" /* default */].interceptors.response.use(res => {
      // console.log({ res });
      setError(null);
      return res;
    }, err => {
      setError(err);
    });
    Object(external_react_["useEffect"])(() => {
      return () => {
        axios_orders["a" /* default */].interceptors.request.eject(reqInterceptor);
        axios_orders["a" /* default */].interceptors.response.eject(resInterceptor);
      };
    }, [reqInterceptor, resInterceptor]); // useEffect(() => {
    //   setTimeout(() => {
    //     if (error) setError(null);
    //   }, 1000);
    // }, [error]);

    console.log({
      error
    });
    const errorDisable = "/admin/owner/s//";
    console.log(errorDisable.search(error === null || error === void 0 ? void 0 : (_error$config = error.config) === null || _error$config === void 0 ? void 0 : _error$config.url));
    const test = error === null || error === void 0 ? void 0 : (_error$config2 = error.config) === null || _error$config2 === void 0 ? void 0 : _error$config2.url.split("/");
    if (test) console.log(test[3], test[4], test[5]);
    Object(external_react_["useEffect"])(() => {
      var _error$response;

      if (error) if (test[3] !== "s" && test[4] !== "" && !test[5]) if (error.message === "Network Error") Object(toastify["a" /* default */])("دسترسی به اینترنت را بررسی کنید", "error");else Object(toastify["a" /* default */])(WithErrorHandler_checkError({
        errorCode: error === null || error === void 0 ? void 0 : (_error$response = error.response) === null || _error$response === void 0 ? void 0 : _error$response.data.Error
      }), "error");
    }, [error]);
    return __jsx(external_react_default.a.Fragment, null, __jsx(WrappedComponent, props));
  };
};

/* harmony default export */ var adminHoc_WithErrorHandler = __webpack_exports__["a"] = (WithErrorHandler);

/***/ }),

/***/ "IZS3":
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "J27u":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _panelAdmin_screen_PanelScreen__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("H8dW");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const routingHandle = ({
  Component,
  pageProps,
  router
}) => {
  if (Component.panelAdminLayout || (router === null || router === void 0 ? void 0 : router.asPath) === "/panelAdmin/") return __jsx(_panelAdmin_screen_PanelScreen__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], null, __jsx(Component, pageProps));else return __jsx(Component, pageProps);
};

/* harmony default export */ __webpack_exports__["a"] = (routingHandle);

/***/ }),

/***/ "OZZ0":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("zr5I");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
 // import Cookie from "js-cookie";

const instance = axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: "https://safirccard.ir/api/v1"
}); // instance.defaults.headers.common["Authorization"] = "Bearer ";

/* harmony default export */ __webpack_exports__["a"] = (instance);

/***/ }),

/***/ "Ol4B":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _BackgrandCover__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("7NiY");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ModalBox = props => {
  const {
    showModal,
    onHideModal
  } = props;
  const {
    0: modalHide,
    1: setModalHide
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (showModal) {
      setModalHide(false);
    }
  }, [showModal]);

  const endAnimation = () => {
    if (!showModal) {
      onHideModal();
      setModalHide(true);
    }
  };

  return __jsx("div", {
    className: "modal_container-box",
    style: {
      display: modalHide ? "none" : ""
    }
  }, __jsx("div", {
    onAnimationEnd: endAnimation,
    id: "subjectModal",
    className: showModal ? "fadeIn" : "fadeOut"
  }, __jsx("div", {
    className: "subjectModal-box"
  }, props.children)), __jsx(_BackgrandCover__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], {
    onClick: onHideModal,
    fadeIn: !modalHide
  }));
};

/* harmony default export */ __webpack_exports__["a"] = (ModalBox);

/***/ }),

/***/ "Osoz":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/router-context.js");

/***/ }),

/***/ "RfFk":
/***/ (function(module, exports) {

module.exports = require("react-perfect-scrollbar");

/***/ }),

/***/ "TqRt":
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "VGcP":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// CONCATENATED MODULE: ./panelAdmin/values/strings/en/navbar.js
const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ var en_navbar = (navbar);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/sideMenu.js
const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";
const CATEGORIES = "دسته بندی ها";
const SEE_ARTIST = "مشاهده هنرمندان";
const ARTIST = "هنرمند";
const ADD_ARTIST = "افزودن هنرمند";
const SEE_ALBUM = "مشاهده آلبوم ها";
const ALBUM = "آلبوم";
const ADD_ALBUM = "افزودن آلبوم";
const SEE_GENRES = "مشاهده ژانر ها";
const GENRES = "ژانر";
const ADD_GENRE = "افزودن ژانر";
const SEE_COUNTRY = "مشاهده کشور ها";
const COUNTRY = "کشور";
const ADD_COUNTRY = "افزودن کشور";
const SEE_INSTRUMENT = "مشاهده ساز ها";
const INSTRUMENT = "ساز";
const ADD_INSTRUMENT = "افزودن ساز";
const SEE_GALLERY = "مشاهده گالری ها";
const GALLERY = "گالری";
const SEE_SONG = "مشاهده آهنگ ها";
const SONG = "آهنگ";
const SEE_MOOD = "مشاهده حالت ها";
const MOOD = "حالت";
const sideMenu = {
  SEE_GALLERY,
  GALLERY,
  DASHBOARD,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  SETTING_WEB,
  CATEGORIES,
  SEE_ARTIST,
  ARTIST,
  ADD_ARTIST,
  ALBUM,
  SEE_ALBUM,
  ADD_ALBUM,
  SEE_GENRES,
  GENRES,
  ADD_GENRE,
  SEE_COUNTRY,
  COUNTRY,
  ADD_COUNTRY,
  SEE_INSTRUMENT,
  INSTRUMENT,
  ADD_INSTRUMENT,
  SEE_SONG,
  SONG,
  SEE_MOOD,
  MOOD
};
/* harmony default export */ var en_sideMenu = (sideMenu);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/global.js
const global_TRACKS = "Tracks";
const NO_ENTRIES = "no entries";
const FOLLOWERS = "followers";
const global = {
  TRACKS: global_TRACKS,
  NO_ENTRIES: NO_ENTRIES,
  FOLLOWERS
};
/* harmony default export */ var en_global = (global);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/constants.js
const ALBUM_CONSTANTS = "album";
const PLAY_LIST_CONSTANTS = "playlist";
const SONG_CONSTANTS = "song";
const FLAG_CONSTANTS = "flag";
const ARTIST_CONSTANTS = "artist";
const INSTRUMENT_CONSTANTS = "instrument";
const MUSIC_VIDEO_CONSTANTS = "musicvideo";
const MOOD_CONSTANTS = "mood";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS
};
/* harmony default export */ var en_constants = (constants);
// CONCATENATED MODULE: ./panelAdmin/values/strings/en/index.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const en = _objectSpread({}, en_navbar, {}, en_sideMenu, {}, en_global, {}, en_constants);

/* harmony default export */ var strings_en = (en);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/navbar.js
const navbar_APP_NAME = "ریمتال";
const navbar_EXPLORE = "کاوش کردن";
const navbar_TRACKS = "آهنگ ها";
const navbar_PLAYLISTS = "لیست های پخش";
const navbar_ALBUMS = "آلبوم ها";
const navbar_ARTISTS = "هنرمندان";
const navbar_VIDEOS = "ویدیو ها";
const navbar_SIGN_IN = "ورود";
const navbar_navbar = {
  APP_NAME: navbar_APP_NAME,
  EXPLORE: navbar_EXPLORE,
  TRACKS: navbar_TRACKS,
  PLAYLISTS: navbar_PLAYLISTS,
  ALBUMS: navbar_ALBUMS,
  ARTISTS: navbar_ARTISTS,
  VIDEOS: navbar_VIDEOS,
  SIGN_IN: navbar_SIGN_IN
};
/* harmony default export */ var fa_navbar = (navbar_navbar);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/sideMenu.js
const sideMenu_DASHBOARD = "داشبورد";
const sideMenu_SIDEBAR_ONE_TITLE = "عمومی";
const sideMenu_SIDEBAR_TWO_TITLE = "کاربردی";
const sideMenu_SETTING_WEB = "تنظیمات سایت";
const SEE_VARIABLE = "مشاهده متغییر";
const VARIABLE = "متغییر";
const ADD_VARIABLE = "افزودن متغییر";
const sideMenu_GALLERY = "گالری";
const GALLERIES = "گالری ها";
const ADD_GALLERY = "افزودن گالری";
const SEE_GALLERIES = "مشاهده گالری ها";
const CATEGORY = "دسته بندی";
const sideMenu_CATEGORIES = "دسته بندی ها";
const ADD_CATEGORY = "افزودن دسته بندی";
const SEE_CATEGORIES = "مشاهده دسته بندی ها";
const PRODUCT = "محصول ";
const ADD_PRODUCT = "افزودن محصول";
const SEE_PRODUCTS = "مشاهده محصولات ";
const OWNER = "فروشنده ";
const ADD_OWNER = "افزودن فروشنده";
const SEE_OWNERS = "مشاهده فروشندگان ";
const STORE = "فروشگاه ";
const ADD_STORE = "افزودن فروشگاه";
const SEE_STORES = "مشاهده فروشگاه ها ";
const SLIDER = "اسلایدر ";
const ADD_SLIDER = "افزودن اسلایدر";
const SEE_SLIDERS = "مشاهده اسلایدر ها ";
const BANNER = "بنر ";
const ADD_BANNER = "افزودن بنر";
const SEE_BANNERS = "مشاهده بنر ها ";
const NOTIFICATION = "اعلان ";
const ADD_NOTIFICATION = "افزودن اعلان";
const SEE_NOTIFICATIONS = "مشاهده اعلان ها ";
const VERSION = "ورژن ";
const ADD_VERSION = "افزودن ورژن";
const SEE_VERSIONS = "مشاهده ورژن ها ";
const USERS = "کاربران ";
const sideMenu_sideMenu = {
  DASHBOARD: sideMenu_DASHBOARD,
  SIDEBAR_ONE_TITLE: sideMenu_SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE: sideMenu_SIDEBAR_TWO_TITLE,
  SETTING_WEB: sideMenu_SETTING_WEB,
  CATEGORIES: sideMenu_CATEGORIES,
  SEE_VARIABLE,
  VARIABLE,
  ADD_VARIABLE,
  GALLERY: sideMenu_GALLERY,
  GALLERIES,
  ADD_GALLERY,
  SEE_GALLERIES,
  CATEGORY,
  ADD_CATEGORY,
  SEE_CATEGORIES,
  PRODUCT,
  ADD_PRODUCT,
  SEE_PRODUCTS,
  OWNER,
  ADD_OWNER,
  SEE_OWNERS,
  STORE,
  ADD_STORE,
  SEE_STORES,
  STORE,
  ADD_STORE,
  SEE_STORES,
  SLIDER,
  ADD_SLIDER,
  SEE_SLIDERS,
  BANNER,
  ADD_BANNER,
  SEE_BANNERS,
  NOTIFICATION,
  ADD_NOTIFICATION,
  SEE_NOTIFICATIONS,
  VERSION,
  ADD_VERSION,
  SEE_VERSIONS,
  USERS
};
/* harmony default export */ var fa_sideMenu = (sideMenu_sideMenu);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/global.js
const fa_global_TRACKS = "آهنگ ها ";
const global_NO_ENTRIES = "وارد نشده";
const global_FOLLOWERS = "دنبال کنندگان";
const global_global = {
  TRACKS: fa_global_TRACKS,
  NO_ENTRIES: global_NO_ENTRIES,
  FOLLOWERS: global_FOLLOWERS
};
/* harmony default export */ var fa_global = (global_global);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/constants.js
const constants_ALBUM_CONSTANTS = "آلبوم";
const constants_PLAY_LIST_CONSTANTS = "لیست پخش";
const constants_SONG_CONSTANTS = "موزیک";
const constants_FLAG_CONSTANTS = "پرچم";
const constants_ARTIST_CONSTANTS = "هنرمند";
const constants_INSTRUMENT_CONSTANTS = "ساز";
const constants_MUSIC_VIDEO_CONSTANTS = "موزیک ویدئو";
const constants_MOOD_CONSTANTS = "حالت";
const constants_constants = {
  ALBUM_CONSTANTS: constants_ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS: constants_PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS: constants_SONG_CONSTANTS,
  FLAG_CONSTANTS: constants_FLAG_CONSTANTS,
  ARTIST_CONSTANTS: constants_ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS: constants_INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS: constants_MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS: constants_MOOD_CONSTANTS
};
/* harmony default export */ var fa_constants = (constants_constants);
// CONCATENATED MODULE: ./panelAdmin/values/strings/fa/index.js
function fa_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function fa_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { fa_ownKeys(Object(source), true).forEach(function (key) { fa_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { fa_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function fa_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const fa = fa_objectSpread({}, fa_navbar, {}, fa_sideMenu, {}, fa_global, {}, fa_constants);

/* harmony default export */ var strings_fa = (fa);
// CONCATENATED MODULE: ./panelAdmin/values/strings/index.js


const Lang = "rtl";
let strings;
if (Lang === "rtl") strings = strings_fa;else strings = strings_en;
/* harmony default export */ var values_strings = (strings);
// CONCATENATED MODULE: ./panelAdmin/values/routes/index.js
const GS_PANEL_ADMIN_TITLE = "/panelAdmin";
const GS_ADMIN_DASHBOARD = GS_PANEL_ADMIN_TITLE + "/dashboard";
const GS_ADMIN_GALLERY = GS_PANEL_ADMIN_TITLE + "/gallery";
const GS_ADMIN_CATEGORY = GS_PANEL_ADMIN_TITLE + "/category";
const GS_ADMIN_ADD_CATEGORY = GS_PANEL_ADMIN_TITLE + "/addCategory";
const GS_ADMIN_PRODUCT = GS_PANEL_ADMIN_TITLE + "/product";
const GS_ADMIN_ADD_PRODUCT = GS_PANEL_ADMIN_TITLE + "/addProduct";
const GS_ADMIN_VARIABLE = GS_PANEL_ADMIN_TITLE + "/variable";
const GS_ADMIN_ADD_VARIABLE = GS_PANEL_ADMIN_TITLE + "/addVariable";
const GS_ADMIN_OWNER = GS_PANEL_ADMIN_TITLE + "/owner";
const GS_ADMIN_ADD_OWNER = GS_PANEL_ADMIN_TITLE + "/addOwner";
const GS_ADMIN_STORE = GS_PANEL_ADMIN_TITLE + "/store";
const GS_ADMIN_ADD_STORE = GS_PANEL_ADMIN_TITLE + "/addStore";
const GS_ADMIN_SLIDER = GS_PANEL_ADMIN_TITLE + "/slider";
const GS_ADMIN_ADD_SLIDER = GS_PANEL_ADMIN_TITLE + "/addSlider";
const GS_ADMIN_BANNER = GS_PANEL_ADMIN_TITLE + "/banner";
const GS_ADMIN_ADD_BANNER = GS_PANEL_ADMIN_TITLE + "/addBanner";
const GS_ADMIN_NOTIFICATION = GS_PANEL_ADMIN_TITLE + "/notification";
const GS_ADMIN_ADD_NOTIFICATION = GS_PANEL_ADMIN_TITLE + "/addNotification";
const GS_ADMIN_VERSION = GS_PANEL_ADMIN_TITLE + "/version";
const GS_ADMIN_ADD_VERSION = GS_PANEL_ADMIN_TITLE + "/addVersion";
const GS_ADMIN_USER = GS_PANEL_ADMIN_TITLE + "/user";
const routes = {
  GS_ADMIN_DASHBOARD,
  GS_PANEL_ADMIN_TITLE,
  GS_ADMIN_GALLERY,
  GS_ADMIN_VARIABLE,
  GS_ADMIN_ADD_VARIABLE,
  GS_ADMIN_CATEGORY,
  GS_ADMIN_ADD_CATEGORY,
  GS_ADMIN_PRODUCT,
  GS_ADMIN_ADD_PRODUCT,
  GS_ADMIN_OWNER,
  GS_ADMIN_ADD_OWNER,
  GS_ADMIN_STORE,
  GS_ADMIN_ADD_STORE,
  GS_ADMIN_SLIDER,
  GS_ADMIN_ADD_SLIDER,
  GS_ADMIN_BANNER,
  GS_ADMIN_ADD_BANNER,
  GS_ADMIN_NOTIFICATION,
  GS_ADMIN_ADD_NOTIFICATION,
  GS_ADMIN_VERSION,
  GS_ADMIN_ADD_VERSION,
  GS_ADMIN_USER
};
/* harmony default export */ var values_routes = (routes);
// CONCATENATED MODULE: ./panelAdmin/values/apiString.js
const ADMIN = "/admin";
const apiString_CATEGORY = ADMIN + "/category";
const apiString_PRODUCT = ADMIN + "/product";
const IMAGE = ADMIN + "/images";
const UPLOAD = ADMIN + "/upload";
const OWNERS = ADMIN + "/owner";
const apiString_STORE = ADMIN + "/store";
const apiString_SLIDER = ADMIN + "/slider";
const apiString_BANNER = ADMIN + "/banner";
const apiString_NOTIFICATION = ADMIN + "/notification";
const apiString_VERSION = ADMIN + "/version";
const LOGIN = ADMIN + "/login";
const USER = ADMIN + "/users";
const apiString = {
  CATEGORY: apiString_CATEGORY,
  PRODUCT: apiString_PRODUCT,
  IMAGE,
  UPLOAD,
  OWNERS,
  STORE: apiString_STORE,
  SLIDER: apiString_SLIDER,
  BANNER: apiString_BANNER,
  NOTIFICATION: apiString_NOTIFICATION,
  VERSION: apiString_VERSION,
  LOGIN,
  USER
};
/* harmony default export */ var values_apiString = (apiString);
// CONCATENATED MODULE: ./panelAdmin/values/strings/constants.js
const strings_constants_ALBUM_CONSTANTS = "album";
const strings_constants_PLAY_LIST_CONSTANTS = "playlist";
const strings_constants_SONG_CONSTANTS = "song";
const strings_constants_FLAG_CONSTANTS = "flag";
const strings_constants_ARTIST_CONSTANTS = "artist";
const strings_constants_INSTRUMENT_CONSTANTS = "instrument";
const strings_constants_MUSIC_VIDEO_CONSTANTS = "musicvideo";
const strings_constants_MOOD_CONSTANTS = "mood";
const strings_constants_constants = {
  ALBUM_CONSTANTS: strings_constants_ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS: strings_constants_PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS: strings_constants_SONG_CONSTANTS,
  FLAG_CONSTANTS: strings_constants_FLAG_CONSTANTS,
  ARTIST_CONSTANTS: strings_constants_ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS: strings_constants_INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS: strings_constants_MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS: strings_constants_MOOD_CONSTANTS
};
/* harmony default export */ var strings_constants = (strings_constants_constants);
// CONCATENATED MODULE: ./panelAdmin/values/index.js






const values = {
  routes: values_routes,
  strings: values_strings,
  apiString: values_apiString,
  constants: strings_constants,
  fa: strings_fa,
  en: strings_en
};
/* harmony default export */ var panelAdmin_values = (values);
// CONCATENATED MODULE: ./panelAdmin/utils/menuFormat.js


const menuFormat = [{
  title: "عمومی",
  menus: [{
    route: panelAdmin_values.routes.GS_ADMIN_DASHBOARD,
    menuTitle: panelAdmin_values.strings.DASHBOARD,
    menuIconImg: false,
    menuIconClass: "fas fa-tachometer-slowest",
    subMenu: [// { title: "SubDashboard", route: "/dashboard1" },
      // { title: "SubDashboard", route: "/dashboard2" }
    ]
  }]
}, {
  title: "کاربردی",
  menus: [{
    route: false,
    menuTitle: panelAdmin_values.strings.GALLERY,
    menuIconImg: false,
    menuIconClass: "fad fa-images",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_GALLERIES,
      route: panelAdmin_values.routes.GS_ADMIN_GALLERY
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.CATEGORY,
    menuIconImg: false,
    menuIconClass: "fad fa-cubes",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_CATEGORIES,
      route: panelAdmin_values.routes.GS_ADMIN_CATEGORY
    }, {
      title: panelAdmin_values.strings.ADD_CATEGORY,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_CATEGORY
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.OWNER,
    menuIconImg: false,
    menuIconClass: "far fa-street-view",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_OWNERS,
      route: panelAdmin_values.routes.GS_ADMIN_OWNER
    }, {
      title: panelAdmin_values.strings.ADD_OWNER,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_OWNER
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.STORE,
    menuIconImg: false,
    menuIconClass: "fad fa-store",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_STORES,
      route: panelAdmin_values.routes.GS_ADMIN_STORE
    }, {
      title: panelAdmin_values.strings.ADD_STORE,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_STORE
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.SLIDER,
    menuIconImg: false,
    menuIconClass: "fas fa-presentation",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_SLIDERS,
      route: panelAdmin_values.routes.GS_ADMIN_SLIDER
    }, {
      title: panelAdmin_values.strings.ADD_SLIDER,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_SLIDER
    }]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.BANNER,
    menuIconImg: false,
    menuIconClass: "far fa-archive",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_BANNERS,
      route: panelAdmin_values.routes.GS_ADMIN_BANNER
    }, {
      title: panelAdmin_values.strings.ADD_BANNER,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_BANNER
    }]
  }, {
    route: panelAdmin_values.routes.GS_ADMIN_ADD_NOTIFICATION,
    menuTitle: panelAdmin_values.strings.NOTIFICATION,
    menuIconImg: false,
    menuIconClass: "fad fa-bells",
    subMenu: [// {
      //   title: values.strings.SEE_NOTIFICATIONS,
      //   route: values.routes.GS_ADMIN_NOTIFICATION,
      // },
      // {
      //   title: values.strings.ADD_NOTIFICATION,
      //   route: values.routes.GS_ADMIN_ADD_NOTIFICATION,
      // },
    ]
  }, {
    route: false,
    menuTitle: panelAdmin_values.strings.VERSION,
    menuIconImg: false,
    menuIconClass: "fab fa-vimeo-v",
    subMenu: [{
      title: panelAdmin_values.strings.SEE_VERSIONS,
      route: panelAdmin_values.routes.GS_ADMIN_VERSION
    }, {
      title: panelAdmin_values.strings.ADD_VERSION,
      route: panelAdmin_values.routes.GS_ADMIN_ADD_VERSION
    }]
  }, {
    route: panelAdmin_values.routes.GS_ADMIN_USER,
    menuTitle: panelAdmin_values.strings.USERS,
    menuIconImg: false,
    menuIconClass: "fad fa-users",
    subMenu: []
  }]
}];
/* harmony default export */ var utils_menuFormat = (menuFormat);
// EXTERNAL MODULE: ./panelAdmin/utils/index.js + 62 modules
var utils = __webpack_require__("xb3y");

// EXTERNAL MODULE: ./panelAdmin/api/index.js + 39 modules
var api = __webpack_require__("FRaV");

// CONCATENATED MODULE: ./panelAdmin/index.js




const panelAdmin = {
  menuFormat: utils_menuFormat,
  values: panelAdmin_values,
  utils: utils["a" /* default */],
  api: api
};
/* harmony default export */ var panelAdmin_0 = __webpack_exports__["a"] = (panelAdmin);

/***/ }),

/***/ "VnI6":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Loadings_LoadingDot1__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("dM+x");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ModalTrueFalse = props => {
  // window.onscroll = function () {
  //   scrollFunction();
  // };
  // const scrollFunction=() =>{
  //   if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
  // }
  // useEffect(() => {
  //   let subjectModal = document.querySelector("#subjectModal");
  //   //console.log({ subjectModal });
  //   subjectModal.scrollTop = 0;
  //   // subjectModal.documentElement.scrollTop = 0;
  // });
  // When the user clicks on the button, scroll to the top of the document
  return __jsx("div", {
    className: "modal-box"
  }, __jsx("div", {
    className: "m-b-question-title"
  }, __jsx("span", null, props.modalHeadline), __jsx("span", null, props.modalQuestion)), __jsx("div", {
    className: "btns-container"
  }, __jsx("div", {
    disabled: props.loading,
    onClick: props.loading ? null : () => props.modalAccept(true),
    className: "submited-box"
  }, __jsx("a", {
    className: "btns btns-primary",
    type: "submit"
  }, props.loading ? __jsx(_Loadings_LoadingDot1__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], {
    width: "1.5rem",
    height: "1.5rem"
  }) : props.modalAcceptTitle)), __jsx("div", {
    onClick: () => props.modalAccept(false),
    className: "submited-box"
  }, __jsx("a", {
    className: "btns btns-warning",
    type: "submit"
  }, " ", props.modalCanselTitle))));
};

/* harmony default export */ __webpack_exports__["a"] = (ModalTrueFalse);

/***/ }),

/***/ "YFqc":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("cTJO")


/***/ }),

/***/ "YLtl":
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ "YPTf":
/***/ (function(module, exports) {

module.exports = require("react-scrollbars-custom");

/***/ }),

/***/ "YTqd":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = (normalizedRoute.replace(/\/$/, '') || '/').replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => {
    const isCatchAll = /^(\\\.){3}/.test($1);
    groups[$1 // Un-escape key
    .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '') // eslint-disable-next-line no-sequences
    ] = {
      pos: groupIndex++,
      repeat: isCatchAll
    };
    return isCatchAll ? '/(.+?)' : '/([^/]+?)';
  });
  return {
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  };
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "aYjl":
/***/ (function(module, exports) {

module.exports = require("swr");

/***/ }),

/***/ "b51O":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./panelAdmin/component/UI/Modals/ModalBox/index.js
var ModalBox = __webpack_require__("Ol4B");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Modals/ModalTrueFalse/index.js
var ModalTrueFalse = __webpack_require__("VnI6");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Inputs/Input.js + 14 modules
var Input = __webpack_require__("xYbV");

// EXTERNAL MODULE: ./panelAdmin/index.js + 16 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/AddGallery/FormInputGallery/index.js
var __jsx = external_react_default.a.createElement;




const FormInputGallery = props => {
  const {
    stateArray,
    removeHandel,
    state,
    _onSubmited,
    inputChangedHandler,
    checkSubmitted
  } = props;
  const dropDown = panelAdmin["a" /* default */].utils.consts.galleryConstants();
  let dropDownData = [];

  for (const index in dropDown) dropDownData.push({
    value: dropDown[index].value,
    title: dropDown[index].title
  });

  return __jsx("form", {
    onSubmit: _onSubmited
  }, stateArray.map(formElement => {
    const invalid = !formElement.config.valid;
    const shouldValidate = formElement.config.validation;
    const touched = formElement.config.touched;
    let changed, accepted, progress;
    const inputClasses = ["InputElement"];
    if (invalid && shouldValidate && touched) inputClasses.push("Invalid");else if (formElement.id === "image") {
      progress = state.progressPercentImage;
    }

    changed = e => inputChangedHandler({
      value: e.currentTarget.value,
      name: formElement.id,
      type: e.currentTarget.type,
      files: e.currentTarget.files
    });

    accepted = value => inputChangedHandler({
      value: value,
      name: formElement.id
    });

    let form = __jsx(Input["a" /* default */], {
      key: formElement.id,
      elementType: formElement.config.elementType,
      elementConfig: formElement.config.elementConfig,
      value: formElement.config.value,
      invalid: invalid,
      shouldValidate: shouldValidate,
      touched: touched,
      changed: changed,
      accepted: accepted,
      removeHandel: index => removeHandel(index, formElement.id),
      label: formElement.config.label,
      progress: progress,
      checkSubmitted: checkSubmitted,
      dropDownData: dropDownData
    });

    return form;
  }));
};

/* harmony default export */ var AddGallery_FormInputGallery = (FormInputGallery);
// EXTERNAL MODULE: ./panelAdmin/api/index.js + 39 modules
var api = __webpack_require__("FRaV");

// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/AddGallery/index.js
var AddGallery_jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








const AddGallery = ({
  onHideModal,
  getApi,
  data,
  setData
}) => {
  const states = panelAdmin["a" /* default */].utils.consts.states;
  const onChanges = panelAdmin["a" /* default */].utils.onChanges; // const [data, setData] = useState({ ...states.addGallery });

  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    progressPercentImage: null,
    progressPercentSongs: null,
    remove: {
      value: "",
      name: ""
    }
  });
  const {
    0: Loading,
    1: setLoading
  } = Object(external_react_["useState"])(false);
  const {
    0: Modal,
    1: setModal
  } = Object(external_react_["useState"])({
    show: false
  });
  const {
    0: checkSubmitted,
    1: setcheckSubmitted
  } = Object(external_react_["useState"])(false); // ======================================== modal
  // ========================= End modal =================
  // ============================= submited
  // ========================= End submited =================
  // ============================= remove

  const __returnPrevstep = value => {
    // onHideModal();
    setState(_objectSpread({}, state, {
      remove: {
        value: "",
        name: ""
      }
    }));
    if (value) inputChangedHandler({
      name: state.remove.name,
      value: state.remove.value
    });
  };

  const removeHandel = (value, name) => {
    // onShowlModal();
    setState(_objectSpread({}, state, {
      remove: {
        value,
        name
      }
    }));
  }; // =========================== End remove  ====================


  const inputChangedHandler = async event => {
    await onChanges.globalChange({
      event,
      data,
      setData,
      setState,
      setLoading,
      fileName: data.Form.imageName.value
    });
  };

  const stateArray = [];

  for (let key in data.Form) stateArray.push({
    id: key,
    config: data.Form[key]
  });

  let form = AddGallery_jsx(AddGallery_FormInputGallery, {
    removeHandel: removeHandel,
    stateArray: stateArray,
    data: data,
    state: state,
    setData: setData,
    Loading: Loading,
    setLoading: setLoading,
    inputChangedHandler: inputChangedHandler,
    checkSubmitted: checkSubmitted
  });

  return AddGallery_jsx("div", {
    className: " centerAll "
  }, AddGallery_jsx(ModalBox["a" /* default */], {
    onHideModal: onHideModal,
    showModal: Modal.show
  }, AddGallery_jsx(ModalTrueFalse["a" /* default */], {
    modalHeadline: "آیا مطمئن به حذف آن هستید !",
    modalAcceptTitle: "بله",
    modalCanselTitle: "خیر",
    modalAccept: __returnPrevstep
  })), AddGallery_jsx("div", {
    className: "form-countainer shadowUnset"
  }, AddGallery_jsx("div", {
    className: "row-give-information"
  }, form)));
};

/* harmony default export */ var Gallery_AddGallery = (AddGallery);
// EXTERNAL MODULE: external "react-bootstrap"
var external_react_bootstrap_ = __webpack_require__("IZS3");

// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__("YLtl");

// CONCATENATED MODULE: ./panelAdmin/component/UI/Modals/MyVerticallyCenteredModal/index.js
var MyVerticallyCenteredModal_jsx = external_react_default.a.createElement;



const MyVerticallyCenteredModal = props => {
  const {
    status,
    setStatus,
    onChange,
    title,
    onSubmit,
    acceptedBtn,
    cancelledBtn,
    size,
    acceptedDisabled
  } = props;

  const submitModal = e => {
    setStatus(false);
    onSubmit(e);
  };

  return MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Modal"] // {...props}
  , {
    size: size,
    style: {
      direction: "rtl"
    },
    "aria-labelledby": "contained-modal-title-vcenter",
    centered: true,
    show: status,
    onHide: submitModal,
    animation: true
  }, MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Modal"].Header, {
    closeButton: true
  }, MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Modal"].Title, null, title)), MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Modal"].Body, null, props.children), MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Modal"].Footer, null, cancelledBtn ? MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Button"], {
    variant: "secondary",
    onClick: () => setStatus(false)
  }, cancelledBtn) : "", acceptedBtn ? MyVerticallyCenteredModal_jsx(external_react_bootstrap_["Button"], {
    disabled: acceptedDisabled,
    variant: "primary",
    onClick: submitModal
  }, acceptedBtn) : ""));
};

/* harmony default export */ var Modals_MyVerticallyCenteredModal = (MyVerticallyCenteredModal);
// EXTERNAL MODULE: ./panelAdmin/component/cards/ShowCardInformation/index.js + 2 modules
var ShowCardInformation = __webpack_require__("vM4n");

// EXTERNAL MODULE: ./globalUtils/index.js + 2 modules
var globalUtils = __webpack_require__("gIEs");

// EXTERNAL MODULE: ./panelAdmin/component/UI/PaginationM/index.js
var PaginationM = __webpack_require__("1WZq");

// EXTERNAL MODULE: ./lib/useApiRequest.js
var useApiRequest = __webpack_require__("Go1v");

// EXTERNAL MODULE: ./panelAdmin/utils/index.js + 62 modules
var utils = __webpack_require__("xb3y");

// EXTERNAL MODULE: ./panelAdmin/utils/updateObject.js
var updateObject = __webpack_require__("ge5p");

// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/Submitted/index.js



const Submitted = async props => {
  const {
    setSubmitLoading,
    data,
    editData,
    put,
    post,
    setData,
    states,
    setEdit,
    propsHideModal,
    setCheckSubmitted,
    checkSubmitted
  } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};

  for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;

  const initialStatePhone = Object(updateObject["a" /* default */])(states.addOwner.Form["phone"], {
    value: []
  });
  const initialStateCoordinate = Object(updateObject["a" /* default */])(states.addOwner.Form["coordinate"], {
    value: {
      lat: "",
      lng: ""
    }
  });
  const updatedForm = Object(updateObject["a" /* default */])(states.addOwner.Form, {
    ["coordinate"]: initialStateCoordinate,
    ["phone"]: initialStatePhone
  });

  if (editData) {
    if (await put.owner({
      id: editData._id,
      data: formData
    })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post.owner(formData)) setData({
    Form: updatedForm,
    formIsValid: false
  });

  setSubmitLoading(false);
};

/* harmony default export */ var DependentComponent_Submitted = (Submitted);
// EXTERNAL MODULE: ./panelAdmin/component/UI/Modals/ModalStructure/index.js
var ModalStructure = __webpack_require__("cVJP");

// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/ModalOption/index.js
var ModalOption_jsx = external_react_default.a.createElement;




const ModalOption = props => {
  const {
    inputChangedHandler,
    modalRequest,
    onHideModal,
    modalDetails,
    imageAccepted,
    acceptedImageFinal,
    acceptedImage
  } = props;
  return ModalOption_jsx(ModalStructure["a" /* default */], {
    modalRequest: modalRequest,
    reqApiRemove: null,
    onHideModal: onHideModal,
    modalDetails: modalDetails
  }, modalDetails.kindOf === "showGallery" && ModalOption_jsx(external_react_["Fragment"], null, ModalOption_jsx(panelAdmin_gallery, {
    clickedParent: value => inputChangedHandler({
      name: modalDetails.name,
      value
    }),
    acceptedCardInfo: {
      handelAcceptedImage: acceptedImage,
      acceptedCard: imageAccepted
    }
  }), ModalOption_jsx("div", {
    className: "btns-container"
  }, ModalOption_jsx("button", {
    disabled: !imageAccepted,
    className: "btns btns-primary",
    onClick: e => acceptedImageFinal(e)
  }, "\u062A\u0627\u06CC\u06CC\u062F", " "), ModalOption_jsx("button", {
    className: "btns btns-warning",
    onClick: onHideModal
  }, "\u0628\u0633\u062A\u0646", " "))));
};

/* harmony default export */ var DependentComponent_ModalOption = (ModalOption);
// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/EditData/index.js


const EditData = props => {
  const {
    editData,
    stateArray,
    inputChangedHandler,
    setStaticTitle
  } = props;
  let arrayData = [];
  if (editData) for (const key in editData) for (let index = 0; index < stateArray.length; index++) {
    if (stateArray[index].id === key) if (key === "category") arrayData.push({
      name: key,
      value: editData[key] ? editData[key]._id : ""
    });else if (key === "owner") {
      if (editData[key]) setStaticTitle({
        value: editData[key] ? editData[key].title : "",
        name: key
      });
      arrayData.push({
        name: key,
        value: editData[key] ? editData[key]._id : ""
      });
    } else arrayData.push({
      name: key,
      value: editData[key] ? editData[key] : editData[key] ? editData[key] : ""
    });
  }
  if (arrayData.length > 0) inputChangedHandler(arrayData, false, {
    value: editData.parentType
  });
};

/* harmony default export */ var DependentComponent_EditData = (EditData);
// EXTERNAL MODULE: external "swr"
var external_swr_ = __webpack_require__("aYjl");

// CONCATENATED MODULE: ./panelAdmin/screen/Gallery/GalleryScreen/index.js
var GalleryScreen_jsx = external_react_default.a.createElement;

function GalleryScreen_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function GalleryScreen_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { GalleryScreen_ownKeys(Object(source), true).forEach(function (key) { GalleryScreen_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { GalleryScreen_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function GalleryScreen_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











 // const axios = globalUtils.axiosBase;







const GalleryScreen = props => {
  const states = panelAdmin["a" /* default */].utils.consts.states;
  const card = panelAdmin["a" /* default */].utils.consts.card;
  const {
    apiPageFetch,
    filters,
    acceptedCardInfo,
    requestData,
    loadingApi
  } = props;
  const {
    0: showUploadModal,
    1: setUploadModal
  } = Object(external_react_["useState"])(false);
  const {
    0: data,
    1: setData
  } = Object(external_react_["useState"])(GalleryScreen_objectSpread({}, states.addGallery));
  const {
    0: modalDetails,
    1: setModalDetails
  } = Object(external_react_["useState"])({
    show: false,
    kindOf: null,
    data: {
      src: false,
      type: false,
      name: false
    },
    name: null,
    removeId: null,
    editId: null,
    editData: []
  });
  const {
    0: removeLoading,
    1: setRemoveLoading
  } = Object(external_react_["useState"])(false); // const CurrentPage = info?.page || "1";

  const strings = panelAdmin["a" /* default */].values.apiString; // const URL = strings.IMAGE + "/" + CurrentPage;
  // ========================================================= remove data with data id

  const reqApiRemove = async id => {
    setRemoveLoading(true);

    if (await panelAdmin["a" /* default */].api.deletes.gallery(id)) {
      apiPageFetch(requestData.page);
      onHideModal();
    }

    setRemoveLoading(false);
  }; // ========================================================= modal


  const _onSubmit = async e => {
    const formData = {};

    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value; // trigger(URL);


    apiPageFetch();
    setData(GalleryScreen_objectSpread({}, states.addGallery));
  }; // ================================== modal close


  const onHideModal = () => {
    setModalDetails(GalleryScreen_objectSpread({}, modalDetails, {
      show: false,
      kindOf: false,
      removeId: null
    }));
  }; // ================================== modal open


  const onShowModal = event => {
    setModalDetails(GalleryScreen_objectSpread({}, modalDetails, {
      show: true,
      kindOf: event.kindOf,
      name: event === null || event === void 0 ? void 0 : event.name,
      editData: event === null || event === void 0 ? void 0 : event.editData,
      removeId: event === null || event === void 0 ? void 0 : event.removeId
    }));
  }; // ================================== handel modal end work


  const modalRequest = async bool => {
    if (bool) reqApiRemove(modalDetails.removeId);else onHideModal();
  }; // ========================================================= END modal


  const modalData = {
    size: "lg",
    status: showUploadModal,
    setStatus: setUploadModal,
    title: "آپلود عکس",
    acceptedBtn: data.formIsValid ? "ثبت" : "اطلاعات کامل نمی باشد",
    acceptedDisabled: !data.formIsValid,
    onSubmit: _onSubmit
  };

  const optionClick = ({
    _id,
    mission,
    index
  }) => {
    //console.log({ _id, mission });
    switch (mission) {
      case "remove":
        onShowModal({
          kindOf: "question",
          removeId: _id
        });
        break;

      case "edit":
        onShowModal({
          kindOf: "component",
          editData: requestData.docs[index]
        });
        break;

      default:
        break;
    }
  }; // ========================================  CARD ELEMENT


  const showDataElement = GalleryScreen_jsx(ShowCardInformation["a" /* default */], {
    optionClick: optionClick,
    options: {
      remove: true
    },
    data: card.gallery(requestData === null || requestData === void 0 ? void 0 : requestData.docs, acceptedCardInfo === null || acceptedCardInfo === void 0 ? void 0 : acceptedCardInfo.acceptedCard),
    onClick: null,
    acceptedCardInfo: acceptedCardInfo
  });

  return GalleryScreen_jsx("div", {
    className: "gallery"
  }, GalleryScreen_jsx(ModalStructure["a" /* default */], {
    modalRequest: modalRequest,
    reqApiRemove: reqApiRemove,
    onHideModal: onHideModal,
    modalDetails: modalDetails,
    loading: removeLoading
  }), GalleryScreen_jsx(Modals_MyVerticallyCenteredModal, modalData, GalleryScreen_jsx(Gallery_AddGallery, {
    data: data,
    setData: setData
  })), GalleryScreen_jsx("div", {
    className: "gallery-header-wrapper"
  }, GalleryScreen_jsx("div", {
    className: "gallery-header"
  }, GalleryScreen_jsx(external_react_bootstrap_["Button"], {
    onClick: () => setUploadModal(true),
    className: ""
  }, "افزودن عکس")), showDataElement, GalleryScreen_jsx(PaginationM["a" /* default */], {
    limited: "3",
    pages: requestData === null || requestData === void 0 ? void 0 : requestData.pages,
    activePage: requestData === null || requestData === void 0 ? void 0 : requestData.page,
    onClick: apiPageFetch
  })));
};

/* harmony default export */ var Gallery_GalleryScreen = (GalleryScreen);
// EXTERNAL MODULE: ./_context/reducer/index.js + 4 modules
var reducer = __webpack_require__("uqW6");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Loadings/SpinnerRotate/index.js
var SpinnerRotate = __webpack_require__("1Awo");

// CONCATENATED MODULE: ./pages/panelAdmin/gallery.js
var gallery_jsx = external_react_default.a.createElement;







 // const axios = globalUtils.axiosBase;
// const fetcher = (url) => axios(url).then((r) => r.json());

const gallery = props => {
  const strings = panelAdmin["a" /* default */].values.apiString;
  const Context = reducer["a" /* default */].panelAdminReducer.optionReducerContext;
  const giveContextData = Object(external_react_["useContext"])(Context);
  const {
    dispatch
  } = giveContextData;
  const {
    acceptedCardInfo,
    parentTrue,
    isServer
  } = props;
  const {
    0: loadingApi,
    1: setLoadingApi
  } = Object(external_react_["useState"])(true);
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])(false);
  const CurrentPage = (state === null || state === void 0 ? void 0 : state.page) || "1"; // //console.log({ resData });
  // ======================================================== SWR
  // const { data } = useSWR(strings.IMAGE, fetcher, { initialData: resData });
  // const { data: resData } = useApiRequest(strings.IMAGE + "/" + CurrentPage, { initialData: state }, { refreshInterval: 0 });

  Object(external_react_["useEffect"])(() => {
    !acceptedCardInfo && dispatch.changePageName("گالری");
    apiPageFetch("1");
  }, []); // //console.log({ resData });

  const apiPageFetch = async (page = CurrentPage) => {
    if (!page) return;
    setLoadingApi(true);
    const res = await panelAdmin["a" /* default */].api.get.gallery({
      page
    }); // //console.log({ res });

    setState(res === null || res === void 0 ? void 0 : res.data);
    setLoadingApi(false);
  };

  return gallery_jsx(external_react_["Fragment"], null, gallery_jsx(Gallery_GalleryScreen, {
    requestData: state,
    acceptedCardInfo: acceptedCardInfo,
    apiPageFetch: apiPageFetch
  }), loadingApi ? gallery_jsx("div", {
    className: "staticStyle bgWhite"
  }, gallery_jsx(SpinnerRotate["a" /* default */], null)) : ""); // return true;
};

gallery.panelAdminLayout = true; // ========================================= getInitialProps
// gallery.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.gallery({ page: "1" });
//   const resData = res.data;
//   return { resData, isServer };
// };

/* harmony default export */ var panelAdmin_gallery = __webpack_exports__["default"] = (gallery);

/***/ }),

/***/ "bFXO":
/***/ (function(module) {

module.exports = JSON.parse("{\"headers\":[\"ردیف\",\"عکس\",\"عنوان\"],\"body\":[\"image\",\"name\"]}");

/***/ }),

/***/ "bzos":
/***/ (function(module, exports) {

module.exports = require("url");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "cDf5":
/***/ (function(module, exports) {

function _typeof2(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "cTJO":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("TqRt");

var _interopRequireWildcard = __webpack_require__("284h");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__("cDcd"));

var _url = __webpack_require__("bzos");

var _utils = __webpack_require__("kYf9");

var _router = _interopRequireDefault(__webpack_require__("nOHt"));

var _router2 = __webpack_require__("elyg");

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new Map();
var IntersectionObserver = false ? undefined : null;
var prefetched = {};

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: (0, _router2.addBasePath)(formatUrl(href)),
        as: asHref ? (0, _router2.addBasePath)(formatUrl(asHref)) : asHref
      };
    });

    this.linkClicked = e => {
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (false) {}

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  getPaths() {
    var {
      pathname
    } = window.location;
    var {
      href: parsedHref,
      as: parsedAs
    } = this.formatUrls(this.props.href, this.props.as);
    var resolvedHref = (0, _url.resolve)(pathname, parsedHref);
    return [resolvedHref, parsedAs ? (0, _url.resolve)(pathname, parsedAs) : resolvedHref];
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      var isPrefetched = prefetched[this.getPaths().join( // Join on an invalid URI character
      '%')];

      if (!isPrefetched) {
        this.cleanUpListeners = listenToIntersections(ref, () => {
          this.prefetch();
        });
      }
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch(options) {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var paths = this.getPaths(); // We need to handle a prefetch error here since we may be
    // loading with priority which can reject but we don't
    // want to force navigation since this is only a prefetch

    _router.default.prefetch(paths[
    /* href */
    0], paths[
    /* asPath */
    1], options).catch(err => {
      if (false) {}
    });

    prefetched[paths.join( // Join on an invalid URI character
    '%')] = true;
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch({
          priority: true
        });
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) { var rewriteUrlForNextExport; }

    return _react.default.cloneElement(child, props);
  }

}

if (false) { var exact, PropTypes, warn; }

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "cVJP":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ModalBox__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("Ol4B");
/* harmony import */ var _ModalTrueFalse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("VnI6");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const ModalStructure = props => {
  const {
    modalDetails,
    onHideModal,
    reqApiRemove,
    modalRequest,
    loading
  } = props; // alert("modal");
  // useEffect(() => {
  //   if (modalDetails.kindOf === "question") ModalTrueFalse({ modalHeadline: "آیا مطمئن به حذف آن هستید !", modalAcceptTitle: "بله", modalCanselTitle: "خیر", modalAccept: modalRequest, loading: loading });
  // }, [modalDetails.kindOf]);

  return __jsx("div", {
    style: {
      position: "absolute"
    },
    className: modalDetails.kindOf === "component" ? "width80" : ""
  }, __jsx(_ModalBox__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"], {
    onHideModal: onHideModal,
    showModal: modalDetails.show
  }, modalDetails.kindOf === "question" && __jsx(_ModalTrueFalse__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"], {
    modalHeadline: "آیا مطمئن به حذف آن هستید !",
    modalAcceptTitle: "بله",
    modalCanselTitle: "خیر",
    modalAccept: modalRequest,
    loading: loading
  }), props.children));
};

/* harmony default export */ __webpack_exports__["a"] = (ModalStructure);

/***/ }),

/***/ "dM+x":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const LoadingDot1 = ({
  width,
  height
}) => {
  return __jsx("div", {
    className: "sk-chase",
    style: {
      width,
      height
    }
  }, __jsx("div", {
    className: "sk-chase-dot"
  }), __jsx("div", {
    className: "sk-chase-dot"
  }), __jsx("div", {
    className: "sk-chase-dot"
  }), __jsx("div", {
    className: "sk-chase-dot"
  }), __jsx("div", {
    className: "sk-chase-dot"
  }), __jsx("div", {
    className: "sk-chase-dot"
  }));
};

/* harmony default export */ __webpack_exports__["a"] = (LoadingDot1);

/***/ }),

/***/ "dZ6Y":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

Object.defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = Object.create(null);
  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "elyg":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");

const mitt_1 = __importDefault(__webpack_require__("dZ6Y"));

const utils_1 = __webpack_require__("g/15");

const is_dynamic_1 = __webpack_require__("/jkW");

const route_matcher_1 = __webpack_require__("gguc");

const route_regex_1 = __webpack_require__("YTqd");

const basePath =  false || '';

function addBasePath(path) {
  return path.indexOf(basePath) !== 0 ? basePath + path : path;
}

exports.addBasePath = addBasePath;

function delBasePath(path) {
  return path.indexOf(basePath) === 0 ? path.substr(basePath.length) || '/' : path;
}

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

const prepareRoute = path => toRoute(!path || path === '/' ? '/index' : path);

function fetchNextData(pathname, query, isServerRender, cb) {
  let attempts = isServerRender ? 3 : 1;

  function getResponse() {
    return fetch(utils_1.formatWithValidation({
      // @ts-ignore __NEXT_DATA__
      pathname: `/_next/data/${__NEXT_DATA__.buildId}${pathname}.json`,
      query
    }), {
      // Cookies are required to be present for Next.js' SSG "Preview Mode".
      // Cookies may also be required for `getServerSideProps`.
      //
      // > `fetch` won’t send cookies, unless you set the credentials init
      // > option.
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      //
      // > For maximum browser compatibility when it comes to sending &
      // > receiving cookies, always supply the `credentials: 'same-origin'`
      // > option instead of relying on the default.
      // https://github.com/github/fetch#caveats
      credentials: 'same-origin'
    }).then(res => {
      if (!res.ok) {
        if (--attempts > 0 && res.status >= 500) {
          return getResponse();
        }

        throw new Error(`Failed to load static props`);
      }

      return res.json();
    });
  }

  return getResponse().then(data => {
    return cb ? cb(data) : data;
  }).catch(err => {
    // We should only trigger a server-side transition if this was caused
    // on a client-side transition. Otherwise, we'd get into an infinite
    // loop.
    if (!isServerRender) {
      ;
      err.code = 'PAGE_LOAD_ERROR';
    }

    throw err;
  });
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription,
    isFallback
  }) {
    // Static Data Cache
    this.sdc = {};

    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state && this.isSsr && e.state.as === this.asPath && url_1.parse(e.state.url).pathname === this.pathname) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (false) {}

      this.replace(url, as, options);
    };

    this._getStaticData = asPath => {
      const pathname = prepareRoute(url_1.parse(asPath).pathname);
      return  true && this.sdc[pathname] ? Promise.resolve(this.sdc[pathname]) : fetchNextData(pathname, null, this.isSsr, data => this.sdc[pathname] = data);
    };

    this._getServerData = asPath => {
      let {
        pathname,
        query
      } = url_1.parse(asPath, true);
      pathname = prepareRoute(pathname);
      return fetchNextData(pathname, query, this.isSsr);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err,
        __N_SSG: initialProps && initialProps.__N_SSG,
        __N_SSP: initialProps && initialProps.__N_SSP
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.autoExport ? pathname : as;
    this.basePath = basePath;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;
    this.isFallback = isFallback;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    if (false) {} else {
      return url;
    }
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = Object.assign(Object.assign({}, data), {
      Component,
      __N_SSG: mod.__N_SSG,
      __N_SSP: mod.__N_SSP
    });
    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new Promise((resolve, reject) => {
      if (!options._h) {
        this.isSsr = false;
      } // marking route changes as a navigation start entry


      if (utils_1.ST) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      let url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as;
      url = addBasePath(url);
      as = addBasePath(as); // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as, options);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (false) {}

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      }

      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const routeRegex = route_regex_1.getRouteRegex(route);
        const routeMatch = route_matcher_1.getRouteMatcher(routeRegex)(asPathname);

        if (!routeMatch) {
          const missingParams = Object.keys(routeRegex.groups).filter(param => !query[param]);

          if (missingParams.length > 0) {
            if (false) {}

            return reject(new Error(`The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). ` + `Read more: https://err.sh/zeit/next.js/incompatible-href-as`));
          }
        } else {
          // Merge params into `query`, overwriting any specified in search
          Object.assign(query, routeMatch);
        }
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);

        if (false) {}

        this.set(route, pathname, query, as, routeInfo);

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (false) {}

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      window.history[method]({
        url,
        as,
        options
      }, // Most browsers currently ignores this parameter, although they may use it in the future.
      // Passing the empty string here should be safe against future changes to the method.
      // https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
      '', as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return Promise.resolve(cachedRouteInfo);
    }

    const handleError = (err, loadErrorFail) => {
      return new Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR' || loadErrorFail) {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(res => {
          const {
            page: Component
          } = res;
          const routeInfo = {
            Component,
            err
          };
          return new Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }).catch(err => handleError(err, true)));
      });
    };

    return new Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(res => resolve({
        Component: res.page,
        __N_SSG: res.mod.__N_SSG,
        __N_SSP: res.mod.__N_SSP
      }), reject);
    }).then(routeInfo => {
      const {
        Component,
        __N_SSG,
        __N_SSP
      } = routeInfo;

      if (false) {}

      return this._getData(() => __N_SSG ? this._getStaticData(as) : __N_SSP ? this._getServerData(as) : this.getInitialProps(Component, // we provide AppTree later so this needs to be `any`
      {
        pathname,
        query,
        asPath: as
      })).then(props => {
        routeInfo.props = props;
        this.components[route] = routeInfo;
        return routeInfo;
      });
    }).catch(handleError);
  }

  set(route, pathname, query, as, data) {
    this.isFallback = false;
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch page code, you may wait for the data during page rendering.
   * This feature only works in production!
   * @param url the href of prefetched page
   * @param asPath the as path of the prefetched page
   */


  prefetch(url, asPath = url, options = {}) {
    return new Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (false) {}

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (false) {}

      const route = delBasePath(toRoute(pathname));
      Promise.all([this.pageLoader.prefetchData(url, delBasePath(asPath)), this.pageLoader[options.priority ? 'loadPage' : 'prefetch'](route)]).then(() => resolve(), reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    route = delBasePath(route);
    const componentResult = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return componentResult;
  }

  _getData(fn) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    return fn().then(data => {
      if (cancel === this.clc) {
        this.clc = null;
      }

      if (cancelled) {
        const err = new Error('Loading initial props cancelled');
        err.cancelled = true;
        throw err;
      }

      return data;
    });
  }

  getInitialProps(Component, ctx) {
    const {
      Component: App
    } = this.components['/_app'];

    const AppTree = this._wrapApp(App);

    ctx.AppTree = AppTree;
    return utils_1.loadGetInitialProps(App, {
      AppTree,
      Component,
      router: this,
      ctx
    });
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

exports.default = Router;
Router.events = mitt_1.default();

/***/ }),

/***/ "faye":
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),

/***/ "g+59":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./panelAdmin/api/index.js + 39 modules
var api = __webpack_require__("FRaV");

// EXTERNAL MODULE: ./panelAdmin/index.js + 16 modules
var panelAdmin = __webpack_require__("VGcP");

// EXTERNAL MODULE: ./pages/panelAdmin/gallery.js + 7 modules
var gallery = __webpack_require__("b51O");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Inputs/Input.js + 14 modules
var Input = __webpack_require__("xYbV");

// EXTERNAL MODULE: external "next/dynamic"
var dynamic_ = __webpack_require__("/T1H");
var dynamic_default = /*#__PURE__*/__webpack_require__.n(dynamic_);

// CONCATENATED MODULE: ./panelAdmin/screen/Owner/AddOwner/FormMap/index.js
var __jsx = external_react_default.a.createElement;


const SimpleExample = dynamic_default()(() => __webpack_require__.e(/* import() */ 0).then(__webpack_require__.bind(null, "9K3d")), {
  ssr: false,
  loadableGenerated: {
    webpack: () => [/*require.resolve*/("9K3d")],
    modules: ["../../../../component/SimpleExample"]
  }
});
const MapWithNoSSR = dynamic_default()(() => __webpack_require__.e(/* import() */ 28).then(__webpack_require__.bind(null, "V84h")), {
  ssr: false,
  loadableGenerated: {
    webpack: () => [/*require.resolve*/("V84h")],
    modules: ["./map"]
  }
});

const FormMap = props => {
  const {
    formElement,
    inputChangedHandler,
    inputClasses,
    openMapModal,
    uploadWeb,
    mapPinData
  } = props;
  const {
    0: map,
    1: setmap
  } = Object(external_react_["useState"])(false);
  const {
    0: display,
    1: setDisplay
  } = Object(external_react_["useState"])(true);

  const showMap = () => {
    setmap(!map);
  };

  Object(external_react_["useEffect"])(() => {
    if (map) setDisplay(false);
  }, [map]);

  const endAnimation = () => {// (!map ? setDisplay(true) : setDisplay(false))
  }; //console.log({ formElement });


  let form;
  form = __jsx("div", {
    className: "Input",
    style: {
      display: "flex",
      flexDirection: "column"
    }
  }, __jsx("div", {
    className: "title-wrapper"
  }, __jsx("label", {
    className: "Label"
  }, "مختصات"), " "), __jsx("div", null, __jsx("div", {
    className: "coordinate-wrapper"
  }, __jsx("div", null, __jsx("span", null, "طول (lng): "), __jsx("div", {
    disabled: true,
    className: inputClasses.join(" ") // value={formElement.config.value.lng} name={"lng"} onChange={(e) => inputChangedHandler({ name: formElement.id, value: e.currentTarget.value, child: e.currentTarget.name })} type={"number"}

  }, __jsx("span", null, formElement.config.value.lng || " 49.585606455802925"))), __jsx("div", null, __jsx("span", null, "عرض (lat) : "), __jsx("div", {
    disabled: true,
    className: inputClasses.join(" ") //  value={formElement.config.value.lat} name={"lat"} onChange={(e) => inputChangedHandler({ name: formElement.id, value: e.currentTarget.value, child: e.currentTarget.name })} type={"number"}

  }, __jsx("span", null, " ", formElement.config.value.lat || "37.281369047367555")))), __jsx("div", null, __jsx(SimpleExample, {
    newPin: true,
    center: formElement.config.value.lat ? formElement.config.value : {
      lat: "37.281369047367555",
      lng: "49.585606455802925"
    },
    onChange: value => inputChangedHandler({
      name: formElement.id,
      value: value
    }),
    data: mapPinData
  }))));
  return form;
};

/* harmony default export */ var AddOwner_FormMap = (FormMap);
// CONCATENATED MODULE: ./panelAdmin/screen/Owner/AddOwner/FormInputOwner/index.js
var FormInputOwner_jsx = external_react_default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




 // import FormMap from "../FormMap";

const FormInputOwner = props => {
  const {
    mapPinData,
    stateArray,
    removeHandel,
    state,
    _onSubmited,
    inputChangedHandler,
    showModal,
    checkSubmitted
  } = props;
  const {
    0: uploadWeb,
    1: setUploadWeb
  } = Object(external_react_["useState"])(false);
  Object(external_react_["useEffect"])(() => {
    setUploadWeb(true);
  }, []); //   =================== onChange Inputs

  return FormInputOwner_jsx("form", null, stateArray.map(formElement => {
    const invalid = !formElement.config.valid;
    const shouldValidate = formElement.config.validation;
    const touched = formElement.config.touched;
    let progress;
    let value = formElement.config.value; // disabled = false;

    const inputClasses = ["InputElement"];
    if (invalid && shouldValidate && touched) inputClasses.push("Invalid"); // if (formElement.id === "thumbnail") {
    //   value = formElement.config.value ? formElement.config.value : "";
    //   disabled = true;
    //   accepted = () => showModal({ kindOf: "showGallery", name: formElement.id });
    // } else if (formElement.id === "coordinate") {
    //   changed = (e, child) => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, child });
    // } else {
    //   changed = (e) =>
    //     inputChangedHandler({
    //       value: e.currentTarget.value,
    //       name: formElement.id,
    //       type: e.currentTarget.type,
    //       files: e.currentTarget.files,
    //     });
    //   accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });
    // }
    // let form = (
    //   <Inputs
    //     key={formElement.id}
    //     elementType={formElement.config.elementType}
    //     elementConfig={formElement.config.elementConfig}
    //     value={value}
    //     invalid={invalid}
    //     shouldValidate={shouldValidate}
    //     touched={touched}
    //     changed={changed}
    //     accepted={accepted}
    //     removeHandel={(index) => removeHandel(index, formElement.id)}
    //     label={formElement.config.label}
    //     progress={progress}
    //     checkSubmitted={checkSubmitted}
    //     disabled={disabled}
    //   />
    // );

    let FormManagement = panelAdmin["a" /* default */].utils.operation.FormManagement({
      formElement,
      showModal,
      inputChangedHandler,
      removeHandel
    });

    let form = FormInputOwner_jsx(Input["a" /* default */], _extends({
      invalid: invalid,
      shouldValidate: shouldValidate,
      touched: touched,
      progress: progress,
      checkSubmitted: checkSubmitted
    }, FormManagement)); // if (formElement.id === "coordinate") {
    //   if (uploadWeb) form = <FormMap mapPinData={mapPinData} uploadWeb={uploadWeb} formElement={formElement} inputChangedHandler={inputChangedHandler} inputClasses={inputClasses} />;
    // }


    return form;
  }));
};

/* harmony default export */ var AddOwner_FormInputOwner = (FormInputOwner);
// EXTERNAL MODULE: ./panelAdmin/component/UI/Loadings/LoadingDot1/index.js
var LoadingDot1 = __webpack_require__("dM+x");

// EXTERNAL MODULE: ./panelAdmin/component/UI/Modals/ModalStructure/index.js
var ModalStructure = __webpack_require__("cVJP");

// CONCATENATED MODULE: ./panelAdmin/screen/Owner/AddOwner/DependentComponent/ModalOption/index.js
var ModalOption_jsx = external_react_default.a.createElement;




const ModalOption = props => {
  const {
    inputChangedHandler,
    modalRequest,
    onHideModal,
    modalDetails,
    imageAccepted,
    acceptedImageFinal,
    acceptedImage
  } = props;
  return ModalOption_jsx(ModalStructure["a" /* default */], {
    modalRequest: modalRequest,
    reqApiRemove: null,
    onHideModal: onHideModal,
    modalDetails: modalDetails
  }, modalDetails.kindOf === "showGallery" && ModalOption_jsx(external_react_["Fragment"], null, ModalOption_jsx(gallery["default"], {
    clickedParent: value => inputChangedHandler({
      name: modalDetails.name,
      value
    }),
    acceptedCardInfo: {
      handelAcceptedImage: acceptedImage,
      acceptedCard: imageAccepted
    }
  }), ModalOption_jsx("div", {
    className: "btns-container"
  }, ModalOption_jsx("button", {
    disabled: !imageAccepted,
    className: "btns btns-primary",
    onClick: e => acceptedImageFinal(e)
  }, "\u062A\u0627\u06CC\u06CC\u062F", " "), ModalOption_jsx("button", {
    className: "btns btns-warning",
    onClick: onHideModal
  }, "\u0628\u0633\u062A\u0646", " "))));
};

/* harmony default export */ var DependentComponent_ModalOption = (ModalOption);
// EXTERNAL MODULE: ./panelAdmin/utils/updateObject.js
var updateObject = __webpack_require__("ge5p");

// CONCATENATED MODULE: ./panelAdmin/screen/Owner/AddOwner/DependentComponent/Submitted/index.js



const Submitted = async props => {
  const {
    setSubmitLoading,
    data,
    editData,
    put,
    post,
    setData,
    states,
    setEdit,
    propsHideModal,
    setCheckSubmitted,
    checkSubmitted
  } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};

  for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;

  const initialStatePhone = Object(updateObject["a" /* default */])(states.addOwner.Form["phone"], {
    value: []
  });
  const initialStateCoordinate = Object(updateObject["a" /* default */])(states.addOwner.Form["coordinate"], {
    value: {
      lat: "",
      lng: ""
    }
  });
  const updatedForm = Object(updateObject["a" /* default */])(states.addOwner.Form, {
    ["coordinate"]: initialStateCoordinate,
    ["phone"]: initialStatePhone
  });

  if (editData) {
    if (await put.owner({
      id: editData._id,
      data: formData
    })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post.owner(formData)) setData({
    Form: updatedForm,
    formIsValid: false
  });

  setSubmitLoading(false);
};

/* harmony default export */ var DependentComponent_Submitted = (Submitted);
// CONCATENATED MODULE: ./panelAdmin/screen/Owner/AddOwner/DependentComponent/EditData/index.js


const EditData = props => {
  const {
    editData,
    stateArray,
    inputChangedHandler,
    setStaticTitle
  } = props;
  let arrayData = [];
  if (editData) for (const key in editData) for (let index = 0; index < stateArray.length; index++) {
    if (stateArray[index].id === key) if (key === "category") arrayData.push({
      name: key,
      value: editData[key] ? editData[key]._id : ""
    });else if (key === "owner") {
      if (editData[key]) setStaticTitle({
        value: editData[key] ? editData[key].title : "",
        name: key
      });
      arrayData.push({
        name: key,
        value: editData[key] ? editData[key]._id : ""
      });
    } else arrayData.push({
      name: key,
      value: editData[key] ? editData[key] : editData[key] ? editData[key] : ""
    });
  }
  if (arrayData.length > 0) inputChangedHandler(arrayData, false, {
    value: editData.parentType
  });
};

/* harmony default export */ var DependentComponent_EditData = (EditData);
// CONCATENATED MODULE: ./panelAdmin/screen/Owner/AddOwner/index.js
var AddOwner_jsx = external_react_default.a.createElement;

function AddOwner_extends() { AddOwner_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return AddOwner_extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











const AddOwner = props => {
  const {
    editData,
    setEdit,
    propsHideModal
  } = props;
  const states = panelAdmin["a" /* default */].utils.consts.states;
  const onChanges = panelAdmin["a" /* default */].utils.onChanges;
  const {
    0: data,
    1: setData
  } = Object(external_react_["useState"])(_objectSpread({}, states.addOwner));
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    progressPercentImage: null,
    remove: {
      value: "",
      name: ""
    }
  });
  const {
    0: Loading,
    1: setLoading
  } = Object(external_react_["useState"])(false);
  const {
    0: submitLoading,
    1: setSubmitLoading
  } = Object(external_react_["useState"])(false);
  const {
    0: modalDetails,
    1: setModalDetails
  } = Object(external_react_["useState"])({
    show: false,
    kindOf: null,
    data: {
      src: false,
      type: false,
      name: false
    },
    name: null,
    removeId: null,
    editId: null,
    editData: []
  });
  const {
    0: imageAccepted,
    1: setImageAccepted
  } = Object(external_react_["useState"])();
  const {
    0: checkSubmitted,
    1: setCheckSubmitted
  } = Object(external_react_["useState"])(false);
  const {
    0: staticTitle,
    1: setStaticTitle
  } = Object(external_react_["useState"])(false);
  console.log({
    editData
  }); // ========================================================= change edit data structure for state

  Object(external_react_["useEffect"])(() => {
    DependentComponent_EditData({
      editData,
      stateArray,
      inputChangedHandler,
      setStaticTitle
    });
  }, [editData]); // ========================================================= modal
  // ================================== modal close

  const onHideModal = () => {
    setModalDetails(_objectSpread({}, modalDetails, {
      show: false,
      kindOf: false,
      removeId: null
    }));
  }; // ================================== modal open


  const onShowModal = event => {
    setModalDetails(_objectSpread({}, modalDetails, {
      show: true,
      kindOf: event.kindOf,
      name: event === null || event === void 0 ? void 0 : event.name,
      editData: event === null || event === void 0 ? void 0 : event.editData,
      removeId: event === null || event === void 0 ? void 0 : event.removeId
    }));
  }; // ================================== handel modal end work


  const modalRequest = async bool => {
    if (bool) inputChangedHandler({
      name: state.remove.name,
      value: state.remove.value
    });
    onHideModal();
  }; // ========================================================= END modal


  const removeHandel = (value, name) => {
    onShowModal({
      kindOf: "question"
    });
    setState(_objectSpread({}, state, {
      remove: {
        value,
        name
      }
    }));
  }; // ========================================================= accepted image for add data


  const acceptedImage = ({
    index,
    data
  }) => {
    let images = data.image.value;
    imageAccepted === images ? setImageAccepted(null) : setImageAccepted(images);
  };

  const acceptedImageFinal = () => {
    onHideModal();
    inputChangedHandler({
      name: "thumbnail",
      value: imageAccepted
    });
  }; // ========================================================= End accepted image for add data
  // ============================= submited


  const _onSubmitted = async e => panelAdmin["a" /* default */].utils.operation.submitted({
    setSubmitLoading,
    data,
    editData,
    put: api["put"].owner,
    post: api["post"].owner,
    setData,
    states: states.addOwner,
    setEdit,
    propsHideModal,
    setCheckSubmitted,
    checkSubmitted
  });

  const checkValid = () => {
    panelAdmin["a" /* default */].utils.operation.formTouchChange({
      data,
      setData
    });
  }; // ========================= End submited =================


  const inputChangedHandler = async event => await onChanges.globalChange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType: "flag"
  });

  const stateArray = [];

  for (let key in data === null || data === void 0 ? void 0 : data.Form) stateArray.push({
    id: key,
    config: data.Form[key]
  });

  let form = AddOwner_jsx(AddOwner_FormInputOwner, AddOwner_extends({
    removeHandel,
    stateArray,
    data,
    state,
    setData,
    Loading,
    setLoading,
    inputChangedHandler,
    checkSubmitted,
    staticTitle
  }, {
    showModal: onShowModal
  }));

  return AddOwner_jsx("div", {
    className: "countainer-main  "
  }, AddOwner_jsx(DependentComponent_ModalOption, {
    inputChangedHandler,
    modalRequest,
    onHideModal,
    modalDetails,
    imageAccepted,
    acceptedImageFinal,
    acceptedImage,
    Gallery: gallery["default"]
  }), AddOwner_jsx("div", {
    className: "form-countainer"
  }, AddOwner_jsx("div", {
    className: "row-give-information"
  }, form, AddOwner_jsx("div", {
    className: "btns-container"
  }, AddOwner_jsx("button", {
    className: "btns btns-primary",
    disabled: submitLoading,
    onClick: data.formIsValid ? _onSubmitted : checkValid
  }, submitLoading ? AddOwner_jsx(LoadingDot1["a" /* default */], {
    width: "1.5rem",
    height: "1.5rem"
  }) : editData ? data.formIsValid ? "ثبت تغییرات" : "تغیراتی مشاهده نمی شود" : data.formIsValid ? "افزودن" : "اطلاعات کامل نمی باشد"), editData ? AddOwner_jsx("button", {
    className: "btns btns-warning",
    onClick: propsHideModal
  }, "بستن") : ""))));
};

/* harmony default export */ var Owner_AddOwner = __webpack_exports__["a"] = (AddOwner);

/***/ }),

/***/ "g/15":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn(...args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  var _a;

  if (false) {} // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (false) {}

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (false) {}

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SP = typeof performance !== 'undefined';
exports.ST = exports.SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "gIEs":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./globalUtils/Loading/index.js
var __jsx = external_react_default.a.createElement;

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import { CoffeeLoading } from "react-loadingg";

function Loading(Component) {
  return function LoadingComponent(_ref) {
    let {
      isLoading
    } = _ref,
        props = _objectWithoutProperties(_ref, ["isLoading"]);

    if (!isLoading) return __jsx(Component, props);else return __jsx("div", {
      style: {
        display: "flex",
        width: "100vw",
        height: "100vh",
        justifyContent: "center",
        alignItem: "center"
      }
    });
  };
}

/* harmony default export */ var globalUtils_Loading = (Loading);
// EXTERNAL MODULE: ./globalUtils/axiosBase.js
var axiosBase = __webpack_require__("OZZ0");

// EXTERNAL MODULE: ./globalUtils/globalHoc/routingHandle.js
var routingHandle = __webpack_require__("J27u");

// CONCATENATED MODULE: ./globalUtils/globalHoc/index.js

const globalHoc = {
  routingHandle: routingHandle["a" /* default */]
};
/* harmony default export */ var globalUtils_globalHoc = (globalHoc);
// CONCATENATED MODULE: ./globalUtils/index.js



const globalUtils = {
  Loading: globalUtils_Loading,
  axiosBase: axiosBase["a" /* default */],
  globalHoc: globalUtils_globalHoc
};
/* harmony default export */ var globalUtils_0 = __webpack_exports__["a"] = (globalUtils);

/***/ }),

/***/ "ge5p":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const updateObject = (oldObject, updatedProperties) => {
  return _objectSpread({}, oldObject, {}, updatedProperties);
};

/* harmony default export */ __webpack_exports__["a"] = (updateObject);

/***/ }),

/***/ "gguc":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = param => {
      try {
        return decodeURIComponent(param);
      } catch (_) {
        const err = new Error('failed to decode param');
        err.code = 'DECODE_FAILED';
        throw err;
      }
    };

    const params = {};
    Object.keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });
    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "hgx0":
/***/ (function(module, exports) {

module.exports = require("leaflet");

/***/ }),

/***/ "kCdF":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _panelAdmin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("VGcP");
/* harmony import */ var _context_reducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("uqW6");
/* harmony import */ var _panelAdmin_screen_Owner_AddOwner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("g+59");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





const addOwner = props => {
  const Context = _context_reducer__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].panelAdminReducer.optionReducerContext;
  const giveContextData = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(Context);
  const {
    dispatch
  } = giveContextData;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    dispatch.changePageName(_panelAdmin__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"].values.strings.ADD_OWNER);
  }, []);
  return __jsx(_panelAdmin_screen_Owner_AddOwner__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"], null);
};

addOwner.panelAdminLayout = true; // addOwner.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   return { isServer };
// };

/* harmony default export */ __webpack_exports__["default"] = (addOwner);

/***/ }),

/***/ "kYf9":
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "n9sB":
/***/ (function(module, exports) {

module.exports = require("@material-ui/lab/Rating");

/***/ }),

/***/ "nOHt":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__("284h");

var _interopRequireDefault = __webpack_require__("TqRt");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _router2 = _interopRequireWildcard(__webpack_require__("elyg"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__("Osoz");

var _withRouter = _interopRequireDefault(__webpack_require__("0Bsm"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components', 'isFallback', 'basePath'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

Object.defineProperty(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  Object.defineProperty(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = Object.assign({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "o6Tf":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("oAEb");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_1__);



const toastify = async (text, type) => {
  react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].configure();

  if (type === "error") {
    // alert("error");
    await react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].error(text, {
      position: "top-right",
      autoClose: 5000
    });
  } else if (type === "success") {
    // alert("success");
    await react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].success(text, {
      position: "top-right",
      autoClose: 5000
    });
  }
};

/* harmony default export */ __webpack_exports__["a"] = (toastify);

/***/ }),

/***/ "oAEb":
/***/ (function(module, exports) {

module.exports = require("react-toastify");

/***/ }),

/***/ "q7pC":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export LazyImage */
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("HJQg");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

const LazyImage = ({
  src,
  alt,
  defaultImage,
  imageOnload,
  style
}) => {
  let placeHolder = defaultImage || "https://www.novok.com/storage/themes/novok/img/noimg.jpg";
  let placeHolderTwo = "https://www.chd.mhrd.gov.in/sites/default/files/no-image-available.jpg";
  const {
    0: imageSrc,
    1: setImageSrc
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(placeHolder); //   const [imageRef, setImageRef] = useState();
  //   //console.log({ imageSrc });

  const imageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  const onLoad = event => {
    //console.log({ onLoad: event });
    event.target.classList.add("loaded"); // imageOnload();
  };

  const onError = event => {
    //console.log({ error: event });
    event.target.classList.remove("loaded");
    event.target.classList.add("has-error");
  }; // useEffect(() => {
  //   imageRef.current.addEventListener("load", onLoad);
  // });


  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    imageRef.current.classList.remove("has-error"); // imageRef.current.classList.add("loaded");

    let observer;
    let didCancel = false; // //console.log("avaaaaaaaaaaaaaaaaz shooood");

    if (imageRef.current && imageSrc !== src) {
      if (IntersectionObserver) {
        observer = new IntersectionObserver(entries => {
          // //console.log({ observer, entries });
          entries.forEach(entry => {
            if (!didCancel && (entry.intersectionRatio > 0 || entry.isIntersecting)) {
              // imageRef.current.classList.add("loaded");
              imageRef.current.classList.remove("has-error");
              setTimeout(() => {
                setImageSrc(src);
              }, 200);
              observer.unobserve(imageRef.current) && observer.unobserve(imageRef.current);
            }
          });
        }, {
          threshold: [0.009999999776482582],
          rootMargin: "75%"
        });
        observer.observe(imageRef.current);
      } else {
        imageRef.current.classList.add("loaded");
        imageRef.current.classList.remove("has-error"); // Old browsers fallback

        setTimeout(() => {
          setImageSrc(src);
        }, 200);
      }
    }

    return () => {
      didCancel = true; // on component cleanup, we remove the listner

      if (observer && observer.unobserve) {
        observer.unobserve(imageRef.current);
      }
    };
  }, [src, imageSrc, imageRef.current]); //   return <Image ref={setImageRef} src={imageSrc} alt={alt} onLoad={onLoad} onError={onError} />;

  return __jsx(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, __jsx("img", {
    style: style,
    ref: imageRef,
    src: imageSrc,
    onLoad: onLoad,
    alt: alt,
    onError: onError,
    className: styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a.dynamic([["1496798165", [placeHolder, placeHolderTwo, placeHolderTwo, placeHolder]]]) + " " + "imageComponent"
  }), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "1496798165",
    dynamic: [placeHolder, placeHolderTwo, placeHolderTwo, placeHolder]
  }, [".imageComponent.__jsx-style-dynamic-selector{display:block;}", ".imageComponent.loaded.__jsx-style-dynamic-selector{-webkit-animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;}", `.imageComponent.has-error.__jsx-style-dynamic-selector{content:url(${placeHolderTwo});width:100%;height:100%;-webkit-animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;}`, `.imageComponent.has-error.__jsx-style-dynamic-selector::not(.loaded){content:url(${placeHolderTwo});width:100%;height:100%;-webkit-animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;}`, `.imageComponent.__jsx-style-dynamic-selector::not(.loaded),.imageComponent.__jsx-style-dynamic-selector::not(.has-error){content:url(${placeHolder});width:100%;height:100%;-webkit-animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;}`, ".image-lazyloading-container.__jsx-style-dynamic-selector{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:100%;height:100%;background-color:#ccc;}", "@-webkit-keyframes loaded-__jsx-style-dynamic-selector{0%{opacity:0.5;}100%{opacity:1;}}", "@keyframes loaded-__jsx-style-dynamic-selector{0%{opacity:0.5;}100%{opacity:1;}}"]));
};
/* harmony default export */ __webpack_exports__["a"] = (LazyImage);

/***/ }),

/***/ "tMBA":
/***/ (function(module, exports) {



/***/ }),

/***/ "uqW6":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./_context/index.js
var __jsx = external_react_default.a.createElement;

/* harmony default export */ var _context = ((reducer, actions, defaultValue) => {
  const Context = external_react_default.a.createContext();

  const Provider = ({
    children
  }) => {
    const {
      0: state,
      1: dispatch
    } = Object(external_react_["useReducer"])(reducer, defaultValue);
    const boundActions = {};

    for (let key in actions) {
      boundActions[key] = actions[key](dispatch);
    }

    return __jsx(Context.Provider, {
      value: {
        state,
        dispatch: boundActions
      }
    }, children);
  };

  return {
    Context,
    Provider
  };
});
// CONCATENATED MODULE: ./_context/reducer/panelAdminReducer/optionReducer.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // ========================================================  REDUCER

const optionReducer = (state, action) => {
  switch (action.type) {
    case "SET_PAGE_NAME":
      return _objectSpread({}, state, {
        pageName: action.payload
      });

    default:
      return state;
  }
}; // ========================================================  DISPATCH

const changePageName = dispatch => title => {
  // //console.log({ dispatch, title });
  dispatch({
    type: "SET_PAGE_NAME",
    payload: title
  });
};

const {
  Provider: optionReducer_Provider,
  Context: optionReducer_Context
} = _context(optionReducer, {
  changePageName
}, {
  pageName: ""
});
// CONCATENATED MODULE: ./_context/reducer/panelAdminReducer/index.js

const panelAdminReducer = {
  optionReducerProvider: optionReducer_Provider,
  optionReducerContext: optionReducer_Context
};
/* harmony default export */ var reducer_panelAdminReducer = (panelAdminReducer);
// CONCATENATED MODULE: ./_context/reducer/webSiteReducer/index.js
const webSiteReducer = {};
/* harmony default export */ var reducer_webSiteReducer = (webSiteReducer);
// CONCATENATED MODULE: ./_context/reducer/index.js


const reducer_reducer = {
  panelAdminReducer: reducer_panelAdminReducer,
  webSiteReducer: reducer_webSiteReducer
};
/* harmony default export */ var _context_reducer = __webpack_exports__["a"] = (reducer_reducer);

/***/ }),

/***/ "vM4n":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: ./panelAdmin/component/UI/Rating/Star/index.js
var Star = __webpack_require__("+3UN");

// EXTERNAL MODULE: ./panelAdmin/index.js + 16 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/component/cards/CardElement/CardRow/index.js
var __jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




const formatMoney = panelAdmin["a" /* default */].utils.formatMoney;

const CardRow = props => {
  const {
    elementType,
    value,
    className,
    style,
    iconStyle,
    title
  } = props;
  let element;

  switch (elementType) {
    case "text":
      element = __jsx("span", {
        title: title,
        style: _objectSpread({}, style)
      }, value);
      break;

    case "star":
      element = __jsx(Star["a" /* default */], {
        rating: value,
        fixed: true,
        size: "small"
      });
      break;

    case "price":
      element = __jsx("span", {
        style: _objectSpread({}, style),
        className: "s-c-card-price"
      }, formatMoney(value));
      break;

    case "icon":
      element = __jsx("div", {
        className: "cardPhonenumber-wrapper"
      }, " ", __jsx("span", {
        style: _objectSpread({}, style)
      }, value), __jsx("i", {
        style: _objectSpread({}, iconStyle),
        className: className
      }));
      break;

    case "district":
      element = __jsx("div", {
        className: "cardDistric-wrapper"
      }, " ", __jsx("i", {
        className: " icon-location-1"
      }), __jsx("span", null, value));
      break;

    default:
      break;
  }

  return element;
};

/* harmony default export */ var CardElement_CardRow = (CardRow);
// EXTERNAL MODULE: ./components/LazyImage/index.js
var LazyImage = __webpack_require__("q7pC");

// CONCATENATED MODULE: ./panelAdmin/component/cards/CardElement/index.js
var CardElement_jsx = external_react_default.a.createElement;



const CardElement = external_react_default.a.memo(props => {
  const {
    data,
    index,
    onClick,
    submitedTitle,
    optionClick,
    options,
    acceptedCardInfo
  } = props; // //console.log({ data });

  return CardElement_jsx("div", {
    style: {
      animationDelay: index * 150 + "ms"
    },
    key: index ? index : "",
    className: `show-Card-Information-row px-1  col-xl-2 col-lg-2 col-6 col-sm-6 col-md-6`,
    onClick: acceptedCardInfo ? () => acceptedCardInfo.handelAcceptedImage({
      index,
      data
    }) : null
  }, CardElement_jsx("div", {
    //  style={{ boxShadow: data.isActive ? "" : "0 0 6px 3px #ff00008a" }}
    className: `card-info transition0-2 ${data.isAccept}`
  }, CardElement_jsx("div", {
    className: "s-c--card-images transition0-2"
  }, CardElement_jsx("div", {
    className: "options-card transition0-2"
  }, options ? CardElement_jsx(external_react_["Fragment"], null, " ", options.remove && CardElement_jsx("span", {
    className: "options-card-cancel",
    onClick: () => optionClick({
      _id: data._id,
      mission: "remove"
    })
  }, CardElement_jsx("i", {
    className: "fas fa-times"
  })), options.edit && CardElement_jsx("span", {
    className: "options-card-edit",
    onClick: () => optionClick({
      _id: data._id,
      mission: "edit",
      index
    })
  }, CardElement_jsx("i", {
    className: "fas fa-pencil-alt"
  })), " ", options.block && CardElement_jsx("span", {
    className: "options-card-block",
    onClick: () => optionClick({
      _id: data._id,
      mission: "block",
      value: !data.isActive
    })
  }, CardElement_jsx("i", {
    className: "fas fa-unlock"
  }))) : ""), data.image ? CardElement_jsx("picture", null, CardElement_jsx("source", {
    media: "(max-width: 375px)",
    srcSet: data.image.value
  }), CardElement_jsx(LazyImage["a" /* default */], {
    src: data.image.value,
    defaultImage: false,
    alt: "cardImage"
  })) : ""), CardElement_jsx("div", {
    className: "s-c-card-body"
  }, CardElement_jsx("div", {
    className: "s-c-body-wrapper"
  }, CardElement_jsx("div", {
    className: "s-c-card-title"
  }, data.body && data.body.length > 0 && data.body.map((info, index) => {
    // //console.log({ info });
    return CardElement_jsx("div", {
      key: index + "mmj"
    }, CardElement_jsx("div", null, info.right && info.right.map(right => CardElement_CardRow(right))), CardElement_jsx("div", null, info.left && info.left.map(left => CardElement_CardRow(left))));
  }))), submitedTitle ? CardElement_jsx("div", {
    className: "btns-container"
  }, CardElement_jsx("button", {
    onClick: () => onClick(index),
    className: "btns btns-primary"
  }, submitedTitle)) : "")));
});
/* harmony default export */ var cards_CardElement = (CardElement);
// EXTERNAL MODULE: external "react-scrollbars-custom"
var external_react_scrollbars_custom_ = __webpack_require__("YPTf");

// EXTERNAL MODULE: external "react-perfect-scrollbar"
var external_react_perfect_scrollbar_ = __webpack_require__("RfFk");

// CONCATENATED MODULE: ./panelAdmin/component/cards/ShowCardInformation/index.js
var ShowCardInformation_jsx = external_react_default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }






const ShowCardInformation = ({
  data,
  onClick,
  submitedTitle,
  optionClick,
  options,
  acceptedCardInfo
}) => {
  // const optionClick = ({ _id, mission }) => {
  //   //console.log({ _id, mission });
  // };
  const showDataAll = data === null || data === void 0 ? void 0 : data.map((information, index) => {
    // //console.log({ information });
    return ShowCardInformation_jsx(cards_CardElement, _extends({
      options,
      optionClick,
      index,
      onClick,
      submitedTitle,
      acceptedCardInfo
    }, {
      key: index + "mmm",
      data: information
    }));
  });

  const ShowData = ShowCardInformation_jsx(cards_CardElement, {
    data,
    onClick,
    submitedTitle,
    optionClick,
    options,
    acceptedCardInfo
  });

  return ShowCardInformation_jsx("div", {
    className: "show-card-elements row m-0"
  }, data.length > 0 ? showDataAll : ShowData);
};

/* harmony default export */ var cards_ShowCardInformation = __webpack_exports__["a"] = (ShowCardInformation);

/***/ }),

/***/ "vmXh":
/***/ (function(module, exports) {

module.exports = require("js-cookie");

/***/ }),

/***/ "xYbV":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/InputPush/index.js
var __jsx = external_react_default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




const InputPush = props => {
  const {
    accepted,
    inputType,
    className,
    value,
    removeHandel,
    name,
    elementConfig,
    disabled,
    checkSubmitted
  } = props; //console.log({ InputPushValue: value });

  const {
    0: Typing,
    1: setTyping
  } = Object(external_react_["useState"])("");

  const acceptedClick = () => {
    if (Typing.length > 0) {
      accepted(Typing);
      setTyping("");
    }
  };

  Object(external_react_["useEffect"])(() => {
    setTyping("");
  }, [checkSubmitted]);
  const submitRef = Object(external_react_["useRef"])(null);
  const inputClasses = [className, "transition0-3"];

  const handelOnkeyDown = e => {
    if (e.key === "Enter") {
      submitRef.current.click();
    }
  };

  return __jsx("div", {
    style: {
      display: "flex",
      flexDirection: "column",
      flex: 1
    }
  }, (value === null || value === void 0 ? void 0 : value.length) > 0 && __jsx("div", {
    className: "data-show-array"
  }, value === null || value === void 0 ? void 0 : value.map((data, index) => {
    return __jsx("span", {
      onClick: () => removeHandel(data),
      key: index
    }, data);
  })), __jsx("div", {
    className: "input-push-wrapper"
  }, __jsx("input", _extends({
    disabled: disabled,
    type: inputType,
    className: inputClasses.join(" "),
    onKeyDown: handelOnkeyDown,
    value: Typing,
    onChange: e => setTyping(e.currentTarget.value),
    autoComplete: "off"
  }, elementConfig)), __jsx("span", {
    ref: submitRef,
    type: "submited",
    onClick: acceptedClick
  }, "\u062B\u0628\u062A")));
};

/* harmony default export */ var Inputs_InputPush = (InputPush);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/InputFile/index.js
var InputFile_jsx = external_react_default.a.createElement;


const InputFile = props => {
  const {
    onChange,
    className,
    inputLabel,
    name,
    value,
    progress,
    disabled,
    accepted,
    cancelUpload,
    placeholder
  } = props; //console.log({ className });

  const elements = InputFile_jsx(external_react_default.a.Fragment, null, InputFile_jsx("div", null, value ? value : "  انتخاب نمایید ..."), InputFile_jsx("label", null, InputFile_jsx("span", {
    onClick: disabled ? accepted : null
  }, progress ? progress + "%" : inputLabel), InputFile_jsx("input", {
    disabled: progress ? true : disabled,
    type: "file",
    onChange: onChange,
    name: name
  })));

  return InputFile_jsx("div", {
    className: `addFileModalContainer ${className}`
  }, elements);
};

/* harmony default export */ var Inputs_InputFile = (InputFile);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/SearchDropDown/AcceptedChild/index.js
var AcceptedChild_jsx = external_react_default.a.createElement;


const AcceptedChild = ({
  data
}) => {
  return data ? AcceptedChild_jsx("div", {
    className: "serached-item"
  }, data.image ? AcceptedChild_jsx("div", {
    className: "searched-image"
  }, AcceptedChild_jsx("img", {
    src: data.image,
    alt: "search title"
  })) : "", AcceptedChild_jsx("div", {
    className: "searched-text"
  }, AcceptedChild_jsx("span", {
    className: "searched-title"
  }, data.title), AcceptedChild_jsx("span", {
    className: "searched-description"
  }, data.description))) : "";
};

/* harmony default export */ var SearchDropDown_AcceptedChild = (AcceptedChild);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/SearchDropDown/index.js
var SearchDropDown_jsx = external_react_default.a.createElement;

function SearchDropDown_extends() { SearchDropDown_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return SearchDropDown_extends.apply(this, arguments); }




const SearchDropDown = props => {
  const {
    onKeyDown,
    dropDownData,
    onChange,
    accepted,
    disabled,
    checkSubmitted,
    className,
    elementConfig,
    value,
    staticTitle = ""
  } = props;
  const {
    0: search,
    1: setSearch
  } = Object(external_react_["useState"])([]);
  const {
    0: searchTitle,
    1: setSearchTitle
  } = Object(external_react_["useState"])("");
  const {
    0: acceptedSave,
    1: setAcceptedSave
  } = Object(external_react_["useState"])("");
  const {
    0: typing,
    1: setTping
  } = Object(external_react_["useState"])(false);
  const {
    0: lock,
    1: setLock
  } = Object(external_react_["useState"])(false); //console.log({ search, searchTitle, acceptedSave, typing, staticTitle }, searchTitle ? searchTitle : typing ? "" : staticTitle);

  Object(external_react_["useEffect"])(() => {
    if (dropDownData && dropDownData.length > 0) setSearch(dropDownData);
  }, [dropDownData]);
  Object(external_react_["useEffect"])(() => {
    setSearch([]);
    setSearchTitle("");
    setAcceptedSave("");
    setTping(false);
  }, [checkSubmitted]);

  const handleBlur = e => {
    if (e.nativeEvent.explicitOriginalTarget && e.nativeEvent.explicitOriginalTarget === e.nativeEvent.originalTarget) return;
    if (search.length > 0) setTimeout(() => {
      setSearch([]);
    }, 300);
  };

  const handelOnChange = e => {
    let value = e.currentTarget.value;
    onChange(e);
    setSearchTitle(value);
    setTping(true);
    setLock(false);
  };

  const clickedElement = index => {
    setAcceptedSave(search[index]);
    setSearchTitle(search[index].title);
    accepted(search[index].value);
    setLock(true);
  };

  const searchContainer = SearchDropDown_jsx("div", {
    className: "text-search-dropDown"
  }, SearchDropDown_jsx("div", null, SearchDropDown_jsx("input", SearchDropDown_extends({
    onKeyDown: onKeyDown,
    disabled: disabled,
    value: searchTitle ? searchTitle : typing ? "" : staticTitle,
    onBlur: handleBlur,
    onChange: handelOnChange,
    className: className
  }, elementConfig)), !lock && search && search.length > 0 && SearchDropDown_jsx("div", {
    className: "searched-dropDown felxRow"
  }, search.map((searched, index) => {
    return SearchDropDown_jsx("div", {
      key: index,
      onClick: () => clickedElement(index),
      className: "serached-item"
    }, searched.image ? SearchDropDown_jsx("div", {
      className: "searched-image"
    }, " ", SearchDropDown_jsx("img", {
      src: searched.image,
      alt: "search title"
    })) : "", SearchDropDown_jsx("div", {
      className: "searched-text"
    }, SearchDropDown_jsx("span", {
      className: "searched-title"
    }, searched.title), SearchDropDown_jsx("span", {
      className: "searched-description"
    }, searched.description)));
  }))), SearchDropDown_jsx("div", null, SearchDropDown_jsx(SearchDropDown_AcceptedChild, {
    data: acceptedSave
  })));

  return searchContainer;
};

/* harmony default export */ var Inputs_SearchDropDown = (SearchDropDown);
// EXTERNAL MODULE: external "react-bootstrap"
var external_react_bootstrap_ = __webpack_require__("IZS3");

// EXTERNAL MODULE: ./components/LazyImage/index.js
var LazyImage = __webpack_require__("q7pC");

// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/InputDropDownSearch/index.js
var InputDropDownSearch_jsx = external_react_default.a.createElement;



const CustomMenu = external_react_default.a.forwardRef(({
  children,
  style,
  className,
  "aria-labelledby": labeledBy
}, ref) => {
  const {
    0: value,
    1: setValue
  } = Object(external_react_["useState"])("");
  console.log({
    value
  });
  return InputDropDownSearch_jsx("div", {
    ref: ref,
    style: style,
    className: className,
    "aria-labelledby": labeledBy
  }, InputDropDownSearch_jsx(external_react_bootstrap_["FormControl"], {
    autoFocus: true,
    className: "mx-3 my-2 w-auto",
    placeholder: "\u062C\u0633\u062A\u062C\u0648 ...",
    onChange: e => setValue(e.target.value),
    value: value
  }), InputDropDownSearch_jsx("ul", {
    className: "list-unstyled"
  }, external_react_default.a.Children.toArray(children).filter(child => {
    var _child$props, _child$props2, _child$props2$childre, _child$props3, _child$props4, _child$props4$childre, _child$props4$childre2;

    // console.log({ child: child?.props?.children.length });
    // if (child?.props?.children.length == 2) console.log({ data: child?.props?.children[child?.props?.children.length - 1].props.children[0].props.children });
    // console.log({map:child?.props?.children.map(()=>{
    // })});
    if (child === null || child === void 0 ? void 0 : (_child$props = child.props) === null || _child$props === void 0 ? void 0 : _child$props.children.length) return !value || (child === null || child === void 0 ? void 0 : (_child$props2 = child.props) === null || _child$props2 === void 0 ? void 0 : (_child$props2$childre = _child$props2.children[(child === null || child === void 0 ? void 0 : (_child$props3 = child.props) === null || _child$props3 === void 0 ? void 0 : _child$props3.children.length) - 1].props.children[0].props.children) === null || _child$props2$childre === void 0 ? void 0 : _child$props2$childre.includes(value)) // ?.startsWith(value)
    // .map((lastChild) => {
    //   console.log({ lastChild });
    //   return lastChild.props.children?.toLowerCase()?.startsWith(value);
    // })
    ;else return !value || (child === null || child === void 0 ? void 0 : (_child$props4 = child.props) === null || _child$props4 === void 0 ? void 0 : (_child$props4$childre = _child$props4.children[0]) === null || _child$props4$childre === void 0 ? void 0 : (_child$props4$childre2 = _child$props4$childre.toLowerCase()) === null || _child$props4$childre2 === void 0 ? void 0 : _child$props4$childre2.includes(value));
  })));
});

const InputDropDownSearch = props => {
  const {
    accepted,
    dropDownData,
    value,
    className,
    checkSubmitted,
    disabled
  } = props; // //console.log({ dropDownData });

  let propsVal, index;
  index = dropDownData === null || dropDownData === void 0 ? void 0 : dropDownData.findIndex(d => d.value === value);
  if (index >= 0) propsVal = dropDownData[index].title;
  const {
    0: Title,
    1: setTitle
  } = Object(external_react_["useState"])("");
  Object(external_react_["useEffect"])(() => {
    setTitle();
  }, [checkSubmitted]);

  const clickedElement = (value, title) => {
    setTitle(title);
    accepted(value);
  };

  const CustomToggle = external_react_default.a.forwardRef(({
    children,
    onClick
  }, ref) => InputDropDownSearch_jsx("a", {
    className: className ? className : "",
    href: "",
    ref: ref,
    onClick: e => {
      e.preventDefault();
      return disabled ? "" : onClick(e);
    }
  }, children, "\u25BC"));
  return InputDropDownSearch_jsx(external_react_bootstrap_["Dropdown"], null, InputDropDownSearch_jsx(external_react_bootstrap_["Dropdown"].Toggle, {
    as: CustomToggle,
    id: "dropdown-custom-components"
  }, Title ? Title : propsVal ? propsVal : value ? value : "انتخاب نمایید  "), InputDropDownSearch_jsx(external_react_bootstrap_["Dropdown"].Menu, {
    as: CustomMenu
  }, dropDownData && dropDownData.map((info, index) => {
    return InputDropDownSearch_jsx(external_react_bootstrap_["Dropdown"].Item, {
      className: "drop-item",
      key: index + "m",
      active: value === info.value,
      onClick: () => clickedElement(info.value, info.title),
      eventKey: index + 1
    }, info.image ? InputDropDownSearch_jsx("div", {
      className: "drop-image"
    }, " ", InputDropDownSearch_jsx(LazyImage["a" /* default */], {
      src: info.image,
      defaultImage: false,
      alt: "search title"
    })) : "", InputDropDownSearch_jsx("div", {
      className: "drop-text"
    }, InputDropDownSearch_jsx("span", {
      className: "drop-title"
    }, info.title), InputDropDownSearch_jsx("span", {
      className: "drop-description"
    }, info.description)));
  })));
};

/* harmony default export */ var Inputs_InputDropDownSearch = (InputDropDownSearch);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/InputFileArray/index.js
var InputFileArray_jsx = external_react_default.a.createElement;


const InputFileArray = props => {
  const {
    onChange,
    inputLabel,
    className,
    name,
    value,
    progress,
    disabled,
    cancelUpload,
    onKeyDown,
    removeHandel,
    accepted
  } = props; //console.log({ className });

  const elements = InputFileArray_jsx("div", {
    style: {
      display: "flex",
      flexDirection: "column",
      flex: 1
    }
  }, (value === null || value === void 0 ? void 0 : value.length) > 0 && InputFileArray_jsx("div", {
    className: "data-show-array"
  }, value.map((data, index) => {
    return InputFileArray_jsx("div", {
      onClick: () => removeHandel(data),
      key: index + "moj"
    }, InputFileArray_jsx("span", null, InputFileArray_jsx("img", {
      src: data,
      alt: "image"
    })));
  })), InputFileArray_jsx("div", {
    className: `addFileModalContainer ${className}`
  }, InputFileArray_jsx("div", null, "  انتخاب نمایید ..."), InputFileArray_jsx("label", null, InputFileArray_jsx("span", {
    onClick: disabled ? accepted : null
  }, progress ? progress + "%" : inputLabel), InputFileArray_jsx("input", {
    onKeyDown: onKeyDown,
    disabled: progress ? true : disabled,
    type: "file",
    onChange: onChange,
    name: name
  }))));

  return elements;
};

/* harmony default export */ var Inputs_InputFileArray = (InputFileArray);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/DropdownBoot/index.js
var DropdownBoot_jsx = external_react_default.a.createElement;



const DropdownBoot = props => {
  const {
    accepted,
    dropDownData,
    value,
    className,
    checkSubmitted,
    disabled
  } = props;
  let index = dropDownData && dropDownData.findIndex(d => d.value === value);
  let propsVal;
  if (index >= 0) propsVal = dropDownData[index].title;
  const {
    0: Title,
    1: setTitle
  } = Object(external_react_["useState"])("");

  const clickedElement = (value, title) => {
    if (value) accepted(value, title);
    if (title) setTitle(title);
  };

  return DropdownBoot_jsx(external_react_bootstrap_["Dropdown"], null, DropdownBoot_jsx(external_react_bootstrap_["Dropdown"].Toggle, {
    variant: "success",
    id: "dropdown-basic"
  }, Title ? Title : propsVal ? propsVal : value ? value : "انتخاب نمایید  "), DropdownBoot_jsx(external_react_bootstrap_["Dropdown"].Menu, null, dropDownData && dropDownData.map((info, index) => {
    // //console.log({ value, infoval: info });
    return DropdownBoot_jsx(external_react_bootstrap_["Dropdown"].Item, {
      key: index + "boot",
      active: value === info.title,
      onClick: () => clickedElement(info.value, info.title)
    }, info.title);
  })));
};

/* harmony default export */ var Inputs_DropdownBoot = (DropdownBoot);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/DateInput/index.js
var DateInput_jsx = external_react_default.a.createElement;

const DateInput = Object(external_react_["memo"])(props => {
  const {
    checkSubmitted,
    onKeyDown,
    disabled,
    className,
    elementConfig,
    value,
    onChange
  } = props;

  const changedDate = event => {
    let name = event.currentTarget.name;
    let value = event.currentTarget.value;
    const valid = ["month", "day"]; // if (value < 10 && value.length < 2 && valid.includes(name)) {
    //   event.currentTarget.value = 0 + value;
    // } else if (value < 10 == 0 && value.length === 2 && value.charAt(0) == 0 && valid.includes(name)) {
    //   event.currentTarget.value = value.slice(0, 0);
    // }

    onChange(event, name);
  };

  return DateInput_jsx("div", {
    className: "date-input-entry-data"
  }, DateInput_jsx("div", null, DateInput_jsx("label", null, "\u0633\u0627\u0644"), DateInput_jsx("input", {
    maxLength: "4",
    name: "year",
    value: value.year,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown
  })), DateInput_jsx("div", null, DateInput_jsx("label", null, "\u0645\u0627\u0647"), DateInput_jsx("input", {
    maxLength: 2,
    name: "month",
    value: value.month,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown
  })), DateInput_jsx("div", null, DateInput_jsx("label", null, "\u0631\u0648\u0632"), DateInput_jsx("input", {
    maxLength: 2,
    name: "day",
    value: value.day,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown
  })));
});
/* harmony default export */ var Inputs_DateInput = (DateInput); // const [state, setState] = useState({ year: "1300", month: "0", day: "0" });
// useEffect(() => {
//   if (value) changeValue();
// }, [value]);
// const changeValue = () => {
//   setState({ year: value.split("/")[0], month: value.split("/")[1], day: value.split("/")[2] });
// };
// const onChangeElement = (event) => {
//   let name = event.currentTarget.name;
//   let value = event.currentTarget.value;
//   let newState = { ...state };
//   let patt1 = /[0-9]/g;
//   let result = value.match(patt1);
//   if (result) newState[name] = value;
//   setState({ ...state, [name]: value });
//   if (!Object.values(state).includes("")) //console.log("etelaat poor ast");
//   event.currentTarget.value = newState.year + "/" + newState.month + "/" + newState.day;
//   onChange(event);
// };
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/SearchDropDownArray/AcceptedChild/index.js
var SearchDropDownArray_AcceptedChild_jsx = external_react_default.a.createElement;


const AcceptedChild_AcceptedChild = ({
  data,
  removeHandel
}) => {
  //console.log({ data });
  return data.length ? data.map((data, index) => {
    return SearchDropDownArray_AcceptedChild_jsx("div", {
      onClick: () => removeHandel(data.value),
      key: index + "sadasd",
      className: "serached-item"
    }, data.image ? SearchDropDownArray_AcceptedChild_jsx("div", {
      className: "searched-image"
    }, SearchDropDownArray_AcceptedChild_jsx("img", {
      src: data.image,
      alt: "search title"
    })) : "", SearchDropDownArray_AcceptedChild_jsx("div", {
      className: "searched-text"
    }, SearchDropDownArray_AcceptedChild_jsx("span", {
      className: "searched-title"
    }, data.title)));
  }) : "";
};

/* harmony default export */ var SearchDropDownArray_AcceptedChild = (AcceptedChild_AcceptedChild);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/SearchDropDownArray/index.js
var SearchDropDownArray_jsx = external_react_default.a.createElement;

function SearchDropDownArray_extends() { SearchDropDownArray_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return SearchDropDownArray_extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




const SearchDropDownArray = props => {
  const {
    removeHandel,
    onKeyDown,
    dropDownData,
    onChange,
    accepted,
    disabled,
    checkSubmitted,
    className,
    elementConfig,
    value,
    staticTitle
  } = props;
  const {
    0: search,
    1: setSearch
  } = Object(external_react_["useState"])([]);
  const {
    0: searchTitle,
    1: setSearchTitle
  } = Object(external_react_["useState"])("");
  const {
    0: acceptedSave,
    1: setAcceptedSave
  } = Object(external_react_["useState"])([]);
  const {
    0: typing,
    1: setTping
  } = Object(external_react_["useState"])(false);
  const {
    0: lock,
    1: setLock
  } = Object(external_react_["useState"])(false);
  const {
    0: state,
    1: setState
  } = Object(external_react_["useState"])({
    removeIndex: null,
    prevValueLength: null
  });
  let propsVal, index;
  Object(external_react_["useEffect"])(() => {
    if (value.length !== state.prevValueLength) {
      //console.log(state.prevValueLength, value.length);
      let newState = acceptedSave;
      newState.splice(state.removeIndex, 1);
      setAcceptedSave(newState);
    }
  }, [value.length]); // index = dropDownData.findIndex((d) => {
  //   //console.log({ dropDownData });
  //   //console.log({ valiiiiiiiid: d.value.includes(value) });
  //   return d.value.includes(value);
  // });
  // if (index >= 0) propsVal = dropDownData[index].title;
  // //console.log({ indexArrrraaaay: index, propsVal });

  Object(external_react_["useEffect"])(() => {
    if (dropDownData && dropDownData.length > 0) setSearch(dropDownData);
  }, [dropDownData]);
  Object(external_react_["useEffect"])(() => {
    setSearch([]);
    setSearchTitle("");
    setAcceptedSave([]);
    setTping(false);
  }, [checkSubmitted]);

  const handleBlur = e => {
    if (e.nativeEvent.explicitOriginalTarget && e.nativeEvent.explicitOriginalTarget === e.nativeEvent.originalTarget) return;
    if (search.length > 0) setTimeout(() => {
      setSearch([]);
    }, 300);
  };

  const handelOnChange = e => {
    let value = e.currentTarget.value;
    onChange(e);
    setSearchTitle(value);
    setTping(true);
    setLock(false);
  };

  const clickedElement = index => {
    let newState = acceptedSave;
    newState.push({
      title: search[index].title,
      value: search[index].value
    });
    setAcceptedSave(newState);
    setSearchTitle("");
    accepted(search[index].value);
    setLock(true);
    setState(_objectSpread({}, state, {
      prevValueLength: acceptedSave.length
    }));
  }; //console.log({ acceptedSave });


  const _removeHandel = data => {
    index = acceptedSave.findIndex(d => {
      return d.value.includes(data);
    });
    removeHandel(data);

    if (index >= 0) {
      setState(_objectSpread({}, state, {
        removeIndex: index
      }));
    }
  };

  const searchContainer = SearchDropDownArray_jsx("div", {
    className: "text-search-dropDown"
  }, SearchDropDownArray_jsx("div", null, SearchDropDownArray_jsx("input", SearchDropDownArray_extends({
    onKeyDown: onKeyDown,
    disabled: disabled,
    value: searchTitle ? searchTitle : typing ? "" : staticTitle,
    onBlur: handleBlur,
    onChange: handelOnChange,
    className: className
  }, elementConfig)), !lock && search && search.length > 0 && SearchDropDownArray_jsx("div", {
    className: "searched-dropDown felxRow"
  }, search.map((searched, index) => {
    return SearchDropDownArray_jsx("div", {
      key: index,
      onClick: () => clickedElement(index),
      className: "serached-item"
    }, searched.image ? SearchDropDownArray_jsx("div", {
      className: "searched-image"
    }, " ", SearchDropDownArray_jsx("img", {
      src: searched.image,
      alt: "search title"
    })) : "", SearchDropDownArray_jsx("div", {
      className: "searched-text"
    }, SearchDropDownArray_jsx("span", {
      className: "searched-title"
    }, searched.title), SearchDropDownArray_jsx("span", {
      className: "searched-description"
    }, searched.description)));
  }))), SearchDropDownArray_jsx("div", null, SearchDropDownArray_jsx(SearchDropDownArray_AcceptedChild, {
    data: acceptedSave,
    removeHandel: _removeHandel
  })));

  return searchContainer;
};

/* harmony default export */ var Inputs_SearchDropDownArray = (SearchDropDownArray);
// EXTERNAL MODULE: ./panelAdmin/index.js + 16 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/InputDropDownSearchArray/index.js
var InputDropDownSearchArray_jsx = external_react_default.a.createElement;


const InputDropDownSearchArray_CustomMenu = external_react_default.a.forwardRef(({
  children,
  style,
  className,
  "aria-labelledby": labeledBy
}, ref) => {
  const {
    0: value,
    1: setValue
  } = Object(external_react_["useState"])("");
  return InputDropDownSearchArray_jsx("div", {
    ref: ref,
    style: style,
    className: className,
    "aria-labelledby": labeledBy
  }, InputDropDownSearchArray_jsx(external_react_bootstrap_["FormControl"], {
    autoFocus: true,
    className: "mx-3 my-2 w-auto",
    placeholder: "\u062C\u0633\u062A\u062C\u0648 ...",
    onChange: e => setValue(e.target.value),
    value: value
  }), InputDropDownSearchArray_jsx("ul", {
    className: "list-unstyled"
  }, external_react_default.a.Children.toArray(children).filter(child => !value || child.props.children.toLowerCase().startsWith(value))));
});

const InputDropDownSearchArray = props => {
  const {
    accepted,
    dropDownData,
    value,
    className,
    checkSubmitted,
    disabled
  } = props; // //console.log({ accepted, dropDownData, value, className, checkSubmitted, disabled });

  let propsVal;
  propsVal = dropDownData.filter(d => {
    // //console.log({ hasan: d.value, value }, value.includes(d.value));
    return value.includes(d.value);
  });
  const {
    0: Title,
    1: setTitle
  } = Object(external_react_["useState"])(""); // //console.log({ checkSubmitted, Title });

  Object(external_react_["useEffect"])(() => {
    setTitle();
  }, [checkSubmitted]);

  const clickedElement = (value, title) => {
    let newTitle = Title ? Title + "," + title : title;
    setTitle(newTitle);
    accepted(value);
  };

  const CustomToggle = external_react_default.a.forwardRef(({
    children,
    onClick
  }, ref) => InputDropDownSearchArray_jsx("a", {
    className: className ? className : "",
    href: "",
    ref: ref,
    onClick: e => {
      e.preventDefault();
      return disabled ? "" : onClick(e);
    }
  }, children, "\u25BC"));
  return InputDropDownSearchArray_jsx(external_react_bootstrap_["Dropdown"], null, InputDropDownSearchArray_jsx(external_react_bootstrap_["Dropdown"].Toggle, {
    as: CustomToggle,
    id: "dropdown-custom-components"
  }, propsVal.length ? propsVal.map(val => {
    return val.title + ",";
  }) : value.length ? value : "انتخاب نمایید  "), InputDropDownSearchArray_jsx(external_react_bootstrap_["Dropdown"].Menu, {
    as: InputDropDownSearchArray_CustomMenu
  }, dropDownData && dropDownData.map((info, index) => {
    return InputDropDownSearchArray_jsx(external_react_bootstrap_["Dropdown"].Item, {
      key: index + "m",
      active: value.includes(info.value),
      onClick: () => clickedElement(info.value, info.title),
      eventKey: index + 1
    }, info.title);
  })));
};

/* harmony default export */ var Inputs_InputDropDownSearchArray = (InputDropDownSearchArray);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/Coordinate/index.js
var Coordinate_jsx = external_react_default.a.createElement;

const Coordinate = Object(external_react_["memo"])(props => {
  const {
    checkSubmitted,
    onKeyDown,
    disabled,
    className,
    elementConfig,
    value,
    onChange
  } = props;

  const changedDate = event => {
    let name = event.currentTarget.name;
    let value = event.currentTarget.value; // const valid = ["month", "day"];
    // if (value < 10 && value.length < 2 && valid.includes(name)) {
    //   event.currentTarget.value = 0 + value;
    // } else if (value < 10 == 0 && value.length === 2 && value.charAt(0) == 0 && valid.includes(name)) {
    //   event.currentTarget.value = value.slice(0, 0);
    // }

    onChange(event, name);
  };

  return Coordinate_jsx("div", {
    className: "date-input-entry-data"
  }, Coordinate_jsx("div", null, Coordinate_jsx("label", null, "\u0637\u0648\u0644"), Coordinate_jsx("input", {
    name: "lat",
    value: value.lat,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown
  })), Coordinate_jsx("div", null, Coordinate_jsx("label", null, "\u0639\u0631\u0636"), Coordinate_jsx("input", {
    name: "lng",
    value: value.lng,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown
  })));
});
/* harmony default export */ var Inputs_Coordinate = (Coordinate); // const [state, setState] = useState({ year: "1300", month: "0", day: "0" });
// useEffect(() => {
//   if (value) changeValue();
// }, [value]);
// const changeValue = () => {
//   setState({ year: value.split("/")[0], month: value.split("/")[1], day: value.split("/")[2] });
// };
// const onChangeElement = (event) => {
//   let name = event.currentTarget.name;
//   let value = event.currentTarget.value;
//   let newState = { ...state };
//   let patt1 = /[0-9]/g;
//   let result = value.match(patt1);
//   if (result) newState[name] = value;
//   setState({ ...state, [name]: value });
//   if (!Object.values(state).includes("")) //console.log("etelaat poor ast");
//   event.currentTarget.value = newState.year + "/" + newState.month + "/" + newState.day;
//   onChange(event);
// };
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/TwoCheckBox/index.js
var TwoCheckBox_jsx = external_react_default.a.createElement;


const TwoCheckBox = props => {
  const {
    accepted
  } = props; //console.log({ props });

  return TwoCheckBox_jsx(external_react_default.a.Fragment, null, TwoCheckBox_jsx("div", {
    className: "margin-1rem centerAll"
  }, TwoCheckBox_jsx("label", null, " بله : "), TwoCheckBox_jsx("input", {
    className: "pointer",
    onClick: () => accepted("true"),
    type: "radio",
    name: "group1"
  })), TwoCheckBox_jsx("div", {
    className: "margin-1rem centerAll"
  }, TwoCheckBox_jsx("label", null, " خیر : "), TwoCheckBox_jsx("input", {
    className: "pointer",
    onClick: () => accepted("false"),
    type: "radio",
    name: "group1"
  })));
};

/* harmony default export */ var Inputs_TwoCheckBox = (TwoCheckBox);
// EXTERNAL MODULE: external "next/dynamic"
var dynamic_ = __webpack_require__("/T1H");
var dynamic_default = /*#__PURE__*/__webpack_require__.n(dynamic_);

// CONCATENATED MODULE: ./panelAdmin/component/FormMap/index.js
var FormMap_jsx = external_react_default.a.createElement;


const SimpleExample = dynamic_default()(() => __webpack_require__.e(/* import() */ 0).then(__webpack_require__.bind(null, "9K3d")), {
  ssr: false,
  loadableGenerated: {
    webpack: () => [/*require.resolve*/("9K3d")],
    modules: ["../SimpleExample"]
  }
}); // const MapWithNoSSR = dynamic(() => import("./map"), {
//   ssr: false,
// });

const FormMap = props => {
  const {
    value,
    className,
    elementConfig,
    formElement,
    onChange,
    inputClasses,
    openMapModal,
    uploadWeb,
    mapPinData
  } = props;
  const {
    0: map,
    1: setmap
  } = Object(external_react_["useState"])(false);
  const {
    0: display,
    1: setDisplay
  } = Object(external_react_["useState"])(false);

  const showMap = () => {
    setmap(!map);
  };

  Object(external_react_["useEffect"])(() => {
    setDisplay(true);
  }, []);

  const endAnimation = () => {// (!map ? setDisplay(true) : setDisplay(false))
  }; // //console.log({ formElement });


  let form;
  form = FormMap_jsx("div", {
    className: "Input",
    style: {
      display: "flex",
      flexDirection: "column"
    }
  }, FormMap_jsx("div", {
    className: "title-wrapper"
  }), FormMap_jsx("div", null, FormMap_jsx("div", {
    className: "coordinate-wrapper"
  }, FormMap_jsx("div", null, FormMap_jsx("span", null, "طول (lng): "), FormMap_jsx("div", {
    disabled: true,
    className: `${className}` // className={inputClasses.join(" ")}
    // value={formElement.config.value.lng} name={"lng"} onChange={(e) => onChange({ name: formElement.id, value: e.currentTarget.value, child: e.currentTarget.name })} type={"number"}

  }, FormMap_jsx("span", null, (value === null || value === void 0 ? void 0 : value.lng) || " 49.585606455802925"))), FormMap_jsx("div", null, FormMap_jsx("span", null, "عرض (lat) : "), FormMap_jsx("div", {
    disabled: true,
    className: `${className}` // className={inputClasses.join(" ")}
    //  value={formElement.config.value.lat} name={"lat"} onChange={(e) => onChange({ name: formElement.id, value: e.currentTarget.value, child: e.currentTarget.name })} type={"number"}

  }, FormMap_jsx("span", null, " ", (value === null || value === void 0 ? void 0 : value.lat) || "37.281369047367555")))), FormMap_jsx("div", null, display ? FormMap_jsx(SimpleExample, {
    newPin: true,
    center: (value === null || value === void 0 ? void 0 : value.lat) ? value : {
      lat: "37.281369047367555",
      lng: "49.585606455802925"
    },
    onChange: onChange,
    data: mapPinData
  }) : "")));
  return form;
};

/* harmony default export */ var component_FormMap = (FormMap);
// CONCATENATED MODULE: ./panelAdmin/component/UI/Inputs/Input.js
var Input_jsx = external_react_default.a.createElement;

function Input_extends() { Input_extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return Input_extends.apply(this, arguments); }






 // import DateCelander from "./DateCelander";









const handleKey = panelAdmin["a" /* default */].utils.handleKey;
const Inputs = Object(external_react_["memo"])(props => {
  const {
    titleValidity,
    progress,
    elementType,
    elementConfig,
    value,
    changed,
    accepted,
    label,
    invalid,
    shouldValidate,
    touched,
    removeHandel,
    dropDownData,
    defaultInputDesable,
    checkSubmitted,
    disabled,
    display,
    searchAccepted,
    setSearchAccepted,
    staticTitle
  } = props;
  let inputElement = null;
  const inputClasses = ["InputElement"]; // //console.log({ titleValidity });

  if (invalid && shouldValidate && touched) {
    inputClasses.push("Invalid");
  }

  let validStyle = "red";

  if (!touched && inputClasses.includes("Invalid")) {
    validStyle = "red";
  }

  if (touched && !inputClasses.includes("Invalid")) {
    validStyle = "green";
  }

  switch (elementType) {
    case "input":
      inputElement = Input_jsx("input", Input_extends({
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" ")
      }, elementConfig, {
        value: value,
        onChange: changed
      }));
      break;

    case "inputPush":
      inputElement = Input_jsx(Inputs_InputPush, {
        checkSubmitted: checkSubmitted,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        removeHandel: removeHandel
      });
      break;

    case "inputFile":
      inputElement = Input_jsx(Inputs_InputFile, Input_extends({
        accepted: accepted,
        disabled: disabled,
        progress: progress,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value
      }, elementConfig, {
        inputLabel: "انتخاب",
        label: label
      }));
      break;

    case "InputFileArray":
      inputElement = Input_jsx(Inputs_InputFileArray, Input_extends({
        onKeyDown: handleKey,
        accepted: accepted,
        removeHandel: removeHandel,
        disabled: disabled,
        progress: progress,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value
      }, elementConfig, {
        inputLabel: "انتخاب",
        label: label
      }));
      break;

    case "inputSearch":
      inputElement = Input_jsx(Inputs_SearchDropDown, {
        onKeyDown: handleKey,
        checkSubmitted: checkSubmitted,
        dropDownData: dropDownData,
        accepted: accepted,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value,
        label: label,
        elementConfig: elementConfig,
        disabled: disabled,
        searchAccepted: searchAccepted,
        setSearchAccepted: setSearchAccepted,
        staticTitle: staticTitle
      });
      break;

    case "inputSearchArray":
      inputElement = Input_jsx(Inputs_SearchDropDownArray, {
        onKeyDown: handleKey,
        checkSubmitted: checkSubmitted,
        dropDownData: dropDownData,
        accepted: accepted,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value,
        label: label,
        elementConfig: elementConfig,
        disabled: disabled,
        searchAccepted: searchAccepted,
        setSearchAccepted: setSearchAccepted,
        staticTitle: staticTitle,
        removeHandel: removeHandel
      });
      break;

    case "inputDropDownSearch":
      inputElement = Input_jsx(Inputs_InputDropDownSearch, {
        dropDownData: dropDownData,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        checkSubmitted: checkSubmitted
      });
      break;

    case "inputDropDownSearchArray":
      inputElement = Input_jsx(Inputs_InputDropDownSearchArray, {
        dropDownData: dropDownData,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        checkSubmitted: checkSubmitted
      });
      break;

    case "textarea":
      inputElement = Input_jsx("textarea", Input_extends({
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" ")
      }, elementConfig, {
        value: value,
        onChange: changed
      }));
      break;

    case "select":
      inputElement = Input_jsx("select", {
        disabled: disabled,
        className: inputClasses.join(" "),
        value: value,
        onChange: changed
      }, elementConfig.options.map(option => Input_jsx("option", {
        key: option.value,
        value: option.value
      }, option.displayValue)));
      break;

    case "inputDropDown":
      inputElement = Input_jsx(Inputs_DropdownBoot, {
        dropDownData: dropDownData,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        checkSubmitted: checkSubmitted
      });
      break;
    // case "date":
    //   inputElement = (
    //     <DateCelander
    //       checkSubmitted={checkSubmitted}
    //       onKeyDown={handleKey}
    //       disabled={disabled}
    //       className={inputClasses.join(" ")}
    //       elementConfig={elementConfig}
    //       value={value}
    //       accepted={accepted}
    //     />
    //   );
    //   break;

    case "dateInput":
      inputElement = Input_jsx(Inputs_DateInput, {
        checkSubmitted: checkSubmitted,
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" "),
        elementConfig: elementConfig,
        value: value,
        onChange: changed
      });
      break;

    case "coordinate":
      inputElement = Input_jsx(component_FormMap, {
        checkSubmitted: checkSubmitted,
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" "),
        elementConfig: elementConfig,
        value: value,
        onChange: changed
      });
      break;

    case "twoCheckBox":
      inputElement = Input_jsx(Inputs_TwoCheckBox, {
        checkSubmitted: checkSubmitted,
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" "),
        elementConfig: elementConfig,
        value: value,
        accepted: accepted
      });
      break;

    default:
      return defaultInputDesable ? "" : inputElement = Input_jsx("input", Input_extends({
        onKeyDown: handleKey,
        className: inputClasses.join(" ")
      }, elementConfig, {
        value: value,
        onChange: changed
      }));
  }

  return Input_jsx("div", {
    className: "Input",
    style: {
      display
    }
  }, Input_jsx("label", {
    className: "Label"
  }, label, (shouldValidate === null || shouldValidate === void 0 ? void 0 : shouldValidate.required) ? Input_jsx("span", {
    style: {
      color: validStyle
    }
  }, " * ") : ""), Input_jsx("div", {
    style: {
      width: "100%"
    }
  }, Input_jsx("div", {
    style: {
      width: "100%",
      display: "flex"
    }
  }, inputElement), Input_jsx("span", {
    className: "input-alert-validation"
  }, titleValidity)));
});
/* harmony default export */ var Input = __webpack_exports__["a"] = (Inputs);

/***/ }),

/***/ "xb3y":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/genres.js


const genres = data => {
  const thead = ["#", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_genres = (genres);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenario.js
var __jsx = external_react_default.a.createElement;


const ShowScenario = scenario => {
  const thead = ["#", "نام", "تاریخ شروع ", " تاریخ پایان", "تعداد برندگان ", "شركت كنندگان", "برندگان", "جوایز", "پوچ ها", "زمان چرخش مجدد (روز)", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < scenario.length; index++) {
    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    tbody.push({
      data: [scenario[index].name, scenario[index].startDate, scenario[index].endDate, scenario[index].winnersCount, {
        option: {
          eye: true,
          name: "participants"
        }
      }, {
        option: {
          eye: true,
          name: "winners"
        }
      }, {
        option: {
          eye: true,
          name: "gifts"
        }
      }, {
        option: {
          eye: true,
          name: "empty"
        }
      }, scenario[index].spinRepeatTime, scenario[index].isActive ? active : deActive, {
        option: {
          edit: true
        }
      }],
      style: {
        background: scenario[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenario = (ShowScenario);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenarioDataInModal.js


const ShowScenarioDataInModal = (data, name) => {
  const NotEntered = "وارد نشده";
  let thead = null;
  const headGift = ["شماره", "نام", "شماره همراه", "جایزه", " تاریخ "];
  const headNotGift = ["شماره", "نام", "شماره همراه", " تاریخ "];
  name === "participants" ? thead = headNotGift : thead = headGift;
  let tbody = [];
  let body = null;

  for (let index = 0; index < data.length; index++) {
    let fullName = data[index].fullName ? data[index].fullName : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let gift = data[index].gift ? data[index].gift : NotEntered;
    let date = data[index].date ? data[index].date : NotEntered;
    const bodyGift = [fullName, phoneNumber, gift, date];
    const bodyNotGift = [fullName, phoneNumber, date];
    name === "participants" ? body = bodyNotGift : body = bodyGift;
    tbody.push({
      data: body,
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenarioDataInModal = (ShowScenarioDataInModal);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowScenarioDataInModalTwo.js


const ShowScenarioDataInModalTwo = (data, name) => {
  const NotEntered = "وارد نشده";
  const thead = ["شماره", name];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    tbody.push({
      data: [data[index] ? data[index] : NotEntered],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowScenarioDataInModalTwo = (ShowScenarioDataInModalTwo);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/owner.js
var owner_jsx = external_react_default.a.createElement;


const owner = data => {
  //console.log({ ownerdata: data });
  const thead = ["#", "عکس", "نام ", "محدوده  ", " شماره همراه", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = owner_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = owner_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : NotEntered;
    let district = data[index].district ? data[index].district : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let balance = data[index].balance ? data[index].balance : "0";
    let isActive = data[index].isActive ? active : deActive; // , { option: { star: true, value: rating } }

    tbody.push({
      data: [thumbnail, title, district, phoneNumber, isActive, {
        option: {
          edit: true,
          remove: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_owner = (owner);
// CONCATENATED MODULE: ./panelAdmin/utils/formatMoney.js
const formatMoney = number => {
  return number === null || number === void 0 ? void 0 : number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

/* harmony default export */ var utils_formatMoney = (formatMoney);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowDiscount.js
var ShowDiscount_jsx = external_react_default.a.createElement;



const ShowDiscount = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = ShowDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = ShowDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let realPrice = data[index].realPrice ? utils_formatMoney(data[index].realPrice) : NotEntered;
    let newPrice = data[index].newPrice ? utils_formatMoney(data[index].newPrice) : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? "%" + data[index].percent : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowDiscount = (ShowDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowClub.js
var ShowClub_jsx = external_react_default.a.createElement;


const ShowClub = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "عضویت ", "دسته بندی ", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let cardElement = ShowClub_jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-credit-card"
    });

    let applicationElement = ShowClub_jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-mobile-android"
    });

    let active = ShowClub_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = ShowClub_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let NotEntered = "وارد نشده";
    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? data[index].percent + " %" : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : "";
    let membership = data[index].membership.length ? data[index].membership : NotEntered;
    let membershipData = membership.includes("CARD") && membership.includes("APPLICATION") ? ShowClub_jsx("div", {
      style: {
        fontSize: "1.2em",
        fontWeight: "900",
        display: "flex",
        justifyContent: "space-around"
      }
    }, " ", ShowClub_jsx("div", null, cardElement, " "), ShowClub_jsx("div", null, " ", applicationElement)) : membership.includes("APPLICATION") ? applicationElement : membership.includes("CARD") ? cardElement : "";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, membershipData, categoryTitleFa, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowClub = (ShowClub);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/ShowTransactions.js
var ShowTransactions_jsx = external_react_default.a.createElement;



const ShowTransactions = data => {
  const thead = ["#", "نام", " شماره همراه ", " تاریخ (زمان) ", " کل مبلغ", "تخفیف", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let NotEntered = "وارد نشده";
    let user = data[index].user ? data[index].user : NotEntered;
    let userName = user ? user.name : NotEntered;
    let userPhoneNumber = user ? user.phoneNumber : NotEntered;
    let date = dateSplit ? ShowTransactions_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = data[index].totalPrice ? utils_formatMoney(data[index].totalPrice) : "0";
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let paymentStatus = data[index].paymentStatus ? ShowTransactions_jsx("span", {
      style: {
        color: "green"
      }
    }, "پرداخت شده") : ShowTransactions_jsx("span", {
      style: {
        color: "red"
      }
    }, " ", "پرداخت نشده");
    tbody.push({
      data: [userName, userPhoneNumber, date, totalPrice, showDiscount, paymentStatus],
      style: {
        background: data[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_ShowTransactions = (ShowTransactions);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/transactionDiscount.js
var transactionDiscount_jsx = external_react_default.a.createElement;



const transactionDiscount = data => {
  // let data =datas.discount
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = transactionDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = transactionDiscount_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let dataIndex = data[index].discount;
    let thumbnail = dataIndex.thumbnail ? dataIndex.thumbnail : NotEntered;
    let title = dataIndex.title ? dataIndex.title : NotEntered;
    let realPrice = dataIndex.realPrice ? utils_formatMoney(dataIndex.realPrice) : NotEntered;
    let newPrice = dataIndex.newPrice ? utils_formatMoney(dataIndex.newPrice) : NotEntered;
    let rating = dataIndex.rating ? dataIndex.rating : 0;
    let percent = dataIndex.percent ? "%" + dataIndex.percent : NotEntered;
    let boughtCount = dataIndex.boughtCount ? dataIndex.boughtCount : "0";
    let category = dataIndex.category ? dataIndex.category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = dataIndex.viewCount ? dataIndex.viewCount : "0";
    let isActive = dataIndex.isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_transactionDiscount = (transactionDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/members.js
var members_jsx = external_react_default.a.createElement;


const members = data => {
  const thead = ["#", "نام", "شماره همراه", "تراکنش ها", "تخفیف ها", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = members_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = members_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    }); // let avatar = data[index].avatar ? data[index].avatar : NotEntered;


    let name = data[index].name ? data[index].name : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let showTransaction = {
      option: {
        eye: true,
        name: "transactions"
      }
    };
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [name, phoneNumber, showTransaction, showDiscount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_members = (members);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/memberTransaction.js
var memberTransaction_jsx = external_react_default.a.createElement;



const memberTransaction = data => {
  //console.log({ data });
  // let data =datas.discount
  const thead = ["#", "تاریخ", "قیمت ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let paymentStatus = data[index].paymentStatus ? memberTransaction_jsx("span", {
      style: {
        color: "green"
      }
    }, "پرداخت شده") : memberTransaction_jsx("span", {
      style: {
        color: "red"
      }
    }, " ", "پرداخت نشده");
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let date = dateSplit ? memberTransaction_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = dataIndex.totalPrice ? utils_formatMoney(dataIndex.totalPrice) : "0";
    tbody.push({
      data: [date, totalPrice, paymentStatus],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_memberTransaction = (memberTransaction);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/memberDiscount.js
var memberDiscount_jsx = external_react_default.a.createElement;



const memberDiscount = data => {
  //console.log({ data });
  // let data =datas.discount
  const thead = ["#", "نام", "قیمت ", "تاریخ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let discount = data[index].discount;
    let discountName = discount.title;
    let dateSplit = data[index].usedDate ? data[index].usedDate.split(" ") : "";
    let date = dateSplit ? memberDiscount_jsx("span", null, dateSplit[1] + " - " + dateSplit[0]) : "استفاده نشده";
    let price = dataIndex.price ? utils_formatMoney(dataIndex.price) : "0";
    let isUsed = dataIndex.isUsed ? "استفاده شده" : "استفاده نشده";
    tbody.push({
      data: [discountName, price, date, isUsed],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ var table_memberDiscount = (memberDiscount);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/country.js


const country = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let flag = data[index].flag ? data[index].flag : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [flag, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_country = (country);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/instrument.js
const instrument = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [thumbnail, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_instrument = (instrument);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/song.js


const song = data => {
  const thead = ["#", "نام ", "پخش "];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let title = data[index].title ? data[index].title : NotEntered;
    tbody.push({
      data: [title, {
        option: {
          play: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_song = (song);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/products.js


const products = data => {
  //console.log({ data });
  const thead = ["#", "عکس", "عنوان", "قیمت اصلی ", " قیمت جدید", "وزن", "تخفیف", "بازدید", "تنظیمات"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let image = data[index].image ? data[index].image : NotEntered;
    let name = data[index].name ? data[index].name : NotEntered;
    let realPrice = data[index].realPrice ? data[index].realPrice : NotEntered;
    let newPrice = data[index].newPrice ? data[index].newPrice : NotEntered;
    let weight = data[index].weight ? data[index].weight : "0";
    let discount = data[index].discount ? data[index].discount : "0";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    tbody.push({
      data: [image, name, realPrice, newPrice, weight, viewCount, discount, {
        option: {
          edit: true,
          remove: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_products = (products);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/store.js
var store_jsx = external_react_default.a.createElement;


const store = data => {
  //console.log({ storedata: data });
  const thead = ["#", "عکس", "نام ", "کمیسیون  ", "اقساط", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = store_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = store_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : NotEntered;
    let commission = data[index].commission ? data[index].commission : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let installments = data[index].installments ? data[index].installments : "0";
    let isActive = data[index].isActive ? active : deActive; // , { option: { star: true, value: rating } }

    tbody.push({
      data: [thumbnail, title, commission, installments, isActive, {
        option: {
          edit: true,
          remove: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_store = (store);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/user.js
var user_jsx = external_react_default.a.createElement;


const user_user = data => {
  //console.log({ userdata: data });
  const thead = ["#", "نام", "شماره همراه  "];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    var _data$index, _data$index2;

    let NotEntered = "وارد نشده";

    let active = user_jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle"
    });

    let deActive = user_jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban"
    });

    let name = ((_data$index = data[index]) === null || _data$index === void 0 ? void 0 : _data$index.name) || "";
    let familyName = ((_data$index2 = data[index]) === null || _data$index2 === void 0 ? void 0 : _data$index2.familyName) || "";
    let fullName = name + " " + familyName;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    if (!name && !familyName) fullName = NotEntered; // , { option: { star: true, value: rating } }

    tbody.push({
      data: [fullName, phoneNumber],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ var table_user = (user_user);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/table/index.js


















const table = {
  instrument: table_instrument,
  country: table_country,
  memberDiscount: table_memberDiscount,
  memberTransaction: table_memberTransaction,
  members: table_members,
  transactionDiscount: table_transactionDiscount,
  showTransaction: table_ShowTransactions,
  ShowClub: table_ShowClub,
  ShowDiscount: table_ShowDiscount,
  owner: table_owner,
  showScenario: table_ShowScenario,
  genres: table_genres,
  ShowScenarioDataInModal: table_ShowScenarioDataInModal,
  ShowScenarioDataInModalTwo: table_ShowScenarioDataInModalTwo,
  song: table_song,
  products: table_products,
  store: table_store,
  user: table_user
};
/* harmony default export */ var consts_table = (table);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addCategory.js
const addCategory_owner = {
  Form: {
    titleFa: {
      label: "نام دسته فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام دسته فارسی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "نام دسته انگلیسی  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام دسته انگلیسی "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var addCategory = (addCategory_owner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addProduct.js
const addArtist = {
  Form: {
    name: {
      label: "نام  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    realPrice: {
      label: "قیمت اصلی :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت اصلی"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    newPrice: {
      label: "قیمت جدید :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت جدید"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    weight: {
      label: "وزن :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "وزن"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    unit: {
      label: "واحد :",
      elementType: "input",
      elementConfig: {
        placeholder: "واحد"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var addProduct = (addArtist);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addGallery.js
const addGallery = {
  Form: {
    imageName: {
      label: "نام عکس :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام عکس"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addGallery = (addGallery);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addOwner.js
const addOwner = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // subTitle: {
    //   label: "توضیحات :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "توضیحات ",
    //   },
    //   value: "",
    //   validation: {
    //     required: false,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    district: {
      label: "محدوده :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "محدوده"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    address: {
      label: "آدرس :",
      elementType: "textarea",
      elementConfig: {
        placeholder: "آدرس"
      },
      value: "",
      validation: {
        required: true,
        minLength: 3
      },
      valid: false,
      touched: false
    },
    phoneNumber: {
      label: "تلفن همراه :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "09xxxxxxxxx"
      },
      value: "",
      validation: {
        required: false // minLength: 11,
        // maxLength: 11,
        // isNumeric: true,
        // isMobile: true,

      },
      valid: true,
      touched: false
    },
    phone: {
      label: "تلفن ثابت :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "013-33333333"
      },
      validation: {
        required: false // minLength: 4,
        // isNumeric: false,

      },
      value: [],
      valid: true,
      touched: false
    },
    // rating: {
    //   label: "امتیاز :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "number",
    //     placeholder: "امتیاز"
    //   },
    //   value: "",
    //   validation: {
    //     minLength: 1,
    //     maxLength: 1,
    //     isNumeric: true,
    //     required: false
    //   },
    //   valid: false,
    //   touched: false
    // },
    thumbnail: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    coordinate: {
      label: "مختصات :",
      elementType: "coordinate",
      kindOf: "image",
      value: {
        lat: "",
        lng: ""
      },
      elementConfig: {
        type: "number",
        placeholder: "مختصات"
      },
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addOwner = (addOwner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addStore.js
const addStore = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    installments: {
      label: "اقساط :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "اقساط "
      },
      value: "",
      validation: {
        required: false,
        isNumeric: true
      },
      valid: true,
      touched: false
    },
    commission: {
      label: "کمیسیون :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "کمیسیون "
      },
      value: "",
      validation: {
        required: false,
        isNumeric: true
      },
      valid: true,
      touched: false
    },
    owner: {
      label: "فروشنده :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشنده"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    areaCode: {
      label: "کد فروشگاه :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "کد فروشگاه "
      },
      value: "",
      validation: {
        minLength: 5,
        required: true // isNumeric: true,

      },
      valid: false,
      touched: false
    },
    // phoneNumber: {
    //   label: "تلفن همراه :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "number",
    //     placeholder: "تلفن همراه",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //     minLength: 11,
    //     maxLength: 11,
    //     isNumeric: true,
    //     isMobile: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    phone: {
      label: "تلفن ثابت :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "013-33333333"
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true
      },
      value: [],
      valid: false,
      touched: false
    },
    aboutStore: {
      label: "درباره فروشگاه :",
      elementType: "textarea",
      elementConfig: {
        placeholder: "درباره فروشگاه"
      },
      value: "",
      validation: {// required: true,
      },
      valid: true,
      touched: true
    },
    features: {
      label: "ویژگی استفاده :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "ویژگی استفاده"
      },
      value: [],
      validation: {// required: true,
      },
      valid: true,
      touched: true
    },
    conditions: {
      label: "شرایط استفاده  :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "شرایط استفاده "
      },
      value: [],
      validation: {// required: true,
      },
      valid: true,
      touched: true
    },
    district: {
      label: "محدوده :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "محدوده"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    slides: {
      label: "اسلاید :",
      elementType: "InputFileArray",
      kindOf: "image",
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    location: {
      label: "مختصات :",
      elementType: "coordinate",
      kindOf: "image",
      value: {
        lat: "",
        lng: ""
      },
      elementConfig: {
        type: "number",
        placeholder: "مختصات :"
      },
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addStore = (addStore);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addSlider.js
const addSlider = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [{
        name: "دسته بندی",
        value: "Category"
      }, {
        name: "فروشگاه",
        value: "Store"
      }],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    store: {
      label: "فروشگاه :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // parent: {
    //   label: "فروشگاه :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "فروشگاه",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    // type: {
    //   label: "ت :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "ت",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addSlider = (addSlider);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addBanner.js
const addBanner = {
  Form: {
    store: {
      label: "فروشگاه :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // type: {
    //   label: "ت :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "ت",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var states_addBanner = (addBanner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addNotification.js
const addNotification_owner = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    content: {
      label: "محتوی  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "محتوی "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var addNotification = (addNotification_owner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/addVersion.js
const version = {
  Form: {
    version: {
      label: "ورژن :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "ورژن"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // link: {
    //   label: "لینک  :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "لینک ",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    isRequired: {
      label: "آیا ضروری میباشد  :",
      elementType: "twoCheckBox",
      elementConfig: {
        type: "checkBox",
        placeholder: "آیا ضروری میباشد "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ var addVersion = (version);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/states/index.js









const states = {
  addCategory: addCategory,
  addProduct: addProduct,
  addGallery: states_addGallery,
  addOwner: states_addOwner,
  addStore: states_addStore,
  addSlider: states_addSlider,
  addBanner: states_addBanner,
  addNotification: addNotification,
  addVersion: addVersion
};
/* harmony default export */ var consts_states = (states);
// EXTERNAL MODULE: ./panelAdmin/index.js + 16 modules
var panelAdmin = __webpack_require__("VGcP");

// CONCATENATED MODULE: ./panelAdmin/utils/consts/galleryConstants.js



const galleryConstants = () => {
  const ConstantsEn = panelAdmin["a" /* default */].values.en;
  const string = panelAdmin["a" /* default */].values.strings;
  return [{
    value: ConstantsEn.ALBUM_CONSTANTS,
    title: string.ALBUM_CONSTANTS
  }, {
    value: ConstantsEn.PLAY_LIST_CONSTANTS,
    title: string.PLAY_LIST_CONSTANTS
  }, {
    value: ConstantsEn.FLAG_CONSTANTS,
    title: string.FLAG_CONSTANTS
  }, {
    value: ConstantsEn.SONG_CONSTANTS,
    title: string.SONG_CONSTANTS
  }, {
    value: ConstantsEn.ARTIST_CONSTANTS,
    title: string.ARTIST_CONSTANTS
  }, {
    value: ConstantsEn.INSTRUMENT_CONSTANTS,
    title: string.INSTRUMENT_CONSTANTS
  }, {
    value: ConstantsEn.MUSIC_VIDEO_CONSTANTS,
    title: string.MUSIC_VIDEO_CONSTANTS
  }, {
    value: ConstantsEn.MOOD_CONSTANTS,
    title: string.MOOD_CONSTANTS
  }];
};

/* harmony default export */ var consts_galleryConstants = (galleryConstants);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/gallery.js
const gallery_instrument = (data, acceptedCard) => {
  const cardFormat = []; //console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let title = dataIndex.title ? dataIndex.title : "";
    let href = dataIndex.href ? dataIndex.href : "";
    let images = href; // //console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard === href ? "activeImage" : "" : "",
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var gallery = (gallery_instrument);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/product.js


const product = data => {
  //console.log({ productCard: data });
  const cardFormat = [];

  for (let index in data) {
    let NO_ENTERED = "وارد نشده";
    let dataIndex = data[index];
    let category = dataIndex.category ? dataIndex.category : "";
    let categoryName = category ? category.name : NO_ENTERED;
    let name = dataIndex.name ? dataIndex.name : NO_ENTERED;
    let unit = dataIndex.unit ? dataIndex.unit : NO_ENTERED;
    let weight = dataIndex.weight ? dataIndex.weight : NO_ENTERED;
    let realPrice = dataIndex.realPrice ? dataIndex.realPrice : NO_ENTERED;
    let newPrice = dataIndex.newPrice ? dataIndex.newPrice : NO_ENTERED;
    let image = dataIndex.image ? dataIndex.image : NO_ENTERED;
    cardFormat.push({
      _id: dataIndex._id,
      // isActive: dataIndex.isActive,
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: name,
          style: {
            color: "black",
            fontSize: "1.3em",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: categoryName,
          title: categoryName,
          style: {
            color: "",
            fontSize: "0.9em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: realPrice,
          direction: "ltr",
          style: {
            color: "red",
            fontSize: "",
            fontWeight: ""
          }
        }],
        right: [{
          elementType: "price",
          value: newPrice,
          direction: "ltr",
          style: {
            color: "green",
            fontSize: "",
            fontWeight: ""
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: unit
        }],
        left: [{
          elementType: "icon",
          value: weight,
          className: "",
          direction: "ltr",
          style: {
            fontSize: "1em",
            fontWeight: "500"
          },
          iconStyle: {
            fontSize: "1.4em"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_product = (product);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/category.js


const category_category = data => {
  const cardFormat = []; //console.log({ data });

  for (let index in data) {
    let dataIndex = data[index];
    let titleFa = dataIndex.titleFa ? dataIndex.titleFa : "";
    let titleEn = dataIndex.titleEn ? dataIndex.titleEn : "";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: titleFa,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        left: [{
          elementType: "text",
          value: titleEn,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_category = (category_category);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/slider.js


const slider = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({
    insss: data
  });

  for (let index in data) {
    let noEntries = "وارد نشده است";
    let dataIndex = data[index];
    let parent = (dataIndex === null || dataIndex === void 0 ? void 0 : dataIndex.parent) || false;
    let parentName = (parent === null || parent === void 0 ? void 0 : parent.titleFa) || (parent === null || parent === void 0 ? void 0 : parent.title) || noEntries;
    let parentType = (dataIndex === null || dataIndex === void 0 ? void 0 : dataIndex.parentType) || "وارد نشده";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard === image ? "activeImage" : "" : "",
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: parentName,
          style: {
            color: "#828181",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        left: [{
          elementType: "text",
          value: panelAdmin["a" /* default */].utils.dictionary(parentType),
          style: {
            color: "#147971",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_slider = (slider);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/banner.js
const banner = (data, acceptedCard) => {
  const cardFormat = []; //console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let NO_ENTERED = "وارد نشده";
    let type = dataIndex.type ? dataIndex.type : NO_ENTERED;
    let store = dataIndex.store ? dataIndex.store : false;
    let title = store.title ? store.title : NO_ENTERED;
    let image = dataIndex.image ? dataIndex.image : NO_ENTERED; // //console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard === image ? "activeImage" : "" : "",
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_banner = (banner);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/version.js


const version_version = data => {
  const cardFormat = []; //console.log({ data });

  for (let index in data) {
    let dataIndex = data[index];
    let link = dataIndex.link ? dataIndex.link : "";
    let version = dataIndex.version ? dataIndex.version : "";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: false,
      body: [{
        right: [{
          elementType: "text",
          value: link,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        left: [{
          elementType: "text",
          value: version,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ var card_version = (version_version);
// CONCATENATED MODULE: ./panelAdmin/utils/consts/card/index.js
// import instrument from "./instrument";
 // import country from "./country";
// import mood from "./mood";
// import hashtag from "./hashtag";
// import genre from "./genre";






const card = {
  category: card_category,
  // instrument,
  gallery: gallery,
  // country,
  // mood,
  // hashtag,
  product: card_product,
  slider: card_slider,
  banner: card_banner,
  version: card_version // genre

};
/* harmony default export */ var consts_card = (card);
// EXTERNAL MODULE: ./panelAdmin/utils/consts/modal/index.js
var modal = __webpack_require__("tMBA");
var modal_default = /*#__PURE__*/__webpack_require__.n(modal);

// CONCATENATED MODULE: ./panelAdmin/utils/consts/index.js





const consts = {
  table: consts_table,
  states: consts_states,
  galleryConstants: consts_galleryConstants,
  card: consts_card,
  modal: modal_default.a
};
/* harmony default export */ var utils_consts = (consts);
// EXTERNAL MODULE: ./panelAdmin/utils/toastify.js
var toastify = __webpack_require__("o6Tf");

// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/convertToCelander.js
const convertToCelander = value => {
  let valDate,
      day,
      month,
      year,
      selectedDay = null;

  if (value) {
    valDate = value.split("/");
    day = Number(valDate[2]);
    month = Number(valDate[1]);
    year = Number(valDate[0]);
    selectedDay = {
      day,
      month,
      year
    };
  }

  return selectedDay;
};

/* harmony default export */ var CelanderConvert_convertToCelander = (convertToCelander);
// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/convertToDate.js
const convertToDate = selectedDay => {
  let day = selectedDay.day;
  let month = selectedDay.month;
  let year = selectedDay.year;
  let date = year + "/" + month + "/" + day;
  return date;
};

/* harmony default export */ var CelanderConvert_convertToDate = (convertToDate);
// CONCATENATED MODULE: ./panelAdmin/utils/CelanderConvert/index.js


const CelanderConvert = {
  convertToCelander: CelanderConvert_convertToCelander,
  convertToDate: CelanderConvert_convertToDate
};
/* harmony default export */ var utils_CelanderConvert = (CelanderConvert);
// CONCATENATED MODULE: ./panelAdmin/utils/checkValidity/minLengthValidity.js
const minLengthValidity = ({
  array,
  beforeValue,
  value,
  rules
}) => {
  let minLength = true;

  if (array) {
    beforeValue.map(val => {
      console.log({
        minLengthVal: val
      });
      return minLength = val.length >= rules.minLength && minLength;
    });
  } else minLength = value.length >= rules.minLength && minLength;

  console.log({
    minLength,
    array,
    beforeValue,
    value,
    rules
  });
  return minLength;
};

/* harmony default export */ var checkValidity_minLengthValidity = (minLengthValidity);
// CONCATENATED MODULE: ./panelAdmin/utils/checkValidity/maxLengthValidity.js
const maxLengthValidity = ({
  array,
  beforeValue,
  value,
  rules
}) => {
  let maxLength = true;
  if (array) beforeValue.map(val => maxLength = val.length >= rules.maxLength && maxLength);else maxLength = value.length <= rules.maxLength && maxLength;
  return maxLength;
};

/* harmony default export */ var checkValidity_maxLengthValidity = (maxLengthValidity);
// CONCATENATED MODULE: ./panelAdmin/utils/checkValidity/numberValidity.js
const numberValidity = ({
  array,
  beforeValue,
  value
}) => {
  let isNumeric = true;
  const pattern = /^\d+$/;
  if (array) beforeValue.map(val => {
    // console.log("number:", { val });
    return isNumeric = pattern.test(val) && isNumeric;
  });else isNumeric = pattern.test(value) && isNumeric;
  return isNumeric;
};

/* harmony default export */ var checkValidity_numberValidity = (numberValidity);
// CONCATENATED MODULE: ./panelAdmin/utils/checkValidity/index.js



const checkValidity_checkValidity = (value, rules, array, beforeValue) => {
  let isValid = true;
  let errorTitle = false;
  let object = true;
  let checkValid = true;
  console.log({
    moojValid: {
      value,
      rules,
      array,
      beforeValue
    }
  }); // console.log({ moooojValidtypeof: typeof value });

  if (!rules.required) return {
    isValid,
    errorTitle
  };

  if (!rules) {
    return {
      isValid,
      errorTitle
    };
  }

  if (array) {
    isValid = beforeValue.length ? true : false; // console.log({ mooj: beforeValue.length ? true : false });

    if (!isValid) errorTitle = "خالی بودن فیلد مجاز نمی باشد";
  } else {
    if (rules.required) {
      if (typeof value === "object") {
        for (const key in value) {
          var _value$key, _value$key$toString;

          object = ((_value$key = value[key]) === null || _value$key === void 0 ? void 0 : (_value$key$toString = _value$key.toString()) === null || _value$key$toString === void 0 ? void 0 : _value$key$toString.trim()) !== "" && object;
        }

        if (!object) errorTitle = "خالی بودن فیلد مجاز نمی باشد";
        isValid = object && isValid;
      } else {
        isValid = value.trim() !== "" && isValid;
        errorTitle = value.trim() === "" && ".خالی بودن فیلد مجاز نمی باشد";
      }
    }
  }

  if (isValid) {
    if (rules.minLength) {
      checkValid = checkValidity_minLengthValidity({
        array,
        beforeValue,
        value,
        rules
      });
      console.log({
        checkValid
      });
      if (!checkValid) errorTitle = ` کمترین مقدار مجاز فیلد ${rules.minLength} کلمه یا رقم می باشد `;
      isValid = isValid && checkValid;
    }

    if (rules.maxLength) {
      checkValid = checkValidity_maxLengthValidity({
        array,
        beforeValue,
        value,
        rules
      });
      if (!checkValid) errorTitle = ` بیشترین مقدار مجاز فیلد ${rules.maxLength} کلمه یا رقم می باشد `;
      isValid = isValid && checkValid;
    }

    if (rules.isNumeric) {
      checkValid = checkValidity_numberValidity({
        array,
        beforeValue,
        value
      });
      if (!checkValid) errorTitle = "فقط شماره مجاز می باشد";
      isValid = isValid && checkValid;
    }

    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    if (rules.isPhone) {
      const pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im; // /^0\d{2,3}-\d{8}|\d{8}$/ regex(/^0\d{2,3}-\d{8}|\d{8}$/)

      isValid = pattern.test(value) && isValid;
    }

    if (rules.isMobile) {
      const pattern = /^09[\w]{9}$/;
      if (array) beforeValue.map(val => isValid = pattern.test(val) && isValid); // const pattern = /[0,9]{2}\d{9}/g;
      // const pattern = /^(+98|0)?9\d{9}$/;
      else isValid = pattern.test(value) && isValid;
    }

    if (rules.isEn) {
      const pattern = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
      if (array) beforeValue.map(val => isValid = pattern.test(val) && isValid);else isValid = pattern.test(value) && isValid;
    }

    if (rules.isFa) {
      const pattern = /^[\u0600-\u06FF\s]+$/;
      if (array) beforeValue.map(val => isValid = pattern.test(val) && isValid);else isValid = pattern.test(value) && isValid;
    }
  } // //console.log({ mooojisValid: isValid });


  return {
    isValid,
    errorTitle
  };
};
// EXTERNAL MODULE: ./panelAdmin/utils/updateObject.js
var utils_updateObject = __webpack_require__("ge5p");

// EXTERNAL MODULE: ./panelAdmin/api/index.js + 39 modules
var api = __webpack_require__("FRaV");

// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/validUpload/voicevalid.js
let formats = ["mp3", "voice", "audio"];

const voiceValid = type => {
  let valid = formats.map(format => {
    if (type.includes(format)) return true;else return false;
  });
  let finalValid = valid.includes(true) ? true : false;
  return finalValid;
};

/* harmony default export */ var voicevalid = (voiceValid);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/uploadChange.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const uploadChange = async props => {
  const {
    event,
    setLoading,
    imageType,
    setState,
    valid,
    fileName,
    dispatch
  } = props; //console.log({ eventUpload: event });

  let files = event.files[0]; //console.log({ imageType, fileName });

  let returnData = false;

  if (files) {
    switch (valid) {
      case "image":
        if (files.type.includes("image")) {
          if (fileName) {
            if ( // dispatch(sagaActions.uploadImageData({ data: files, imageType, fileName }))
            await api["post"].imageUpload(files, setLoading, setState, fileName)) returnData = fileName;
          } else {
            Object(toastify["a" /* default */])("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      case "video":
        if (files.type.includes("video")) returnData = await api["post"].videoUpload(files, setLoading, imageType, setState);
        break;

      case "voice":
        if (voicevalid(files.type)) {
          if (fileName) {
            if (await api["post"].voiceUpload(files, setLoading, imageType, setState, fileName)) returnData = fileName;
          } else {
            Object(toastify["a" /* default */])("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      default:
        Object(toastify["a" /* default */])("فایل شما نباید " + files.type + " باشد", "error");
        break;
    }
  } //console.log({ files: files, returnData, fileName });


  if (!returnData && files && (valid === "image" ? fileName : true)) Object(toastify["a" /* default */])("فایل شما نباید " + files.type + " باشد", "error");
  setState(prev => _objectSpread({}, prev, {
    progressPercentImage: null,
    progressPercentVideo: null,
    progressPercentSongs: null
  }));
  return returnData;
};

/* harmony default export */ var onChanges_uploadChange = (uploadChange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/handelOnchange.js
function handelOnchange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function handelOnchange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { handelOnchange_ownKeys(Object(source), true).forEach(function (key) { handelOnchange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { handelOnchange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function handelOnchange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const handelOnchange = async ({
  event,
  data,
  setData,
  setState,
  setLoading,
  imageType,
  validName,
  checkValidity,
  updateObject,
  uploadChange,
  fileName
}) => {
  let changeValid,
      updatedForm,
      updatedFormElement = {};
  let formIsValid = true;
  let value = data.Form[event.name].value;

  let update = handelOnchange_objectSpread({}, value);

  let eventValue = event.value;
  let typeofData = typeof value;
  let isArray, isObject, isString;
  typeofData === "object" && value != undefined ? value && value.length >= 0 ? isArray = true : isObject = true : isString = true;

  const remove = index => value.splice(index, 1)[0];

  const push = (val, newVal) => newVal != undefined ? val.push(newVal) : "";

  if (event.type === "file") {
    const uploadFile = await uploadChange({
      event,
      setLoading,
      imageType,
      setState,
      valid: data.Form[event.name].kindOf,
      fileName
    });

    if (uploadFile) {
      if (isArray) value.includes(uploadFile) ? remove(value.findIndex(d => d === uploadFile)) : push(value, uploadFile);else value = uploadFile;
    } else return;
  } else if (isArray) value.includes(eventValue) ? remove(value.findIndex(d => d === eventValue)) : push(value, eventValue);else if (isObject) {
    if (event.child) {
      value[event.child] = eventValue;
    } else value = eventValue;
  } else if (isString) value = eventValue;

  let checkValidValue;
  if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value; // ////console.log({ moooooj: checkValidity(checkValidValue, data.Form[event.name].validation, isArray).isValid });
  // console.log({ checkValidValue, eventValue, value, typeofData: typeofData, checkEvent: data.Form[event.name] });

  updatedFormElement[event.name] = updateObject(data.Form[event.name], {
    value: value,
    valid: data.Form[event.name].validation.required ? typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).isValid : true,
    touched: true,
    block: false,
    titleValidity: checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).errorTitle
  });

  if (validName) {
    changeValid = updateObject(data.Form[validName], {
      block: true
    });
    updatedForm = updateObject(data.Form, handelOnchange_objectSpread({}, updatedFormElement, {
      [validName]: changeValid
    }));
  } else updatedForm = updateObject(data.Form, handelOnchange_objectSpread({}, updatedFormElement));

  console.log({
    updatedFormElement,
    updatedForm
  });

  for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid || updatedForm[name].block && formIsValid;

  return setData({
    Form: updatedForm,
    formIsValid: formIsValid
  }); // setData({ Form: updatedForm, formIsValid: formIsValid });
};

/* harmony default export */ var onChanges_handelOnchange = (handelOnchange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/arrayOnchange.js
function arrayOnchange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function arrayOnchange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { arrayOnchange_ownKeys(Object(source), true).forEach(function (key) { arrayOnchange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { arrayOnchange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function arrayOnchange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const arrayOnchange = async props => {
  const {
    event: e,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity,
    updateObject,
    uploadChange
  } = props; // //console.log({ validName });

  let changeValid,
      updatedForm,
      updatedFormElement = {};
  e.map(async event => {
    let value = data.Form[event.name].value;

    let update = arrayOnchange_objectSpread({}, value);

    let eventValue = event.value;
    let typeofData = typeof value;
    let isArray, isObject, isString;
    typeofData === "object" && value != null ? value.length >= 0 ? isArray = true : isObject = true : isString = true;

    const remove = index => value.splice(index, 1)[0];

    const push = (val, newVal) => newVal != undefined ? val.push(newVal) : ""; // //console.log({ eventValue, value });
    // if (event.type === "file") {
    //   const uploadFile = await uploadChange(event, setLoading, imageType, setState);
    //   if (uploadFile) {
    //     if (isArray) value.includes(uploadFile) ? remove(value.findIndex((d) => d === uploadFile)) : push(value, uploadFile);
    //     else value = uploadFile;
    //   } else return;
    // } else if (isArray) value.includes(eventValue) ? remove(value.findIndex((d) => d === eventValue)) : push(value, eventValue);
    // else if (isObject) {
    //   value = eventValue;
    //   if (event.child) value = update[event.child] = eventValue;
    // } else if (isString)


    value = eventValue;
    let checkValidValue;
    if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value;
    updatedFormElement[event.name] = updateObject(data.Form[event.name], {
      value: value,
      valid: data.Form[event.name].validation.required ? typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).isValid : true,
      touched: true,
      titleValidity: checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).errorTitle
    });

    if (validName) {
      changeValid = updateObject(data.Form[validName], {
        valid: true
      });
      updatedForm = updateObject(data.Form, arrayOnchange_objectSpread({}, updatedFormElement, {
        [validName]: changeValid
      }));
    } else updatedForm = updateObject(data.Form, arrayOnchange_objectSpread({}, updatedFormElement)); // //console.log({ updatedFormElement, updatedForm });


    let formIsValid = false;

    for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid || updatedForm[name].block && formIsValid;

    return setData({
      Form: updatedForm,
      formIsValid: formIsValid
    });
  });
};

/* harmony default export */ var onChanges_arrayOnchange = (arrayOnchange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/globalChange.js






const globalChange = async props => {
  const {
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    fileName,
    dispatch
  } = props;
  let typeCheck = typeof event; // //console.log(typeCheck === "object" && event.length > 0);
  //console.log({ event });

  if (typeCheck === "object" && event.length > 0) return onChanges_arrayOnchange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: checkValidity_checkValidity,
    updateObject: utils_updateObject["a" /* default */],
    uploadChange: onChanges_uploadChange,
    fileName,
    dispatch
  });else if (typeCheck === "object") return onChanges_handelOnchange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: checkValidity_checkValidity,
    updateObject: utils_updateObject["a" /* default */],
    uploadChange: onChanges_uploadChange,
    fileName,
    dispatch
  });
};

/* harmony default export */ var onChanges_globalChange = (globalChange);
// CONCATENATED MODULE: ./panelAdmin/utils/onChanges/index.js


const onChanges = {
  globalChange: onChanges_globalChange,
  arrayOnchange: onChanges_arrayOnchange
};
/* harmony default export */ var utils_onChanges = (onChanges);
// CONCATENATED MODULE: ./panelAdmin/utils/handleKey.js
const handleKey = event => {
  const form = event.target.form;
  const index = Array.prototype.indexOf.call(form, event.target);
  let keyCode = event.keyCode;
  let numberAccepted = [8, 13];

  if (numberAccepted.includes(keyCode)) {
    if (keyCode === 13) {
      if (form.elements[index + 1]) return form.elements[index + 1].focus();
    } else if (keyCode === 8) if (form.elements[index].value) return form.elements[index].value.length - 1; // else if (form.elements[index - 1]) form.elements[index - 1].focus();


    event.preventDefault();
  }
};

/* harmony default export */ var utils_handleKey = (handleKey);
// EXTERNAL MODULE: ./panelAdmin/utils/json/table/category.json
var table_category = __webpack_require__("bFXO");

// EXTERNAL MODULE: ./panelAdmin/utils/json/table/gallery.json
var table_gallery = __webpack_require__("7KhZ");

// CONCATENATED MODULE: ./panelAdmin/utils/json/table/index.js


const table_table = {
  category: table_category,
  gallery: table_gallery
};
/* harmony default export */ var json_table = (table_table);
// CONCATENATED MODULE: ./panelAdmin/utils/json/index.js

const json = {
  table: json_table
};
/* harmony default export */ var utils_json = (json);
// CONCATENATED MODULE: ./panelAdmin/utils/operation/submitted.js
function submitted_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function submitted_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { submitted_ownKeys(Object(source), true).forEach(function (key) { submitted_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { submitted_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function submitted_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const submitted = async props => {
  const {
    removeKey,
    editKey,
    setSubmitLoading,
    data,
    editData,
    put,
    post,
    setData,
    states,
    setEdit,
    propsHideModal,
    setCheckSubmitted,
    checkSubmitted
  } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};

  for (let formElementIdentifier in data.Form) {
    if (formElementIdentifier === "isRequired") {
      formData[formElementIdentifier] = data.Form[formElementIdentifier].value === "true" ? true : false;
    } else if (formElementIdentifier === (editKey === null || editKey === void 0 ? void 0 : editKey.beforeKey)) formData[editKey === null || editKey === void 0 ? void 0 : editKey.afterKey] = data.Form[formElementIdentifier].value;else if (formElementIdentifier !== removeKey) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
  }

  console.log({
    formData
  });

  if (editData) {
    if (await put({
      id: editData._id,
      data: formData
    })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post(formData)) setData(submitted_objectSpread({}, states));

  if (states.Form["phone"]) {
    states.Form["phone"].value = [];
  }

  if (states.Form["coordinate"]) {
    states.Form["coordinate"].value = {
      lat: "",
      lng: ""
    };
  }

  if (states.Form["location"]) {
    states.Form["location"].value = {
      lat: "",
      lng: ""
    };
  }

  if (states.Form["slides"]) {
    states.Form["slides"].value = [];
  }

  setSubmitLoading(false);
};

/* harmony default export */ var operation_submitted = (submitted);
// CONCATENATED MODULE: ./panelAdmin/utils/operation/FormManagement.js
function FormManagement_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function FormManagement_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { FormManagement_ownKeys(Object(source), true).forEach(function (key) { FormManagement_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { FormManagement_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function FormManagement_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const FormManagement = ({
  categoryData,
  formElement,
  showModal,
  inputChangedHandler,
  accept,
  OwnerData,
  removeHandel,
  staticTitle,
  storeData
}) => {
  let disabled, staticTitleValue;
  let value = formElement.config.value;
  if (formElement.id === (staticTitle === null || staticTitle === void 0 ? void 0 : staticTitle.name)) staticTitleValue = staticTitle === null || staticTitle === void 0 ? void 0 : staticTitle.value;
  let key = formElement.id;
  let elementType = formElement.config.elementType;
  let elementConfig = formElement.config.elementConfig;

  let remove = index => removeHandel(index, formElement.id);

  let label = formElement.config.label;
  let titleValidity = formElement.config.titleValidity;
  let dataChunk = {
    titleValidity,
    value,
    key,
    elementType,
    elementConfig,
    removeHandel: remove,
    label,
    disabled,
    staticTitle: staticTitleValue
  };

  switch (formElement.id) {
    case "slides":
      disabled = true;
      return FormManagement_objectSpread({}, dataChunk, {
        disabled: true,
        accepted: () => showModal({
          kindOf: "showGallery",
          name: formElement.id
        })
      });
    // break;

    case "image":
      disabled = true;
      return FormManagement_objectSpread({}, dataChunk, {
        disabled: true,
        accepted: () => showModal({
          kindOf: "showGallery",
          name: formElement.id
        })
      });
    // break;

    case "thumbnail":
      disabled = true;
      return FormManagement_objectSpread({}, dataChunk, {
        disabled: true,
        accepted: () => showModal({
          kindOf: "showGallery",
          name: formElement.id
        })
      });

    case "isRequire":
      accepted = value => inputChangedHandler({
        value: value || "",
        name: formElement.id
      });

    case "coordinate":
      return FormManagement_objectSpread({}, dataChunk, {
        changed: value => {
          //console.log({ value });
          inputChangedHandler({
            value: value,
            name: formElement.id
          });
        }
      });

    case "location":
      return FormManagement_objectSpread({}, dataChunk, {
        changed: value => {
          //console.log({ value });
          inputChangedHandler({
            value: value,
            name: formElement.id
          });
        }
      });
    // break;

    case "category":
      return FormManagement_objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: categoryData
      });
    // break;

    case "owner":
      return FormManagement_objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: OwnerData
      });
    // break;

    case "store":
      return FormManagement_objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: storeData
      });
    // break;

    case "parent":
      return FormManagement_objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: storeData
      });
    // break;

    default:
      return FormManagement_objectSpread({}, dataChunk, {
        accepted: value => inputChangedHandler({
          value: value,
          name: formElement.id
        }),
        changed: e => {
          var _e$currentTarget, _e$currentTarget2, _e$currentTarget3;

          return inputChangedHandler({
            value: (_e$currentTarget = e.currentTarget) === null || _e$currentTarget === void 0 ? void 0 : _e$currentTarget.value,
            name: formElement.id,
            type: (_e$currentTarget2 = e.currentTarget) === null || _e$currentTarget2 === void 0 ? void 0 : _e$currentTarget2.type,
            files: (_e$currentTarget3 = e.currentTarget) === null || _e$currentTarget3 === void 0 ? void 0 : _e$currentTarget3.files
          });
        }
      });
  }
};

/* harmony default export */ var operation_FormManagement = (FormManagement);
// CONCATENATED MODULE: ./panelAdmin/utils/operation/formTouchChange.js
function formTouchChange_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function formTouchChange_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { formTouchChange_ownKeys(Object(source), true).forEach(function (key) { formTouchChange_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { formTouchChange_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function formTouchChange_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const formTouchChange = ({
  data,
  setData
}) => {
  const updateObject = panelAdmin["a" /* default */].utils.updateObject;
  const checkValidity = panelAdmin["a" /* default */].utils.checkValidity;
  let changeValid,
      updatedForm,
      updatedFormElement = {};

  for (const key in data.Form) {
    let value = data.Form[key].value;
    let typeofData = typeof value;
    let isArray, isObject, isString;
    typeofData === "object" && value != undefined ? value && value.length >= 0 ? isArray = true : isObject = true : isString = true; // console.log({ data }, isArray, isObject, isString);

    updatedFormElement[key] = updateObject(data.Form[key], {
      touched: true,
      titleValidity: checkValidity(data.Form[key].value, data.Form[key].validation, isArray, data.Form[key].value).errorTitle
    });
  }

  updatedForm = updateObject(data.Form, formTouchChange_objectSpread({}, updatedFormElement));
  return setData({
    Form: updatedForm,
    formIsValid: false
  });
};

/* harmony default export */ var operation_formTouchChange = (formTouchChange);
// CONCATENATED MODULE: ./panelAdmin/utils/operation/index.js



const operation = {
  submitted: operation_submitted,
  FormManagement: operation_FormManagement,
  formTouchChange: operation_formTouchChange
};
/* harmony default export */ var utils_operation = (operation);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__("vmXh");
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);

// CONCATENATED MODULE: ./panelAdmin/utils/authorization.js


const authorization = () => {
  return external_js_cookie_default.a.get("SafirAdminToken") !== undefined;
};

/* harmony default export */ var utils_authorization = (authorization);
// EXTERNAL MODULE: ./panelAdmin/utils/adminHoc/WithErrorHandler/index.js + 1 modules
var WithErrorHandler = __webpack_require__("HaeA");

// CONCATENATED MODULE: ./panelAdmin/utils/adminHoc/index.js

const adminHoc = {
  WithErrorHandler: WithErrorHandler["a" /* default */]
};
/* harmony default export */ var utils_adminHoc = (adminHoc);
// CONCATENATED MODULE: ./panelAdmin/utils/dictionary/index.js
const dictionary = text => {
  console.log({
    text
  });
  let translated;
  let lowerText = (text === null || text === void 0 ? void 0 : text.toLowerCase()) || "";

  switch (lowerText) {
    case "product":
      translated = "محصول";
      break;

    case "category":
      translated = "دسته بندی";
      break;

    case "store":
      translated = "فروشگاه";
      break;

    default:
      translated = text;
      break;
  }

  return translated;
};

/* harmony default export */ var utils_dictionary = (dictionary);
// CONCATENATED MODULE: ./panelAdmin/utils/index.js













const utils = {
  dictionary: utils_dictionary,
  adminHoc: utils_adminHoc,
  checkValidity: checkValidity_checkValidity,
  authorization: utils_authorization,
  operation: utils_operation,
  consts: utils_consts,
  toastify: toastify["a" /* default */],
  CelanderConvert: utils_CelanderConvert,
  onChanges: utils_onChanges,
  handleKey: utils_handleKey,
  formatMoney: utils_formatMoney,
  json: utils_json,
  updateObject: utils_updateObject["a" /* default */]
};
/* harmony default export */ var panelAdmin_utils = __webpack_exports__["a"] = (utils);

/***/ }),

/***/ "zr5I":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })

/******/ });