module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../ssr-module-cache.js');
/******/
/******/ 	// object to store loaded chunks
/******/ 	// "0" means "already loaded"
/******/ 	var installedChunks = {
/******/ 		"static/development/pages/panelAdmin/banner.js": 0
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// require() chunk loading for javascript
/******/
/******/ 		// "0" is the signal for "already loaded"
/******/ 		if(installedChunks[chunkId] !== 0) {
/******/ 			var chunk = require("../../../../" + ({}[chunkId]||chunkId) + ".js");
/******/ 			var moreModules = chunk.modules, chunkIds = chunk.ids;
/******/ 			for(var moduleId in moreModules) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 			for(var i = 0; i < chunkIds.length; i++)
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// uncaught error handler for webpack runtime
/******/ 	__webpack_require__.oe = function(err) {
/******/ 		process.nextTick(function() {
/******/ 			throw err; // catch this error by using import().catch()
/******/ 		});
/******/ 	};
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "../next-server/lib/router-context":
/*!**************************************************************!*\
  !*** external "next/dist/next-server/lib/router-context.js" ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/router-context.js");

/***/ }),

/***/ "../next-server/lib/utils":
/*!*****************************************************!*\
  !*** external "next/dist/next-server/lib/utils.js" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "./_context/index.js":
/*!***************************!*\
  !*** ./_context/index.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/_context/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

/* harmony default export */ __webpack_exports__["default"] = ((reducer, actions, defaultValue) => {
  const Context = react__WEBPACK_IMPORTED_MODULE_0___default.a.createContext();

  const Provider = ({
    children
  }) => {
    const {
      0: state,
      1: dispatch
    } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useReducer"])(reducer, defaultValue);
    const boundActions = {};

    for (let key in actions) {
      boundActions[key] = actions[key](dispatch);
    }

    return __jsx(Context.Provider, {
      value: {
        state,
        dispatch: boundActions
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14,
        columnNumber: 12
      }
    }, children);
  };

  return {
    Context,
    Provider
  };
});

/***/ }),

/***/ "./_context/reducer/index.js":
/*!***********************************!*\
  !*** ./_context/reducer/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _panelAdminReducer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./panelAdminReducer */ "./_context/reducer/panelAdminReducer/index.js");
/* harmony import */ var _webSiteReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./webSiteReducer */ "./_context/reducer/webSiteReducer/index.js");


const reducer = {
  panelAdminReducer: _panelAdminReducer__WEBPACK_IMPORTED_MODULE_0__["default"],
  webSiteReducer: _webSiteReducer__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (reducer);

/***/ }),

/***/ "./_context/reducer/panelAdminReducer/index.js":
/*!*****************************************************!*\
  !*** ./_context/reducer/panelAdminReducer/index.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _optionReducer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./optionReducer */ "./_context/reducer/panelAdminReducer/optionReducer.js");

const panelAdminReducer = {
  optionReducerProvider: _optionReducer__WEBPACK_IMPORTED_MODULE_0__["Provider"],
  optionReducerContext: _optionReducer__WEBPACK_IMPORTED_MODULE_0__["Context"]
};
/* harmony default export */ __webpack_exports__["default"] = (panelAdminReducer);

/***/ }),

/***/ "./_context/reducer/panelAdminReducer/optionReducer.js":
/*!*************************************************************!*\
  !*** ./_context/reducer/panelAdminReducer/optionReducer.js ***!
  \*************************************************************/
/*! exports provided: optionReducer, Provider, Context */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "optionReducer", function() { return optionReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Provider", function() { return Provider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Context", function() { return Context; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../ */ "./_context/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // ========================================================  REDUCER

const optionReducer = (state, action) => {
  switch (action.type) {
    case "SET_PAGE_NAME":
      return _objectSpread({}, state, {
        pageName: action.payload
      });

    default:
      return state;
  }
}; // ========================================================  DISPATCH

const changePageName = dispatch => title => {
  // //console.log({ dispatch, title });
  dispatch({
    type: "SET_PAGE_NAME",
    payload: title
  });
};

const {
  Provider,
  Context
} = Object(___WEBPACK_IMPORTED_MODULE_0__["default"])(optionReducer, {
  changePageName
}, {
  pageName: ""
});

/***/ }),

/***/ "./_context/reducer/webSiteReducer/index.js":
/*!**************************************************!*\
  !*** ./_context/reducer/webSiteReducer/index.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const webSiteReducer = {};
/* harmony default export */ __webpack_exports__["default"] = (webSiteReducer);

/***/ }),

/***/ "./components/LazyImage/index.js":
/*!***************************************!*\
  !*** ./components/LazyImage/index.js ***!
  \***************************************/
/*! exports provided: LazyImage, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LazyImage", function() { return LazyImage; });
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/components/LazyImage/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

const LazyImage = ({
  src,
  alt,
  defaultImage,
  imageOnload,
  style
}) => {
  let placeHolder = defaultImage || "https://www.novok.com/storage/themes/novok/img/noimg.jpg";
  let placeHolderTwo = "https://www.chd.mhrd.gov.in/sites/default/files/no-image-available.jpg";
  const {
    0: imageSrc,
    1: setImageSrc
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(placeHolder); //   const [imageRef, setImageRef] = useState();
  //   //console.log({ imageSrc });

  const imageRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);

  const onLoad = event => {
    //console.log({ onLoad: event });
    event.target.classList.add("loaded"); // imageOnload();
  };

  const onError = event => {
    //console.log({ error: event });
    event.target.classList.remove("loaded");
    event.target.classList.add("has-error");
  }; // useEffect(() => {
  //   imageRef.current.addEventListener("load", onLoad);
  // });


  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    imageRef.current.classList.remove("has-error"); // imageRef.current.classList.add("loaded");

    let observer;
    let didCancel = false; // //console.log("avaaaaaaaaaaaaaaaaz shooood");

    if (imageRef.current && imageSrc !== src) {
      if (IntersectionObserver) {
        observer = new IntersectionObserver(entries => {
          // //console.log({ observer, entries });
          entries.forEach(entry => {
            if (!didCancel && (entry.intersectionRatio > 0 || entry.isIntersecting)) {
              // imageRef.current.classList.add("loaded");
              imageRef.current.classList.remove("has-error");
              setTimeout(() => {
                setImageSrc(src);
              }, 200);
              observer.unobserve(imageRef.current) && observer.unobserve(imageRef.current);
            }
          });
        }, {
          threshold: [0.009999999776482582],
          rootMargin: "75%"
        });
        observer.observe(imageRef.current);
      } else {
        imageRef.current.classList.add("loaded");
        imageRef.current.classList.remove("has-error"); // Old browsers fallback

        setTimeout(() => {
          setImageSrc(src);
        }, 200);
      }
    }

    return () => {
      didCancel = true; // on component cleanup, we remove the listner

      if (observer && observer.unobserve) {
        observer.unobserve(imageRef.current);
      }
    };
  }, [src, imageSrc, imageRef.current]); //   return <Image ref={setImageRef} src={imageSrc} alt={alt} onLoad={onLoad} onError={onError} />;

  return __jsx(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 5
    }
  }, __jsx("img", {
    style: style,
    ref: imageRef,
    src: imageSrc,
    onLoad: onLoad,
    alt: alt,
    onError: onError,
    className: styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a.dynamic([["1496798165", [placeHolder, placeHolderTwo, placeHolderTwo, placeHolder]]]) + " " + "imageComponent",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 7
    }
  }), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "1496798165",
    dynamic: [placeHolder, placeHolderTwo, placeHolderTwo, placeHolder],
    __self: undefined
  }, `.imageComponent.__jsx-style-dynamic-selector{display:block;}.imageComponent.loaded.__jsx-style-dynamic-selector{-webkit-animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;}.imageComponent.has-error.__jsx-style-dynamic-selector{content:url(${placeHolderTwo});width:100%;height:100%;-webkit-animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;}.imageComponent.has-error.__jsx-style-dynamic-selector::not(.loaded){content:url(${placeHolderTwo});width:100%;height:100%;-webkit-animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;}.imageComponent.__jsx-style-dynamic-selector::not(.loaded),.imageComponent.__jsx-style-dynamic-selector::not(.has-error){content:url(${placeHolder});width:100%;height:100%;-webkit-animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;animation:loaded-__jsx-style-dynamic-selector 0.7s ease-in-out;}.image-lazyloading-container.__jsx-style-dynamic-selector{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;width:100%;height:100%;background-color:#ccc;}@-webkit-keyframes loaded-__jsx-style-dynamic-selector{0%{opacity:0.5;}100%{opacity:1;}}@keyframes loaded-__jsx-style-dynamic-selector{0%{opacity:0.5;}100%{opacity:1;}}
/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL21vanRhYmEvZGV2L3Byb2plY3RzL3NhZmlyL2NvbXBvbmVudHMvTGF6eUltYWdlL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQTRFa0IsQUFHeUIsQUFHb0IsQUFTUSxBQU1BLEFBT0EsQUFPN0IsQUFTQyxBQUdGLFVBQ1osRUFIQSxFQXpDRiw0QkFZYSxBQU1BLEFBT0EsV0FaQyxBQU1BLEFBT0EsWUFac0IsQUFNQSxBQU9BLFNBS1gsNERBN0J6Qix1Q0E4QnFCLDBCQWxCckIsQUFNQSxBQU9BLG1FQU1hLFdBQ0MsWUFDVSxzQkFDeEIiLCJmaWxlIjoiL2hvbWUvbW9qdGFiYS9kZXYvcHJvamVjdHMvc2FmaXIvY29tcG9uZW50cy9MYXp5SW1hZ2UvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlUmVmLCBGcmFnbWVudCB9IGZyb20gXCJyZWFjdFwiO1xyXG5leHBvcnQgY29uc3QgTGF6eUltYWdlID0gKHsgc3JjLCBhbHQsIGRlZmF1bHRJbWFnZSwgaW1hZ2VPbmxvYWQsIHN0eWxlIH0pID0+IHtcclxuICBsZXQgcGxhY2VIb2xkZXIgPSBkZWZhdWx0SW1hZ2UgfHwgXCJodHRwczovL3d3dy5ub3Zvay5jb20vc3RvcmFnZS90aGVtZXMvbm92b2svaW1nL25vaW1nLmpwZ1wiO1xyXG4gIGxldCBwbGFjZUhvbGRlclR3byA9IFwiaHR0cHM6Ly93d3cuY2hkLm1ocmQuZ292LmluL3NpdGVzL2RlZmF1bHQvZmlsZXMvbm8taW1hZ2UtYXZhaWxhYmxlLmpwZ1wiO1xyXG4gIGNvbnN0IFtpbWFnZVNyYywgc2V0SW1hZ2VTcmNdID0gdXNlU3RhdGUocGxhY2VIb2xkZXIpO1xyXG4gIC8vICAgY29uc3QgW2ltYWdlUmVmLCBzZXRJbWFnZVJlZl0gPSB1c2VTdGF0ZSgpO1xyXG4gIC8vICAgLy9jb25zb2xlLmxvZyh7IGltYWdlU3JjIH0pO1xyXG5cclxuICBjb25zdCBpbWFnZVJlZiA9IHVzZVJlZihudWxsKTtcclxuICBjb25zdCBvbkxvYWQgPSAoZXZlbnQpID0+IHtcclxuICAgIC8vY29uc29sZS5sb2coeyBvbkxvYWQ6IGV2ZW50IH0pO1xyXG4gICAgZXZlbnQudGFyZ2V0LmNsYXNzTGlzdC5hZGQoXCJsb2FkZWRcIik7XHJcbiAgICAvLyBpbWFnZU9ubG9hZCgpO1xyXG4gIH07XHJcblxyXG4gIGNvbnN0IG9uRXJyb3IgPSAoZXZlbnQpID0+IHtcclxuICAgIC8vY29uc29sZS5sb2coeyBlcnJvcjogZXZlbnQgfSk7XHJcbiAgICBldmVudC50YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImxvYWRlZFwiKTtcclxuXHJcbiAgICBldmVudC50YXJnZXQuY2xhc3NMaXN0LmFkZChcImhhcy1lcnJvclwiKTtcclxuICB9O1xyXG4gIC8vIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgLy8gICBpbWFnZVJlZi5jdXJyZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJsb2FkXCIsIG9uTG9hZCk7XHJcbiAgLy8gfSk7XHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIGltYWdlUmVmLmN1cnJlbnQuY2xhc3NMaXN0LnJlbW92ZShcImhhcy1lcnJvclwiKTtcclxuICAgIC8vIGltYWdlUmVmLmN1cnJlbnQuY2xhc3NMaXN0LmFkZChcImxvYWRlZFwiKTtcclxuXHJcbiAgICBsZXQgb2JzZXJ2ZXI7XHJcbiAgICBsZXQgZGlkQ2FuY2VsID0gZmFsc2U7XHJcbiAgICAvLyAvL2NvbnNvbGUubG9nKFwiYXZhYWFhYWFhYWFhYWFhYWFheiBzaG9vb29kXCIpO1xyXG4gICAgaWYgKGltYWdlUmVmLmN1cnJlbnQgJiYgaW1hZ2VTcmMgIT09IHNyYykge1xyXG4gICAgICBpZiAoSW50ZXJzZWN0aW9uT2JzZXJ2ZXIpIHtcclxuICAgICAgICBvYnNlcnZlciA9IG5ldyBJbnRlcnNlY3Rpb25PYnNlcnZlcihcclxuICAgICAgICAgIChlbnRyaWVzKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIC8vY29uc29sZS5sb2coeyBvYnNlcnZlciwgZW50cmllcyB9KTtcclxuXHJcbiAgICAgICAgICAgIGVudHJpZXMuZm9yRWFjaCgoZW50cnkpID0+IHtcclxuICAgICAgICAgICAgICBpZiAoIWRpZENhbmNlbCAmJiAoZW50cnkuaW50ZXJzZWN0aW9uUmF0aW8gPiAwIHx8IGVudHJ5LmlzSW50ZXJzZWN0aW5nKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gaW1hZ2VSZWYuY3VycmVudC5jbGFzc0xpc3QuYWRkKFwibG9hZGVkXCIpO1xyXG4gICAgICAgICAgICAgICAgaW1hZ2VSZWYuY3VycmVudC5jbGFzc0xpc3QucmVtb3ZlKFwiaGFzLWVycm9yXCIpO1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIHNldEltYWdlU3JjKHNyYyk7XHJcbiAgICAgICAgICAgICAgICB9LCAyMDApO1xyXG5cclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLnVub2JzZXJ2ZShpbWFnZVJlZi5jdXJyZW50KSAmJiBvYnNlcnZlci51bm9ic2VydmUoaW1hZ2VSZWYuY3VycmVudCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIHRocmVzaG9sZDogWzAuMDA5OTk5OTk5Nzc2NDgyNTgyXSxcclxuICAgICAgICAgICAgcm9vdE1hcmdpbjogXCI3NSVcIixcclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgICAgIG9ic2VydmVyLm9ic2VydmUoaW1hZ2VSZWYuY3VycmVudCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaW1hZ2VSZWYuY3VycmVudC5jbGFzc0xpc3QuYWRkKFwibG9hZGVkXCIpO1xyXG4gICAgICAgIGltYWdlUmVmLmN1cnJlbnQuY2xhc3NMaXN0LnJlbW92ZShcImhhcy1lcnJvclwiKTtcclxuICAgICAgICAvLyBPbGQgYnJvd3NlcnMgZmFsbGJhY2tcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgIHNldEltYWdlU3JjKHNyYyk7XHJcbiAgICAgICAgfSwgMjAwKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuICgpID0+IHtcclxuICAgICAgZGlkQ2FuY2VsID0gdHJ1ZTtcclxuICAgICAgLy8gb24gY29tcG9uZW50IGNsZWFudXAsIHdlIHJlbW92ZSB0aGUgbGlzdG5lclxyXG4gICAgICBpZiAob2JzZXJ2ZXIgJiYgb2JzZXJ2ZXIudW5vYnNlcnZlKSB7XHJcbiAgICAgICAgb2JzZXJ2ZXIudW5vYnNlcnZlKGltYWdlUmVmLmN1cnJlbnQpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gIH0sIFtzcmMsIGltYWdlU3JjLCBpbWFnZVJlZi5jdXJyZW50XSk7XHJcbiAgLy8gICByZXR1cm4gPEltYWdlIHJlZj17c2V0SW1hZ2VSZWZ9IHNyYz17aW1hZ2VTcmN9IGFsdD17YWx0fSBvbkxvYWQ9e29uTG9hZH0gb25FcnJvcj17b25FcnJvcn0gLz47XHJcbiAgcmV0dXJuIChcclxuICAgIDxGcmFnbWVudD5cclxuICAgICAgPGltZyBjbGFzc05hbWU9XCJpbWFnZUNvbXBvbmVudFwiIHN0eWxlPXtzdHlsZX0gcmVmPXtpbWFnZVJlZn0gc3JjPXtpbWFnZVNyY30gb25Mb2FkPXtvbkxvYWR9IGFsdD17YWx0fSBvbkVycm9yPXtvbkVycm9yfSAvPlxyXG4gICAgICA8c3R5bGUganN4PntgXHJcbiAgICAgICAgLmltYWdlQ29tcG9uZW50IHtcclxuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW1hZ2VDb21wb25lbnQubG9hZGVkIHtcclxuICAgICAgICAgIGFuaW1hdGlvbjogbG9hZGVkIDAuN3MgZWFzZS1pbi1vdXQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIC5pbWFnZUNvbXBvbmVudCB7XHJcbiAgICAgICAgLy8gICBjb250ZW50OiB1cmwoJHtwbGFjZUhvbGRlcn0pO1xyXG4gICAgICAgIC8vICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgLy8gICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgXHJcbiAgICAgICAgLmltYWdlQ29tcG9uZW50Lmhhcy1lcnJvciB7XHJcbiAgICAgICAgICBjb250ZW50OiB1cmwoJHtwbGFjZUhvbGRlclR3b30pO1xyXG4gICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICBhbmltYXRpb246IGxvYWRlZCAwLjdzIGVhc2UtaW4tb3V0O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuaW1hZ2VDb21wb25lbnQuaGFzLWVycm9yOjpub3QoLmxvYWRlZCkge1xyXG4gICAgICAgICAgY29udGVudDogdXJsKCR7cGxhY2VIb2xkZXJUd299KTtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgYW5pbWF0aW9uOiBsb2FkZWQgMC43cyBlYXNlLWluLW91dDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmltYWdlQ29tcG9uZW50Ojpub3QoLmxvYWRlZCksXHJcbiAgICAgICAgLmltYWdlQ29tcG9uZW50Ojpub3QoLmhhcy1lcnJvcikge1xyXG4gICAgICAgICAgY29udGVudDogdXJsKCR7cGxhY2VIb2xkZXJ9KTtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgYW5pbWF0aW9uOiBsb2FkZWQgMC43cyBlYXNlLWluLW91dDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5pbWFnZS1sYXp5bG9hZGluZy1jb250YWluZXIge1xyXG4gICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2NjYztcclxuICAgICAgICB9XHJcbiAgICAgICAgQGtleWZyYW1lcyBsb2FkZWQge1xyXG4gICAgICAgICAgMCUge1xyXG4gICAgICAgICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAxMDAlIHtcclxuICAgICAgICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIGB9PC9zdHlsZT5cclxuICAgIDwvRnJhZ21lbnQ+XHJcbiAgKTtcclxufTtcclxuZXhwb3J0IGRlZmF1bHQgTGF6eUltYWdlO1xyXG4iXX0= */
/*@ sourceURL=/home/mojtaba/dev/projects/safir/components/LazyImage/index.js */`));
};
/* harmony default export */ __webpack_exports__["default"] = (LazyImage);

/***/ }),

/***/ "./globalUtils/Loading/index.js":
/*!**************************************!*\
  !*** ./globalUtils/Loading/index.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/globalUtils/Loading/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

 // import { CoffeeLoading } from "react-loadingg";

function Loading(Component) {
  return function LoadingComponent(_ref) {
    let {
      isLoading
    } = _ref,
        props = _objectWithoutProperties(_ref, ["isLoading"]);

    if (!isLoading) return __jsx(Component, _extends({}, props, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 6,
        columnNumber: 28
      }
    }));else return __jsx("div", {
      style: {
        display: "flex",
        width: "100vw",
        height: "100vh",
        justifyContent: "center",
        alignItem: "center"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 7,
        columnNumber: 17
      }
    });
  };
}

/* harmony default export */ __webpack_exports__["default"] = (Loading);

/***/ }),

/***/ "./globalUtils/axiosBase.js":
/*!**********************************!*\
  !*** ./globalUtils/axiosBase.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
 // import Cookie from "js-cookie";

const instance = axios__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  baseURL: "https://safirccard.ir/api/v1"
}); // instance.defaults.headers.common["Authorization"] = "Bearer ";

/* harmony default export */ __webpack_exports__["default"] = (instance);

/***/ }),

/***/ "./globalUtils/globalHoc/index.js":
/*!****************************************!*\
  !*** ./globalUtils/globalHoc/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _routingHandle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./routingHandle */ "./globalUtils/globalHoc/routingHandle.js");

const globalHoc = {
  routingHandle: _routingHandle__WEBPACK_IMPORTED_MODULE_0__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (globalHoc);

/***/ }),

/***/ "./globalUtils/globalHoc/routingHandle.js":
/*!************************************************!*\
  !*** ./globalUtils/globalHoc/routingHandle.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _panelAdmin_screen_PanelScreen__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../panelAdmin/screen/PanelScreen */ "./panelAdmin/screen/PanelScreen.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/globalUtils/globalHoc/routingHandle.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




const routingHandle = ({
  Component,
  pageProps,
  router
}) => {
  if (Component.panelAdminLayout || (router === null || router === void 0 ? void 0 : router.asPath) === "/panelAdmin/") return __jsx(_panelAdmin_screen_PanelScreen__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 7
    }
  }, __jsx(Component, _extends({}, pageProps, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 9
    }
  })));else return __jsx(Component, _extends({}, pageProps, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 15
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (routingHandle);

/***/ }),

/***/ "./globalUtils/index.js":
/*!******************************!*\
  !*** ./globalUtils/index.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Loading__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Loading */ "./globalUtils/Loading/index.js");
/* harmony import */ var _axiosBase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./axiosBase */ "./globalUtils/axiosBase.js");
/* harmony import */ var _globalHoc__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./globalHoc */ "./globalUtils/globalHoc/index.js");



const globalUtils = {
  Loading: _Loading__WEBPACK_IMPORTED_MODULE_0__["default"],
  axiosBase: _axiosBase__WEBPACK_IMPORTED_MODULE_1__["default"],
  globalHoc: _globalHoc__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (globalUtils);

/***/ }),

/***/ "./lib/useApiRequest.js":
/*!******************************!*\
  !*** ./lib/useApiRequest.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return useApiRequest; });
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! swr */ "swr");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _globalUtils_axiosBase__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../globalUtils/axiosBase */ "./globalUtils/axiosBase.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



function useApiRequest(request, _ref = {}) {
  let {
    initialData
  } = _ref,
      config = _objectWithoutProperties(_ref, ["initialData"]);

  return swr__WEBPACK_IMPORTED_MODULE_0___default()(request && JSON.stringify(request), () => Object(_globalUtils_axiosBase__WEBPACK_IMPORTED_MODULE_1__["default"])(request || {}).then(response => {
    // //console.log({ response });
    return response.data;
  }), _objectSpread({}, config, {
    initialData
  }));
}

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/interopRequireDefault.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/interopRequireWildcard.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/interopRequireWildcard.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! ../helpers/typeof */ "./node_modules/@babel/runtime/helpers/typeof.js");

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/typeof.js":
/*!*******************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/typeof.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof2(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "./node_modules/next/dist/client/link.js":
/*!***********************************************!*\
  !*** ./node_modules/next/dist/client/link.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime/helpers/interopRequireWildcard.js");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(/*! react */ "react"));

var _url = __webpack_require__(/*! url */ "url");

var _utils = __webpack_require__(/*! ../next-server/lib/utils */ "../next-server/lib/utils");

var _router = _interopRequireDefault(__webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js"));

var _router2 = __webpack_require__(/*! ../next-server/lib/router/router */ "./node_modules/next/dist/next-server/lib/router/router.js");

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new Map();
var IntersectionObserver = false ? undefined : null;
var prefetched = {};

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: (0, _router2.addBasePath)(formatUrl(href)),
        as: asHref ? (0, _router2.addBasePath)(formatUrl(asHref)) : asHref
      };
    });

    this.linkClicked = e => {
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (true) {
      if (props.prefetch) {
        console.warn('Next.js auto-prefetches automatically based on viewport. The prefetch attribute is no longer needed. More: https://err.sh/zeit/next.js/prefetch-true-deprecated');
      }
    }

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  getPaths() {
    var {
      pathname
    } = window.location;
    var {
      href: parsedHref,
      as: parsedAs
    } = this.formatUrls(this.props.href, this.props.as);
    var resolvedHref = (0, _url.resolve)(pathname, parsedHref);
    return [resolvedHref, parsedAs ? (0, _url.resolve)(pathname, parsedAs) : resolvedHref];
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      var isPrefetched = prefetched[this.getPaths().join( // Join on an invalid URI character
      '%')];

      if (!isPrefetched) {
        this.cleanUpListeners = listenToIntersections(ref, () => {
          this.prefetch();
        });
      }
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch(options) {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var paths = this.getPaths(); // We need to handle a prefetch error here since we may be
    // loading with priority which can reject but we don't
    // want to force navigation since this is only a prefetch

    _router.default.prefetch(paths[
    /* href */
    0], paths[
    /* asPath */
    1], options).catch(err => {
      if (true) {
        // rethrow to show invalid URL errors
        throw err;
      }
    });

    prefetched[paths.join( // Join on an invalid URI character
    '%')] = true;
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch({
          priority: true
        });
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) { var rewriteUrlForNextExport; }

    return _react.default.cloneElement(child, props);
  }

}

if (true) {
  var warn = (0, _utils.execOnce)(console.error); // This module gets removed by webpack.IgnorePlugin

  var PropTypes = __webpack_require__(/*! prop-types */ "prop-types");

  var exact = __webpack_require__(/*! prop-types-exact */ "prop-types-exact"); // @ts-ignore the property is supported, when declaring it on the class it outputs an extra bit of code which is not needed.


  Link.propTypes = exact({
    href: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    as: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    prefetch: PropTypes.bool,
    replace: PropTypes.bool,
    shallow: PropTypes.bool,
    passHref: PropTypes.bool,
    scroll: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.element, (props, propName) => {
      var value = props[propName];

      if (typeof value === 'string') {
        warn("Warning: You're using a string directly inside <Link>. This usage has been deprecated. Please add an <a> tag as child of <Link>");
      }

      return null;
    }]).isRequired
  });
}

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "./node_modules/next/dist/client/router.js":
/*!*************************************************!*\
  !*** ./node_modules/next/dist/client/router.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime/helpers/interopRequireWildcard */ "./node_modules/@babel/runtime/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router2 = _interopRequireWildcard(__webpack_require__(/*! ../next-server/lib/router/router */ "./node_modules/next/dist/next-server/lib/router/router.js"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__(/*! ../next-server/lib/router-context */ "../next-server/lib/router-context");

var _withRouter = _interopRequireDefault(__webpack_require__(/*! ./with-router */ "./node_modules/next/dist/client/with-router.js"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components', 'isFallback', 'basePath'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

Object.defineProperty(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  Object.defineProperty(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = Object.assign({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "./node_modules/next/dist/client/with-router.js":
/*!******************************************************!*\
  !*** ./node_modules/next/dist/client/with-router.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = withRouter;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router = __webpack_require__(/*! ./router */ "./node_modules/next/dist/client/router.js");

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return _react.default.createElement(ComposedComponent, Object.assign({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (true) {
    var name = ComposedComponent.displayName || ComposedComponent.name || 'Unknown';
    WithRouterWrapper.displayName = "withRouter(" + name + ")";
  }

  return WithRouterWrapper;
}

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/mitt.js":
/*!********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/mitt.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

Object.defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = Object.create(null);
  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/router.js":
/*!*****************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/router.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");

const mitt_1 = __importDefault(__webpack_require__(/*! ../mitt */ "./node_modules/next/dist/next-server/lib/mitt.js"));

const utils_1 = __webpack_require__(/*! ../utils */ "./node_modules/next/dist/next-server/lib/utils.js");

const is_dynamic_1 = __webpack_require__(/*! ./utils/is-dynamic */ "./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js");

const route_matcher_1 = __webpack_require__(/*! ./utils/route-matcher */ "./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js");

const route_regex_1 = __webpack_require__(/*! ./utils/route-regex */ "./node_modules/next/dist/next-server/lib/router/utils/route-regex.js");

const basePath =  false || '';

function addBasePath(path) {
  return path.indexOf(basePath) !== 0 ? basePath + path : path;
}

exports.addBasePath = addBasePath;

function delBasePath(path) {
  return path.indexOf(basePath) === 0 ? path.substr(basePath.length) || '/' : path;
}

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

const prepareRoute = path => toRoute(!path || path === '/' ? '/index' : path);

function fetchNextData(pathname, query, isServerRender, cb) {
  let attempts = isServerRender ? 3 : 1;

  function getResponse() {
    return fetch(utils_1.formatWithValidation({
      // @ts-ignore __NEXT_DATA__
      pathname: `/_next/data/${__NEXT_DATA__.buildId}${pathname}.json`,
      query
    }), {
      // Cookies are required to be present for Next.js' SSG "Preview Mode".
      // Cookies may also be required for `getServerSideProps`.
      //
      // > `fetch` won’t send cookies, unless you set the credentials init
      // > option.
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      //
      // > For maximum browser compatibility when it comes to sending &
      // > receiving cookies, always supply the `credentials: 'same-origin'`
      // > option instead of relying on the default.
      // https://github.com/github/fetch#caveats
      credentials: 'same-origin'
    }).then(res => {
      if (!res.ok) {
        if (--attempts > 0 && res.status >= 500) {
          return getResponse();
        }

        throw new Error(`Failed to load static props`);
      }

      return res.json();
    });
  }

  return getResponse().then(data => {
    return cb ? cb(data) : data;
  }).catch(err => {
    // We should only trigger a server-side transition if this was caused
    // on a client-side transition. Otherwise, we'd get into an infinite
    // loop.
    if (!isServerRender) {
      ;
      err.code = 'PAGE_LOAD_ERROR';
    }

    throw err;
  });
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription,
    isFallback
  }) {
    // Static Data Cache
    this.sdc = {};

    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state && this.isSsr && e.state.as === this.asPath && url_1.parse(e.state.url).pathname === this.pathname) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (true) {
        if (typeof url === 'undefined' || typeof as === 'undefined') {
          console.warn('`popstate` event triggered but `event.state` did not have `url` or `as` https://err.sh/zeit/next.js/popstate-state-empty');
        }
      }

      this.replace(url, as, options);
    };

    this._getStaticData = asPath => {
      const pathname = prepareRoute(url_1.parse(asPath).pathname);
      return  false ? undefined : fetchNextData(pathname, null, this.isSsr, data => this.sdc[pathname] = data);
    };

    this._getServerData = asPath => {
      let {
        pathname,
        query
      } = url_1.parse(asPath, true);
      pathname = prepareRoute(pathname);
      return fetchNextData(pathname, query, this.isSsr);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err,
        __N_SSG: initialProps && initialProps.__N_SSG,
        __N_SSP: initialProps && initialProps.__N_SSP
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.autoExport ? pathname : as;
    this.basePath = basePath;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;
    this.isFallback = isFallback;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    if (false) {} else {
      return url;
    }
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = Object.assign(Object.assign({}, data), {
      Component,
      __N_SSG: mod.__N_SSG,
      __N_SSP: mod.__N_SSP
    });
    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new Promise((resolve, reject) => {
      if (!options._h) {
        this.isSsr = false;
      } // marking route changes as a navigation start entry


      if (utils_1.ST) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      let url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as;
      url = addBasePath(url);
      as = addBasePath(as); // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as, options);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      }

      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const routeRegex = route_regex_1.getRouteRegex(route);
        const routeMatch = route_matcher_1.getRouteMatcher(routeRegex)(asPathname);

        if (!routeMatch) {
          const missingParams = Object.keys(routeRegex.groups).filter(param => !query[param]);

          if (missingParams.length > 0) {
            if (true) {
              console.warn(`Mismatching \`as\` and \`href\` failed to manually provide ` + `the params: ${missingParams.join(', ')} in the \`href\`'s \`query\``);
            }

            return reject(new Error(`The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). ` + `Read more: https://err.sh/zeit/next.js/incompatible-href-as`));
          }
        } else {
          // Merge params into `query`, overwriting any specified in search
          Object.assign(query, routeMatch);
        }
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);

        if (true) {
          const appComp = this.components['/_app'].Component;
          window.next.isPrerendered = appComp.getInitialProps === appComp.origGetInitialProps && !routeInfo.Component.getInitialProps;
        }

        this.set(route, pathname, query, as, routeInfo);

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (true) {
      if (typeof window.history === 'undefined') {
        console.error(`Warning: window.history is not available.`);
        return;
      }

      if (typeof window.history[method] === 'undefined') {
        console.error(`Warning: window.history.${method} is not available`);
        return;
      }
    }

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      window.history[method]({
        url,
        as,
        options
      }, // Most browsers currently ignores this parameter, although they may use it in the future.
      // Passing the empty string here should be safe against future changes to the method.
      // https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
      '', as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return Promise.resolve(cachedRouteInfo);
    }

    const handleError = (err, loadErrorFail) => {
      return new Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR' || loadErrorFail) {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(res => {
          const {
            page: Component
          } = res;
          const routeInfo = {
            Component,
            err
          };
          return new Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }).catch(err => handleError(err, true)));
      });
    };

    return new Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(res => resolve({
        Component: res.page,
        __N_SSG: res.mod.__N_SSG,
        __N_SSP: res.mod.__N_SSP
      }), reject);
    }).then(routeInfo => {
      const {
        Component,
        __N_SSG,
        __N_SSP
      } = routeInfo;

      if (true) {
        const {
          isValidElementType
        } = __webpack_require__(/*! react-is */ "react-is");

        if (!isValidElementType(Component)) {
          throw new Error(`The default export is not a React Component in page: "${pathname}"`);
        }
      }

      return this._getData(() => __N_SSG ? this._getStaticData(as) : __N_SSP ? this._getServerData(as) : this.getInitialProps(Component, // we provide AppTree later so this needs to be `any`
      {
        pathname,
        query,
        asPath: as
      })).then(props => {
        routeInfo.props = props;
        this.components[route] = routeInfo;
        return routeInfo;
      });
    }).catch(handleError);
  }

  set(route, pathname, query, as, data) {
    this.isFallback = false;
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch page code, you may wait for the data during page rendering.
   * This feature only works in production!
   * @param url the href of prefetched page
   * @param asPath the as path of the prefetched page
   */


  prefetch(url, asPath = url, options = {}) {
    return new Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (true) {
        return;
      }

      const route = delBasePath(toRoute(pathname));
      Promise.all([this.pageLoader.prefetchData(url, delBasePath(asPath)), this.pageLoader[options.priority ? 'loadPage' : 'prefetch'](route)]).then(() => resolve(), reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    route = delBasePath(route);
    const componentResult = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return componentResult;
  }

  _getData(fn) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    return fn().then(data => {
      if (cancel === this.clc) {
        this.clc = null;
      }

      if (cancelled) {
        const err = new Error('Loading initial props cancelled');
        err.cancelled = true;
        throw err;
      }

      return data;
    });
  }

  getInitialProps(Component, ctx) {
    const {
      Component: App
    } = this.components['/_app'];

    const AppTree = this._wrapApp(App);

    ctx.AppTree = AppTree;
    return utils_1.loadGetInitialProps(App, {
      AppTree,
      Component,
      router: this,
      ctx
    });
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

exports.default = Router;
Router.events = mitt_1.default();

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js":
/*!***************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string

const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js":
/*!******************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/route-matcher.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = param => {
      try {
        return decodeURIComponent(param);
      } catch (_) {
        const err = new Error('failed to decode param');
        err.code = 'DECODE_FAILED';
        throw err;
      }
    };

    const params = {};
    Object.keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });
    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/router/utils/route-regex.js":
/*!****************************************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/router/utils/route-regex.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = (normalizedRoute.replace(/\/$/, '') || '/').replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => {
    const isCatchAll = /^(\\\.){3}/.test($1);
    groups[$1 // Un-escape key
    .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '') // eslint-disable-next-line no-sequences
    ] = {
      pos: groupIndex++,
      repeat: isCatchAll
    };
    return isCatchAll ? '/(.+?)' : '/([^/]+?)';
  });
  return {
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  };
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "./node_modules/next/dist/next-server/lib/utils.js":
/*!*********************************************************!*\
  !*** ./node_modules/next/dist/next-server/lib/utils.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn(...args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  var _a;

  if (true) {
    if ((_a = App.prototype) === null || _a === void 0 ? void 0 : _a.getInitialProps) {
      const message = `"${getDisplayName(App)}.getInitialProps()" is defined as an instance method - visit https://err.sh/zeit/next.js/get-initial-props-as-an-instance-method for more information.`;
      throw new Error(message);
    }
  } // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (true) {
    if (Object.keys(props).length === 0 && !ctx.ctx) {
      console.warn(`${getDisplayName(App)} returned an empty object from \`getInitialProps\`. This de-optimizes and prevents automatic static optimization. https://err.sh/zeit/next.js/empty-object-getInitialProps`);
    }
  }

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (true) {
    if (url !== null && typeof url === 'object') {
      Object.keys(url).forEach(key => {
        if (exports.urlObjectKeys.indexOf(key) === -1) {
          console.warn(`Unknown key passed via urlObject into url.format: ${key}`);
        }
      });
    }
  }

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SP = typeof performance !== 'undefined';
exports.ST = exports.SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "./node_modules/next/link.js":
/*!***********************************!*\
  !*** ./node_modules/next/link.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/client/link */ "./node_modules/next/dist/client/link.js")


/***/ }),

/***/ "./pages/panelAdmin/banner.js":
/*!************************************!*\
  !*** ./pages/panelAdmin/banner.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _context_reducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../_context/reducer */ "./_context/reducer/index.js");
/* harmony import */ var _panelAdmin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../panelAdmin */ "./panelAdmin/index.js");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! swr */ "swr");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _lib_useApiRequest__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../lib/useApiRequest */ "./lib/useApiRequest.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var _panelAdmin_component_UI_Loadings_SpinnerRotate__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../panelAdmin/component/UI/Loadings/SpinnerRotate */ "./panelAdmin/component/UI/Loadings/SpinnerRotate/index.js");
/* harmony import */ var _panelAdmin_screen_Banner_BannerScreen__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../panelAdmin/screen/Banner/BannerScreen */ "./panelAdmin/screen/Banner/BannerScreen/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/pages/panelAdmin/banner.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;







 // const axios = globalUtils.axiosBase;
// const fetcher = (url) => axios(url).then((r) => r.json());

const banner = props => {
  const strings = _panelAdmin__WEBPACK_IMPORTED_MODULE_2__["default"].values.apiString;
  const Context = _context_reducer__WEBPACK_IMPORTED_MODULE_1__["default"].panelAdminReducer.optionReducerContext;
  const giveContextData = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(Context);
  const {
    dispatch
  } = giveContextData;
  const {
    acceptedCardInfo,
    parentTrue,
    isServer
  } = props;
  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const CurrentPage = (state === null || state === void 0 ? void 0 : state.page) || "1";
  const {
    0: loadingApi,
    1: setLoadingApi
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true); // ======================================================== SWR
  // const { data } = useSWR(strings.IMAGE, fetcher, { initialData: resData });
  // const { data: image } = useApiRequest(strings.IMAGE + "/" + CurrentPage, { initialData: resData }, { refreshInterval: 0 });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    dispatch.changePageName("بنر");
    apiPageFetch();
  }, []); // //console.log({ data, resData });

  const apiPageFetch = async (page = 1) => {
    if (!page) return;
    setLoadingApi(true);
    const res = await _panelAdmin__WEBPACK_IMPORTED_MODULE_2__["default"].api.get.banners(page); // //console.log({ res });

    setState(res);
    setLoadingApi(false);
  };

  return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, __jsx(_panelAdmin_screen_Banner_BannerScreen__WEBPACK_IMPORTED_MODULE_7__["default"], {
    requestData: state,
    acceptedCardInfo: acceptedCardInfo,
    apiPageFetch: apiPageFetch,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 7
    }
  }), loadingApi ? __jsx("div", {
    className: "staticStyle bgWhite",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 9
    }
  }, __jsx(_panelAdmin_component_UI_Loadings_SpinnerRotate__WEBPACK_IMPORTED_MODULE_6__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 11
    }
  })) : ""); // return true;
}; // ========================================= getInitialProps
// banner.getInitialProps = async (props) => {
//   const { banner, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.banner({ page: "1" });
//   const resData = res.data;
//   return { resData, isServer };
// };


banner.panelAdminLayout = true;
/* harmony default export */ __webpack_exports__["default"] = (banner);

/***/ }),

/***/ "./pages/panelAdmin/gallery.js":
/*!*************************************!*\
  !*** ./pages/panelAdmin/gallery.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _panelAdmin_screen_Gallery_GalleryScreen__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../panelAdmin/screen/Gallery/GalleryScreen */ "./panelAdmin/screen/Gallery/GalleryScreen/index.js");
/* harmony import */ var _context_reducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../_context/reducer */ "./_context/reducer/index.js");
/* harmony import */ var _panelAdmin__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../panelAdmin */ "./panelAdmin/index.js");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! swr */ "swr");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _lib_useApiRequest__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../lib/useApiRequest */ "./lib/useApiRequest.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var _panelAdmin_component_UI_Loadings_SpinnerRotate__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../panelAdmin/component/UI/Loadings/SpinnerRotate */ "./panelAdmin/component/UI/Loadings/SpinnerRotate/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/pages/panelAdmin/gallery.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;







 // const axios = globalUtils.axiosBase;
// const fetcher = (url) => axios(url).then((r) => r.json());

const gallery = props => {
  const strings = _panelAdmin__WEBPACK_IMPORTED_MODULE_3__["default"].values.apiString;
  const Context = _context_reducer__WEBPACK_IMPORTED_MODULE_2__["default"].panelAdminReducer.optionReducerContext;
  const giveContextData = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(Context);
  const {
    dispatch
  } = giveContextData;
  const {
    acceptedCardInfo,
    parentTrue,
    isServer
  } = props;
  const {
    0: loadingApi,
    1: setLoadingApi
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true);
  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const CurrentPage = (state === null || state === void 0 ? void 0 : state.page) || "1"; // //console.log({ resData });
  // ======================================================== SWR
  // const { data } = useSWR(strings.IMAGE, fetcher, { initialData: resData });
  // const { data: resData } = useApiRequest(strings.IMAGE + "/" + CurrentPage, { initialData: state }, { refreshInterval: 0 });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    !acceptedCardInfo && dispatch.changePageName("گالری");
    apiPageFetch("1");
  }, []); // //console.log({ resData });

  const apiPageFetch = async (page = CurrentPage) => {
    if (!page) return;
    setLoadingApi(true);
    const res = await _panelAdmin__WEBPACK_IMPORTED_MODULE_3__["default"].api.get.gallery({
      page
    }); // //console.log({ res });

    setState(res === null || res === void 0 ? void 0 : res.data);
    setLoadingApi(false);
  };

  return __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 5
    }
  }, __jsx(_panelAdmin_screen_Gallery_GalleryScreen__WEBPACK_IMPORTED_MODULE_1__["default"], {
    requestData: state,
    acceptedCardInfo: acceptedCardInfo,
    apiPageFetch: apiPageFetch,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 7
    }
  }), loadingApi ? __jsx("div", {
    className: "staticStyle bgWhite",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 9
    }
  }, __jsx(_panelAdmin_component_UI_Loadings_SpinnerRotate__WEBPACK_IMPORTED_MODULE_7__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 11
    }
  })) : ""); // return true;
};

gallery.panelAdminLayout = true; // ========================================= getInitialProps
// gallery.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.gallery({ page: "1" });
//   const resData = res.data;
//   return { resData, isServer };
// };

/* harmony default export */ __webpack_exports__["default"] = (gallery);

/***/ }),

/***/ "./panelAdmin/api/Delete/banner.js":
/*!*****************************************!*\
  !*** ./panelAdmin/api/Delete/banner.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");

 // import Axios from "axios";

const banner = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString.BANNER; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (banner);

/***/ }),

/***/ "./panelAdmin/api/Delete/category.js":
/*!*******************************************!*\
  !*** ./panelAdmin/api/Delete/category.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");

 // import Axios from "axios";

const category = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString.CATEGORY; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (category);

/***/ }),

/***/ "./panelAdmin/api/Delete/gallery.js":
/*!******************************************!*\
  !*** ./panelAdmin/api/Delete/gallery.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");

 // import Axios from "axios";

const gallery = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString.IMAGE; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (gallery);

/***/ }),

/***/ "./panelAdmin/api/Delete/index.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Delete/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _product__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./product */ "./panelAdmin/api/Delete/product.js");
/* harmony import */ var _gallery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./gallery */ "./panelAdmin/api/Delete/gallery.js");
/* harmony import */ var _category__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./category */ "./panelAdmin/api/Delete/category.js");
/* harmony import */ var _owner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./owner */ "./panelAdmin/api/Delete/owner.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./store */ "./panelAdmin/api/Delete/store.js");
/* harmony import */ var _slider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./slider */ "./panelAdmin/api/Delete/slider.js");
/* harmony import */ var _banner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./banner */ "./panelAdmin/api/Delete/banner.js");
// import owner from "./owner";
// import discount from "./discount";
// import club from "./club";
// import slider from "./slider";
// import banner from "./banner";
// import category from "./category";







const Delete = {
  product: _product__WEBPACK_IMPORTED_MODULE_0__["default"],
  gallery: _gallery__WEBPACK_IMPORTED_MODULE_1__["default"],
  category: _category__WEBPACK_IMPORTED_MODULE_2__["default"],
  owner: _owner__WEBPACK_IMPORTED_MODULE_3__["default"],
  store: _store__WEBPACK_IMPORTED_MODULE_4__["default"],
  slider: _slider__WEBPACK_IMPORTED_MODULE_5__["default"],
  banner: _banner__WEBPACK_IMPORTED_MODULE_6__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (Delete);

/***/ }),

/***/ "./panelAdmin/api/Delete/owner.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Delete/owner.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");

 // import Axios from "axios";

const owner = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString.OWNERS; // //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (owner);

/***/ }),

/***/ "./panelAdmin/api/Delete/product.js":
/*!******************************************!*\
  !*** ./panelAdmin/api/Delete/product.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");

 // import Axios from "axios";

const product = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  let URL = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString.PRODUCT;
  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (product);

/***/ }),

/***/ "./panelAdmin/api/Delete/slider.js":
/*!*****************************************!*\
  !*** ./panelAdmin/api/Delete/slider.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");

 // import Axios from "axios";

const slider = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString.SLIDER; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (slider);

/***/ }),

/***/ "./panelAdmin/api/Delete/store.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Delete/store.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");

 // import Axios from "axios";

const owner = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString.STORE; //console.log({ URL });

  return axios.delete(URL + "/" + param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت حذف شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (owner);

/***/ }),

/***/ "./panelAdmin/api/Get/banners.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Get/banners.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const banners = async page => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  const axiosData = page ? strings.BANNER + "/" + page : strings.BANNER;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(axiosData).then(banners => {
    //console.log({ banners });
    return banners.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (banners);

/***/ }),

/***/ "./panelAdmin/api/Get/categories.js":
/*!******************************************!*\
  !*** ./panelAdmin/api/Get/categories.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");

 // import axios from "../axios-orders";

const categories = async page => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let getUrl = page ? strings.CATEGORY + "/" + page : strings.CATEGORY;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(getUrl).then(res => {
    console.log({
      categories: res
    });
    return res;
  }).catch(error => {
    console.log({
      error
    }); // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (categories);

/***/ }),

/***/ "./panelAdmin/api/Get/gallery.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Get/gallery.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");

 // //console.log(axios);

const gallery = async ({
  page
}) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(strings.IMAGE + "/" + page).then(gallery => {
    //console.log({ gallery });
    return gallery; // loading(false);
  }).catch(error => {//console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ __webpack_exports__["default"] = (gallery);

/***/ }),

/***/ "./panelAdmin/api/Get/index.js":
/*!*************************************!*\
  !*** ./panelAdmin/api/Get/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _categories__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./categories */ "./panelAdmin/api/Get/categories.js");
/* harmony import */ var _products__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./products */ "./panelAdmin/api/Get/products.js");
/* harmony import */ var _gallery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./gallery */ "./panelAdmin/api/Get/gallery.js");
/* harmony import */ var _sliders__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sliders */ "./panelAdmin/api/Get/sliders.js");
/* harmony import */ var _owners__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./owners */ "./panelAdmin/api/Get/owners.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./store */ "./panelAdmin/api/Get/store.js");
/* harmony import */ var _ownersSearch__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ownersSearch */ "./panelAdmin/api/Get/ownersSearch.js");
/* harmony import */ var _storeSearch__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./storeSearch */ "./panelAdmin/api/Get/storeSearch.js");
/* harmony import */ var _banners__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./banners */ "./panelAdmin/api/Get/banners.js");
/* harmony import */ var _notifications__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./notifications */ "./panelAdmin/api/Get/notifications.js");
/* harmony import */ var _versions__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./versions */ "./panelAdmin/api/Get/versions.js");
/* harmony import */ var _users__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./users */ "./panelAdmin/api/Get/users.js");












const get = {
  categories: _categories__WEBPACK_IMPORTED_MODULE_0__["default"],
  products: _products__WEBPACK_IMPORTED_MODULE_1__["default"],
  gallery: _gallery__WEBPACK_IMPORTED_MODULE_2__["default"],
  sliders: _sliders__WEBPACK_IMPORTED_MODULE_3__["default"],
  owners: _owners__WEBPACK_IMPORTED_MODULE_4__["default"],
  store: _store__WEBPACK_IMPORTED_MODULE_5__["default"],
  ownersSearch: _ownersSearch__WEBPACK_IMPORTED_MODULE_6__["default"],
  storeSearch: _storeSearch__WEBPACK_IMPORTED_MODULE_7__["default"],
  banners: _banners__WEBPACK_IMPORTED_MODULE_8__["default"],
  notifications: _notifications__WEBPACK_IMPORTED_MODULE_9__["default"],
  versions: _versions__WEBPACK_IMPORTED_MODULE_10__["default"],
  users: _users__WEBPACK_IMPORTED_MODULE_11__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (get);

/***/ }),

/***/ "./panelAdmin/api/Get/notifications.js":
/*!*********************************************!*\
  !*** ./panelAdmin/api/Get/notifications.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const notifications = async page => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let getUrl = page ? strings.NOTIFICATION + "/" + page : strings.NOTIFICATION;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(getUrl); // .then((res) => {
  //   //console.log({ notifications: res });
  //   return res;
  // })
  // .catch((error) => {
  //   //console.log({ error });
  //  // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  //   return error;
  // });
};

/* harmony default export */ __webpack_exports__["default"] = (notifications);

/***/ }),

/***/ "./panelAdmin/api/Get/owners.js":
/*!**************************************!*\
  !*** ./panelAdmin/api/Get/owners.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const owners = async page => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.OWNERS;
  let url = page ? strings + "/" + page : strings;
  return axios.get(url).then(owners => {
    //console.log({ owners });
    return owners;
  }).catch(error => {//console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  });
};

/* harmony default export */ __webpack_exports__["default"] = (owners);

/***/ }),

/***/ "./panelAdmin/api/Get/ownersSearch.js":
/*!********************************************!*\
  !*** ./panelAdmin/api/Get/ownersSearch.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");




const ownersSearch = async (param, page = 1) => {
  // const axios = globalUtils.axiosBase;
  //console.log({ param, page });
  const toastify = ___WEBPACK_IMPORTED_MODULE_2__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_2__["default"].values.apiString.OWNERS + "/s/" + param + "/" + page; //console.log(strings);

  return _axios_orders__WEBPACK_IMPORTED_MODULE_1__["default"].get(strings).then(ownersSearch => {
    return ownersSearch.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (ownersSearch);

/***/ }),

/***/ "./panelAdmin/api/Get/products.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Get/products.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const products = async ({
  page
}) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.PRODUCT;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(strings + "/" + page); // .then((products) => {
  //   //console.log({ products });
  //   returnData(products.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   //console.log({ error });
  //  // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

/* harmony default export */ __webpack_exports__["default"] = (products);

/***/ }),

/***/ "./panelAdmin/api/Get/sliders.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Get/sliders.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const sliders = async page => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  const axiosData = page ? strings.SLIDER + "/" + page : strings.SLIDER;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].get(axiosData).then(sliders => {
    //console.log({ sliders });
    return sliders.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (sliders);

/***/ }),

/***/ "./panelAdmin/api/Get/store.js":
/*!*************************************!*\
  !*** ./panelAdmin/api/Get/store.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const store = async page => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.STORE;
  let url = page ? strings + "/" + page : strings;
  return axios.get(url).then(store => {
    //console.log({ store });
    return store;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (store);

/***/ }),

/***/ "./panelAdmin/api/Get/storeSearch.js":
/*!*******************************************!*\
  !*** ./panelAdmin/api/Get/storeSearch.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const storeSearch = async (param, page = 1) => {
  //console.log({ param, page });
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.STORE + "/s/" + param + "/" + page; //console.log({ strings });

  return axios.get(strings).then(storeSearch => {
    //console.log({ storeSearch });
    return storeSearch.data;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (storeSearch);

/***/ }),

/***/ "./panelAdmin/api/Get/users.js":
/*!*************************************!*\
  !*** ./panelAdmin/api/Get/users.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const users = async page => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.USER;
  let url = page ? strings + "/" + page : strings;
  return axios.get(url).then(users => {
    console.log({
      users
    });
    return users === null || users === void 0 ? void 0 : users.data;
  }).catch(error => {
    console.log({
      error
    }); // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (users);

/***/ }),

/***/ "./panelAdmin/api/Get/versions.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Get/versions.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");



const versions = async page => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString.VERSION;
  let getUrl = page ? strings + "/" + page : strings;
  return axios.get(getUrl).then(res => {
    //console.log({ versions: res });
    return res;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return error;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (versions);

/***/ }),

/***/ "./panelAdmin/api/Patch/index.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Patch/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// import category from "./category";
// import editDiscount from "./discount";
// import club from "./club";
// import owner from "./owner";
// import slider from "./slider";
// import banner from "./banner";
const Patch = {// category,
  // editDiscount,
  // club,
  // owner,
  // slider,
  // banner,
};
/* harmony default export */ __webpack_exports__["default"] = (Patch);

/***/ }),

/***/ "./panelAdmin/api/Post/banner.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Post/banner.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");



const banner = async (param, setLoading) => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString;
  let URL = strings.BANNER; //console.log({ param });

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (banner);

/***/ }),

/***/ "./panelAdmin/api/Post/category.js":
/*!*****************************************!*\
  !*** ./panelAdmin/api/Post/category.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const category = async (param, setLoading) => {
  // setLoading(true);
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let URL = strings.CATEGORY;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, param).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // setLoading(false);
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    // if (error.response.data)
    //   switch (error.response.data.Error) {
    //     case 1016:
    //       toastify("وزن تکراری می باشد", "error");
    //       break;
    //     case 1017:
    //       toastify("عنوان انگلیسی تکراری می باشد", "error");
    //       break;
    //     case 1018:
    //       toastify("عنوان فارسی تکراری می باشد", "error");
    //       break;
    //     default:
    //       toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    //       break;
    //   }
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (category);

/***/ }),

/***/ "./panelAdmin/api/Post/imageUpload.js":
/*!********************************************!*\
  !*** ./panelAdmin/api/Post/imageUpload.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const imageUpload = async (files, setLoading, setState, imageName) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_2__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_2__["default"].values.apiString; //console.log({ files, setLoading, setState, imageName });

  setLoading(true); // ============================================= const

  const CancelToken = axios__WEBPACK_IMPORTED_MODULE_1___default.a.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
      setState(prev => _objectSpread({}, prev, {
        progressPercentImage: percentCompleted
      }));
    },
    cancelToken: source.token
  };
  const URL = strings.UPLOAD;
  const formData = new FormData();
  formData.append("imageName", imageName); // formData.append("imageType", type);

  formData.append("image", files); //=============================================== axios

  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, formData, settings).then(Response => {
    //console.log({ Response });
    setLoading(false);
    return Response.data;
  }).catch(error => {
    //console.log({ error });
    setLoading(false); // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (imageUpload);

/***/ }),

/***/ "./panelAdmin/api/Post/index.js":
/*!**************************************!*\
  !*** ./panelAdmin/api/Post/index.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _imageUpload__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./imageUpload */ "./panelAdmin/api/Post/imageUpload.js");
/* harmony import */ var _category__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./category */ "./panelAdmin/api/Post/category.js");
/* harmony import */ var _product__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./product */ "./panelAdmin/api/Post/product.js");
/* harmony import */ var _owner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./owner */ "./panelAdmin/api/Post/owner.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./store */ "./panelAdmin/api/Post/store.js");
/* harmony import */ var _slider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./slider */ "./panelAdmin/api/Post/slider.js");
/* harmony import */ var _banner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./banner */ "./panelAdmin/api/Post/banner.js");
/* harmony import */ var _notification__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./notification */ "./panelAdmin/api/Post/notification.js");
/* harmony import */ var _version__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./version */ "./panelAdmin/api/Post/version.js");
/* harmony import */ var _login__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./login */ "./panelAdmin/api/Post/login.js");










const post = {
  imageUpload: _imageUpload__WEBPACK_IMPORTED_MODULE_0__["default"],
  category: _category__WEBPACK_IMPORTED_MODULE_1__["default"],
  product: _product__WEBPACK_IMPORTED_MODULE_2__["default"],
  owner: _owner__WEBPACK_IMPORTED_MODULE_3__["default"],
  store: _store__WEBPACK_IMPORTED_MODULE_4__["default"],
  slider: _slider__WEBPACK_IMPORTED_MODULE_5__["default"],
  banner: _banner__WEBPACK_IMPORTED_MODULE_6__["default"],
  notification: _notification__WEBPACK_IMPORTED_MODULE_7__["default"],
  version: _version__WEBPACK_IMPORTED_MODULE_8__["default"],
  login: _login__WEBPACK_IMPORTED_MODULE_9__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (post);

/***/ }),

/***/ "./panelAdmin/api/Post/login.js":
/*!**************************************!*\
  !*** ./panelAdmin/api/Post/login.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-cookie */ "js-cookie");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");




const login = async (param, setLoading) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  const pageRoutes = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.routes.GS_ADMIN_DASHBOARD;
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_2__["default"].axiosBase;
  console.log({
    param
  });
  let URL = strings.LOGIN; // setLoading(true);

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.set("SafirAdminToken", Response.data.token, {
      expires: 7
    }); // // window.location = pageRoutes.GS_PANEL_ADMIN_TITLE;

    toastify("شما تایید شده اید", "success");
    setTimeout(() => {
      window.location = pageRoutes;
    }, 1000);
    return true;
  }).catch(error => {
    console.log({
      error
    });
    setLoading(false);
    if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    if (error.response.data.Error === 1019) toastify("این شماره ثبت نشده است", "error");else if (error.response.data.Error === 1099) toastify("این شماره ثبت نشده است", "error");else if (error.response.data.Error === 1098) toastify("پسورد شما نامعتبر است", "error");else toastify("خطایی در سرور . لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (login);

/***/ }),

/***/ "./panelAdmin/api/Post/notification.js":
/*!*********************************************!*\
  !*** ./panelAdmin/api/Post/notification.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const notification = async (param, setLoading) => {
  // setLoading(true);
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let URL = strings.NOTIFICATION;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, param).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    console.log({
      error
    }); // setLoading(false);
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //  if (error.response.data)
    //   switch (error.response.data.Error) {
    //     case 1016:
    //       toastify("وزن تکراری می باشد", "error");
    //       break;
    //     case 1017:
    //       toastify("عنوان انگلیسی تکراری می باشد", "error");
    //       break;
    //     case 1018:
    //       toastify("عنوان فارسی تکراری می باشد", "error");
    //       break;
    //     default:
    //       toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    //       break;
    //   }

    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (notification);

/***/ }),

/***/ "./panelAdmin/api/Post/owner.js":
/*!**************************************!*\
  !*** ./panelAdmin/api/Post/owner.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");



const owner = async (param, setLoading) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString;
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  let URL = strings.OWNERS;
  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (owner);

/***/ }),

/***/ "./panelAdmin/api/Post/product.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Post/product.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _axios_orders__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const product = async (param, setLoading) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString;
  let URL = strings.PRODUCT;
  return _axios_orders__WEBPACK_IMPORTED_MODULE_0__["default"].post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (product);

/***/ }),

/***/ "./panelAdmin/api/Post/slider.js":
/*!***************************************!*\
  !*** ./panelAdmin/api/Post/slider.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");



const slider = async (param, setLoading) => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString;
  let URL = strings.SLIDER; //console.log({ param });

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (slider);

/***/ }),

/***/ "./panelAdmin/api/Post/store.js":
/*!**************************************!*\
  !*** ./panelAdmin/api/Post/store.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");



const store = async (param, setLoading) => {
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString;
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  let URL = strings.STORE;
  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (store);

/***/ }),

/***/ "./panelAdmin/api/Post/version.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Post/version.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");



const version = async (param, setLoading) => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_1__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.toastify;
  const strings = ___WEBPACK_IMPORTED_MODULE_0__["default"].values.apiString;
  let URL = strings.VERSION; //console.log({ param });

  return axios.post(URL, param).then(Response => {
    //console.log({ Response });
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (version);

/***/ }),

/***/ "./panelAdmin/api/Put/banner.js":
/*!**************************************!*\
  !*** ./panelAdmin/api/Put/banner.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const banner = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.BANNER; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (banner);

/***/ }),

/***/ "./panelAdmin/api/Put/category.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/Put/category.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const category = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.CATEGORY; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (category);

/***/ }),

/***/ "./panelAdmin/api/Put/index.js":
/*!*************************************!*\
  !*** ./panelAdmin/api/Put/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _owner__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./owner */ "./panelAdmin/api/Put/owner.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./store */ "./panelAdmin/api/Put/store.js");
/* harmony import */ var _category__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./category */ "./panelAdmin/api/Put/category.js");
/* harmony import */ var _banner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./banner */ "./panelAdmin/api/Put/banner.js");
/* harmony import */ var _slider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./slider */ "./panelAdmin/api/Put/slider.js");
// import editSection from "./editSection";





const put = {
  owner: _owner__WEBPACK_IMPORTED_MODULE_0__["default"],
  store: _store__WEBPACK_IMPORTED_MODULE_1__["default"],
  category: _category__WEBPACK_IMPORTED_MODULE_2__["default"],
  banner: _banner__WEBPACK_IMPORTED_MODULE_3__["default"],
  slider: _slider__WEBPACK_IMPORTED_MODULE_4__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (put);

/***/ }),

/***/ "./panelAdmin/api/Put/owner.js":
/*!*************************************!*\
  !*** ./panelAdmin/api/Put/owner.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const owner = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.OWNERS; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (owner);

/***/ }),

/***/ "./panelAdmin/api/Put/slider.js":
/*!**************************************!*\
  !*** ./panelAdmin/api/Put/slider.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const slider = async param => {
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.SLIDER; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (slider);

/***/ }),

/***/ "./panelAdmin/api/Put/store.js":
/*!*************************************!*\
  !*** ./panelAdmin/api/Put/store.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const store = async param => {
  //console.log({ param });
  const axios = _globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase;
  const toastify = ___WEBPACK_IMPORTED_MODULE_1__["default"].utils.toastify;
  const URL = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.apiString.STORE; //console.log({ apiParam: param });
  // setLoading(true);

  return axios.put(URL + "/" + param.id, param.data).then(Response => {
    //console.log({ Response });
    // setLoading(false);
    if (Response.data) ;
    toastify("با موفقیت ثبت شد", "success");
    return true;
  }).catch(error => {
    //console.log({ error });
    // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
    //else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    return false;
  });
};

/* harmony default export */ __webpack_exports__["default"] = (store);

/***/ }),

/***/ "./panelAdmin/api/axios-orders.js":
/*!****************************************!*\
  !*** ./panelAdmin/api/axios-orders.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! js-cookie */ "js-cookie");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_1__);

 // const instance = axios.create({ baseURL: "https://rimtal.com/api/v1" });
// const instance = create({ baseURL: "https://pernymarket.ir/api/v1" });

_globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase.defaults.headers.common["Authorization"] = "Bearer " + js_cookie__WEBPACK_IMPORTED_MODULE_1___default.a.get("SafirAdminToken");
/* harmony default export */ __webpack_exports__["default"] = (_globalUtils__WEBPACK_IMPORTED_MODULE_0__["default"].axiosBase);

/***/ }),

/***/ "./panelAdmin/api/index.js":
/*!*********************************!*\
  !*** ./panelAdmin/api/index.js ***!
  \*********************************/
/*! exports provided: get, post, put, patch, deletes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Get__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Get */ "./panelAdmin/api/Get/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "get", function() { return _Get__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _Put__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Put */ "./panelAdmin/api/Put/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "put", function() { return _Put__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _Post__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Post */ "./panelAdmin/api/Post/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "post", function() { return _Post__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _Patch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Patch */ "./panelAdmin/api/Patch/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "patch", function() { return _Patch__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _Delete__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Delete */ "./panelAdmin/api/Delete/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "deletes", function() { return _Delete__WEBPACK_IMPORTED_MODULE_4__["default"]; });








/***/ }),

/***/ "./panelAdmin/component/FormMap/index.js":
/*!***********************************************!*\
  !*** ./panelAdmin/component/FormMap/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/dynamic */ "next/dynamic");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/FormMap/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const SimpleExample = next_dynamic__WEBPACK_IMPORTED_MODULE_1___default()(() => __webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! ../SimpleExample */ "./panelAdmin/component/SimpleExample/index.js")), {
  ssr: false,
  loadableGenerated: {
    webpack: () => [/*require.resolve*/(/*! ../SimpleExample */ "./panelAdmin/component/SimpleExample/index.js")],
    modules: ["../SimpleExample"]
  }
}); // const MapWithNoSSR = dynamic(() => import("./map"), {
//   ssr: false,
// });

const FormMap = props => {
  const {
    value,
    className,
    elementConfig,
    formElement,
    onChange,
    inputClasses,
    openMapModal,
    uploadWeb,
    mapPinData
  } = props;
  const {
    0: map,
    1: setmap
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: display,
    1: setDisplay
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);

  const showMap = () => {
    setmap(!map);
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    setDisplay(true);
  }, []);

  const endAnimation = () => {// (!map ? setDisplay(true) : setDisplay(false))
  }; // //console.log({ formElement });


  let form;
  form = __jsx("div", {
    className: "Input",
    style: {
      display: "flex",
      flexDirection: "column"
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "title-wrapper",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }
  }), __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "coordinate-wrapper",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 9
    }
  }, __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 11
    }
  }, __jsx("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 13
    }
  }, "طول (lng): "), __jsx("div", {
    disabled: true,
    className: `${className}` // className={inputClasses.join(" ")}
    // value={formElement.config.value.lng} name={"lng"} onChange={(e) => onChange({ name: formElement.id, value: e.currentTarget.value, child: e.currentTarget.name })} type={"number"}
    ,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 13
    }
  }, __jsx("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 15
    }
  }, (value === null || value === void 0 ? void 0 : value.lng) || " 49.585606455802925"))), __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 11
    }
  }, __jsx("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 13
    }
  }, "عرض (lat) : "), __jsx("div", {
    disabled: true,
    className: `${className}` // className={inputClasses.join(" ")}
    //  value={formElement.config.value.lat} name={"lat"} onChange={(e) => onChange({ name: formElement.id, value: e.currentTarget.value, child: e.currentTarget.name })} type={"number"}
    ,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 13
    }
  }, __jsx("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 15
    }
  }, " ", (value === null || value === void 0 ? void 0 : value.lat) || "37.281369047367555")))), __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 9
    }
  }, display ? __jsx(SimpleExample, {
    newPin: true,
    center: (value === null || value === void 0 ? void 0 : value.lat) ? value : {
      lat: "37.281369047367555",
      lng: "49.585606455802925"
    },
    onChange: onChange,
    data: mapPinData,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 22
    }
  }) : "")));
  return form;
};

/* harmony default export */ __webpack_exports__["default"] = (FormMap);

/***/ }),

/***/ "./panelAdmin/component/Header/HeaderProfile/index.js":
/*!************************************************************!*\
  !*** ./panelAdmin/component/Header/HeaderProfile/index.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! js-cookie */ "js-cookie");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/Header/HeaderProfile/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

 // import profileImage from "../../../assets/Images/icons/user.png";
// import "./index.scss";





const HeaderProfile = () => {
  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    showModal: false,
    clickedComponent: false
  });
  const wrapperRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const routers = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();

  const showModalprofile = () => {
    // let noting = ;
    setState(prev => _objectSpread({}, prev, {
      showModal: !state.showModal,
      clickedComponent: true
    }));
  }; // //console.log({ state: state.showModal });


  const logOut = () => {
    js_cookie__WEBPACK_IMPORTED_MODULE_2___default.a.remove("SafirAdminToken");
    routers.push("/panelAdmin/login");
  };

  const adminTitleModal = [// {
  //   title: "پروفایل",
  //   iconClass: "fas fa-user",
  //   href: "#",
  //   onClick: null,
  // },
  // {
  //   title: "پیام ها",
  //   iconClass: "fas fa-envelope",
  //   href: "#",
  //   value: "",
  //   onClick: null,
  // },
  // {
  //   title: "تنظیمات",
  //   iconClass: "fas fa-cog",
  //   href: "#",
  //   onClick: null,
  // },
  {
    title: "خروج",
    iconClass: " fas fa-sign-out-alt",
    href: "#",
    onClick: logOut
  }];

  const handleClickOutside = event => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      showModalprofile();
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (state.showModal) {
      document.addEventListener("click", handleClickOutside);
      return () => {
        document.removeEventListener("click", handleClickOutside);
      };
    }
  });

  const adminTitleModal_map = __jsx("ul", {
    className: `profile-modal-container ${state.showModal ? "actived" : state.clickedComponent ? "deActive" : "showOutStatic"}`,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 5
    }
  }, adminTitleModal.map((admin, index) => {
    return __jsx("li", {
      onClick: admin.onClick,
      key: index,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 11
      }
    }, __jsx("i", {
      className: admin.iconClass,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70,
        columnNumber: 13
      }
    }), __jsx(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
      href: admin.href,
      as: admin.href,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 13
      }
    }, __jsx("a", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 73,
        columnNumber: 15
      }
    }, admin.title)), admin.value ? __jsx("span", {
      className: "show-modal-icon-value",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75,
        columnNumber: 28
      }
    }, admin.value) : "");
  }));

  return __jsx("ul", {
    onClick: showModalprofile,
    ref: wrapperRef,
    className: "panel-navbar-profile ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 5
    }
  }, __jsx("li", {
    className: "pointer hoverColorblack normalTransition",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "centerAll",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 9
    }
  }, __jsx("i", {
    className: "fas fa-angle-down",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 11
    }
  })), __jsx("div", {
    className: "admin-profile-name  icon-up-dir ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 9
    }
  }, __jsx("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 11
    }
  }, "\u0627\u062F\u0645\u06CC\u0646"), " "), __jsx("div", {
    className: "admin-profile-image",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91,
      columnNumber: 9
    }
  })), adminTitleModal_map);
};

/* harmony default export */ __webpack_exports__["default"] = (HeaderProfile);

/***/ }),

/***/ "./panelAdmin/component/Header/index.js":
/*!**********************************************!*\
  !*** ./panelAdmin/component/Header/index.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _HeaderProfile__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HeaderProfile */ "./panelAdmin/component/Header/HeaderProfile/index.js");
/* harmony import */ var _context_reducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../_context/reducer */ "./_context/reducer/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/Header/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const Context = _context_reducer__WEBPACK_IMPORTED_MODULE_2__["default"].panelAdminReducer.optionReducerContext;

const Header = ({
  _handelSidebarToggle
}) => {
  const giveContextData = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(Context);
  const {
    state
  } = giveContextData;
  return __jsx("nav", {
    className: "panelAdmin-navbar-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "panel-navbar-box",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "panel-navbar-side-element smallDisplay",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 9
    }
  }, __jsx("i", {
    onClick: _handelSidebarToggle,
    className: "fas fa-bars",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 11
    }
  }), __jsx("span", {
    className: "page-accepted-name",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 11
    }
  }, state.pageName)), __jsx("div", {
    className: "panel-navbar-side-element",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 9
    }
  }, __jsx("ul", {
    className: "panel-navbar-notifications",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 11
    }
  }, __jsx("li", {
    className: "pointer hoverColorblack normalTransition",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 13
    }
  }, __jsx("i", {
    className: "icon-search",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 15
    }
  })), __jsx("li", {
    className: "navbar-icon-massege pointer hoverColorblack normalTransition",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 13
    }
  })), __jsx(_HeaderProfile__WEBPACK_IMPORTED_MODULE_1__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 11
    }
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./panelAdmin/component/SideMenu/MainMenu/index.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/component/SideMenu/MainMenu/index.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _MenuTitle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../MenuTitle */ "./panelAdmin/component/SideMenu/MenuTitle/index.js");
/* harmony import */ var _Menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Menu */ "./panelAdmin/component/SideMenu/Menu/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/SideMenu/MainMenu/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const MainMenu = ({
  mainMenus,
  windowLocation
}) => {
  const {
    0: showLi,
    1: setShowLi
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  const {
    0: selectedMenuTitle,
    1: setMenuTitle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(); // //console.log({ windowLocation });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    checkMenu();
  }, []);

  const checkMenu = () => {
    mainMenus.map(menu => {
      return menu.menus.map(menu => {
        return menu.subMenu.map(subMenu => {
          if (subMenu.route === "/" + windowLocation.substr(windowLocation.indexOf("panelAdmin"))) {
            setMenuTitle(menu.menuTitle);
            setShowLi(menu.menuTitle);
            return true;
          }
        });
      });
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    checkMenu();
  }, [windowLocation]); // //console.log({ showLi, selectedMenuTitle });

  return mainMenus.map((mainMenu, index) => {
    return __jsx("ul", {
      key: index + "m",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 7
      }
    }, __jsx(_MenuTitle__WEBPACK_IMPORTED_MODULE_1__["default"], {
      title: mainMenu.title,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 9
      }
    }), __jsx(_Menu__WEBPACK_IMPORTED_MODULE_2__["default"], {
      windowLocation: windowLocation,
      menus: mainMenu.menus,
      showLi: showLi,
      setShowLi: setShowLi,
      selectedMenuTitle: selectedMenuTitle,
      setMenuTitle: setMenuTitle,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 9
      }
    }));
  });
};

/* harmony default export */ __webpack_exports__["default"] = (MainMenu);

/***/ }),

/***/ "./panelAdmin/component/SideMenu/Menu/index.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/component/SideMenu/Menu/index.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _SubMenu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../SubMenu */ "./panelAdmin/component/SideMenu/SubMenu/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/SideMenu/Menu/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const Menu = ({
  menus,
  showLi,
  setShowLi,
  selectedMenuTitle,
  setMenuTitle,
  selectedMenu,
  windowLocation
}) => {
  const location = windowLocation;

  const activedSideTitle = name => {
    let newName = name;

    if (showLi === name) {
      newName = "";
    } else {
      newName = name;
    }

    setShowLi(newName);
  };

  const classNameForMenu = (length, title) => {
    let classes;
    classes = [length ? "icon-right-open " : "", // selectedMenuTitle === title ? (showLi === title ? "" : "activeMenu") : "",
    showLi === title ? "arrowRotate" : "unsetRotate"].join(" ");
    return classes;
  };

  const _handelStateNull = () => {
    setShowLi("");
    setMenuTitle("");
  };

  return menus.map((menu, index) => {
    return __jsx("li", {
      key: "menus-" + index,
      className: "side-iteme",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 7
      }
    }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
      href: !menu.subMenu.length > 0 ? menu.route : "#",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 9
      }
    }, __jsx("a", {
      onClick: menu.route ? _handelStateNull : () => activedSideTitle(menu.subMenu && menu.menuTitle),
      id: location.includes(menu.route) ? "activedSide" : selectedMenuTitle === menu.menuTitle ? showLi === menu.menuTitle ? "" : "activedSide" : "",
      className: `side-link ${classNameForMenu(menu.subMenu.length, menu.menuTitle)}`,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 11
      }
    }, menu.menuIconImg ? __jsx("img", {
      src: menu.menuIconImg,
      alt: "icon menu",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 33
      }
    }) : __jsx("i", {
      className: menu.menuIconClass,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 82
      }
    }), __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 13
      }
    }, menu.menuTitle), menu.subMenu.length ? __jsx("div", {
      className: "menu-arrow-icon",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 15
      }
    }, __jsx("i", {
      className: "fas fa-angle-left",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41,
        columnNumber: 17
      }
    })) : "")), __jsx(_SubMenu__WEBPACK_IMPORTED_MODULE_2__["default"], {
      windowLocation: windowLocation,
      menu: menu,
      setMenuTitle: setMenuTitle,
      showLi: showLi,
      selectedMenu: selectedMenu,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 48,
        columnNumber: 9
      }
    }));
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Menu);

/***/ }),

/***/ "./panelAdmin/component/SideMenu/MenuTitle/index.js":
/*!**********************************************************!*\
  !*** ./panelAdmin/component/SideMenu/MenuTitle/index.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/SideMenu/MenuTitle/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const MenuTitle = ({
  title
}) => {
  return __jsx("li", {
    className: "side-header change-position",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5,
      columnNumber: 3
    }
  }, __jsx("h6", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 4
    }
  }, title));
};

/* harmony default export */ __webpack_exports__["default"] = (MenuTitle);

/***/ }),

/***/ "./panelAdmin/component/SideMenu/SubMenu/index.js":
/*!********************************************************!*\
  !*** ./panelAdmin/component/SideMenu/SubMenu/index.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/SideMenu/SubMenu/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const SubMenu = ({
  menu,
  setMenuTitle,
  showLi,
  selectedMenu,
  windowLocation
}) => {
  const location = windowLocation;
  const sideMenuLi = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);

  const onSubMenuClicked = (subMenu, menu) => {
    setMenuTitle(menu.menuTitle);
  };

  let liWidth = 0;
  if (sideMenuLi.current) liWidth = sideMenuLi.current.clientHeight;
  return __jsx("ul", {
    style: {
      height: showLi === menu.menuTitle ? menu.subMenu.length * liWidth + "px" : ""
    },
    className: `side-child-navigation transition0-3 ${showLi === menu.menuTitle && menu.subMenu.length ? "showIn" : "showOut"}`,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 5
    }
  }, menu.subMenu.map((child, i) => __jsx("li", {
    ref: sideMenuLi,
    className: `side-iteme`,
    key: "subMenu-" + i,
    onClick: () => onSubMenuClicked(child, menu),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 9
    }
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
    href: child.route,
    as: child.route,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 11
    }
  }, __jsx("a", {
    className: `side-link side-child-ling ${child.route === selectedMenu || location.includes(child.route) ? "activedSideChild" : ""} `,
    id: "sideChildTitle",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 13
    }
  }, child.title)))));
};

/* harmony default export */ __webpack_exports__["default"] = (SubMenu);

/***/ }),

/***/ "./panelAdmin/component/SideMenu/index.js":
/*!************************************************!*\
  !*** ./panelAdmin/component/SideMenu/index.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dom */ "react-dom");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _MainMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./MainMenu */ "./panelAdmin/component/SideMenu/MainMenu/index.js");
/* harmony import */ var react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-scrollbars-custom */ "react-scrollbars-custom");
/* harmony import */ var react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-perfect-scrollbar */ "react-perfect-scrollbar");
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/SideMenu/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



 // import logo from "../../assets/img/Rimtal.png";

 // import "./index.scss";
// import "react-perfect-scrollbar/dist/css/styles.css";

 // import ScrollArea from "react-scrollbar";

 // const ScrollArea = require("react-scrollbar");



const SideMenu = ({
  windowLocation,
  prefetch
}) => {
  // return useMemo(() => {
  const router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])(); // useEffect(() => {
  //   if (prefetch) router.prefetch();
  // });
  //console.log({ router });

  return __jsx("div", {
    className: `panelAdmin-sideBar-container `,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 5
    }
  }, __jsx(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6___default.a, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 7
    }
  }, __jsx(_MainMenu__WEBPACK_IMPORTED_MODULE_4__["default"], {
    mainMenus: ___WEBPACK_IMPORTED_MODULE_7__["default"].menuFormat,
    windowLocation: router.route,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 9
    }
  }))); // }, []);
};

/* harmony default export */ __webpack_exports__["default"] = (SideMenu);

/***/ }),

/***/ "./panelAdmin/component/UI/BackgrandCover/index.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/component/UI/BackgrandCover/index.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/BackgrandCover/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const BackgrandCover = props => {
  return __jsx("div", {
    id: "coverContainer",
    onClick: props.onClick,
    className: props.fadeIn ? " fadeIn" : " fadeOut",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5,
      columnNumber: 5
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (BackgrandCover);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/Coordinate/index.js":
/*!************************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/Coordinate/index.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/Coordinate/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

const Coordinate = Object(react__WEBPACK_IMPORTED_MODULE_0__["memo"])(props => {
  const {
    checkSubmitted,
    onKeyDown,
    disabled,
    className,
    elementConfig,
    value,
    onChange
  } = props;

  const changedDate = event => {
    let name = event.currentTarget.name;
    let value = event.currentTarget.value; // const valid = ["month", "day"];
    // if (value < 10 && value.length < 2 && valid.includes(name)) {
    //   event.currentTarget.value = 0 + value;
    // } else if (value < 10 == 0 && value.length === 2 && value.charAt(0) == 0 && valid.includes(name)) {
    //   event.currentTarget.value = value.slice(0, 0);
    // }

    onChange(event, name);
  };

  return __jsx("div", {
    className: "date-input-entry-data",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 5
    }
  }, __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }
  }, __jsx("label", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 9
    }
  }, "\u0637\u0648\u0644"), __jsx("input", {
    name: "lat",
    value: value.lat,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 9
    }
  })), __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }
  }, __jsx("label", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 9
    }
  }, "\u0639\u0631\u0636"), __jsx("input", {
    name: "lng",
    value: value.lng,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 9
    }
  })));
});
/* harmony default export */ __webpack_exports__["default"] = (Coordinate); // const [state, setState] = useState({ year: "1300", month: "0", day: "0" });
// useEffect(() => {
//   if (value) changeValue();
// }, [value]);
// const changeValue = () => {
//   setState({ year: value.split("/")[0], month: value.split("/")[1], day: value.split("/")[2] });
// };
// const onChangeElement = (event) => {
//   let name = event.currentTarget.name;
//   let value = event.currentTarget.value;
//   let newState = { ...state };
//   let patt1 = /[0-9]/g;
//   let result = value.match(patt1);
//   if (result) newState[name] = value;
//   setState({ ...state, [name]: value });
//   if (!Object.values(state).includes("")) //console.log("etelaat poor ast");
//   event.currentTarget.value = newState.year + "/" + newState.month + "/" + newState.day;
//   onChange(event);
// };

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/DateInput/index.js":
/*!***********************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/DateInput/index.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/DateInput/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

const DateInput = Object(react__WEBPACK_IMPORTED_MODULE_0__["memo"])(props => {
  const {
    checkSubmitted,
    onKeyDown,
    disabled,
    className,
    elementConfig,
    value,
    onChange
  } = props;

  const changedDate = event => {
    let name = event.currentTarget.name;
    let value = event.currentTarget.value;
    const valid = ["month", "day"]; // if (value < 10 && value.length < 2 && valid.includes(name)) {
    //   event.currentTarget.value = 0 + value;
    // } else if (value < 10 == 0 && value.length === 2 && value.charAt(0) == 0 && valid.includes(name)) {
    //   event.currentTarget.value = value.slice(0, 0);
    // }

    onChange(event, name);
  };

  return __jsx("div", {
    className: "date-input-entry-data",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 5
    }
  }, __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }
  }, __jsx("label", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 9
    }
  }, "\u0633\u0627\u0644"), __jsx("input", {
    maxLength: "4",
    name: "year",
    value: value.year,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 9
    }
  })), __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }
  }, __jsx("label", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 9
    }
  }, "\u0645\u0627\u0647"), __jsx("input", {
    maxLength: 2,
    name: "month",
    value: value.month,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 9
    }
  })), __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }
  }, __jsx("label", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 9
    }
  }, "\u0631\u0648\u0632"), __jsx("input", {
    maxLength: 2,
    name: "day",
    value: value.day,
    onChange: changedDate,
    className: className,
    onKeyDown: onKeyDown,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 9
    }
  })));
});
/* harmony default export */ __webpack_exports__["default"] = (DateInput); // const [state, setState] = useState({ year: "1300", month: "0", day: "0" });
// useEffect(() => {
//   if (value) changeValue();
// }, [value]);
// const changeValue = () => {
//   setState({ year: value.split("/")[0], month: value.split("/")[1], day: value.split("/")[2] });
// };
// const onChangeElement = (event) => {
//   let name = event.currentTarget.name;
//   let value = event.currentTarget.value;
//   let newState = { ...state };
//   let patt1 = /[0-9]/g;
//   let result = value.match(patt1);
//   if (result) newState[name] = value;
//   setState({ ...state, [name]: value });
//   if (!Object.values(state).includes("")) //console.log("etelaat poor ast");
//   event.currentTarget.value = newState.year + "/" + newState.month + "/" + newState.day;
//   onChange(event);
// };

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/DropdownBoot/index.js":
/*!**************************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/DropdownBoot/index.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/DropdownBoot/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const DropdownBoot = props => {
  const {
    accepted,
    dropDownData,
    value,
    className,
    checkSubmitted,
    disabled
  } = props;
  let index = dropDownData && dropDownData.findIndex(d => d.value === value);
  let propsVal;
  if (index >= 0) propsVal = dropDownData[index].title;
  const {
    0: Title,
    1: setTitle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");

  const clickedElement = (value, title) => {
    if (value) accepted(value, title);
    if (title) setTitle(title);
  };

  return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 5
    }
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Toggle, {
    variant: "success",
    id: "dropdown-basic",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 7
    }
  }, Title ? Title : propsVal ? propsVal : value ? value : "انتخاب نمایید  "), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Menu, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 7
    }
  }, dropDownData && dropDownData.map((info, index) => {
    // //console.log({ value, infoval: info });
    return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Item, {
      key: index + "boot",
      active: value === info.title,
      onClick: () => clickedElement(info.value, info.title),
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 15
      }
    }, info.title);
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (DropdownBoot);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/Input.js":
/*!*************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/Input.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _InputPush__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./InputPush */ "./panelAdmin/component/UI/Inputs/InputPush/index.js");
/* harmony import */ var _InputFile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./InputFile */ "./panelAdmin/component/UI/Inputs/InputFile/index.js");
/* harmony import */ var _SearchDropDown__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./SearchDropDown */ "./panelAdmin/component/UI/Inputs/SearchDropDown/index.js");
/* harmony import */ var _InputDropDownSearch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./InputDropDownSearch */ "./panelAdmin/component/UI/Inputs/InputDropDownSearch/index.js");
/* harmony import */ var _InputFileArray__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./InputFileArray */ "./panelAdmin/component/UI/Inputs/InputFileArray/index.js");
/* harmony import */ var _DropdownBoot__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./DropdownBoot */ "./panelAdmin/component/UI/Inputs/DropdownBoot/index.js");
/* harmony import */ var _DateInput__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./DateInput */ "./panelAdmin/component/UI/Inputs/DateInput/index.js");
/* harmony import */ var _SearchDropDownArray__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./SearchDropDownArray */ "./panelAdmin/component/UI/Inputs/SearchDropDownArray/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../.. */ "./panelAdmin/index.js");
/* harmony import */ var _InputDropDownSearchArray__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./InputDropDownSearchArray */ "./panelAdmin/component/UI/Inputs/InputDropDownSearchArray/index.js");
/* harmony import */ var _Coordinate__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./Coordinate */ "./panelAdmin/component/UI/Inputs/Coordinate/index.js");
/* harmony import */ var _TwoCheckBox__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./TwoCheckBox */ "./panelAdmin/component/UI/Inputs/TwoCheckBox/index.js");
/* harmony import */ var _FormMap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../FormMap */ "./panelAdmin/component/FormMap/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/Input.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }






 // import DateCelander from "./DateCelander";









const handleKey = ___WEBPACK_IMPORTED_MODULE_9__["default"].utils.handleKey;
const Inputs = Object(react__WEBPACK_IMPORTED_MODULE_0__["memo"])(props => {
  const {
    titleValidity,
    progress,
    elementType,
    elementConfig,
    value,
    changed,
    accepted,
    label,
    invalid,
    shouldValidate,
    touched,
    removeHandel,
    dropDownData,
    defaultInputDesable,
    checkSubmitted,
    disabled,
    display,
    searchAccepted,
    setSearchAccepted,
    staticTitle
  } = props;
  let inputElement = null;
  const inputClasses = ["InputElement"]; // //console.log({ titleValidity });

  if (invalid && shouldValidate && touched) {
    inputClasses.push("Invalid");
  }

  let validStyle = "red";

  if (!touched && inputClasses.includes("Invalid")) {
    validStyle = "red";
  }

  if (touched && !inputClasses.includes("Invalid")) {
    validStyle = "green";
  }

  switch (elementType) {
    case "input":
      inputElement = __jsx("input", _extends({
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" ")
      }, elementConfig, {
        value: value,
        onChange: changed,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37,
          columnNumber: 22
        }
      }));
      break;

    case "inputPush":
      inputElement = __jsx(_InputPush__WEBPACK_IMPORTED_MODULE_1__["default"], {
        checkSubmitted: checkSubmitted,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        removeHandel: removeHandel,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 22
        }
      });
      break;

    case "inputFile":
      inputElement = __jsx(_InputFile__WEBPACK_IMPORTED_MODULE_2__["default"], _extends({
        accepted: accepted,
        disabled: disabled,
        progress: progress,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value
      }, elementConfig, {
        inputLabel: "انتخاب",
        label: label,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44,
          columnNumber: 22
        }
      }));
      break;

    case "InputFileArray":
      inputElement = __jsx(_InputFileArray__WEBPACK_IMPORTED_MODULE_5__["default"], _extends({
        onKeyDown: handleKey,
        accepted: accepted,
        removeHandel: removeHandel,
        disabled: disabled,
        progress: progress,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value
      }, elementConfig, {
        inputLabel: "انتخاب",
        label: label,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47,
          columnNumber: 22
        }
      }));
      break;

    case "inputSearch":
      inputElement = __jsx(_SearchDropDown__WEBPACK_IMPORTED_MODULE_3__["default"], {
        onKeyDown: handleKey,
        checkSubmitted: checkSubmitted,
        dropDownData: dropDownData,
        accepted: accepted,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value,
        label: label,
        elementConfig: elementConfig,
        disabled: disabled,
        searchAccepted: searchAccepted,
        setSearchAccepted: setSearchAccepted,
        staticTitle: staticTitle,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52,
          columnNumber: 9
        }
      });
      break;

    case "inputSearchArray":
      inputElement = __jsx(_SearchDropDownArray__WEBPACK_IMPORTED_MODULE_8__["default"], {
        onKeyDown: handleKey,
        checkSubmitted: checkSubmitted,
        dropDownData: dropDownData,
        accepted: accepted,
        className: inputClasses.join(" "),
        onChange: changed,
        value: value,
        label: label,
        elementConfig: elementConfig,
        disabled: disabled,
        searchAccepted: searchAccepted,
        setSearchAccepted: setSearchAccepted,
        staticTitle: staticTitle,
        removeHandel: removeHandel,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71,
          columnNumber: 9
        }
      });
      break;

    case "inputDropDownSearch":
      inputElement = __jsx(_InputDropDownSearch__WEBPACK_IMPORTED_MODULE_4__["default"], {
        dropDownData: dropDownData,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        checkSubmitted: checkSubmitted,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91,
          columnNumber: 22
        }
      });
      break;

    case "inputDropDownSearchArray":
      inputElement = __jsx(_InputDropDownSearchArray__WEBPACK_IMPORTED_MODULE_10__["default"], {
        dropDownData: dropDownData,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        checkSubmitted: checkSubmitted,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94,
          columnNumber: 22
        }
      });
      break;

    case "textarea":
      inputElement = __jsx("textarea", _extends({
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" ")
      }, elementConfig, {
        value: value,
        onChange: changed,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97,
          columnNumber: 22
        }
      }));
      break;

    case "select":
      inputElement = __jsx("select", {
        disabled: disabled,
        className: inputClasses.join(" "),
        value: value,
        onChange: changed,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101,
          columnNumber: 9
        }
      }, elementConfig.options.map(option => __jsx("option", {
        key: option.value,
        value: option.value,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 103,
          columnNumber: 13
        }
      }, option.displayValue)));
      break;

    case "inputDropDown":
      inputElement = __jsx(_DropdownBoot__WEBPACK_IMPORTED_MODULE_6__["default"], {
        dropDownData: dropDownData,
        disabled: disabled,
        className: inputClasses.join(" "),
        accepted: accepted,
        value: value,
        elementConfig: elementConfig,
        checkSubmitted: checkSubmitted,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111,
          columnNumber: 22
        }
      });
      break;
    // case "date":
    //   inputElement = (
    //     <DateCelander
    //       checkSubmitted={checkSubmitted}
    //       onKeyDown={handleKey}
    //       disabled={disabled}
    //       className={inputClasses.join(" ")}
    //       elementConfig={elementConfig}
    //       value={value}
    //       accepted={accepted}
    //     />
    //   );
    //   break;

    case "dateInput":
      inputElement = __jsx(_DateInput__WEBPACK_IMPORTED_MODULE_7__["default"], {
        checkSubmitted: checkSubmitted,
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" "),
        elementConfig: elementConfig,
        value: value,
        onChange: changed,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127,
          columnNumber: 22
        }
      });
      break;

    case "coordinate":
      inputElement = __jsx(_FormMap__WEBPACK_IMPORTED_MODULE_13__["default"], {
        checkSubmitted: checkSubmitted,
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" "),
        elementConfig: elementConfig,
        value: value,
        onChange: changed,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 130,
          columnNumber: 22
        }
      });
      break;

    case "twoCheckBox":
      inputElement = __jsx(_TwoCheckBox__WEBPACK_IMPORTED_MODULE_12__["default"], {
        checkSubmitted: checkSubmitted,
        onKeyDown: handleKey,
        disabled: disabled,
        className: inputClasses.join(" "),
        elementConfig: elementConfig,
        value: value,
        accepted: accepted,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133,
          columnNumber: 22
        }
      });
      break;

    default:
      return defaultInputDesable ? "" : inputElement = __jsx("input", _extends({
        onKeyDown: handleKey,
        className: inputClasses.join(" ")
      }, elementConfig, {
        value: value,
        onChange: changed,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 137,
          columnNumber: 57
        }
      }));
  }

  return __jsx("div", {
    className: "Input",
    style: {
      display
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 140,
      columnNumber: 5
    }
  }, __jsx("label", {
    className: "Label",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 141,
      columnNumber: 7
    }
  }, label, (shouldValidate === null || shouldValidate === void 0 ? void 0 : shouldValidate.required) ? __jsx("span", {
    style: {
      color: validStyle
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 143,
      columnNumber: 37
    }
  }, " * ") : ""), __jsx("div", {
    style: {
      width: "100%"
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 145,
      columnNumber: 7
    }
  }, __jsx("div", {
    style: {
      width: "100%",
      display: "flex"
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 146,
      columnNumber: 9
    }
  }, inputElement), __jsx("span", {
    className: "input-alert-validation",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 147,
      columnNumber: 9
    }
  }, titleValidity)));
});
/* harmony default export */ __webpack_exports__["default"] = (Inputs);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/InputDropDownSearch/index.js":
/*!*********************************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/InputDropDownSearch/index.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_LazyImage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../components/LazyImage */ "./components/LazyImage/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/InputDropDownSearch/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const CustomMenu = react__WEBPACK_IMPORTED_MODULE_0___default.a.forwardRef(({
  children,
  style,
  className,
  "aria-labelledby": labeledBy
}, ref) => {
  const {
    0: value,
    1: setValue
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  console.log({
    value
  });
  return __jsx("div", {
    ref: ref,
    style: style,
    className: className,
    "aria-labelledby": labeledBy,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 5
    }
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["FormControl"], {
    autoFocus: true,
    className: "mx-3 my-2 w-auto",
    placeholder: "\u062C\u0633\u062A\u062C\u0648 ...",
    onChange: e => setValue(e.target.value),
    value: value,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 7
    }
  }), __jsx("ul", {
    className: "list-unstyled",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 7
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.Children.toArray(children).filter(child => {
    var _child$props, _child$props2, _child$props2$childre, _child$props3, _child$props4, _child$props4$childre, _child$props4$childre2;

    // console.log({ child: child?.props?.children.length });
    // if (child?.props?.children.length == 2) console.log({ data: child?.props?.children[child?.props?.children.length - 1].props.children[0].props.children });
    // console.log({map:child?.props?.children.map(()=>{
    // })});
    if (child === null || child === void 0 ? void 0 : (_child$props = child.props) === null || _child$props === void 0 ? void 0 : _child$props.children.length) return !value || (child === null || child === void 0 ? void 0 : (_child$props2 = child.props) === null || _child$props2 === void 0 ? void 0 : (_child$props2$childre = _child$props2.children[(child === null || child === void 0 ? void 0 : (_child$props3 = child.props) === null || _child$props3 === void 0 ? void 0 : _child$props3.children.length) - 1].props.children[0].props.children) === null || _child$props2$childre === void 0 ? void 0 : _child$props2$childre.includes(value)) // ?.startsWith(value)
    // .map((lastChild) => {
    //   console.log({ lastChild });
    //   return lastChild.props.children?.toLowerCase()?.startsWith(value);
    // })
    ;else return !value || (child === null || child === void 0 ? void 0 : (_child$props4 = child.props) === null || _child$props4 === void 0 ? void 0 : (_child$props4$childre = _child$props4.children[0]) === null || _child$props4$childre === void 0 ? void 0 : (_child$props4$childre2 = _child$props4$childre.toLowerCase()) === null || _child$props4$childre2 === void 0 ? void 0 : _child$props4$childre2.includes(value));
  })));
});

const InputDropDownSearch = props => {
  const {
    accepted,
    dropDownData,
    value,
    className,
    checkSubmitted,
    disabled
  } = props; // //console.log({ dropDownData });

  let propsVal, index;
  index = dropDownData === null || dropDownData === void 0 ? void 0 : dropDownData.findIndex(d => d.value === value);
  if (index >= 0) propsVal = dropDownData[index].title;
  const {
    0: Title,
    1: setTitle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    setTitle();
  }, [checkSubmitted]);

  const clickedElement = (value, title) => {
    setTitle(title);
    accepted(value);
  };

  const CustomToggle = react__WEBPACK_IMPORTED_MODULE_0___default.a.forwardRef(({
    children,
    onClick
  }, ref) => __jsx("a", {
    className: className ? className : "",
    href: "",
    ref: ref,
    onClick: e => {
      e.preventDefault();
      return disabled ? "" : onClick(e);
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 5
    }
  }, children, "\u25BC"));
  return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 5
    }
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Toggle, {
    as: CustomToggle,
    id: "dropdown-custom-components",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 7
    }
  }, Title ? Title : propsVal ? propsVal : value ? value : "انتخاب نمایید  "), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Menu, {
    as: CustomMenu,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 7
    }
  }, dropDownData && dropDownData.map((info, index) => {
    return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Item, {
      className: "drop-item",
      key: index + "m",
      active: value === info.value,
      onClick: () => clickedElement(info.value, info.title),
      eventKey: index + 1,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 75,
        columnNumber: 15
      }
    }, info.image ? __jsx("div", {
      className: "drop-image",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77,
        columnNumber: 19
      }
    }, " ", __jsx(_components_LazyImage__WEBPACK_IMPORTED_MODULE_2__["default"], {
      src: info.image,
      defaultImage: false,
      alt: "search title",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 21
      }
    })) : "", __jsx("div", {
      className: "drop-text",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85,
        columnNumber: 17
      }
    }, __jsx("span", {
      className: "drop-title",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 19
      }
    }, info.title), __jsx("span", {
      className: "drop-description",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 87,
        columnNumber: 19
      }
    }, info.description)));
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (InputDropDownSearch);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/InputDropDownSearchArray/index.js":
/*!**************************************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/InputDropDownSearchArray/index.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/InputDropDownSearchArray/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const CustomMenu = react__WEBPACK_IMPORTED_MODULE_0___default.a.forwardRef(({
  children,
  style,
  className,
  "aria-labelledby": labeledBy
}, ref) => {
  const {
    0: value,
    1: setValue
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  return __jsx("div", {
    ref: ref,
    style: style,
    className: className,
    "aria-labelledby": labeledBy,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 5
    }
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["FormControl"], {
    autoFocus: true,
    className: "mx-3 my-2 w-auto",
    placeholder: "\u062C\u0633\u062A\u062C\u0648 ...",
    onChange: e => setValue(e.target.value),
    value: value,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 7
    }
  }), __jsx("ul", {
    className: "list-unstyled",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.Children.toArray(children).filter(child => !value || child.props.children.toLowerCase().startsWith(value))));
});

const InputDropDownSearchArray = props => {
  const {
    accepted,
    dropDownData,
    value,
    className,
    checkSubmitted,
    disabled
  } = props; // //console.log({ accepted, dropDownData, value, className, checkSubmitted, disabled });

  let propsVal;
  propsVal = dropDownData.filter(d => {
    // //console.log({ hasan: d.value, value }, value.includes(d.value));
    return value.includes(d.value);
  });
  const {
    0: Title,
    1: setTitle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(""); // //console.log({ checkSubmitted, Title });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    setTitle();
  }, [checkSubmitted]);

  const clickedElement = (value, title) => {
    let newTitle = Title ? Title + "," + title : title;
    setTitle(newTitle);
    accepted(value);
  };

  const CustomToggle = react__WEBPACK_IMPORTED_MODULE_0___default.a.forwardRef(({
    children,
    onClick
  }, ref) => __jsx("a", {
    className: className ? className : "",
    href: "",
    ref: ref,
    onClick: e => {
      e.preventDefault();
      return disabled ? "" : onClick(e);
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 5
    }
  }, children, "\u25BC"));
  return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 5
    }
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Toggle, {
    as: CustomToggle,
    id: "dropdown-custom-components",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 7
    }
  }, propsVal.length ? propsVal.map(val => {
    return val.title + ",";
  }) : value.length ? value : "انتخاب نمایید  "), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Menu, {
    as: CustomMenu,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61,
      columnNumber: 7
    }
  }, dropDownData && dropDownData.map((info, index) => {
    return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Dropdown"].Item, {
      key: index + "m",
      active: value.includes(info.value),
      onClick: () => clickedElement(info.value, info.title),
      eventKey: index + 1,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 15
      }
    }, info.title);
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (InputDropDownSearchArray);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/InputFile/index.js":
/*!***********************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/InputFile/index.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/InputFile/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const InputFile = props => {
  const {
    onChange,
    className,
    inputLabel,
    name,
    value,
    progress,
    disabled,
    accepted,
    cancelUpload,
    placeholder
  } = props; //console.log({ className });

  const elements = __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 5
    }
  }, __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 7
    }
  }, value ? value : "  انتخاب نمایید ..."), __jsx("label", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 7
    }
  }, __jsx("span", {
    onClick: disabled ? accepted : null,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 9
    }
  }, progress ? progress + "%" : inputLabel), __jsx("input", {
    disabled: progress ? true : disabled,
    type: "file",
    onChange: onChange,
    name: name,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 9
    }
  })));

  return __jsx("div", {
    className: `addFileModalContainer ${className}`,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 10
    }
  }, elements);
};

/* harmony default export */ __webpack_exports__["default"] = (InputFile);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/InputFileArray/index.js":
/*!****************************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/InputFileArray/index.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/InputFileArray/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const InputFileArray = props => {
  const {
    onChange,
    inputLabel,
    className,
    name,
    value,
    progress,
    disabled,
    cancelUpload,
    onKeyDown,
    removeHandel,
    accepted
  } = props; //console.log({ className });

  const elements = __jsx("div", {
    style: {
      display: "flex",
      flexDirection: "column",
      flex: 1
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 5
    }
  }, (value === null || value === void 0 ? void 0 : value.length) > 0 && __jsx("div", {
    className: "data-show-array",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 9
    }
  }, value.map((data, index) => {
    return __jsx("div", {
      onClick: () => removeHandel(data),
      key: index + "moj",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 15
      }
    }, __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14,
        columnNumber: 17
      }
    }, __jsx("img", {
      src: data,
      alt: "image",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15,
        columnNumber: 19
      }
    })));
  })), __jsx("div", {
    className: `addFileModalContainer ${className}`,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }
  }, __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 9
    }
  }, "  انتخاب نمایید ..."), __jsx("label", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 9
    }
  }, __jsx("span", {
    onClick: disabled ? accepted : null,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 11
    }
  }, progress ? progress + "%" : inputLabel), __jsx("input", {
    onKeyDown: onKeyDown,
    disabled: progress ? true : disabled,
    type: "file",
    onChange: onChange,
    name: name,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 11
    }
  }))));

  return elements;
};

/* harmony default export */ __webpack_exports__["default"] = (InputFileArray);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/InputPush/index.js":
/*!***********************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/InputPush/index.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/InputPush/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




const InputPush = props => {
  const {
    accepted,
    inputType,
    className,
    value,
    removeHandel,
    name,
    elementConfig,
    disabled,
    checkSubmitted
  } = props; //console.log({ InputPushValue: value });

  const {
    0: Typing,
    1: setTyping
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");

  const acceptedClick = () => {
    if (Typing.length > 0) {
      accepted(Typing);
      setTyping("");
    }
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    setTyping("");
  }, [checkSubmitted]);
  const submitRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const inputClasses = [className, "transition0-3"];

  const handelOnkeyDown = e => {
    if (e.key === "Enter") {
      submitRef.current.click();
    }
  };

  return __jsx("div", {
    style: {
      display: "flex",
      flexDirection: "column",
      flex: 1
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 5
    }
  }, (value === null || value === void 0 ? void 0 : value.length) > 0 && __jsx("div", {
    className: "data-show-array",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 9
    }
  }, value === null || value === void 0 ? void 0 : value.map((data, index) => {
    return __jsx("span", {
      onClick: () => removeHandel(data),
      key: index,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 15
      }
    }, data);
  })), __jsx("div", {
    className: "input-push-wrapper",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 7
    }
  }, __jsx("input", _extends({
    disabled: disabled,
    type: inputType,
    className: inputClasses.join(" "),
    onKeyDown: handelOnkeyDown,
    value: Typing,
    onChange: e => setTyping(e.currentTarget.value),
    autoComplete: "off"
  }, elementConfig, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 9
    }
  })), __jsx("span", {
    ref: submitRef,
    type: "submited",
    onClick: acceptedClick,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 9
    }
  }, "\u062B\u0628\u062A")));
};

/* harmony default export */ __webpack_exports__["default"] = (InputPush);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/SearchDropDown/AcceptedChild/index.js":
/*!******************************************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/SearchDropDown/AcceptedChild/index.js ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/SearchDropDown/AcceptedChild/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const AcceptedChild = ({
  data
}) => {
  return data ? __jsx("div", {
    className: "serached-item",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5,
      columnNumber: 5
    }
  }, data.image ? __jsx("div", {
    className: "searched-image",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 9
    }
  }, __jsx("img", {
    src: data.image,
    alt: "search title",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 11
    }
  })) : "", __jsx("div", {
    className: "searched-text",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }
  }, __jsx("span", {
    className: "searched-title",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 9
    }
  }, data.title), __jsx("span", {
    className: "searched-description",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 9
    }
  }, data.description))) : "";
};

/* harmony default export */ __webpack_exports__["default"] = (AcceptedChild);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/SearchDropDown/index.js":
/*!****************************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/SearchDropDown/index.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _AcceptedChild__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AcceptedChild */ "./panelAdmin/component/UI/Inputs/SearchDropDown/AcceptedChild/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/SearchDropDown/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




const SearchDropDown = props => {
  const {
    onKeyDown,
    dropDownData,
    onChange,
    accepted,
    disabled,
    checkSubmitted,
    className,
    elementConfig,
    value,
    staticTitle = ""
  } = props;
  const {
    0: search,
    1: setSearch
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]);
  const {
    0: searchTitle,
    1: setSearchTitle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  const {
    0: acceptedSave,
    1: setAcceptedSave
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  const {
    0: typing,
    1: setTping
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: lock,
    1: setLock
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false); //console.log({ search, searchTitle, acceptedSave, typing, staticTitle }, searchTitle ? searchTitle : typing ? "" : staticTitle);

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (dropDownData && dropDownData.length > 0) setSearch(dropDownData);
  }, [dropDownData]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    setSearch([]);
    setSearchTitle("");
    setAcceptedSave("");
    setTping(false);
  }, [checkSubmitted]);

  const handleBlur = e => {
    if (e.nativeEvent.explicitOriginalTarget && e.nativeEvent.explicitOriginalTarget === e.nativeEvent.originalTarget) return;
    if (search.length > 0) setTimeout(() => {
      setSearch([]);
    }, 300);
  };

  const handelOnChange = e => {
    let value = e.currentTarget.value;
    onChange(e);
    setSearchTitle(value);
    setTping(true);
    setLock(false);
  };

  const clickedElement = index => {
    setAcceptedSave(search[index]);
    setSearchTitle(search[index].title);
    accepted(search[index].value);
    setLock(true);
  };

  const searchContainer = __jsx("div", {
    className: "text-search-dropDown",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 5
    }
  }, __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 7
    }
  }, __jsx("input", _extends({
    onKeyDown: onKeyDown,
    disabled: disabled,
    value: searchTitle ? searchTitle : typing ? "" : staticTitle,
    onBlur: handleBlur,
    onChange: handelOnChange,
    className: className
  }, elementConfig, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 9
    }
  })), !lock && search && search.length > 0 && __jsx("div", {
    className: "searched-dropDown felxRow",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 11
    }
  }, search.map((searched, index) => {
    return __jsx("div", {
      key: index,
      onClick: () => clickedElement(index),
      className: "serached-item",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 51,
        columnNumber: 17
      }
    }, searched.image ? __jsx("div", {
      className: "searched-image",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 21
      }
    }, " ", __jsx("img", {
      src: searched.image,
      alt: "search title",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 55,
        columnNumber: 23
      }
    })) : "", __jsx("div", {
      className: "searched-text",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 19
      }
    }, __jsx("span", {
      className: "searched-title",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61,
        columnNumber: 21
      }
    }, searched.title), __jsx("span", {
      className: "searched-description",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62,
        columnNumber: 21
      }
    }, searched.description)));
  }))), __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70,
      columnNumber: 7
    }
  }, __jsx(_AcceptedChild__WEBPACK_IMPORTED_MODULE_1__["default"], {
    data: acceptedSave,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 9
    }
  })));

  return searchContainer;
};

/* harmony default export */ __webpack_exports__["default"] = (SearchDropDown);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/SearchDropDownArray/AcceptedChild/index.js":
/*!***********************************************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/SearchDropDownArray/AcceptedChild/index.js ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/SearchDropDownArray/AcceptedChild/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const AcceptedChild = ({
  data,
  removeHandel
}) => {
  //console.log({ data });
  return data.length ? data.map((data, index) => {
    return __jsx("div", {
      onClick: () => removeHandel(data.value),
      key: index + "sadasd",
      className: "serached-item",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9,
        columnNumber: 11
      }
    }, data.image ? __jsx("div", {
      className: "searched-image",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 15
      }
    }, __jsx("img", {
      src: data.image,
      alt: "search title",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 17
      }
    })) : "", __jsx("div", {
      className: "searched-text",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17,
        columnNumber: 13
      }
    }, __jsx("span", {
      className: "searched-title",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 18,
        columnNumber: 15
      }
    }, data.title)));
  }) : "";
};

/* harmony default export */ __webpack_exports__["default"] = (AcceptedChild);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/SearchDropDownArray/index.js":
/*!*********************************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/SearchDropDownArray/index.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _AcceptedChild__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AcceptedChild */ "./panelAdmin/component/UI/Inputs/SearchDropDownArray/AcceptedChild/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/SearchDropDownArray/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




const SearchDropDownArray = props => {
  const {
    removeHandel,
    onKeyDown,
    dropDownData,
    onChange,
    accepted,
    disabled,
    checkSubmitted,
    className,
    elementConfig,
    value,
    staticTitle
  } = props;
  const {
    0: search,
    1: setSearch
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]);
  const {
    0: searchTitle,
    1: setSearchTitle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  const {
    0: acceptedSave,
    1: setAcceptedSave
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]);
  const {
    0: typing,
    1: setTping
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: lock,
    1: setLock
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    removeIndex: null,
    prevValueLength: null
  });
  let propsVal, index;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (value.length !== state.prevValueLength) {
      //console.log(state.prevValueLength, value.length);
      let newState = acceptedSave;
      newState.splice(state.removeIndex, 1);
      setAcceptedSave(newState);
    }
  }, [value.length]); // index = dropDownData.findIndex((d) => {
  //   //console.log({ dropDownData });
  //   //console.log({ valiiiiiiiid: d.value.includes(value) });
  //   return d.value.includes(value);
  // });
  // if (index >= 0) propsVal = dropDownData[index].title;
  // //console.log({ indexArrrraaaay: index, propsVal });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (dropDownData && dropDownData.length > 0) setSearch(dropDownData);
  }, [dropDownData]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    setSearch([]);
    setSearchTitle("");
    setAcceptedSave([]);
    setTping(false);
  }, [checkSubmitted]);

  const handleBlur = e => {
    if (e.nativeEvent.explicitOriginalTarget && e.nativeEvent.explicitOriginalTarget === e.nativeEvent.originalTarget) return;
    if (search.length > 0) setTimeout(() => {
      setSearch([]);
    }, 300);
  };

  const handelOnChange = e => {
    let value = e.currentTarget.value;
    onChange(e);
    setSearchTitle(value);
    setTping(true);
    setLock(false);
  };

  const clickedElement = index => {
    let newState = acceptedSave;
    newState.push({
      title: search[index].title,
      value: search[index].value
    });
    setAcceptedSave(newState);
    setSearchTitle("");
    accepted(search[index].value);
    setLock(true);
    setState(_objectSpread({}, state, {
      prevValueLength: acceptedSave.length
    }));
  }; //console.log({ acceptedSave });


  const _removeHandel = data => {
    index = acceptedSave.findIndex(d => {
      return d.value.includes(data);
    });
    removeHandel(data);

    if (index >= 0) {
      setState(_objectSpread({}, state, {
        removeIndex: index
      }));
    }
  };

  const searchContainer = __jsx("div", {
    className: "text-search-dropDown",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79,
      columnNumber: 5
    }
  }, __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80,
      columnNumber: 7
    }
  }, __jsx("input", _extends({
    onKeyDown: onKeyDown,
    disabled: disabled,
    value: searchTitle ? searchTitle : typing ? "" : staticTitle,
    onBlur: handleBlur,
    onChange: handelOnChange,
    className: className
  }, elementConfig, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81,
      columnNumber: 9
    }
  })), !lock && search && search.length > 0 && __jsx("div", {
    className: "searched-dropDown felxRow",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 11
    }
  }, search.map((searched, index) => {
    return __jsx("div", {
      key: index,
      onClick: () => clickedElement(index),
      className: "serached-item",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 17
      }
    }, searched.image ? __jsx("div", {
      className: "searched-image",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 21
      }
    }, " ", __jsx("img", {
      src: searched.image,
      alt: "search title",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90,
        columnNumber: 23
      }
    })) : "", __jsx("div", {
      className: "searched-text",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95,
        columnNumber: 19
      }
    }, __jsx("span", {
      className: "searched-title",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 96,
        columnNumber: 21
      }
    }, searched.title), __jsx("span", {
      className: "searched-description",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97,
        columnNumber: 21
      }
    }, searched.description)));
  }))), __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 105,
      columnNumber: 7
    }
  }, __jsx(_AcceptedChild__WEBPACK_IMPORTED_MODULE_1__["default"], {
    data: acceptedSave,
    removeHandel: _removeHandel,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 106,
      columnNumber: 9
    }
  })));

  return searchContainer;
};

/* harmony default export */ __webpack_exports__["default"] = (SearchDropDownArray);

/***/ }),

/***/ "./panelAdmin/component/UI/Inputs/TwoCheckBox/index.js":
/*!*************************************************************!*\
  !*** ./panelAdmin/component/UI/Inputs/TwoCheckBox/index.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Inputs/TwoCheckBox/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const TwoCheckBox = props => {
  const {
    accepted
  } = props; //console.log({ props });

  return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, __jsx("div", {
    className: "margin-1rem centerAll",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }
  }, __jsx("label", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 9
    }
  }, " بله : "), __jsx("input", {
    className: "pointer",
    onClick: () => accepted("true"),
    type: "radio",
    name: "group1",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 9
    }
  })), __jsx("div", {
    className: "margin-1rem centerAll",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }
  }, __jsx("label", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 9
    }
  }, " خیر : "), __jsx("input", {
    className: "pointer",
    onClick: () => accepted("false"),
    type: "radio",
    name: "group1",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 9
    }
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (TwoCheckBox);

/***/ }),

/***/ "./panelAdmin/component/UI/Loadings/LoadingDot1/index.js":
/*!***************************************************************!*\
  !*** ./panelAdmin/component/UI/Loadings/LoadingDot1/index.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Loadings/LoadingDot1/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const LoadingDot1 = ({
  width,
  height
}) => {
  return __jsx("div", {
    className: "sk-chase",
    style: {
      width,
      height
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "sk-chase-dot",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5,
      columnNumber: 7
    }
  }), __jsx("div", {
    className: "sk-chase-dot",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 7
    }
  }), __jsx("div", {
    className: "sk-chase-dot",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 7
    }
  }), __jsx("div", {
    className: "sk-chase-dot",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 7
    }
  }), __jsx("div", {
    className: "sk-chase-dot",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }
  }), __jsx("div", {
    className: "sk-chase-dot",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 7
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (LoadingDot1);

/***/ }),

/***/ "./panelAdmin/component/UI/Loadings/SpinnerRotate/index.js":
/*!*****************************************************************!*\
  !*** ./panelAdmin/component/UI/Loadings/SpinnerRotate/index.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Loadings/SpinnerRotate/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const SpinnerRotate = () => {
  return __jsx("div", {
    className: "spinner",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "cube1",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5,
      columnNumber: 7
    }
  }), __jsx("div", {
    className: "cube2",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 7
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (SpinnerRotate);

/***/ }),

/***/ "./panelAdmin/component/UI/Modals/ModalBox/index.js":
/*!**********************************************************!*\
  !*** ./panelAdmin/component/UI/Modals/ModalBox/index.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _BackgrandCover__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../BackgrandCover */ "./panelAdmin/component/UI/BackgrandCover/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Modals/ModalBox/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ModalBox = props => {
  const {
    showModal,
    onHideModal
  } = props;
  const {
    0: modalHide,
    1: setModalHide
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(true);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (showModal) {
      setModalHide(false);
    }
  }, [showModal]);

  const endAnimation = () => {
    if (!showModal) {
      onHideModal();
      setModalHide(true);
    }
  };

  return __jsx("div", {
    className: "modal_container-box",
    style: {
      display: modalHide ? "none" : ""
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 5
    }
  }, __jsx("div", {
    onAnimationEnd: endAnimation,
    id: "subjectModal",
    className: showModal ? "fadeIn" : "fadeOut",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "subjectModal-box",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 9
    }
  }, props.children)), __jsx(_BackgrandCover__WEBPACK_IMPORTED_MODULE_1__["default"], {
    onClick: onHideModal,
    fadeIn: !modalHide,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 7
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (ModalBox);

/***/ }),

/***/ "./panelAdmin/component/UI/Modals/ModalStructure/index.js":
/*!****************************************************************!*\
  !*** ./panelAdmin/component/UI/Modals/ModalStructure/index.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ModalBox__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../ModalBox */ "./panelAdmin/component/UI/Modals/ModalBox/index.js");
/* harmony import */ var _ModalTrueFalse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ModalTrueFalse */ "./panelAdmin/component/UI/Modals/ModalTrueFalse/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Modals/ModalStructure/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const ModalStructure = props => {
  const {
    modalDetails,
    onHideModal,
    reqApiRemove,
    modalRequest,
    loading
  } = props; // alert("modal");
  // useEffect(() => {
  //   if (modalDetails.kindOf === "question") ModalTrueFalse({ modalHeadline: "آیا مطمئن به حذف آن هستید !", modalAcceptTitle: "بله", modalCanselTitle: "خیر", modalAccept: modalRequest, loading: loading });
  // }, [modalDetails.kindOf]);

  return __jsx("div", {
    style: {
      position: "absolute"
    },
    className: modalDetails.kindOf === "component" ? "width80" : "",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 5
    }
  }, __jsx(_ModalBox__WEBPACK_IMPORTED_MODULE_1__["default"], {
    onHideModal: onHideModal,
    showModal: modalDetails.show,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }
  }, modalDetails.kindOf === "question" && __jsx(_ModalTrueFalse__WEBPACK_IMPORTED_MODULE_2__["default"], {
    modalHeadline: "آیا مطمئن به حذف آن هستید !",
    modalAcceptTitle: "بله",
    modalCanselTitle: "خیر",
    modalAccept: modalRequest,
    loading: loading,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 48
    }
  }), props.children));
};

/* harmony default export */ __webpack_exports__["default"] = (ModalStructure);

/***/ }),

/***/ "./panelAdmin/component/UI/Modals/ModalTrueFalse/index.js":
/*!****************************************************************!*\
  !*** ./panelAdmin/component/UI/Modals/ModalTrueFalse/index.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Loadings_LoadingDot1__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Loadings/LoadingDot1 */ "./panelAdmin/component/UI/Loadings/LoadingDot1/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Modals/ModalTrueFalse/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ModalTrueFalse = props => {
  // window.onscroll = function () {
  //   scrollFunction();
  // };
  // const scrollFunction=() =>{
  //   if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
  // }
  // useEffect(() => {
  //   let subjectModal = document.querySelector("#subjectModal");
  //   //console.log({ subjectModal });
  //   subjectModal.scrollTop = 0;
  //   // subjectModal.documentElement.scrollTop = 0;
  // });
  // When the user clicks on the button, scroll to the top of the document
  return __jsx("div", {
    className: "modal-box",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "m-b-question-title",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }
  }, __jsx("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 9
    }
  }, props.modalHeadline), __jsx("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 9
    }
  }, props.modalQuestion)), __jsx("div", {
    className: "btns-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }
  }, __jsx("div", {
    disabled: props.loading,
    onClick: props.loading ? null : () => props.modalAccept(true),
    className: "submited-box",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 9
    }
  }, __jsx("a", {
    className: "btns btns-primary",
    type: "submit",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 11
    }
  }, props.loading ? __jsx(_Loadings_LoadingDot1__WEBPACK_IMPORTED_MODULE_1__["default"], {
    width: "1.5rem",
    height: "1.5rem",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 30
    }
  }) : props.modalAcceptTitle)), __jsx("div", {
    onClick: () => props.modalAccept(false),
    className: "submited-box",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 9
    }
  }, __jsx("a", {
    className: "btns btns-warning",
    type: "submit",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 11
    }
  }, " ", props.modalCanselTitle))));
};

/* harmony default export */ __webpack_exports__["default"] = (ModalTrueFalse);

/***/ }),

/***/ "./panelAdmin/component/UI/Modals/MyVerticallyCenteredModal/index.js":
/*!***************************************************************************!*\
  !*** ./panelAdmin/component/UI/Modals/MyVerticallyCenteredModal/index.js ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Modals/MyVerticallyCenteredModal/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const MyVerticallyCenteredModal = props => {
  const {
    status,
    setStatus,
    onChange,
    title,
    onSubmit,
    acceptedBtn,
    cancelledBtn,
    size,
    acceptedDisabled
  } = props;

  const submitModal = e => {
    setStatus(false);
    onSubmit(e);
  };

  return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"] // {...props}
  , {
    size: size,
    style: {
      direction: "rtl"
    },
    "aria-labelledby": "contained-modal-title-vcenter",
    centered: true,
    show: status,
    onHide: submitModal,
    animation: true,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 5
    }
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"].Header, {
    closeButton: true,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"].Title, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 9
    }
  }, title)), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"].Body, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 7
    }
  }, props.children), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"].Footer, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }
  }, cancelledBtn ? __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    variant: "secondary",
    onClick: () => setStatus(false),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 11
    }
  }, cancelledBtn) : "", acceptedBtn ? __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    disabled: acceptedDisabled,
    variant: "primary",
    onClick: submitModal,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 11
    }
  }, acceptedBtn) : ""));
};

/* harmony default export */ __webpack_exports__["default"] = (MyVerticallyCenteredModal);

/***/ }),

/***/ "./panelAdmin/component/UI/PaginationM/index.js":
/*!******************************************************!*\
  !*** ./panelAdmin/component/UI/PaginationM/index.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/PaginationM/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const PaginationM = props => {
  const {
    limited,
    pages,
    activePage,
    onClick
  } = props;
  let sort = [];
  if (pages === undefined || pages === 1) return __jsx("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 50
    }
  });
  Number(activePage); // console.log({ limited, pages, activePage });

  let viewNext = +limited + +activePage;
  let viewPrev = activePage - limited; // console.log({ viewNext, viewPrev });

  for (let index = 1; index <= pages; index++) {
    if (pages >= 9) {
      // console.log({ check: pages - limited }, limited - pages === index, { index });
      if (2 == index && viewPrev == 2) sort.unshift(1);
      if (viewPrev <= index && viewNext >= index) sort.push(index);
    } else if (pages <= 9) sort.push(index);
  }

  if (pages - limited - 1 == activePage && pages >= 9) sort.push(pages); // console.log({ active: pages - limited - 1 == activePage, sort });

  let prevFirst = Number(activePage) < Number(limited);
  let prevSecond = Number(activePage) === Number(1);
  let nextFirst = Number(activePage) === Number(pages);
  let nextSecond = Number(activePage) > Number(pages - limited); // console.log({ prevFirst });
  // console.log({ prevSecond });
  // console.log({ nextFirst });
  // console.log({ nextSecond });
  // console.log({ sort });

  let Pagination_Sort = __jsx("div", {
    className: "Pagination-wrapper",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 5
    }
  }, " ", __jsx("div", {
    className: "paginationIcon centerAll translateR transition0-2",
    disabled: prevFirst,
    onClick: () => prevFirst ? "" : onClick(1),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 7
    }
  }, __jsx("i", {
    className: "fa fa-angle-double-right",
    "aria-hidden": "true",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 9
    }
  })), __jsx("div", {
    className: "paginationIcon centerAll translateR transition0-2",
    disabled: prevSecond,
    onClick: () => prevSecond ? "" : onClick(activePage - 1),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 7
    }
  }, __jsx("i", {
    className: "fa fa-angle-right",
    "aria-hidden": "true",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 9
    }
  })), Number(activePage) > 2 + Number(limited) && pages >= 9 && __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "paginationIcon centerAll translateT transition0-2",
    onClick: () => onClick(1),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 11
    }
  }, 1), __jsx("div", {
    className: "paginationIcon centerAll",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 11
    }
  }, __jsx("i", {
    className: "fa fa-ellipsis-h",
    "aria-hidden": "true",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 13
    }
  }))), sort.map((number, index) => {
    if (number == 1 && +activePage > +limited + 2 && +pages >= 9) return;else if (number === pages && activePage < pages - limited && pages >= 9) return;else return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, __jsx("div", {
      className: `paginationIcon centerAll translateT transition0-2 ${number === Number(activePage) && "actived"}`,
      onClick: () => onClick(number === Number(activePage) ? false : number),
      key: index + "m",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 58,
        columnNumber: 15
      }
    }, number));
  }), pages >= 9 && pages - limited - 1 == activePage && __jsx("div", {
    className: `paginationIcon centerAll translateT transition0-2 `,
    onClick: () => onClick(number === Number(activePage) ? false : pages),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 9
    }
  }, pages), activePage < pages - limited - 1 && pages >= 9 && __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "paginationIcon centerAll",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 11
    }
  }, __jsx("i", {
    className: "fa fa-ellipsis-h",
    "aria-hidden": "true",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 13
    }
  })), __jsx("div", {
    className: "paginationIcon centerAll translateT transition0-2",
    onClick: () => onClick(pages),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 11
    }
  }, pages)), __jsx("div", {
    className: "paginationIcon centerAll translateL transition0-2",
    disabled: nextFirst,
    onClick: () => nextFirst ? "" : onClick(+activePage + +1),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80,
      columnNumber: 7
    }
  }, __jsx("i", {
    className: "fa fa-angle-left",
    "aria-hidden": "true",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81,
      columnNumber: 9
    }
  })), __jsx("div", {
    className: "paginationIcon centerAll translateL transition0-2",
    disabled: nextSecond,
    onClick: () => nextSecond ? "" : onClick(Number(pages)),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 7
    }
  }, __jsx("i", {
    className: "fa fa-angle-double-left",
    "aria-hidden": "true",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 9
    }
  })));

  return Pagination_Sort;
};

/* harmony default export */ __webpack_exports__["default"] = (PaginationM);

/***/ }),

/***/ "./panelAdmin/component/UI/Rating/Star/index.js":
/*!******************************************************!*\
  !*** ./panelAdmin/component/UI/Rating/Star/index.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_lab_Rating__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/lab/Rating */ "@material-ui/lab/Rating");
/* harmony import */ var _material_ui_lab_Rating__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_lab_Rating__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/UI/Rating/Star/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const Star = props => {
  const {
    rating,
    fixed,
    size
  } = props;
  const {
    0: Value,
    1: setValue
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])();
  const {
    0: Hover,
    1: setHover
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])();
  const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__["makeStyles"])(theme => ({
    root: {
      display: "flex",
      flexDirection: "column",
      "& > * + *": {
        marginTop: theme.spacing(1)
      }
    }
  }));
  const classes = useStyles();

  let element = __jsx(_material_ui_lab_Rating__WEBPACK_IMPORTED_MODULE_1___default.a, {
    name: "half-rating",
    defaultValue: Value ? Value : 2.5,
    precision: 0.5,
    onChange: (event, newValue) => {
      setValue(newValue);
    },
    onChangeActive: (event, newHover) => {
      setHover(newHover);
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 5
    }
  });

  if (fixed) {
    element = __jsx(_material_ui_lab_Rating__WEBPACK_IMPORTED_MODULE_1___default.a, {
      name: "half-rating-read",
      defaultValue: rating,
      precision: 0.5,
      readOnly: true,
      size: size,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 15
      }
    });
  }

  return __jsx("div", {
    style: {
      direction: "ltr",
      marginRight: "auto",
      textAlign: "center",
      justifyContent: "center",
      alignItems: "center"
    },
    className: classes.root,
    size: size,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 5
    }
  }, element);
};

/* harmony default export */ __webpack_exports__["default"] = (Star);

/***/ }),

/***/ "./panelAdmin/component/cards/CardElement/CardRow/index.js":
/*!*****************************************************************!*\
  !*** ./panelAdmin/component/cards/CardElement/CardRow/index.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _UI_Rating_Star__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../UI/Rating/Star */ "./panelAdmin/component/UI/Rating/Star/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../.. */ "./panelAdmin/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/cards/CardElement/CardRow/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




const formatMoney = ___WEBPACK_IMPORTED_MODULE_2__["default"].utils.formatMoney;

const CardRow = props => {
  const {
    elementType,
    value,
    className,
    style,
    iconStyle,
    title
  } = props;
  let element;

  switch (elementType) {
    case "text":
      element = __jsx("span", {
        title: title,
        style: _objectSpread({}, style),
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 13,
          columnNumber: 9
        }
      }, value);
      break;

    case "star":
      element = __jsx(_UI_Rating_Star__WEBPACK_IMPORTED_MODULE_1__["default"], {
        rating: value,
        fixed: true,
        size: "small",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 19,
          columnNumber: 17
        }
      });
      break;

    case "price":
      element = __jsx("span", {
        style: _objectSpread({}, style),
        className: "s-c-card-price",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 24,
          columnNumber: 9
        }
      }, formatMoney(value));
      break;

    case "icon":
      element = __jsx("div", {
        className: "cardPhonenumber-wrapper",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 31,
          columnNumber: 9
        }
      }, " ", __jsx("span", {
        style: _objectSpread({}, style),
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 11
        }
      }, value), __jsx("i", {
        style: _objectSpread({}, iconStyle),
        className: className,
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 11
        }
      }));
      break;

    case "district":
      element = __jsx("div", {
        className: "cardDistric-wrapper",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 9
        }
      }, " ", __jsx("i", {
        className: " icon-location-1",
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42,
          columnNumber: 11
        }
      }), __jsx("span", {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43,
          columnNumber: 11
        }
      }, value));
      break;

    default:
      break;
  }

  return element;
};

/* harmony default export */ __webpack_exports__["default"] = (CardRow);

/***/ }),

/***/ "./panelAdmin/component/cards/CardElement/index.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/component/cards/CardElement/index.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CardRow__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CardRow */ "./panelAdmin/component/cards/CardElement/CardRow/index.js");
/* harmony import */ var _components_LazyImage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../components/LazyImage */ "./components/LazyImage/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/cards/CardElement/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const CardElement = react__WEBPACK_IMPORTED_MODULE_0___default.a.memo(props => {
  const {
    data,
    index,
    onClick,
    submitedTitle,
    optionClick,
    options,
    acceptedCardInfo
  } = props; // //console.log({ data });

  return __jsx("div", {
    style: {
      animationDelay: index * 150 + "ms"
    },
    key: index ? index : "",
    className: `show-Card-Information-row px-1  col-xl-2 col-lg-2 col-6 col-sm-6 col-md-6`,
    onClick: acceptedCardInfo ? () => acceptedCardInfo.handelAcceptedImage({
      index,
      data
    }) : null,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 5
    }
  }, __jsx("div", {
    //  style={{ boxShadow: data.isActive ? "" : "0 0 6px 3px #ff00008a" }}
    className: `card-info transition0-2 ${data.isAccept}`,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "s-c--card-images transition0-2",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "options-card transition0-2",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 11
    }
  }, options ? __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 15
    }
  }, " ", options.remove && __jsx("span", {
    className: "options-card-cancel",
    onClick: () => optionClick({
      _id: data._id,
      mission: "remove"
    }),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 19
    }
  }, __jsx("i", {
    className: "fas fa-times",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 21
    }
  })), options.edit && __jsx("span", {
    className: "options-card-edit",
    onClick: () => optionClick({
      _id: data._id,
      mission: "edit",
      index
    }),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 19
    }
  }, __jsx("i", {
    className: "fas fa-pencil-alt",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 21
    }
  })), " ", options.block && __jsx("span", {
    className: "options-card-block",
    onClick: () => optionClick({
      _id: data._id,
      mission: "block",
      value: !data.isActive
    }),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 19
    }
  }, __jsx("i", {
    className: "fas fa-unlock",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 21
    }
  }))) : ""), data.image ? __jsx("picture", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 13
    }
  }, __jsx("source", {
    media: "(max-width: 375px)",
    srcSet: data.image.value,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 15
    }
  }), __jsx(_components_LazyImage__WEBPACK_IMPORTED_MODULE_2__["default"], {
    src: data.image.value,
    defaultImage: false,
    alt: "cardImage",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 15
    }
  })) : ""), __jsx("div", {
    className: "s-c-card-body",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "s-c-body-wrapper",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "s-c-card-title",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 13
    }
  }, data.body && data.body.length > 0 && data.body.map((info, index) => {
    // //console.log({ info });
    return __jsx("div", {
      key: index + "mmj",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62,
        columnNumber: 21
      }
    }, __jsx("div", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63,
        columnNumber: 23
      }
    }, info.right && info.right.map(right => Object(_CardRow__WEBPACK_IMPORTED_MODULE_1__["default"])(right))), __jsx("div", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64,
        columnNumber: 23
      }
    }, info.left && info.left.map(left => Object(_CardRow__WEBPACK_IMPORTED_MODULE_1__["default"])(left))));
  }))), submitedTitle ? __jsx("div", {
    className: "btns-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 13
    }
  }, __jsx("button", {
    onClick: () => onClick(index),
    className: "btns btns-primary",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 15
    }
  }, submitedTitle)) : "")));
});
/* harmony default export */ __webpack_exports__["default"] = (CardElement);

/***/ }),

/***/ "./panelAdmin/component/cards/ShowCardInformation/index.js":
/*!*****************************************************************!*\
  !*** ./panelAdmin/component/cards/ShowCardInformation/index.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _CardElement__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../CardElement */ "./panelAdmin/component/cards/CardElement/index.js");
/* harmony import */ var react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-scrollbars-custom */ "react-scrollbars-custom");
/* harmony import */ var react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_scrollbars_custom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-perfect-scrollbar */ "react-perfect-scrollbar");
/* harmony import */ var react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/cards/ShowCardInformation/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }






const ShowCardInformation = ({
  data,
  onClick,
  submitedTitle,
  optionClick,
  options,
  acceptedCardInfo
}) => {
  // const optionClick = ({ _id, mission }) => {
  //   //console.log({ _id, mission });
  // };
  const showDataAll = data === null || data === void 0 ? void 0 : data.map((information, index) => {
    // //console.log({ information });
    return __jsx(_CardElement__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({
      options,
      optionClick,
      index,
      onClick,
      submitedTitle,
      acceptedCardInfo
    }, {
      key: index + "mmm",
      data: information,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 12
      }
    }));
  });

  const ShowData = __jsx(_CardElement__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({
    data,
    onClick,
    submitedTitle,
    optionClick,
    options,
    acceptedCardInfo
  }, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 20
    }
  }));

  return __jsx("div", {
    className: "show-card-elements row m-0",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 10
    }
  }, data.length > 0 ? showDataAll : ShowData);
};

/* harmony default export */ __webpack_exports__["default"] = (ShowCardInformation);

/***/ }),

/***/ "./panelAdmin/index.js":
/*!*****************************!*\
  !*** ./panelAdmin/index.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_menuFormat__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils/menuFormat */ "./panelAdmin/utils/menuFormat.js");
/* harmony import */ var _values__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./values */ "./panelAdmin/values/index.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils */ "./panelAdmin/utils/index.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./api */ "./panelAdmin/api/index.js");




const panelAdmin = {
  menuFormat: _utils_menuFormat__WEBPACK_IMPORTED_MODULE_0__["default"],
  values: _values__WEBPACK_IMPORTED_MODULE_1__["default"],
  utils: _utils__WEBPACK_IMPORTED_MODULE_2__["default"],
  api: _api__WEBPACK_IMPORTED_MODULE_3__
};
/* harmony default export */ __webpack_exports__["default"] = (panelAdmin);

/***/ }),

/***/ "./panelAdmin/screen/Banner/AddBanner/DependentComponent/EditData/index.js":
/*!*********************************************************************************!*\
  !*** ./panelAdmin/screen/Banner/AddBanner/DependentComponent/EditData/index.js ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const EditData = props => {
  const {
    editData,
    stateArray,
    inputChangedHandler,
    setStaticTitle
  } = props;
  let arrayData = [];
  if (editData) for (const key in editData) for (let index = 0; index < stateArray.length; index++) {
    if (stateArray[index].id === key) if (key === "category") arrayData.push({
      name: key,
      value: editData[key] ? editData[key]._id : ""
    });else if (key === "store") {
      if (editData[key]) setStaticTitle({
        value: editData[key] ? editData[key].title : "",
        name: key
      });
      arrayData.push({
        name: key,
        value: editData[key] ? editData[key]._id : ""
      });
    } else arrayData.push({
      name: key,
      value: editData[key] ? editData[key] : editData[key] ? editData[key] : ""
    });
  }
  if (arrayData.length > 0) inputChangedHandler(arrayData, false, {
    value: editData.parentType
  });
};

/* harmony default export */ __webpack_exports__["default"] = (EditData);

/***/ }),

/***/ "./panelAdmin/screen/Banner/AddBanner/DependentComponent/ModalOption/index.js":
/*!************************************************************************************!*\
  !*** ./panelAdmin/screen/Banner/AddBanner/DependentComponent/ModalOption/index.js ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _component_UI_Modals_ModalStructure__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../component/UI/Modals/ModalStructure */ "./panelAdmin/component/UI/Modals/ModalStructure/index.js");
/* harmony import */ var _pages_panelAdmin_gallery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../pages/panelAdmin/gallery */ "./pages/panelAdmin/gallery.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/Banner/AddBanner/DependentComponent/ModalOption/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const ModalOption = props => {
  const {
    inputChangedHandler,
    modalRequest,
    onHideModal,
    modalDetails,
    imageAccepted,
    acceptedImageFinal,
    acceptedImage
  } = props;
  return __jsx(_component_UI_Modals_ModalStructure__WEBPACK_IMPORTED_MODULE_1__["default"], {
    modalRequest: modalRequest,
    reqApiRemove: null,
    onHideModal: onHideModal,
    modalDetails: modalDetails,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 5
    }
  }, modalDetails.kindOf === "showGallery" && __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 9
    }
  }, __jsx(_pages_panelAdmin_gallery__WEBPACK_IMPORTED_MODULE_2__["default"], {
    clickedParent: value => inputChangedHandler({
      name: modalDetails.name,
      value
    }),
    acceptedCardInfo: {
      handelAcceptedImage: acceptedImage,
      acceptedCard: imageAccepted
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 11
    }
  }), __jsx("div", {
    className: "btns-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 11
    }
  }, __jsx("button", {
    disabled: !imageAccepted,
    className: "btns btns-primary",
    onClick: e => acceptedImageFinal(e),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 13
    }
  }, "\u062A\u0627\u06CC\u06CC\u062F", " "), __jsx("button", {
    className: "btns btns-warning",
    onClick: onHideModal,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 13
    }
  }, "\u0628\u0633\u062A\u0646", " "))));
};

/* harmony default export */ __webpack_exports__["default"] = (ModalOption);

/***/ }),

/***/ "./panelAdmin/screen/Banner/AddBanner/DependentComponent/Submitted/index.js":
/*!**********************************************************************************!*\
  !*** ./panelAdmin/screen/Banner/AddBanner/DependentComponent/Submitted/index.js ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_updateObject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../utils/updateObject */ "./panelAdmin/utils/updateObject.js");



const Submitted = async props => {
  const {
    setSubmitLoading,
    data,
    editData,
    put,
    post,
    setData,
    states,
    setEdit,
    propsHideModal,
    setCheckSubmitted,
    checkSubmitted
  } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};

  for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;

  const initialStateOwner = Object(_utils_updateObject__WEBPACK_IMPORTED_MODULE_1__["default"])(states.addBanner.Form["owner"], {
    value: []
  });
  const updatedForm = Object(_utils_updateObject__WEBPACK_IMPORTED_MODULE_1__["default"])(states.addBanner.Form, {
    ["owner"]: initialStateOwner
  });

  if (editData) {
    if (await put.banner({
      id: editData._id,
      data: formData
    })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post.banner(formData)) setData({
    Form: updatedForm,
    formIsValid: false
  });

  setSubmitLoading(false);
};

/* harmony default export */ __webpack_exports__["default"] = (Submitted);

/***/ }),

/***/ "./panelAdmin/screen/Banner/AddBanner/FormInputBanner/index.js":
/*!*********************************************************************!*\
  !*** ./panelAdmin/screen/Banner/AddBanner/FormInputBanner/index.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _component_UI_Inputs_Input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../component/UI/Inputs/Input */ "./panelAdmin/component/UI/Inputs/Input.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../.. */ "./panelAdmin/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/Banner/AddBanner/FormInputBanner/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }





const FormInputBanner = props => {
  const {
    stateArray,
    removeHandel,
    state,
    _onSubmited,
    inputChangedHandler,
    checkSubmitted,
    showModal,
    staticTitle
  } = props;
  const {
    0: Store,
    1: setStore
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);

  const accept = event => {
    inputChangedHandler(event);
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    getApi();
  }, []);

  const getApi = async () => {
    const resStore = await ___WEBPACK_IMPORTED_MODULE_2__["default"].api.get.store();
    setStore(resStore === null || resStore === void 0 ? void 0 : resStore.data);
  };

  let storeData = [];

  for (const index in Store) storeData.push({
    value: Store[index]._id,
    title: Store[index].title,
    description: Store[index].district,
    image: Store[index].slides[0]
  });

  return __jsx("form", {
    onSubmit: _onSubmited,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 5
    }
  }, stateArray.map(formElement => {
    const invalid = !formElement.config.valid;
    const shouldValidate = formElement.config.validation;
    const touched = formElement.config.touched;
    let progress, disabled, staticTitleValue;
    let value = formElement.config.value;
    disabled = false;
    const inputClasses = ["InputElement"];
    if (formElement.id === staticTitle.name) staticTitleValue = staticTitle.value;
    if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
    let FormManagement = ___WEBPACK_IMPORTED_MODULE_2__["default"].utils.operation.FormManagement({
      formElement,
      showModal,
      inputChangedHandler,
      accept,
      storeData,
      removeHandel,
      staticTitle
    });

    let form = __jsx(_component_UI_Inputs_Input__WEBPACK_IMPORTED_MODULE_1__["default"], _extends({
      invalid: invalid,
      shouldValidate: shouldValidate,
      touched: touched,
      progress: progress,
      checkSubmitted: checkSubmitted
    }, FormManagement, {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 20
      }
    }));

    return form;
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (FormInputBanner);

/***/ }),

/***/ "./panelAdmin/screen/Banner/AddBanner/index.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/screen/Banner/AddBanner/index.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../api */ "./panelAdmin/api/index.js");
/* harmony import */ var _FormInputBanner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./FormInputBanner */ "./panelAdmin/screen/Banner/AddBanner/FormInputBanner/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../.. */ "./panelAdmin/index.js");
/* harmony import */ var _component_UI_Modals_ModalBox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../component/UI/Modals/ModalBox */ "./panelAdmin/component/UI/Modals/ModalBox/index.js");
/* harmony import */ var _pages_panelAdmin_gallery__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../pages/panelAdmin/gallery */ "./pages/panelAdmin/gallery.js");
/* harmony import */ var _component_UI_Loadings_LoadingDot1__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component/UI/Loadings/LoadingDot1 */ "./panelAdmin/component/UI/Loadings/LoadingDot1/index.js");
/* harmony import */ var _DependentComponent_ModalOption__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./DependentComponent/ModalOption */ "./panelAdmin/screen/Banner/AddBanner/DependentComponent/ModalOption/index.js");
/* harmony import */ var _DependentComponent_Submitted__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./DependentComponent/Submitted */ "./panelAdmin/screen/Banner/AddBanner/DependentComponent/Submitted/index.js");
/* harmony import */ var _DependentComponent_EditData__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./DependentComponent/EditData */ "./panelAdmin/screen/Banner/AddBanner/DependentComponent/EditData/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/Banner/AddBanner/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }












const AddBanner = props => {
  const {
    editData,
    setEdit,
    propsHideModal
  } = props;
  const states = ___WEBPACK_IMPORTED_MODULE_3__["default"].utils.consts.states;
  const onChanges = ___WEBPACK_IMPORTED_MODULE_3__["default"].utils.onChanges;
  const {
    0: data,
    1: setData
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(_objectSpread({}, states.addBanner));
  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    progressPercentImage: null,
    remove: {
      value: "",
      name: ""
    }
  });
  const {
    0: Loading,
    1: setLoading
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: submitLoading,
    1: setSubmitLoading
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: modalDetails,
    1: setModalDetails
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    show: false,
    kindOf: null,
    data: {
      src: false,
      type: false,
      name: false
    },
    name: null,
    removeId: null,
    editId: null,
    editData: []
  });
  const {
    0: imageAccepted,
    1: setImageAccepted
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])();
  const {
    0: checkSubmitted,
    1: setCheckSubmitted
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: staticTitle,
    1: setStaticTitle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false); // ========================================================= change edit data structure for state

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    Object(_DependentComponent_EditData__WEBPACK_IMPORTED_MODULE_9__["default"])({
      editData,
      stateArray,
      inputChangedHandler,
      setStaticTitle
    });
  }, [editData]); // ========================================================= modal
  // ================================== modal close

  const onHideModal = () => {
    setModalDetails(_objectSpread({}, modalDetails, {
      show: false,
      kindOf: false,
      removeId: null
    }));
  }; // ================================== modal open


  const onShowModal = event => {
    setModalDetails(_objectSpread({}, modalDetails, {
      show: true,
      kindOf: event.kindOf,
      name: event === null || event === void 0 ? void 0 : event.name,
      editData: event === null || event === void 0 ? void 0 : event.editData,
      removeId: event === null || event === void 0 ? void 0 : event.removeId
    }));
  }; // ================================== handel modal end work


  const modalRequest = async bool => {
    if (bool) inputChangedHandler({
      name: state.remove.name,
      value: state.remove.value
    });
    onHideModal();
  }; // ========================================================= END modal


  const removeHandel = (value, name) => {
    onShowModal({
      kindOf: "question"
    });
    setState(_objectSpread({}, state, {
      remove: {
        value,
        name
      }
    }));
  }; // ========================================================= accepted image for add data


  const acceptedImage = ({
    index,
    data
  }) => {
    let images = data.image.value;
    imageAccepted === images ? setImageAccepted(null) : setImageAccepted(images);
  };

  const acceptedImageFinal = () => {
    onHideModal();
    inputChangedHandler({
      name: "image",
      value: imageAccepted
    });
  }; // ========================================================= End accepted image for add data
  // ============================= submited


  const _onSubmitted = async e => ___WEBPACK_IMPORTED_MODULE_3__["default"].utils.operation.submitted({
    setSubmitLoading,
    data,
    editData,
    put: _api__WEBPACK_IMPORTED_MODULE_1__["put"].banner,
    post: _api__WEBPACK_IMPORTED_MODULE_1__["post"].banner,
    setData,
    states: states.addBanner,
    setEdit,
    propsHideModal,
    setCheckSubmitted,
    checkSubmitted
  });

  const checkValid = () => ___WEBPACK_IMPORTED_MODULE_3__["default"].utils.operation.formTouchChange({
    data,
    setData
  }); // ========================= End submited =================


  const inputChangedHandler = async event => await onChanges.globalChange({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType: "flag"
  });

  const stateArray = [];

  for (let key in data.Form) stateArray.push({
    id: key,
    config: data.Form[key]
  });

  let form = __jsx(_FormInputBanner__WEBPACK_IMPORTED_MODULE_2__["default"], _extends({
    removeHandel,
    stateArray,
    data,
    state,
    setData,
    Loading,
    setLoading,
    inputChangedHandler,
    checkSubmitted,
    staticTitle
  }, {
    showModal: onShowModal,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 14
    }
  }));

  return __jsx("div", {
    className: "countainer-main  ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 5
    }
  }, __jsx(_DependentComponent_ModalOption__WEBPACK_IMPORTED_MODULE_7__["default"], _extends({
    inputChangedHandler,
    modalRequest,
    onHideModal,
    modalDetails,
    imageAccepted,
    acceptedImageFinal,
    acceptedImage,
    Gallery: _pages_panelAdmin_gallery__WEBPACK_IMPORTED_MODULE_5__["default"]
  }, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79,
      columnNumber: 7
    }
  })), __jsx("div", {
    className: "form-countainer",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "row-give-information",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81,
      columnNumber: 9
    }
  }, form, __jsx("div", {
    className: "btns-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 11
    }
  }, __jsx("button", {
    className: "btns btns-primary",
    disabled: submitLoading,
    onClick: data.formIsValid ? _onSubmitted : checkValid,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 13
    }
  }, submitLoading ? __jsx(_component_UI_Loadings_LoadingDot1__WEBPACK_IMPORTED_MODULE_6__["default"], {
    width: "1.5rem",
    height: "1.5rem",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 32
    }
  }) : editData ? data.formIsValid ? "ثبت تغییرات" : "تغیراتی مشاهده نمی شود" : data.formIsValid ? "افزودن" : "اطلاعات کامل نمی باشد"), editData ? __jsx("button", {
    className: "btns btns-warning",
    onClick: propsHideModal,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 15
    }
  }, "بستن") : ""))));
};

/* harmony default export */ __webpack_exports__["default"] = (AddBanner);

/***/ }),

/***/ "./panelAdmin/screen/Banner/BannerScreen/index.js":
/*!********************************************************!*\
  !*** ./panelAdmin/screen/Banner/BannerScreen/index.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _component_UI_Loadings_SpinnerRotate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../component/UI/Loadings/SpinnerRotate */ "./panelAdmin/component/UI/Loadings/SpinnerRotate/index.js");
/* harmony import */ var _component_cards_ShowCardInformation__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../component/cards/ShowCardInformation */ "./panelAdmin/component/cards/ShowCardInformation/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../.. */ "./panelAdmin/index.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../utils */ "./panelAdmin/utils/index.js");
/* harmony import */ var _component_UI_PaginationM__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../component/UI/PaginationM */ "./panelAdmin/component/UI/PaginationM/index.js");
/* harmony import */ var _component_UI_Modals_ModalStructure__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component/UI/Modals/ModalStructure */ "./panelAdmin/component/UI/Modals/ModalStructure/index.js");
/* harmony import */ var _AddBanner__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../AddBanner */ "./panelAdmin/screen/Banner/AddBanner/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/Banner/BannerScreen/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }










const BannerScreen = props => {
  const {
    apiPageFetch,
    onDataChange,
    filters,
    acceptedCardInfo,
    requestData,
    handelPage
  } = props; //console.log({ requestData });

  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    remove: {
      index: "",
      name: ""
    },
    genreTitle: ""
  });
  const inputRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const {
    0: modalDetails,
    1: setModalDetails
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    show: false,
    kindOf: null,
    data: {
      src: false,
      type: false,
      name: false
    },
    name: null,
    removeId: null,
    editId: null,
    editData: []
  });
  const {
    0: removeLoading,
    1: setRemoveLoading
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false); // ========================================================= remove data with data id

  const reqApiRemove = async id => {
    setRemoveLoading(true);

    if (await ___WEBPACK_IMPORTED_MODULE_3__["default"].api.deletes.banner(id)) {
      apiPageFetch(requestData.page);
      onHideModal();
    }

    setRemoveLoading(false);
  }; // ========================================================= modal
  // ================================== modal close


  const onHideModal = () => {
    setModalDetails(_objectSpread({}, modalDetails, {
      show: false,
      kindOf: false,
      removeId: null
    }));
  }; // ================================== modal open


  const onShowModal = event => {
    setModalDetails(_objectSpread({}, modalDetails, {
      show: true,
      kindOf: event.kindOf,
      name: event === null || event === void 0 ? void 0 : event.name,
      editData: event === null || event === void 0 ? void 0 : event.editData,
      removeId: event === null || event === void 0 ? void 0 : event.removeId
    }));
  }; // ================================== handel modal end work


  const modalRequest = async bool => {
    if (bool) reqApiRemove(modalDetails.removeId);else onHideModal();
  }; // ========================================================= END modal


  const optionClick = ({
    _id,
    mission,
    index
  }) => {
    //console.log({ _id, mission });
    switch (mission) {
      case "remove":
        onShowModal({
          kindOf: "question",
          removeId: _id
        });
        break;

      case "edit":
        onShowModal({
          kindOf: "component",
          editData: requestData.docs[index]
        });
        break;

      default:
        break;
    }
  };

  const card = ___WEBPACK_IMPORTED_MODULE_3__["default"].utils.consts.card;

  const showDataElement = __jsx(_component_cards_ShowCardInformation__WEBPACK_IMPORTED_MODULE_2__["default"], {
    data: card.banner(requestData === null || requestData === void 0 ? void 0 : requestData.docs),
    onClick: null,
    optionClick: optionClick,
    options: {
      remove: true
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 27
    }
  }); // const showDataElement = <Table tableStructure={utils.json.table.slider} data={requestData} />;


  return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 5
    }
  }, __jsx(_component_UI_Modals_ModalStructure__WEBPACK_IMPORTED_MODULE_6__["default"], {
    modalRequest: modalRequest,
    reqApiRemove: reqApiRemove,
    onHideModal: onHideModal,
    modalDetails: modalDetails,
    loading: removeLoading,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 7
    }
  }, modalDetails.kindOf === "component" && __jsx(_AddBanner__WEBPACK_IMPORTED_MODULE_7__["default"], {
    propsHideModal: onHideModal,
    setEdit: apiPageFetch,
    editData: modalDetails === null || modalDetails === void 0 ? void 0 : modalDetails.editData,
    modalAccept: modalRequest,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73,
      columnNumber: 49
    }
  })), __jsx("div", {
    style: {
      flex: "1"
    },
    className: "flexColum",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 7
    }
  }, showDataElement, __jsx(_component_UI_PaginationM__WEBPACK_IMPORTED_MODULE_5__["default"], {
    limited: "4",
    pages: requestData === null || requestData === void 0 ? void 0 : requestData.pages,
    activePage: requestData === null || requestData === void 0 ? void 0 : requestData.page,
    onClick: handelPage,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 9
    }
  })), " ");
};

/* harmony default export */ __webpack_exports__["default"] = (BannerScreen);

/***/ }),

/***/ "./panelAdmin/screen/Gallery/AddGallery/FormInputGallery/index.js":
/*!************************************************************************!*\
  !*** ./panelAdmin/screen/Gallery/AddGallery/FormInputGallery/index.js ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _component_UI_Inputs_Input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../component/UI/Inputs/Input */ "./panelAdmin/component/UI/Inputs/Input.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../.. */ "./panelAdmin/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/Gallery/AddGallery/FormInputGallery/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const FormInputGallery = props => {
  const {
    stateArray,
    removeHandel,
    state,
    _onSubmited,
    inputChangedHandler,
    checkSubmitted
  } = props;
  const dropDown = ___WEBPACK_IMPORTED_MODULE_2__["default"].utils.consts.galleryConstants();
  let dropDownData = [];

  for (const index in dropDown) dropDownData.push({
    value: dropDown[index].value,
    title: dropDown[index].title
  });

  return __jsx("form", {
    onSubmit: _onSubmited,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 5
    }
  }, stateArray.map(formElement => {
    const invalid = !formElement.config.valid;
    const shouldValidate = formElement.config.validation;
    const touched = formElement.config.touched;
    let changed, accepted, progress;
    const inputClasses = ["InputElement"];
    if (invalid && shouldValidate && touched) inputClasses.push("Invalid");else if (formElement.id === "image") {
      progress = state.progressPercentImage;
    }

    changed = e => inputChangedHandler({
      value: e.currentTarget.value,
      name: formElement.id,
      type: e.currentTarget.type,
      files: e.currentTarget.files
    });

    accepted = value => inputChangedHandler({
      value: value,
      name: formElement.id
    });

    let form = __jsx(_component_UI_Inputs_Input__WEBPACK_IMPORTED_MODULE_1__["default"], {
      key: formElement.id,
      elementType: formElement.config.elementType,
      elementConfig: formElement.config.elementConfig,
      value: formElement.config.value,
      invalid: invalid,
      shouldValidate: shouldValidate,
      touched: touched,
      changed: changed,
      accepted: accepted,
      removeHandel: index => removeHandel(index, formElement.id),
      label: formElement.config.label,
      progress: progress,
      checkSubmitted: checkSubmitted,
      dropDownData: dropDownData,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 11
      }
    });

    return form;
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (FormInputGallery);

/***/ }),

/***/ "./panelAdmin/screen/Gallery/AddGallery/index.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/screen/Gallery/AddGallery/index.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _component_UI_Modals_ModalBox__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../component/UI/Modals/ModalBox */ "./panelAdmin/component/UI/Modals/ModalBox/index.js");
/* harmony import */ var _component_UI_Modals_ModalTrueFalse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../component/UI/Modals/ModalTrueFalse */ "./panelAdmin/component/UI/Modals/ModalTrueFalse/index.js");
/* harmony import */ var _FormInputGallery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./FormInputGallery */ "./panelAdmin/screen/Gallery/AddGallery/FormInputGallery/index.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../api */ "./panelAdmin/api/index.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../.. */ "./panelAdmin/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/Gallery/AddGallery/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








const AddGallery = ({
  onHideModal,
  getApi,
  data,
  setData
}) => {
  const states = ___WEBPACK_IMPORTED_MODULE_5__["default"].utils.consts.states;
  const onChanges = ___WEBPACK_IMPORTED_MODULE_5__["default"].utils.onChanges; // const [data, setData] = useState({ ...states.addGallery });

  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    progressPercentImage: null,
    progressPercentSongs: null,
    remove: {
      value: "",
      name: ""
    }
  });
  const {
    0: Loading,
    1: setLoading
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: Modal,
    1: setModal
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    show: false
  });
  const {
    0: checkSubmitted,
    1: setcheckSubmitted
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false); // ======================================== modal
  // ========================= End modal =================
  // ============================= submited
  // ========================= End submited =================
  // ============================= remove

  const __returnPrevstep = value => {
    // onHideModal();
    setState(_objectSpread({}, state, {
      remove: {
        value: "",
        name: ""
      }
    }));
    if (value) inputChangedHandler({
      name: state.remove.name,
      value: state.remove.value
    });
  };

  const removeHandel = (value, name) => {
    // onShowlModal();
    setState(_objectSpread({}, state, {
      remove: {
        value,
        name
      }
    }));
  }; // =========================== End remove  ====================


  const inputChangedHandler = async event => {
    await onChanges.globalChange({
      event,
      data,
      setData,
      setState,
      setLoading,
      fileName: data.Form.imageName.value
    });
  };

  const stateArray = [];

  for (let key in data.Form) stateArray.push({
    id: key,
    config: data.Form[key]
  });

  let form = __jsx(_FormInputGallery__WEBPACK_IMPORTED_MODULE_3__["default"], {
    removeHandel: removeHandel,
    stateArray: stateArray,
    data: data,
    state: state,
    setData: setData,
    Loading: Loading,
    setLoading: setLoading,
    inputChangedHandler: inputChangedHandler,
    checkSubmitted: checkSubmitted,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 14
    }
  });

  return __jsx("div", {
    className: " centerAll ",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 5
    }
  }, __jsx(_component_UI_Modals_ModalBox__WEBPACK_IMPORTED_MODULE_1__["default"], {
    onHideModal: onHideModal,
    showModal: Modal.show,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 7
    }
  }, __jsx(_component_UI_Modals_ModalTrueFalse__WEBPACK_IMPORTED_MODULE_2__["default"], {
    modalHeadline: "آیا مطمئن به حذف آن هستید !",
    modalAcceptTitle: "بله",
    modalCanselTitle: "خیر",
    modalAccept: __returnPrevstep,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 9
    }
  })), __jsx("div", {
    className: "form-countainer shadowUnset",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "row-give-information",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 9
    }
  }, form)));
};

/* harmony default export */ __webpack_exports__["default"] = (AddGallery);

/***/ }),

/***/ "./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/EditData/index.js":
/*!**************************************************************************************!*\
  !*** ./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/EditData/index.js ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const EditData = props => {
  const {
    editData,
    stateArray,
    inputChangedHandler,
    setStaticTitle
  } = props;
  let arrayData = [];
  if (editData) for (const key in editData) for (let index = 0; index < stateArray.length; index++) {
    if (stateArray[index].id === key) if (key === "category") arrayData.push({
      name: key,
      value: editData[key] ? editData[key]._id : ""
    });else if (key === "owner") {
      if (editData[key]) setStaticTitle({
        value: editData[key] ? editData[key].title : "",
        name: key
      });
      arrayData.push({
        name: key,
        value: editData[key] ? editData[key]._id : ""
      });
    } else arrayData.push({
      name: key,
      value: editData[key] ? editData[key] : editData[key] ? editData[key] : ""
    });
  }
  if (arrayData.length > 0) inputChangedHandler(arrayData, false, {
    value: editData.parentType
  });
};

/* harmony default export */ __webpack_exports__["default"] = (EditData);

/***/ }),

/***/ "./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/ModalOption/index.js":
/*!*****************************************************************************************!*\
  !*** ./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/ModalOption/index.js ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _component_UI_Modals_ModalStructure__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../component/UI/Modals/ModalStructure */ "./panelAdmin/component/UI/Modals/ModalStructure/index.js");
/* harmony import */ var _pages_panelAdmin_gallery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../pages/panelAdmin/gallery */ "./pages/panelAdmin/gallery.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/ModalOption/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const ModalOption = props => {
  const {
    inputChangedHandler,
    modalRequest,
    onHideModal,
    modalDetails,
    imageAccepted,
    acceptedImageFinal,
    acceptedImage
  } = props;
  return __jsx(_component_UI_Modals_ModalStructure__WEBPACK_IMPORTED_MODULE_1__["default"], {
    modalRequest: modalRequest,
    reqApiRemove: null,
    onHideModal: onHideModal,
    modalDetails: modalDetails,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 5
    }
  }, modalDetails.kindOf === "showGallery" && __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 9
    }
  }, __jsx(_pages_panelAdmin_gallery__WEBPACK_IMPORTED_MODULE_2__["default"], {
    clickedParent: value => inputChangedHandler({
      name: modalDetails.name,
      value
    }),
    acceptedCardInfo: {
      handelAcceptedImage: acceptedImage,
      acceptedCard: imageAccepted
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 11
    }
  }), __jsx("div", {
    className: "btns-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 11
    }
  }, __jsx("button", {
    disabled: !imageAccepted,
    className: "btns btns-primary",
    onClick: e => acceptedImageFinal(e),
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 13
    }
  }, "\u062A\u0627\u06CC\u06CC\u062F", " "), __jsx("button", {
    className: "btns btns-warning",
    onClick: onHideModal,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 13
    }
  }, "\u0628\u0633\u062A\u0646", " "))));
};

/* harmony default export */ __webpack_exports__["default"] = (ModalOption);

/***/ }),

/***/ "./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/Submitted/index.js":
/*!***************************************************************************************!*\
  !*** ./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/Submitted/index.js ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_updateObject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../utils/updateObject */ "./panelAdmin/utils/updateObject.js");



const Submitted = async props => {
  const {
    setSubmitLoading,
    data,
    editData,
    put,
    post,
    setData,
    states,
    setEdit,
    propsHideModal,
    setCheckSubmitted,
    checkSubmitted
  } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};

  for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;

  const initialStatePhone = Object(_utils_updateObject__WEBPACK_IMPORTED_MODULE_1__["default"])(states.addOwner.Form["phone"], {
    value: []
  });
  const initialStateCoordinate = Object(_utils_updateObject__WEBPACK_IMPORTED_MODULE_1__["default"])(states.addOwner.Form["coordinate"], {
    value: {
      lat: "",
      lng: ""
    }
  });
  const updatedForm = Object(_utils_updateObject__WEBPACK_IMPORTED_MODULE_1__["default"])(states.addOwner.Form, {
    ["coordinate"]: initialStateCoordinate,
    ["phone"]: initialStatePhone
  });

  if (editData) {
    if (await put.owner({
      id: editData._id,
      data: formData
    })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post.owner(formData)) setData({
    Form: updatedForm,
    formIsValid: false
  });

  setSubmitLoading(false);
};

/* harmony default export */ __webpack_exports__["default"] = (Submitted);

/***/ }),

/***/ "./panelAdmin/screen/Gallery/GalleryScreen/index.js":
/*!**********************************************************!*\
  !*** ./panelAdmin/screen/Gallery/GalleryScreen/index.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _AddGallery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../AddGallery */ "./panelAdmin/screen/Gallery/AddGallery/index.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../.. */ "./panelAdmin/index.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash */ "lodash");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _component_UI_Modals_MyVerticallyCenteredModal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../component/UI/Modals/MyVerticallyCenteredModal */ "./panelAdmin/component/UI/Modals/MyVerticallyCenteredModal/index.js");
/* harmony import */ var _component_cards_ShowCardInformation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../component/cards/ShowCardInformation */ "./panelAdmin/component/cards/ShowCardInformation/index.js");
/* harmony import */ var _globalUtils__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../globalUtils */ "./globalUtils/index.js");
/* harmony import */ var _component_UI_PaginationM__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../component/UI/PaginationM */ "./panelAdmin/component/UI/PaginationM/index.js");
/* harmony import */ var _lib_useApiRequest__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../lib/useApiRequest */ "./lib/useApiRequest.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../utils */ "./panelAdmin/utils/index.js");
/* harmony import */ var _DependentComponent_Submitted__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./DependentComponent/Submitted */ "./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/Submitted/index.js");
/* harmony import */ var _DependentComponent_ModalOption__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./DependentComponent/ModalOption */ "./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/ModalOption/index.js");
/* harmony import */ var _DependentComponent_EditData__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./DependentComponent/EditData */ "./panelAdmin/screen/Gallery/GalleryScreen/DependentComponent/EditData/index.js");
/* harmony import */ var _component_UI_Modals_ModalStructure__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../component/UI/Modals/ModalStructure */ "./panelAdmin/component/UI/Modals/ModalStructure/index.js");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! swr */ "swr");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_15__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/Gallery/GalleryScreen/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











 // const axios = globalUtils.axiosBase;







const GalleryScreen = props => {
  const states = ___WEBPACK_IMPORTED_MODULE_3__["default"].utils.consts.states;
  const card = ___WEBPACK_IMPORTED_MODULE_3__["default"].utils.consts.card;
  const {
    apiPageFetch,
    filters,
    acceptedCardInfo,
    requestData,
    loadingApi
  } = props;
  const {
    0: showUploadModal,
    1: setUploadModal
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: data,
    1: setData
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(_objectSpread({}, states.addGallery));
  const {
    0: modalDetails,
    1: setModalDetails
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    show: false,
    kindOf: null,
    data: {
      src: false,
      type: false,
      name: false
    },
    name: null,
    removeId: null,
    editId: null,
    editData: []
  });
  const {
    0: removeLoading,
    1: setRemoveLoading
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false); // const CurrentPage = info?.page || "1";

  const strings = ___WEBPACK_IMPORTED_MODULE_3__["default"].values.apiString; // const URL = strings.IMAGE + "/" + CurrentPage;
  // ========================================================= remove data with data id

  const reqApiRemove = async id => {
    setRemoveLoading(true);

    if (await ___WEBPACK_IMPORTED_MODULE_3__["default"].api.deletes.gallery(id)) {
      apiPageFetch(requestData.page);
      onHideModal();
    }

    setRemoveLoading(false);
  }; // ========================================================= modal


  const _onSubmit = async e => {
    const formData = {};

    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value; // trigger(URL);


    apiPageFetch();
    setData(_objectSpread({}, states.addGallery));
  }; // ================================== modal close


  const onHideModal = () => {
    setModalDetails(_objectSpread({}, modalDetails, {
      show: false,
      kindOf: false,
      removeId: null
    }));
  }; // ================================== modal open


  const onShowModal = event => {
    setModalDetails(_objectSpread({}, modalDetails, {
      show: true,
      kindOf: event.kindOf,
      name: event === null || event === void 0 ? void 0 : event.name,
      editData: event === null || event === void 0 ? void 0 : event.editData,
      removeId: event === null || event === void 0 ? void 0 : event.removeId
    }));
  }; // ================================== handel modal end work


  const modalRequest = async bool => {
    if (bool) reqApiRemove(modalDetails.removeId);else onHideModal();
  }; // ========================================================= END modal


  const modalData = {
    size: "lg",
    status: showUploadModal,
    setStatus: setUploadModal,
    title: "آپلود عکس",
    acceptedBtn: data.formIsValid ? "ثبت" : "اطلاعات کامل نمی باشد",
    acceptedDisabled: !data.formIsValid,
    onSubmit: _onSubmit
  };

  const optionClick = ({
    _id,
    mission,
    index
  }) => {
    //console.log({ _id, mission });
    switch (mission) {
      case "remove":
        onShowModal({
          kindOf: "question",
          removeId: _id
        });
        break;

      case "edit":
        onShowModal({
          kindOf: "component",
          editData: requestData.docs[index]
        });
        break;

      default:
        break;
    }
  }; // ========================================  CARD ELEMENT


  const showDataElement = __jsx(_component_cards_ShowCardInformation__WEBPACK_IMPORTED_MODULE_6__["default"], {
    optionClick: optionClick,
    options: {
      remove: true
    },
    data: card.gallery(requestData === null || requestData === void 0 ? void 0 : requestData.docs, acceptedCardInfo === null || acceptedCardInfo === void 0 ? void 0 : acceptedCardInfo.acceptedCard),
    onClick: null,
    acceptedCardInfo: acceptedCardInfo,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95,
      columnNumber: 27
    }
  });

  return __jsx("div", {
    className: "gallery",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98,
      columnNumber: 5
    }
  }, __jsx(_component_UI_Modals_ModalStructure__WEBPACK_IMPORTED_MODULE_14__["default"], {
    modalRequest: modalRequest,
    reqApiRemove: reqApiRemove,
    onHideModal: onHideModal,
    modalDetails: modalDetails,
    loading: removeLoading,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 99,
      columnNumber: 7
    }
  }), __jsx(_component_UI_Modals_MyVerticallyCenteredModal__WEBPACK_IMPORTED_MODULE_5__["default"], _extends({}, modalData, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 101,
      columnNumber: 7
    }
  }), __jsx(_AddGallery__WEBPACK_IMPORTED_MODULE_1__["default"], {
    data: data,
    setData: setData,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 102,
      columnNumber: 9
    }
  })), __jsx("div", {
    className: "gallery-header-wrapper",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "gallery-header",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 105,
      columnNumber: 9
    }
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    onClick: () => setUploadModal(true),
    className: "",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 106,
      columnNumber: 11
    }
  }, "افزودن عکس")), showDataElement, __jsx(_component_UI_PaginationM__WEBPACK_IMPORTED_MODULE_8__["default"], {
    limited: "3",
    pages: requestData === null || requestData === void 0 ? void 0 : requestData.pages,
    activePage: requestData === null || requestData === void 0 ? void 0 : requestData.page,
    onClick: apiPageFetch,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 9
    }
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (GalleryScreen);

/***/ }),

/***/ "./panelAdmin/screen/PanelScreen.js":
/*!******************************************!*\
  !*** ./panelAdmin/screen/PanelScreen.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _component_SideMenu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../component/SideMenu */ "./panelAdmin/component/SideMenu/index.js");
/* harmony import */ var _component_Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../component/Header */ "./panelAdmin/component/Header/index.js");
/* harmony import */ var _component_UI_BackgrandCover__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../component/UI/BackgrandCover */ "./panelAdmin/component/UI/BackgrandCover/index.js");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-toastify */ "react-toastify");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _context_reducer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../_context/reducer */ "./_context/reducer/index.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! .. */ "./panelAdmin/index.js");
/* harmony import */ var _utils_adminHoc_WithErrorHandler__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utils/adminHoc/WithErrorHandler */ "./panelAdmin/utils/adminHoc/WithErrorHandler/index.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/PanelScreen.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;









const OptionReducer = _context_reducer__WEBPACK_IMPORTED_MODULE_5__["default"].panelAdminReducer.optionReducerProvider;

const PanelScreen = props => {
  const {
    0: sidebarToggle,
    1: setSidebarToggle
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const router = Object(next_router__WEBPACK_IMPORTED_MODULE_6__["useRouter"])();

  const _handelSidebarToggle = () => {
    setSidebarToggle(!sidebarToggle);
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    if (!___WEBPACK_IMPORTED_MODULE_7__["default"].utils.authorization()) router.push("/panelAdmin/login");else if (router.asPath === "/panelAdmin" || router.asPath === "/panelAdmin/") router.push("/panelAdmin/dashboard");
  });
  return ___WEBPACK_IMPORTED_MODULE_7__["default"].utils.authorization() ? __jsx(OptionReducer, {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: `panelAdmin-wrapper  ${sidebarToggle ? "fadeIn" : "fadeOut"}`,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 7
    }
  }, __jsx("div", {
    style: {
      direction: "rtl",
      display: "flex"
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 9
    }
  }, __jsx(_component_SideMenu__WEBPACK_IMPORTED_MODULE_1__["default"], {
    windowLocation: router.asPath,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 11
    }
  }), __jsx("div", {
    className: "panelAdmin-container",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 11
    }
  }, __jsx(_component_Header__WEBPACK_IMPORTED_MODULE_2__["default"], {
    _handelSidebarToggle: _handelSidebarToggle,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 13
    }
  }), __jsx("div", {
    className: "panelAdmin-content",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 13
    }
  }, props.children))), __jsx(_component_UI_BackgrandCover__WEBPACK_IMPORTED_MODULE_3__["default"], {
    fadeIn: sidebarToggle,
    onClick: _handelSidebarToggle,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 9
    }
  }), __jsx(react_toastify__WEBPACK_IMPORTED_MODULE_4__["ToastContainer"], {
    rtl: true,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 9
    }
  }))) : "";
};

/* harmony default export */ __webpack_exports__["default"] = (Object(_utils_adminHoc_WithErrorHandler__WEBPACK_IMPORTED_MODULE_8__["default"])(PanelScreen));

/***/ }),

/***/ "./panelAdmin/utils/CelanderConvert/convertToCelander.js":
/*!***************************************************************!*\
  !*** ./panelAdmin/utils/CelanderConvert/convertToCelander.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const convertToCelander = value => {
  let valDate,
      day,
      month,
      year,
      selectedDay = null;

  if (value) {
    valDate = value.split("/");
    day = Number(valDate[2]);
    month = Number(valDate[1]);
    year = Number(valDate[0]);
    selectedDay = {
      day,
      month,
      year
    };
  }

  return selectedDay;
};

/* harmony default export */ __webpack_exports__["default"] = (convertToCelander);

/***/ }),

/***/ "./panelAdmin/utils/CelanderConvert/convertToDate.js":
/*!***********************************************************!*\
  !*** ./panelAdmin/utils/CelanderConvert/convertToDate.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const convertToDate = selectedDay => {
  let day = selectedDay.day;
  let month = selectedDay.month;
  let year = selectedDay.year;
  let date = year + "/" + month + "/" + day;
  return date;
};

/* harmony default export */ __webpack_exports__["default"] = (convertToDate);

/***/ }),

/***/ "./panelAdmin/utils/CelanderConvert/index.js":
/*!***************************************************!*\
  !*** ./panelAdmin/utils/CelanderConvert/index.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _convertToCelander__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./convertToCelander */ "./panelAdmin/utils/CelanderConvert/convertToCelander.js");
/* harmony import */ var _convertToDate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./convertToDate */ "./panelAdmin/utils/CelanderConvert/convertToDate.js");


const CelanderConvert = {
  convertToCelander: _convertToCelander__WEBPACK_IMPORTED_MODULE_0__["default"],
  convertToDate: _convertToDate__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (CelanderConvert);

/***/ }),

/***/ "./panelAdmin/utils/adminHoc/WithErrorHandler/checkError.js":
/*!******************************************************************!*\
  !*** ./panelAdmin/utils/adminHoc/WithErrorHandler/checkError.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const checkError = ({
  errorCode
}) => {
  let errorTitle;

  switch (errorCode) {
    case 1096:
      errorTitle = "آیدی اشتباه است";
      break;

    case 1098:
      errorTitle = "پسورد شما نامعتبر است";
      break;

    case 1099:
      errorTitle = "خطایی در سرور . لطفا دوباره تلاش کنید";
      break;

    case 1019:
      errorTitle = "ارسال موفقیت آمیز نبود";
      break;

    case 1011:
      errorTitle = "اطلاعات ارسالی نامعتبر است";
      break;

    case 1017:
      errorTitle = "نام انگلیسی تکراری می باشد";
      break;

    case 1018:
      errorTitle = "نام فارسی تکراری می باشد";
      break;

    case 1020:
      errorTitle = "شماره تلفن ثبت نشده است";
      break;

    case 1021:
      errorTitle = "عکس هایی ک در حال استفاده می باشند را نمیتوان حذف کرد";
      break;

    default:
      errorTitle = "خطا در سرور . لطفا دوباره تلاش کنید";
      break;
  }

  return errorTitle;
};

/* harmony default export */ __webpack_exports__["default"] = (checkError);

/***/ }),

/***/ "./panelAdmin/utils/adminHoc/WithErrorHandler/index.js":
/*!*************************************************************!*\
  !*** ./panelAdmin/utils/adminHoc/WithErrorHandler/index.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api_axios_orders__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../api/axios-orders */ "./panelAdmin/api/axios-orders.js");
/* harmony import */ var _checkError__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./checkError */ "./panelAdmin/utils/adminHoc/WithErrorHandler/checkError.js");
/* harmony import */ var _toastify__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../toastify */ "./panelAdmin/utils/toastify.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/adminHoc/WithErrorHandler/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }






const WithErrorHandler = WrappedComponent => {
  return props => {
    var _error$config, _error$config2;

    const {
      0: error,
      1: setError
    } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(null);
    const reqInterceptor = _api_axios_orders__WEBPACK_IMPORTED_MODULE_1__["default"].interceptors.request.use(req => {
      // console.log({ req });
      setError(null);
      return req;
    });
    const resInterceptor = _api_axios_orders__WEBPACK_IMPORTED_MODULE_1__["default"].interceptors.response.use(res => {
      // console.log({ res });
      setError(null);
      return res;
    }, err => {
      setError(err);
    });
    Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
      return () => {
        _api_axios_orders__WEBPACK_IMPORTED_MODULE_1__["default"].interceptors.request.eject(reqInterceptor);
        _api_axios_orders__WEBPACK_IMPORTED_MODULE_1__["default"].interceptors.response.eject(resInterceptor);
      };
    }, [reqInterceptor, resInterceptor]); // useEffect(() => {
    //   setTimeout(() => {
    //     if (error) setError(null);
    //   }, 1000);
    // }, [error]);

    console.log({
      error
    });
    const errorDisable = "/admin/owner/s//";
    console.log(errorDisable.search(error === null || error === void 0 ? void 0 : (_error$config = error.config) === null || _error$config === void 0 ? void 0 : _error$config.url));
    const test = error === null || error === void 0 ? void 0 : (_error$config2 = error.config) === null || _error$config2 === void 0 ? void 0 : _error$config2.url.split("/");
    if (test) console.log(test[3], test[4], test[5]);
    Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
      var _error$response;

      if (error) if (test[3] !== "s" && test[4] !== "" && !test[5]) if (error.message === "Network Error") Object(_toastify__WEBPACK_IMPORTED_MODULE_3__["default"])("دسترسی به اینترنت را بررسی کنید", "error");else Object(_toastify__WEBPACK_IMPORTED_MODULE_3__["default"])(Object(_checkError__WEBPACK_IMPORTED_MODULE_2__["default"])({
        errorCode: error === null || error === void 0 ? void 0 : (_error$response = error.response) === null || _error$response === void 0 ? void 0 : _error$response.data.Error
      }), "error");
    }, [error]);
    return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, __jsx(WrappedComponent, _extends({}, props, {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 49,
        columnNumber: 9
      }
    })));
  };
};

/* harmony default export */ __webpack_exports__["default"] = (WithErrorHandler);

/***/ }),

/***/ "./panelAdmin/utils/adminHoc/index.js":
/*!********************************************!*\
  !*** ./panelAdmin/utils/adminHoc/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _WithErrorHandler__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./WithErrorHandler */ "./panelAdmin/utils/adminHoc/WithErrorHandler/index.js");

const adminHoc = {
  WithErrorHandler: _WithErrorHandler__WEBPACK_IMPORTED_MODULE_0__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (adminHoc);

/***/ }),

/***/ "./panelAdmin/utils/authorization.js":
/*!*******************************************!*\
  !*** ./panelAdmin/utils/authorization.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-cookie */ "js-cookie");
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_0__);


const authorization = () => {
  return js_cookie__WEBPACK_IMPORTED_MODULE_0___default.a.get("SafirAdminToken") !== undefined;
};

/* harmony default export */ __webpack_exports__["default"] = (authorization);

/***/ }),

/***/ "./panelAdmin/utils/checkValidity/index.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/checkValidity/index.js ***!
  \*************************************************/
/*! exports provided: checkValidity */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkValidity", function() { return checkValidity; });
/* harmony import */ var _minLengthValidity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./minLengthValidity */ "./panelAdmin/utils/checkValidity/minLengthValidity.js");
/* harmony import */ var _maxLengthValidity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./maxLengthValidity */ "./panelAdmin/utils/checkValidity/maxLengthValidity.js");
/* harmony import */ var _numberValidity__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./numberValidity */ "./panelAdmin/utils/checkValidity/numberValidity.js");



const checkValidity = (value, rules, array, beforeValue) => {
  let isValid = true;
  let errorTitle = false;
  let object = true;
  let checkValid = true;
  console.log({
    moojValid: {
      value,
      rules,
      array,
      beforeValue
    }
  }); // console.log({ moooojValidtypeof: typeof value });

  if (!rules.required) return {
    isValid,
    errorTitle
  };

  if (!rules) {
    return {
      isValid,
      errorTitle
    };
  }

  if (array) {
    isValid = beforeValue.length ? true : false; // console.log({ mooj: beforeValue.length ? true : false });

    if (!isValid) errorTitle = "خالی بودن فیلد مجاز نمی باشد";
  } else {
    if (rules.required) {
      if (typeof value === "object") {
        for (const key in value) {
          var _value$key, _value$key$toString;

          object = ((_value$key = value[key]) === null || _value$key === void 0 ? void 0 : (_value$key$toString = _value$key.toString()) === null || _value$key$toString === void 0 ? void 0 : _value$key$toString.trim()) !== "" && object;
        }

        if (!object) errorTitle = "خالی بودن فیلد مجاز نمی باشد";
        isValid = object && isValid;
      } else {
        isValid = value.trim() !== "" && isValid;
        errorTitle = value.trim() === "" && ".خالی بودن فیلد مجاز نمی باشد";
      }
    }
  }

  if (isValid) {
    if (rules.minLength) {
      checkValid = Object(_minLengthValidity__WEBPACK_IMPORTED_MODULE_0__["default"])({
        array,
        beforeValue,
        value,
        rules
      });
      console.log({
        checkValid
      });
      if (!checkValid) errorTitle = ` کمترین مقدار مجاز فیلد ${rules.minLength} کلمه یا رقم می باشد `;
      isValid = isValid && checkValid;
    }

    if (rules.maxLength) {
      checkValid = Object(_maxLengthValidity__WEBPACK_IMPORTED_MODULE_1__["default"])({
        array,
        beforeValue,
        value,
        rules
      });
      if (!checkValid) errorTitle = ` بیشترین مقدار مجاز فیلد ${rules.maxLength} کلمه یا رقم می باشد `;
      isValid = isValid && checkValid;
    }

    if (rules.isNumeric) {
      checkValid = Object(_numberValidity__WEBPACK_IMPORTED_MODULE_2__["default"])({
        array,
        beforeValue,
        value
      });
      if (!checkValid) errorTitle = "فقط شماره مجاز می باشد";
      isValid = isValid && checkValid;
    }

    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    if (rules.isPhone) {
      const pattern = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im; // /^0\d{2,3}-\d{8}|\d{8}$/ regex(/^0\d{2,3}-\d{8}|\d{8}$/)

      isValid = pattern.test(value) && isValid;
    }

    if (rules.isMobile) {
      const pattern = /^09[\w]{9}$/;
      if (array) beforeValue.map(val => isValid = pattern.test(val) && isValid); // const pattern = /[0,9]{2}\d{9}/g;
      // const pattern = /^(+98|0)?9\d{9}$/;
      else isValid = pattern.test(value) && isValid;
    }

    if (rules.isEn) {
      const pattern = /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/;
      if (array) beforeValue.map(val => isValid = pattern.test(val) && isValid);else isValid = pattern.test(value) && isValid;
    }

    if (rules.isFa) {
      const pattern = /^[\u0600-\u06FF\s]+$/;
      if (array) beforeValue.map(val => isValid = pattern.test(val) && isValid);else isValid = pattern.test(value) && isValid;
    }
  } // //console.log({ mooojisValid: isValid });


  return {
    isValid,
    errorTitle
  };
};

/***/ }),

/***/ "./panelAdmin/utils/checkValidity/maxLengthValidity.js":
/*!*************************************************************!*\
  !*** ./panelAdmin/utils/checkValidity/maxLengthValidity.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const maxLengthValidity = ({
  array,
  beforeValue,
  value,
  rules
}) => {
  let maxLength = true;
  if (array) beforeValue.map(val => maxLength = val.length >= rules.maxLength && maxLength);else maxLength = value.length <= rules.maxLength && maxLength;
  return maxLength;
};

/* harmony default export */ __webpack_exports__["default"] = (maxLengthValidity);

/***/ }),

/***/ "./panelAdmin/utils/checkValidity/minLengthValidity.js":
/*!*************************************************************!*\
  !*** ./panelAdmin/utils/checkValidity/minLengthValidity.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const minLengthValidity = ({
  array,
  beforeValue,
  value,
  rules
}) => {
  let minLength = true;

  if (array) {
    beforeValue.map(val => {
      console.log({
        minLengthVal: val
      });
      return minLength = val.length >= rules.minLength && minLength;
    });
  } else minLength = value.length >= rules.minLength && minLength;

  console.log({
    minLength,
    array,
    beforeValue,
    value,
    rules
  });
  return minLength;
};

/* harmony default export */ __webpack_exports__["default"] = (minLengthValidity);

/***/ }),

/***/ "./panelAdmin/utils/checkValidity/numberValidity.js":
/*!**********************************************************!*\
  !*** ./panelAdmin/utils/checkValidity/numberValidity.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const numberValidity = ({
  array,
  beforeValue,
  value
}) => {
  let isNumeric = true;
  const pattern = /^\d+$/;
  if (array) beforeValue.map(val => {
    // console.log("number:", { val });
    return isNumeric = pattern.test(val) && isNumeric;
  });else isNumeric = pattern.test(value) && isNumeric;
  return isNumeric;
};

/* harmony default export */ __webpack_exports__["default"] = (numberValidity);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/banner.js":
/*!************************************************!*\
  !*** ./panelAdmin/utils/consts/card/banner.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const banner = (data, acceptedCard) => {
  const cardFormat = []; //console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let NO_ENTERED = "وارد نشده";
    let type = dataIndex.type ? dataIndex.type : NO_ENTERED;
    let store = dataIndex.store ? dataIndex.store : false;
    let title = store.title ? store.title : NO_ENTERED;
    let image = dataIndex.image ? dataIndex.image : NO_ENTERED; // //console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard === image ? "activeImage" : "" : "",
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (banner);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/category.js":
/*!**************************************************!*\
  !*** ./panelAdmin/utils/consts/card/category.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const category = data => {
  const cardFormat = []; //console.log({ data });

  for (let index in data) {
    let dataIndex = data[index];
    let titleFa = dataIndex.titleFa ? dataIndex.titleFa : "";
    let titleEn = dataIndex.titleEn ? dataIndex.titleEn : "";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: titleFa,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        left: [{
          elementType: "text",
          value: titleEn,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (category);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/gallery.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/card/gallery.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const instrument = (data, acceptedCard) => {
  const cardFormat = []; //console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let title = dataIndex.title ? dataIndex.title : "";
    let href = dataIndex.href ? dataIndex.href : "";
    let images = href; // //console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard === href ? "activeImage" : "" : "",
      image: {
        value: images
      },
      body: [{
        right: [{
          elementType: "text",
          value: title,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (instrument);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/index.js":
/*!***********************************************!*\
  !*** ./panelAdmin/utils/consts/card/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _gallery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./gallery */ "./panelAdmin/utils/consts/card/gallery.js");
/* harmony import */ var _product__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product */ "./panelAdmin/utils/consts/card/product.js");
/* harmony import */ var _category__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./category */ "./panelAdmin/utils/consts/card/category.js");
/* harmony import */ var _slider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./slider */ "./panelAdmin/utils/consts/card/slider.js");
/* harmony import */ var _banner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./banner */ "./panelAdmin/utils/consts/card/banner.js");
/* harmony import */ var _version__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./version */ "./panelAdmin/utils/consts/card/version.js");
// import instrument from "./instrument";
 // import country from "./country";
// import mood from "./mood";
// import hashtag from "./hashtag";
// import genre from "./genre";






const card = {
  category: _category__WEBPACK_IMPORTED_MODULE_2__["default"],
  // instrument,
  gallery: _gallery__WEBPACK_IMPORTED_MODULE_0__["default"],
  // country,
  // mood,
  // hashtag,
  product: _product__WEBPACK_IMPORTED_MODULE_1__["default"],
  slider: _slider__WEBPACK_IMPORTED_MODULE_3__["default"],
  banner: _banner__WEBPACK_IMPORTED_MODULE_4__["default"],
  version: _version__WEBPACK_IMPORTED_MODULE_5__["default"] // genre

};
/* harmony default export */ __webpack_exports__["default"] = (card);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/product.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/card/product.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const product = data => {
  //console.log({ productCard: data });
  const cardFormat = [];

  for (let index in data) {
    let NO_ENTERED = "وارد نشده";
    let dataIndex = data[index];
    let category = dataIndex.category ? dataIndex.category : "";
    let categoryName = category ? category.name : NO_ENTERED;
    let name = dataIndex.name ? dataIndex.name : NO_ENTERED;
    let unit = dataIndex.unit ? dataIndex.unit : NO_ENTERED;
    let weight = dataIndex.weight ? dataIndex.weight : NO_ENTERED;
    let realPrice = dataIndex.realPrice ? dataIndex.realPrice : NO_ENTERED;
    let newPrice = dataIndex.newPrice ? dataIndex.newPrice : NO_ENTERED;
    let image = dataIndex.image ? dataIndex.image : NO_ENTERED;
    cardFormat.push({
      _id: dataIndex._id,
      // isActive: dataIndex.isActive,
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: name,
          style: {
            color: "black",
            fontSize: "1.3em",
            fontWeight: "bold"
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: categoryName,
          title: categoryName,
          style: {
            color: "",
            fontSize: "0.9em",
            fontWeight: "500"
          }
        }]
      }, {
        left: [{
          elementType: "price",
          value: realPrice,
          direction: "ltr",
          style: {
            color: "red",
            fontSize: "",
            fontWeight: ""
          }
        }],
        right: [{
          elementType: "price",
          value: newPrice,
          direction: "ltr",
          style: {
            color: "green",
            fontSize: "",
            fontWeight: ""
          }
        }]
      }, {
        right: [{
          elementType: "text",
          value: unit
        }],
        left: [{
          elementType: "icon",
          value: weight,
          className: "",
          direction: "ltr",
          style: {
            fontSize: "1em",
            fontWeight: "500"
          },
          iconStyle: {
            fontSize: "1.4em"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (product);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/slider.js":
/*!************************************************!*\
  !*** ./panelAdmin/utils/consts/card/slider.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../.. */ "./panelAdmin/index.js");


const slider = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({
    insss: data
  });

  for (let index in data) {
    let noEntries = "وارد نشده است";
    let dataIndex = data[index];
    let parent = (dataIndex === null || dataIndex === void 0 ? void 0 : dataIndex.parent) || false;
    let parentName = (parent === null || parent === void 0 ? void 0 : parent.titleFa) || (parent === null || parent === void 0 ? void 0 : parent.title) || noEntries;
    let parentType = (dataIndex === null || dataIndex === void 0 ? void 0 : dataIndex.parentType) || "وارد نشده";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? acceptedCard === image ? "activeImage" : "" : "",
      image: {
        value: image
      },
      body: [{
        right: [{
          elementType: "text",
          value: parentName,
          style: {
            color: "#828181",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        left: [{
          elementType: "text",
          value: ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.dictionary(parentType),
          style: {
            color: "#147971",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (slider);

/***/ }),

/***/ "./panelAdmin/utils/consts/card/version.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/card/version.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const version = data => {
  const cardFormat = []; //console.log({ data });

  for (let index in data) {
    let dataIndex = data[index];
    let link = dataIndex.link ? dataIndex.link : "";
    let version = dataIndex.version ? dataIndex.version : "";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: false,
      body: [{
        right: [{
          elementType: "text",
          value: link,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }, {
        left: [{
          elementType: "text",
          value: version,
          style: {
            color: "black",
            fontSize: "0.67rem",
            fontWeight: "bold"
          }
        }]
      }]
    });
  }

  return cardFormat;
};

/* harmony default export */ __webpack_exports__["default"] = (version);

/***/ }),

/***/ "./panelAdmin/utils/consts/galleryConstants.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/consts/galleryConstants.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");



const galleryConstants = () => {
  const ConstantsEn = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.en;
  const string = ___WEBPACK_IMPORTED_MODULE_1__["default"].values.strings;
  return [{
    value: ConstantsEn.ALBUM_CONSTANTS,
    title: string.ALBUM_CONSTANTS
  }, {
    value: ConstantsEn.PLAY_LIST_CONSTANTS,
    title: string.PLAY_LIST_CONSTANTS
  }, {
    value: ConstantsEn.FLAG_CONSTANTS,
    title: string.FLAG_CONSTANTS
  }, {
    value: ConstantsEn.SONG_CONSTANTS,
    title: string.SONG_CONSTANTS
  }, {
    value: ConstantsEn.ARTIST_CONSTANTS,
    title: string.ARTIST_CONSTANTS
  }, {
    value: ConstantsEn.INSTRUMENT_CONSTANTS,
    title: string.INSTRUMENT_CONSTANTS
  }, {
    value: ConstantsEn.MUSIC_VIDEO_CONSTANTS,
    title: string.MUSIC_VIDEO_CONSTANTS
  }, {
    value: ConstantsEn.MOOD_CONSTANTS,
    title: string.MOOD_CONSTANTS
  }];
};

/* harmony default export */ __webpack_exports__["default"] = (galleryConstants);

/***/ }),

/***/ "./panelAdmin/utils/consts/index.js":
/*!******************************************!*\
  !*** ./panelAdmin/utils/consts/index.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _table__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./table */ "./panelAdmin/utils/consts/table/index.js");
/* harmony import */ var _states__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./states */ "./panelAdmin/utils/consts/states/index.js");
/* harmony import */ var _galleryConstants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./galleryConstants */ "./panelAdmin/utils/consts/galleryConstants.js");
/* harmony import */ var _card__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./card */ "./panelAdmin/utils/consts/card/index.js");
/* harmony import */ var _modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modal */ "./panelAdmin/utils/consts/modal/index.js");
/* harmony import */ var _modal__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_modal__WEBPACK_IMPORTED_MODULE_4__);





const consts = {
  table: _table__WEBPACK_IMPORTED_MODULE_0__["default"],
  states: _states__WEBPACK_IMPORTED_MODULE_1__["default"],
  galleryConstants: _galleryConstants__WEBPACK_IMPORTED_MODULE_2__["default"],
  card: _card__WEBPACK_IMPORTED_MODULE_3__["default"],
  modal: (_modal__WEBPACK_IMPORTED_MODULE_4___default())
};
/* harmony default export */ __webpack_exports__["default"] = (consts);

/***/ }),

/***/ "./panelAdmin/utils/consts/modal/index.js":
/*!************************************************!*\
  !*** ./panelAdmin/utils/consts/modal/index.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./panelAdmin/utils/consts/states/addBanner.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addBanner.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addBanner = {
  Form: {
    store: {
      label: "فروشگاه :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // type: {
    //   label: "ت :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "ت",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addBanner);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addCategory.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addCategory.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const owner = {
  Form: {
    titleFa: {
      label: "نام دسته فارسی :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام دسته فارسی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    titleEn: {
      label: "نام دسته انگلیسی  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام دسته انگلیسی "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (owner);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addGallery.js":
/*!******************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addGallery.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addGallery = {
  Form: {
    imageName: {
      label: "نام عکس :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام عکس"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: " عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addGallery);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addNotification.js":
/*!***********************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addNotification.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const owner = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    content: {
      label: "محتوی  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "محتوی "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (owner);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addOwner.js":
/*!****************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addOwner.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addOwner = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // subTitle: {
    //   label: "توضیحات :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "توضیحات ",
    //   },
    //   value: "",
    //   validation: {
    //     required: false,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    district: {
      label: "محدوده :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "محدوده"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    address: {
      label: "آدرس :",
      elementType: "textarea",
      elementConfig: {
        placeholder: "آدرس"
      },
      value: "",
      validation: {
        required: true,
        minLength: 3
      },
      valid: false,
      touched: false
    },
    phoneNumber: {
      label: "تلفن همراه :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "09xxxxxxxxx"
      },
      value: "",
      validation: {
        required: false // minLength: 11,
        // maxLength: 11,
        // isNumeric: true,
        // isMobile: true,

      },
      valid: true,
      touched: false
    },
    phone: {
      label: "تلفن ثابت :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "013-33333333"
      },
      validation: {
        required: false // minLength: 4,
        // isNumeric: false,

      },
      value: [],
      valid: true,
      touched: false
    },
    // rating: {
    //   label: "امتیاز :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "number",
    //     placeholder: "امتیاز"
    //   },
    //   value: "",
    //   validation: {
    //     minLength: 1,
    //     maxLength: 1,
    //     isNumeric: true,
    //     required: false
    //   },
    //   valid: false,
    //   touched: false
    // },
    thumbnail: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    coordinate: {
      label: "مختصات :",
      elementType: "coordinate",
      kindOf: "image",
      value: {
        lat: "",
        lng: ""
      },
      elementConfig: {
        type: "number",
        placeholder: "مختصات"
      },
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addOwner);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addProduct.js":
/*!******************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addProduct.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addArtist = {
  Form: {
    name: {
      label: "نام  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    realPrice: {
      label: "قیمت اصلی :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت اصلی"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    newPrice: {
      label: "قیمت جدید :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت جدید"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    weight: {
      label: "وزن :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "وزن"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    unit: {
      label: "واحد :",
      elementType: "input",
      elementConfig: {
        placeholder: "واحد"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addArtist);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addSlider.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addSlider.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addSlider = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [{
        name: "دسته بندی",
        value: "Category"
      }, {
        name: "فروشگاه",
        value: "Store"
      }],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    store: {
      label: "فروشگاه :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // parent: {
    //   label: "فروشگاه :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "فروشگاه",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    // type: {
    //   label: "ت :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "ت",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addSlider);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addStore.js":
/*!****************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addStore.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const addStore = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    installments: {
      label: "اقساط :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "اقساط "
      },
      value: "",
      validation: {
        required: false,
        isNumeric: true
      },
      valid: true,
      touched: false
    },
    commission: {
      label: "کمیسیون :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "کمیسیون "
      },
      value: "",
      validation: {
        required: false,
        isNumeric: true
      },
      valid: true,
      touched: false
    },
    owner: {
      label: "فروشنده :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "فروشنده"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    areaCode: {
      label: "کد فروشگاه :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "کد فروشگاه "
      },
      value: "",
      validation: {
        minLength: 5,
        required: true // isNumeric: true,

      },
      valid: false,
      touched: false
    },
    // phoneNumber: {
    //   label: "تلفن همراه :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "number",
    //     placeholder: "تلفن همراه",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //     minLength: 11,
    //     maxLength: 11,
    //     isNumeric: true,
    //     isMobile: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    phone: {
      label: "تلفن ثابت :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "013-33333333"
      },
      validation: {
        required: true,
        minLength: 4,
        isNumeric: true
      },
      value: [],
      valid: false,
      touched: false
    },
    aboutStore: {
      label: "درباره فروشگاه :",
      elementType: "textarea",
      elementConfig: {
        placeholder: "درباره فروشگاه"
      },
      value: "",
      validation: {// required: true,
      },
      valid: true,
      touched: true
    },
    features: {
      label: "ویژگی استفاده :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "ویژگی استفاده"
      },
      value: [],
      validation: {// required: true,
      },
      valid: true,
      touched: true
    },
    conditions: {
      label: "شرایط استفاده  :",
      elementType: "inputPush",
      elementConfig: {
        placeholder: "شرایط استفاده "
      },
      value: [],
      validation: {// required: true,
      },
      valid: true,
      touched: true
    },
    district: {
      label: "محدوده :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "محدوده"
      },
      value: "",
      validation: {
        required: false
      },
      valid: true,
      touched: false
    },
    slides: {
      label: "اسلاید :",
      elementType: "InputFileArray",
      kindOf: "image",
      value: [],
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    location: {
      label: "مختصات :",
      elementType: "coordinate",
      kindOf: "image",
      value: {
        lat: "",
        lng: ""
      },
      elementConfig: {
        type: "number",
        placeholder: "مختصات :"
      },
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (addStore);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/addVersion.js":
/*!******************************************************!*\
  !*** ./panelAdmin/utils/consts/states/addVersion.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const version = {
  Form: {
    version: {
      label: "ورژن :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "ورژن"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    // link: {
    //   label: "لینک  :",
    //   elementType: "input",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "لینک ",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    isRequired: {
      label: "آیا ضروری میباشد  :",
      elementType: "twoCheckBox",
      elementConfig: {
        type: "checkBox",
        placeholder: "آیا ضروری میباشد "
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },
  formIsValid: false
};
/* harmony default export */ __webpack_exports__["default"] = (version);

/***/ }),

/***/ "./panelAdmin/utils/consts/states/index.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/states/index.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _addCategory__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addCategory */ "./panelAdmin/utils/consts/states/addCategory.js");
/* harmony import */ var _addProduct__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addProduct */ "./panelAdmin/utils/consts/states/addProduct.js");
/* harmony import */ var _addGallery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./addGallery */ "./panelAdmin/utils/consts/states/addGallery.js");
/* harmony import */ var _addOwner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./addOwner */ "./panelAdmin/utils/consts/states/addOwner.js");
/* harmony import */ var _addStore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./addStore */ "./panelAdmin/utils/consts/states/addStore.js");
/* harmony import */ var _addSlider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./addSlider */ "./panelAdmin/utils/consts/states/addSlider.js");
/* harmony import */ var _addBanner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./addBanner */ "./panelAdmin/utils/consts/states/addBanner.js");
/* harmony import */ var _addNotification__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./addNotification */ "./panelAdmin/utils/consts/states/addNotification.js");
/* harmony import */ var _addVersion__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./addVersion */ "./panelAdmin/utils/consts/states/addVersion.js");









const states = {
  addCategory: _addCategory__WEBPACK_IMPORTED_MODULE_0__["default"],
  addProduct: _addProduct__WEBPACK_IMPORTED_MODULE_1__["default"],
  addGallery: _addGallery__WEBPACK_IMPORTED_MODULE_2__["default"],
  addOwner: _addOwner__WEBPACK_IMPORTED_MODULE_3__["default"],
  addStore: _addStore__WEBPACK_IMPORTED_MODULE_4__["default"],
  addSlider: _addSlider__WEBPACK_IMPORTED_MODULE_5__["default"],
  addBanner: _addBanner__WEBPACK_IMPORTED_MODULE_6__["default"],
  addNotification: _addNotification__WEBPACK_IMPORTED_MODULE_7__["default"],
  addVersion: _addVersion__WEBPACK_IMPORTED_MODULE_8__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (states);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowClub.js":
/*!***************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowClub.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/ShowClub.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const ShowClub = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "عضویت ", "دسته بندی ", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let cardElement = __jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-credit-card",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 7,
        columnNumber: 23
      }
    });

    let applicationElement = __jsx("i", {
      style: {
        fontSize: "1.2em"
      },
      className: "fal fa-mobile-android",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 8,
        columnNumber: 30
      }
    });

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 20
      }
    });

    let NotEntered = "وارد نشده";
    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? data[index].percent + " %" : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : "";
    let membership = data[index].membership.length ? data[index].membership : NotEntered;
    let membershipData = membership.includes("CARD") && membership.includes("APPLICATION") ? __jsx("div", {
      style: {
        fontSize: "1.2em",
        fontWeight: "900",
        display: "flex",
        justifyContent: "space-around"
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 9
      }
    }, " ", __jsx("div", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 11
      }
    }, cardElement, " "), __jsx("div", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 25,
        columnNumber: 11
      }
    }, " ", applicationElement)) : membership.includes("APPLICATION") ? applicationElement : membership.includes("CARD") ? cardElement : "";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, membershipData, categoryTitleFa, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowClub);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowDiscount.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowDiscount.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../formatMoney */ "./panelAdmin/utils/formatMoney.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/ShowDiscount.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ShowDiscount = data => {
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 20
      }
    });

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let realPrice = data[index].realPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(data[index].realPrice) : NotEntered;
    let newPrice = data[index].newPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(data[index].newPrice) : NotEntered;
    let rating = data[index].rating ? data[index].rating : 0;
    let percent = data[index].percent ? "%" + data[index].percent : NotEntered;
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive, {
        option: {
          edit: true
        }
      }],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowDiscount);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowScenario.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowScenario.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/ShowScenario.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const ShowScenario = scenario => {
  const thead = ["#", "نام", "تاریخ شروع ", " تاریخ پایان", "تعداد برندگان ", "شركت كنندگان", "برندگان", "جوایز", "پوچ ها", "زمان چرخش مجدد (روز)", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < scenario.length; index++) {
    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 7,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 8,
        columnNumber: 20
      }
    });

    tbody.push({
      data: [scenario[index].name, scenario[index].startDate, scenario[index].endDate, scenario[index].winnersCount, {
        option: {
          eye: true,
          name: "participants"
        }
      }, {
        option: {
          eye: true,
          name: "winners"
        }
      }, {
        option: {
          eye: true,
          name: "gifts"
        }
      }, {
        option: {
          eye: true,
          name: "empty"
        }
      }, scenario[index].spinRepeatTime, scenario[index].isActive ? active : deActive, {
        option: {
          edit: true
        }
      }],
      style: {
        background: scenario[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowScenario);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowScenarioDataInModal.js":
/*!******************************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowScenarioDataInModal.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const ShowScenarioDataInModal = (data, name) => {
  const NotEntered = "وارد نشده";
  let thead = null;
  const headGift = ["شماره", "نام", "شماره همراه", "جایزه", " تاریخ "];
  const headNotGift = ["شماره", "نام", "شماره همراه", " تاریخ "];
  name === "participants" ? thead = headNotGift : thead = headGift;
  let tbody = [];
  let body = null;

  for (let index = 0; index < data.length; index++) {
    let fullName = data[index].fullName ? data[index].fullName : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let gift = data[index].gift ? data[index].gift : NotEntered;
    let date = data[index].date ? data[index].date : NotEntered;
    const bodyGift = [fullName, phoneNumber, gift, date];
    const bodyNotGift = [fullName, phoneNumber, date];
    name === "participants" ? body = bodyNotGift : body = bodyGift;
    tbody.push({
      data: body,
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowScenarioDataInModal);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowScenarioDataInModalTwo.js":
/*!*********************************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowScenarioDataInModalTwo.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const ShowScenarioDataInModalTwo = (data, name) => {
  const NotEntered = "وارد نشده";
  const thead = ["شماره", name];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    tbody.push({
      data: [data[index] ? data[index] : NotEntered],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowScenarioDataInModalTwo);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/ShowTransactions.js":
/*!***********************************************************!*\
  !*** ./panelAdmin/utils/consts/table/ShowTransactions.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../formatMoney */ "./panelAdmin/utils/formatMoney.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/ShowTransactions.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ShowTransactions = data => {
  const thead = ["#", "نام", " شماره همراه ", " تاریخ (زمان) ", " کل مبلغ", "تخفیف", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let NotEntered = "وارد نشده";
    let user = data[index].user ? data[index].user : NotEntered;
    let userName = user ? user.name : NotEntered;
    let userPhoneNumber = user ? user.phoneNumber : NotEntered;
    let date = dateSplit ? __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 28
      }
    }, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = data[index].totalPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(data[index].totalPrice) : "0";
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let paymentStatus = data[index].paymentStatus ? __jsx("span", {
      style: {
        color: "green"
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 53
      }
    }, "پرداخت شده") : __jsx("span", {
      style: {
        color: "red"
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 110
      }
    }, " ", "پرداخت نشده");
    tbody.push({
      data: [userName, userPhoneNumber, date, totalPrice, showDiscount, paymentStatus],
      style: {
        background: data[index].isActive ? "rgb(100, 221, 23,0.3)" : ""
      }
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (ShowTransactions);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/country.js":
/*!**************************************************!*\
  !*** ./panelAdmin/utils/consts/table/country.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const country = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let flag = data[index].flag ? data[index].flag : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [flag, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (country);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/genres.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/consts/table/genres.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const genres = data => {
  const thead = ["#", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (genres);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/index.js":
/*!************************************************!*\
  !*** ./panelAdmin/utils/consts/table/index.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _genres__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./genres */ "./panelAdmin/utils/consts/table/genres.js");
/* harmony import */ var _ShowScenario__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowScenario */ "./panelAdmin/utils/consts/table/ShowScenario.js");
/* harmony import */ var _ShowScenarioDataInModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ShowScenarioDataInModal */ "./panelAdmin/utils/consts/table/ShowScenarioDataInModal.js");
/* harmony import */ var _ShowScenarioDataInModalTwo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ShowScenarioDataInModalTwo */ "./panelAdmin/utils/consts/table/ShowScenarioDataInModalTwo.js");
/* harmony import */ var _owner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./owner */ "./panelAdmin/utils/consts/table/owner.js");
/* harmony import */ var _ShowDiscount__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ShowDiscount */ "./panelAdmin/utils/consts/table/ShowDiscount.js");
/* harmony import */ var _ShowClub__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ShowClub */ "./panelAdmin/utils/consts/table/ShowClub.js");
/* harmony import */ var _ShowTransactions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ShowTransactions */ "./panelAdmin/utils/consts/table/ShowTransactions.js");
/* harmony import */ var _transactionDiscount__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./transactionDiscount */ "./panelAdmin/utils/consts/table/transactionDiscount.js");
/* harmony import */ var _members__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./members */ "./panelAdmin/utils/consts/table/members.js");
/* harmony import */ var _memberTransaction__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./memberTransaction */ "./panelAdmin/utils/consts/table/memberTransaction.js");
/* harmony import */ var _memberDiscount__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./memberDiscount */ "./panelAdmin/utils/consts/table/memberDiscount.js");
/* harmony import */ var _country__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./country */ "./panelAdmin/utils/consts/table/country.js");
/* harmony import */ var _instrument__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./instrument */ "./panelAdmin/utils/consts/table/instrument.js");
/* harmony import */ var _song__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./song */ "./panelAdmin/utils/consts/table/song.js");
/* harmony import */ var _products__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./products */ "./panelAdmin/utils/consts/table/products.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./store */ "./panelAdmin/utils/consts/table/store.js");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./user */ "./panelAdmin/utils/consts/table/user.js");


















const table = {
  instrument: _instrument__WEBPACK_IMPORTED_MODULE_13__["default"],
  country: _country__WEBPACK_IMPORTED_MODULE_12__["default"],
  memberDiscount: _memberDiscount__WEBPACK_IMPORTED_MODULE_11__["default"],
  memberTransaction: _memberTransaction__WEBPACK_IMPORTED_MODULE_10__["default"],
  members: _members__WEBPACK_IMPORTED_MODULE_9__["default"],
  transactionDiscount: _transactionDiscount__WEBPACK_IMPORTED_MODULE_8__["default"],
  showTransaction: _ShowTransactions__WEBPACK_IMPORTED_MODULE_7__["default"],
  ShowClub: _ShowClub__WEBPACK_IMPORTED_MODULE_6__["default"],
  ShowDiscount: _ShowDiscount__WEBPACK_IMPORTED_MODULE_5__["default"],
  owner: _owner__WEBPACK_IMPORTED_MODULE_4__["default"],
  showScenario: _ShowScenario__WEBPACK_IMPORTED_MODULE_1__["default"],
  genres: _genres__WEBPACK_IMPORTED_MODULE_0__["default"],
  ShowScenarioDataInModal: _ShowScenarioDataInModal__WEBPACK_IMPORTED_MODULE_2__["default"],
  ShowScenarioDataInModalTwo: _ShowScenarioDataInModalTwo__WEBPACK_IMPORTED_MODULE_3__["default"],
  song: _song__WEBPACK_IMPORTED_MODULE_14__["default"],
  products: _products__WEBPACK_IMPORTED_MODULE_15__["default"],
  store: _store__WEBPACK_IMPORTED_MODULE_16__["default"],
  user: _user__WEBPACK_IMPORTED_MODULE_17__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (table);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/instrument.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/consts/table/instrument.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const instrument = data => {
  const thead = ["#", "عکس", "عنوان فارسی ", "عنوان انگلیسی", "دنبال کنندگان", "آلبوم", "تک آهنگ", "تعداد پخش", "موزیک ویدیو"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let titleFa = data[index].titleFa ? data[index].titleFa : NotEntered;
    let titleEn = data[index].titleEn ? data[index].titleEn : NotEntered;
    let followersCount = data[index].followersCount ? data[index].followersCount : "0";
    let albumsCount = data[index].albumsCount ? data[index].albumsCount : "0";
    let singlesCount = data[index].singlesCount ? data[index].singlesCount : "0";
    let musicVideoCounts = data[index].musicVideoCounts ? data[index].musicVideoCounts : "0";
    let playedCount = data[index].playedCount ? data[index].playedCount : "0";
    tbody.push({
      data: [thumbnail, titleFa, titleEn, followersCount, albumsCount, singlesCount, playedCount, musicVideoCounts],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (instrument);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/memberDiscount.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/utils/consts/table/memberDiscount.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../formatMoney */ "./panelAdmin/utils/formatMoney.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/memberDiscount.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const memberDiscount = data => {
  //console.log({ data });
  // let data =datas.discount
  const thead = ["#", "نام", "قیمت ", "تاریخ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let discount = data[index].discount;
    let discountName = discount.title;
    let dateSplit = data[index].usedDate ? data[index].usedDate.split(" ") : "";
    let date = dateSplit ? __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 28
      }
    }, dateSplit[1] + " - " + dateSplit[0]) : "استفاده نشده";
    let price = dataIndex.price ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(dataIndex.price) : "0";
    let isUsed = dataIndex.isUsed ? "استفاده شده" : "استفاده نشده";
    tbody.push({
      data: [discountName, price, date, isUsed],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (memberDiscount);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/memberTransaction.js":
/*!************************************************************!*\
  !*** ./panelAdmin/utils/consts/table/memberTransaction.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../formatMoney */ "./panelAdmin/utils/formatMoney.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/memberTransaction.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const memberTransaction = data => {
  //console.log({ data });
  // let data =datas.discount
  const thead = ["#", "تاریخ", "قیمت ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let dataIndex = data[index];
    let paymentStatus = data[index].paymentStatus ? __jsx("span", {
      style: {
        color: "green"
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 53
      }
    }, "پرداخت شده") : __jsx("span", {
      style: {
        color: "red"
      },
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 110
      }
    }, " ", "پرداخت نشده");
    let dateSplit = data[index].date ? data[index].date.split(" ") : "";
    let date = dateSplit ? __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 15,
        columnNumber: 28
      }
    }, dateSplit[1] + " - " + dateSplit[0]) : NotEntered;
    let totalPrice = dataIndex.totalPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(dataIndex.totalPrice) : "0";
    tbody.push({
      data: [date, totalPrice, paymentStatus],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (memberTransaction);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/members.js":
/*!**************************************************!*\
  !*** ./panelAdmin/utils/consts/table/members.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/members.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const members = data => {
  const thead = ["#", "نام", "شماره همراه", "تراکنش ها", "تخفیف ها", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 8,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 9,
        columnNumber: 20
      }
    }); // let avatar = data[index].avatar ? data[index].avatar : NotEntered;


    let name = data[index].name ? data[index].name : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let showTransaction = {
      option: {
        eye: true,
        name: "transactions"
      }
    };
    let showDiscount = {
      option: {
        eye: true,
        name: "discounts"
      }
    };
    let isActive = data[index].isActive ? active : deActive;
    tbody.push({
      data: [name, phoneNumber, showTransaction, showDiscount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (members);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/owner.js":
/*!************************************************!*\
  !*** ./panelAdmin/utils/consts/table/owner.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/owner.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const owner = data => {
  //console.log({ ownerdata: data });
  const thead = ["#", "عکس", "نام ", "محدوده  ", " شماره همراه", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 20
      }
    });

    let thumbnail = data[index].thumbnail ? data[index].thumbnail : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : NotEntered;
    let district = data[index].district ? data[index].district : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let balance = data[index].balance ? data[index].balance : "0";
    let isActive = data[index].isActive ? active : deActive; // , { option: { star: true, value: rating } }

    tbody.push({
      data: [thumbnail, title, district, phoneNumber, isActive, {
        option: {
          edit: true,
          remove: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (owner);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/products.js":
/*!***************************************************!*\
  !*** ./panelAdmin/utils/consts/table/products.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const products = data => {
  //console.log({ data });
  const thead = ["#", "عکس", "عنوان", "قیمت اصلی ", " قیمت جدید", "وزن", "تخفیف", "بازدید", "تنظیمات"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let image = data[index].image ? data[index].image : NotEntered;
    let name = data[index].name ? data[index].name : NotEntered;
    let realPrice = data[index].realPrice ? data[index].realPrice : NotEntered;
    let newPrice = data[index].newPrice ? data[index].newPrice : NotEntered;
    let weight = data[index].weight ? data[index].weight : "0";
    let discount = data[index].discount ? data[index].discount : "0";
    let viewCount = data[index].viewCount ? data[index].viewCount : "0";
    tbody.push({
      data: [image, name, realPrice, newPrice, weight, viewCount, discount, {
        option: {
          edit: true,
          remove: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (products);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/song.js":
/*!***********************************************!*\
  !*** ./panelAdmin/utils/consts/table/song.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


const song = data => {
  const thead = ["#", "نام ", "پخش "];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let title = data[index].title ? data[index].title : NotEntered;
    tbody.push({
      data: [title, {
        option: {
          play: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (song);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/store.js":
/*!************************************************!*\
  !*** ./panelAdmin/utils/consts/table/store.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/store.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const store = data => {
  //console.log({ storedata: data });
  const thead = ["#", "عکس", "نام ", "کمیسیون  ", "اقساط", "وضعیت", "ویرایش"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 20
      }
    });

    let thumbnail = data[index].slides[0] ? data[index].slides[0] : NotEntered;
    let title = data[index].title ? data[index].title : NotEntered;
    let rating = data[index].rating ? data[index].rating : NotEntered;
    let commission = data[index].commission ? data[index].commission : NotEntered;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    let installments = data[index].installments ? data[index].installments : "0";
    let isActive = data[index].isActive ? active : deActive; // , { option: { star: true, value: rating } }

    tbody.push({
      data: [thumbnail, title, commission, installments, isActive, {
        option: {
          edit: true,
          remove: true
        }
      }],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (store);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/transactionDiscount.js":
/*!**************************************************************!*\
  !*** ./panelAdmin/utils/consts/table/transactionDiscount.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../formatMoney */ "./panelAdmin/utils/formatMoney.js");
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/transactionDiscount.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const transactionDiscount = data => {
  // let data =datas.discount
  const thead = ["#", "عکس", "نام ", " امتیاز ", "دسته بندی  ", "قیمت اصلی", "قیمت جدید", " تخفیف", "فروش", "بازدید ", "وضعیت"];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 20
      }
    });

    let dataIndex = data[index].discount;
    let thumbnail = dataIndex.thumbnail ? dataIndex.thumbnail : NotEntered;
    let title = dataIndex.title ? dataIndex.title : NotEntered;
    let realPrice = dataIndex.realPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(dataIndex.realPrice) : NotEntered;
    let newPrice = dataIndex.newPrice ? Object(_formatMoney__WEBPACK_IMPORTED_MODULE_1__["default"])(dataIndex.newPrice) : NotEntered;
    let rating = dataIndex.rating ? dataIndex.rating : 0;
    let percent = dataIndex.percent ? "%" + dataIndex.percent : NotEntered;
    let boughtCount = dataIndex.boughtCount ? dataIndex.boughtCount : "0";
    let category = dataIndex.category ? dataIndex.category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : NotEntered;
    let viewCount = dataIndex.viewCount ? dataIndex.viewCount : "0";
    let isActive = dataIndex.isActive ? active : deActive;
    tbody.push({
      data: [thumbnail, title, {
        option: {
          star: true,
          value: rating
        }
      }, categoryTitleFa, realPrice, newPrice, percent, boughtCount, viewCount, isActive],
      style: {}
    });
  }

  return [thead, tbody];
};

/* harmony default export */ __webpack_exports__["default"] = (transactionDiscount);

/***/ }),

/***/ "./panelAdmin/utils/consts/table/user.js":
/*!***********************************************!*\
  !*** ./panelAdmin/utils/consts/table/user.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/utils/consts/table/user.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const user = data => {
  //console.log({ userdata: data });
  const thead = ["#", "نام", "شماره همراه  "];
  let tbody = [];

  for (let index = 0; index < data.length; index++) {
    var _data$index, _data$index2;

    let NotEntered = "وارد نشده";

    let active = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "green",
        fontWeight: "900"
      },
      className: "far fa-check-circle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 18
      }
    });

    let deActive = __jsx("i", {
      style: {
        fontSize: "1em",
        color: "red",
        fontWeight: "900"
      },
      className: "fas fa-ban",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 20
      }
    });

    let name = ((_data$index = data[index]) === null || _data$index === void 0 ? void 0 : _data$index.name) || "";
    let familyName = ((_data$index2 = data[index]) === null || _data$index2 === void 0 ? void 0 : _data$index2.familyName) || "";
    let fullName = name + " " + familyName;
    let phoneNumber = data[index].phoneNumber ? data[index].phoneNumber : NotEntered;
    if (!name && !familyName) fullName = NotEntered; // , { option: { star: true, value: rating } }

    tbody.push({
      data: [fullName, phoneNumber],
      style: {}
    });
  }

  return {
    thead,
    tbody
  };
};

/* harmony default export */ __webpack_exports__["default"] = (user);

/***/ }),

/***/ "./panelAdmin/utils/dictionary/index.js":
/*!**********************************************!*\
  !*** ./panelAdmin/utils/dictionary/index.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const dictionary = text => {
  console.log({
    text
  });
  let translated;
  let lowerText = (text === null || text === void 0 ? void 0 : text.toLowerCase()) || "";

  switch (lowerText) {
    case "product":
      translated = "محصول";
      break;

    case "category":
      translated = "دسته بندی";
      break;

    case "store":
      translated = "فروشگاه";
      break;

    default:
      translated = text;
      break;
  }

  return translated;
};

/* harmony default export */ __webpack_exports__["default"] = (dictionary);

/***/ }),

/***/ "./panelAdmin/utils/formatMoney.js":
/*!*****************************************!*\
  !*** ./panelAdmin/utils/formatMoney.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const formatMoney = number => {
  return number === null || number === void 0 ? void 0 : number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

/* harmony default export */ __webpack_exports__["default"] = (formatMoney);

/***/ }),

/***/ "./panelAdmin/utils/handleKey.js":
/*!***************************************!*\
  !*** ./panelAdmin/utils/handleKey.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const handleKey = event => {
  const form = event.target.form;
  const index = Array.prototype.indexOf.call(form, event.target);
  let keyCode = event.keyCode;
  let numberAccepted = [8, 13];

  if (numberAccepted.includes(keyCode)) {
    if (keyCode === 13) {
      if (form.elements[index + 1]) return form.elements[index + 1].focus();
    } else if (keyCode === 8) if (form.elements[index].value) return form.elements[index].value.length - 1; // else if (form.elements[index - 1]) form.elements[index - 1].focus();


    event.preventDefault();
  }
};

/* harmony default export */ __webpack_exports__["default"] = (handleKey);

/***/ }),

/***/ "./panelAdmin/utils/index.js":
/*!***********************************!*\
  !*** ./panelAdmin/utils/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _consts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./consts */ "./panelAdmin/utils/consts/index.js");
/* harmony import */ var _toastify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./toastify */ "./panelAdmin/utils/toastify.js");
/* harmony import */ var _CelanderConvert__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CelanderConvert */ "./panelAdmin/utils/CelanderConvert/index.js");
/* harmony import */ var _onChanges__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./onChanges */ "./panelAdmin/utils/onChanges/index.js");
/* harmony import */ var _handleKey__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./handleKey */ "./panelAdmin/utils/handleKey.js");
/* harmony import */ var _formatMoney__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./formatMoney */ "./panelAdmin/utils/formatMoney.js");
/* harmony import */ var _json__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./json */ "./panelAdmin/utils/json/index.js");
/* harmony import */ var _updateObject__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./updateObject */ "./panelAdmin/utils/updateObject.js");
/* harmony import */ var _operation__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./operation */ "./panelAdmin/utils/operation/index.js");
/* harmony import */ var _authorization__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./authorization */ "./panelAdmin/utils/authorization.js");
/* harmony import */ var _adminHoc__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./adminHoc */ "./panelAdmin/utils/adminHoc/index.js");
/* harmony import */ var _checkValidity__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./checkValidity */ "./panelAdmin/utils/checkValidity/index.js");
/* harmony import */ var _dictionary__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./dictionary */ "./panelAdmin/utils/dictionary/index.js");













const utils = {
  dictionary: _dictionary__WEBPACK_IMPORTED_MODULE_12__["default"],
  adminHoc: _adminHoc__WEBPACK_IMPORTED_MODULE_10__["default"],
  checkValidity: _checkValidity__WEBPACK_IMPORTED_MODULE_11__["checkValidity"],
  authorization: _authorization__WEBPACK_IMPORTED_MODULE_9__["default"],
  operation: _operation__WEBPACK_IMPORTED_MODULE_8__["default"],
  consts: _consts__WEBPACK_IMPORTED_MODULE_0__["default"],
  toastify: _toastify__WEBPACK_IMPORTED_MODULE_1__["default"],
  CelanderConvert: _CelanderConvert__WEBPACK_IMPORTED_MODULE_2__["default"],
  onChanges: _onChanges__WEBPACK_IMPORTED_MODULE_3__["default"],
  handleKey: _handleKey__WEBPACK_IMPORTED_MODULE_4__["default"],
  formatMoney: _formatMoney__WEBPACK_IMPORTED_MODULE_5__["default"],
  json: _json__WEBPACK_IMPORTED_MODULE_6__["default"],
  updateObject: _updateObject__WEBPACK_IMPORTED_MODULE_7__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (utils);

/***/ }),

/***/ "./panelAdmin/utils/json/index.js":
/*!****************************************!*\
  !*** ./panelAdmin/utils/json/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _table__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./table */ "./panelAdmin/utils/json/table/index.js");

const json = {
  table: _table__WEBPACK_IMPORTED_MODULE_0__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (json);

/***/ }),

/***/ "./panelAdmin/utils/json/table/category.json":
/*!***************************************************!*\
  !*** ./panelAdmin/utils/json/table/category.json ***!
  \***************************************************/
/*! exports provided: headers, body, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"headers\":[\"ردیف\",\"عکس\",\"عنوان\"],\"body\":[\"image\",\"name\"]}");

/***/ }),

/***/ "./panelAdmin/utils/json/table/gallery.json":
/*!**************************************************!*\
  !*** ./panelAdmin/utils/json/table/gallery.json ***!
  \**************************************************/
/*! exports provided: headers, body, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"headers\":[\"ردیف\",\"عکس\",\"عنوان\"],\"body\":[\"url\",\"name\"]}");

/***/ }),

/***/ "./panelAdmin/utils/json/table/index.js":
/*!**********************************************!*\
  !*** ./panelAdmin/utils/json/table/index.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _category_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category.json */ "./panelAdmin/utils/json/table/category.json");
var _category_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./category.json */ "./panelAdmin/utils/json/table/category.json", 1);
/* harmony import */ var _gallery_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./gallery.json */ "./panelAdmin/utils/json/table/gallery.json");
var _gallery_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ./gallery.json */ "./panelAdmin/utils/json/table/gallery.json", 1);


const table = {
  category: _category_json__WEBPACK_IMPORTED_MODULE_0__,
  gallery: _gallery_json__WEBPACK_IMPORTED_MODULE_1__
};
/* harmony default export */ __webpack_exports__["default"] = (table);

/***/ }),

/***/ "./panelAdmin/utils/menuFormat.js":
/*!****************************************!*\
  !*** ./panelAdmin/utils/menuFormat.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! .. */ "./panelAdmin/index.js");
/* harmony import */ var _values_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../values/index */ "./panelAdmin/values/index.js");


const menuFormat = [{
  title: "عمومی",
  menus: [{
    route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_DASHBOARD,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.DASHBOARD,
    menuIconImg: false,
    menuIconClass: "fas fa-tachometer-slowest",
    subMenu: [// { title: "SubDashboard", route: "/dashboard1" },
      // { title: "SubDashboard", route: "/dashboard2" }
    ]
  }]
}, {
  title: "کاربردی",
  menus: [{
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.GALLERY,
    menuIconImg: false,
    menuIconClass: "fad fa-images",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_GALLERIES,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_GALLERY
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.CATEGORY,
    menuIconImg: false,
    menuIconClass: "fad fa-cubes",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_CATEGORIES,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_CATEGORY
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_CATEGORY,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_CATEGORY
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.OWNER,
    menuIconImg: false,
    menuIconClass: "far fa-street-view",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_OWNERS,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_OWNER
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_OWNER,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_OWNER
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.STORE,
    menuIconImg: false,
    menuIconClass: "fad fa-store",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_STORES,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_STORE
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_STORE,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_STORE
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SLIDER,
    menuIconImg: false,
    menuIconClass: "fas fa-presentation",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_SLIDERS,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_SLIDER
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_SLIDER,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_SLIDER
    }]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.BANNER,
    menuIconImg: false,
    menuIconClass: "far fa-archive",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_BANNERS,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_BANNER
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_BANNER,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_BANNER
    }]
  }, {
    route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_NOTIFICATION,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.NOTIFICATION,
    menuIconImg: false,
    menuIconClass: "fad fa-bells",
    subMenu: [// {
      //   title: values.strings.SEE_NOTIFICATIONS,
      //   route: values.routes.GS_ADMIN_NOTIFICATION,
      // },
      // {
      //   title: values.strings.ADD_NOTIFICATION,
      //   route: values.routes.GS_ADMIN_ADD_NOTIFICATION,
      // },
    ]
  }, {
    route: false,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.VERSION,
    menuIconImg: false,
    menuIconClass: "fab fa-vimeo-v",
    subMenu: [{
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.SEE_VERSIONS,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_VERSION
    }, {
      title: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.ADD_VERSION,
      route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_ADD_VERSION
    }]
  }, {
    route: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].routes.GS_ADMIN_USER,
    menuTitle: _values_index__WEBPACK_IMPORTED_MODULE_1__["default"].strings.USERS,
    menuIconImg: false,
    menuIconClass: "fad fa-users",
    subMenu: []
  }]
}];
/* harmony default export */ __webpack_exports__["default"] = (menuFormat);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/arrayOnchange.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/utils/onChanges/arrayOnchange.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const arrayOnchange = async props => {
  const {
    event: e,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity,
    updateObject,
    uploadChange
  } = props; // //console.log({ validName });

  let changeValid,
      updatedForm,
      updatedFormElement = {};
  e.map(async event => {
    let value = data.Form[event.name].value;

    let update = _objectSpread({}, value);

    let eventValue = event.value;
    let typeofData = typeof value;
    let isArray, isObject, isString;
    typeofData === "object" && value != null ? value.length >= 0 ? isArray = true : isObject = true : isString = true;

    const remove = index => value.splice(index, 1)[0];

    const push = (val, newVal) => newVal != undefined ? val.push(newVal) : ""; // //console.log({ eventValue, value });
    // if (event.type === "file") {
    //   const uploadFile = await uploadChange(event, setLoading, imageType, setState);
    //   if (uploadFile) {
    //     if (isArray) value.includes(uploadFile) ? remove(value.findIndex((d) => d === uploadFile)) : push(value, uploadFile);
    //     else value = uploadFile;
    //   } else return;
    // } else if (isArray) value.includes(eventValue) ? remove(value.findIndex((d) => d === eventValue)) : push(value, eventValue);
    // else if (isObject) {
    //   value = eventValue;
    //   if (event.child) value = update[event.child] = eventValue;
    // } else if (isString)


    value = eventValue;
    let checkValidValue;
    if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value;
    updatedFormElement[event.name] = updateObject(data.Form[event.name], {
      value: value,
      valid: data.Form[event.name].validation.required ? typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).isValid : true,
      touched: true,
      titleValidity: checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).errorTitle
    });

    if (validName) {
      changeValid = updateObject(data.Form[validName], {
        valid: true
      });
      updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement, {
        [validName]: changeValid
      }));
    } else updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement)); // //console.log({ updatedFormElement, updatedForm });


    let formIsValid = false;

    for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid || updatedForm[name].block && formIsValid;

    return setData({
      Form: updatedForm,
      formIsValid: formIsValid
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (arrayOnchange);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/globalChange.js":
/*!****************************************************!*\
  !*** ./panelAdmin/utils/onChanges/globalChange.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _checkValidity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../checkValidity */ "./panelAdmin/utils/checkValidity/index.js");
/* harmony import */ var _updateObject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../updateObject */ "./panelAdmin/utils/updateObject.js");
/* harmony import */ var _uploadChange__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./uploadChange */ "./panelAdmin/utils/onChanges/uploadChange.js");
/* harmony import */ var _handelOnchange__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./handelOnchange */ "./panelAdmin/utils/onChanges/handelOnchange.js");
/* harmony import */ var _arrayOnchange__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./arrayOnchange */ "./panelAdmin/utils/onChanges/arrayOnchange.js");






const globalChange = async props => {
  const {
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    fileName,
    dispatch
  } = props;
  let typeCheck = typeof event; // //console.log(typeCheck === "object" && event.length > 0);
  //console.log({ event });

  if (typeCheck === "object" && event.length > 0) return Object(_arrayOnchange__WEBPACK_IMPORTED_MODULE_4__["default"])({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: _checkValidity__WEBPACK_IMPORTED_MODULE_0__["checkValidity"],
    updateObject: _updateObject__WEBPACK_IMPORTED_MODULE_1__["default"],
    uploadChange: _uploadChange__WEBPACK_IMPORTED_MODULE_2__["default"],
    fileName,
    dispatch
  });else if (typeCheck === "object") return Object(_handelOnchange__WEBPACK_IMPORTED_MODULE_3__["default"])({
    event,
    data,
    setData,
    setState,
    setLoading,
    imageType,
    validName,
    checkValidity: _checkValidity__WEBPACK_IMPORTED_MODULE_0__["checkValidity"],
    updateObject: _updateObject__WEBPACK_IMPORTED_MODULE_1__["default"],
    uploadChange: _uploadChange__WEBPACK_IMPORTED_MODULE_2__["default"],
    fileName,
    dispatch
  });
};

/* harmony default export */ __webpack_exports__["default"] = (globalChange);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/handelOnchange.js":
/*!******************************************************!*\
  !*** ./panelAdmin/utils/onChanges/handelOnchange.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const handelOnchange = async ({
  event,
  data,
  setData,
  setState,
  setLoading,
  imageType,
  validName,
  checkValidity,
  updateObject,
  uploadChange,
  fileName
}) => {
  let changeValid,
      updatedForm,
      updatedFormElement = {};
  let formIsValid = true;
  let value = data.Form[event.name].value;

  let update = _objectSpread({}, value);

  let eventValue = event.value;
  let typeofData = typeof value;
  let isArray, isObject, isString;
  typeofData === "object" && value != undefined ? value && value.length >= 0 ? isArray = true : isObject = true : isString = true;

  const remove = index => value.splice(index, 1)[0];

  const push = (val, newVal) => newVal != undefined ? val.push(newVal) : "";

  if (event.type === "file") {
    const uploadFile = await uploadChange({
      event,
      setLoading,
      imageType,
      setState,
      valid: data.Form[event.name].kindOf,
      fileName
    });

    if (uploadFile) {
      if (isArray) value.includes(uploadFile) ? remove(value.findIndex(d => d === uploadFile)) : push(value, uploadFile);else value = uploadFile;
    } else return;
  } else if (isArray) value.includes(eventValue) ? remove(value.findIndex(d => d === eventValue)) : push(value, eventValue);else if (isObject) {
    if (event.child) {
      value[event.child] = eventValue;
    } else value = eventValue;
  } else if (isString) value = eventValue;

  let checkValidValue;
  if (typeofData === "object") checkValidValue = eventValue;else checkValidValue = value; // ////console.log({ moooooj: checkValidity(checkValidValue, data.Form[event.name].validation, isArray).isValid });
  // console.log({ checkValidValue, eventValue, value, typeofData: typeofData, checkEvent: data.Form[event.name] });

  updatedFormElement[event.name] = updateObject(data.Form[event.name], {
    value: value,
    valid: data.Form[event.name].validation.required ? typeof event.value === "object" ? true : checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).isValid : true,
    touched: true,
    block: false,
    titleValidity: checkValidity(checkValidValue, data.Form[event.name].validation, isArray, value).errorTitle
  });

  if (validName) {
    changeValid = updateObject(data.Form[validName], {
      block: true
    });
    updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement, {
      [validName]: changeValid
    }));
  } else updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement));

  console.log({
    updatedFormElement,
    updatedForm
  });

  for (let name in updatedForm) formIsValid = updatedForm[name].valid && formIsValid || updatedForm[name].block && formIsValid;

  return setData({
    Form: updatedForm,
    formIsValid: formIsValid
  }); // setData({ Form: updatedForm, formIsValid: formIsValid });
};

/* harmony default export */ __webpack_exports__["default"] = (handelOnchange);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/index.js":
/*!*********************************************!*\
  !*** ./panelAdmin/utils/onChanges/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalChange__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./globalChange */ "./panelAdmin/utils/onChanges/globalChange.js");
/* harmony import */ var _arrayOnchange__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./arrayOnchange */ "./panelAdmin/utils/onChanges/arrayOnchange.js");


const onChanges = {
  globalChange: _globalChange__WEBPACK_IMPORTED_MODULE_0__["default"],
  arrayOnchange: _arrayOnchange__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (onChanges);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/uploadChange.js":
/*!****************************************************!*\
  !*** ./panelAdmin/utils/onChanges/uploadChange.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../api */ "./panelAdmin/api/index.js");
/* harmony import */ var _toastify__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../toastify */ "./panelAdmin/utils/toastify.js");
/* harmony import */ var _validUpload_voicevalid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./validUpload/voicevalid */ "./panelAdmin/utils/onChanges/validUpload/voicevalid.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const uploadChange = async props => {
  const {
    event,
    setLoading,
    imageType,
    setState,
    valid,
    fileName,
    dispatch
  } = props; //console.log({ eventUpload: event });

  let files = event.files[0]; //console.log({ imageType, fileName });

  let returnData = false;

  if (files) {
    switch (valid) {
      case "image":
        if (files.type.includes("image")) {
          if (fileName) {
            if ( // dispatch(sagaActions.uploadImageData({ data: files, imageType, fileName }))
            await _api__WEBPACK_IMPORTED_MODULE_1__["post"].imageUpload(files, setLoading, setState, fileName)) returnData = fileName;
          } else {
            Object(_toastify__WEBPACK_IMPORTED_MODULE_2__["default"])("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      case "video":
        if (files.type.includes("video")) returnData = await _api__WEBPACK_IMPORTED_MODULE_1__["post"].videoUpload(files, setLoading, imageType, setState);
        break;

      case "voice":
        if (Object(_validUpload_voicevalid__WEBPACK_IMPORTED_MODULE_3__["default"])(files.type)) {
          if (fileName) {
            if (await _api__WEBPACK_IMPORTED_MODULE_1__["post"].voiceUpload(files, setLoading, imageType, setState, fileName)) returnData = fileName;
          } else {
            Object(_toastify__WEBPACK_IMPORTED_MODULE_2__["default"])("لطفا اطلاعات قبلی را پر کنید", "error");
          }
        }

        break;

      default:
        Object(_toastify__WEBPACK_IMPORTED_MODULE_2__["default"])("فایل شما نباید " + files.type + " باشد", "error");
        break;
    }
  } //console.log({ files: files, returnData, fileName });


  if (!returnData && files && (valid === "image" ? fileName : true)) Object(_toastify__WEBPACK_IMPORTED_MODULE_2__["default"])("فایل شما نباید " + files.type + " باشد", "error");
  setState(prev => _objectSpread({}, prev, {
    progressPercentImage: null,
    progressPercentVideo: null,
    progressPercentSongs: null
  }));
  return returnData;
};

/* harmony default export */ __webpack_exports__["default"] = (uploadChange);

/***/ }),

/***/ "./panelAdmin/utils/onChanges/validUpload/voicevalid.js":
/*!**************************************************************!*\
  !*** ./panelAdmin/utils/onChanges/validUpload/voicevalid.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
let formats = ["mp3", "voice", "audio"];

const voiceValid = type => {
  let valid = formats.map(format => {
    if (type.includes(format)) return true;else return false;
  });
  let finalValid = valid.includes(true) ? true : false;
  return finalValid;
};

/* harmony default export */ __webpack_exports__["default"] = (voiceValid);

/***/ }),

/***/ "./panelAdmin/utils/operation/FormManagement.js":
/*!******************************************************!*\
  !*** ./panelAdmin/utils/operation/FormManagement.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const FormManagement = ({
  categoryData,
  formElement,
  showModal,
  inputChangedHandler,
  accept,
  OwnerData,
  removeHandel,
  staticTitle,
  storeData
}) => {
  let disabled, staticTitleValue;
  let value = formElement.config.value;
  if (formElement.id === (staticTitle === null || staticTitle === void 0 ? void 0 : staticTitle.name)) staticTitleValue = staticTitle === null || staticTitle === void 0 ? void 0 : staticTitle.value;
  let key = formElement.id;
  let elementType = formElement.config.elementType;
  let elementConfig = formElement.config.elementConfig;

  let remove = index => removeHandel(index, formElement.id);

  let label = formElement.config.label;
  let titleValidity = formElement.config.titleValidity;
  let dataChunk = {
    titleValidity,
    value,
    key,
    elementType,
    elementConfig,
    removeHandel: remove,
    label,
    disabled,
    staticTitle: staticTitleValue
  };

  switch (formElement.id) {
    case "slides":
      disabled = true;
      return _objectSpread({}, dataChunk, {
        disabled: true,
        accepted: () => showModal({
          kindOf: "showGallery",
          name: formElement.id
        })
      });
    // break;

    case "image":
      disabled = true;
      return _objectSpread({}, dataChunk, {
        disabled: true,
        accepted: () => showModal({
          kindOf: "showGallery",
          name: formElement.id
        })
      });
    // break;

    case "thumbnail":
      disabled = true;
      return _objectSpread({}, dataChunk, {
        disabled: true,
        accepted: () => showModal({
          kindOf: "showGallery",
          name: formElement.id
        })
      });

    case "isRequire":
      accepted = value => inputChangedHandler({
        value: value || "",
        name: formElement.id
      });

    case "coordinate":
      return _objectSpread({}, dataChunk, {
        changed: value => {
          //console.log({ value });
          inputChangedHandler({
            value: value,
            name: formElement.id
          });
        }
      });

    case "location":
      return _objectSpread({}, dataChunk, {
        changed: value => {
          //console.log({ value });
          inputChangedHandler({
            value: value,
            name: formElement.id
          });
        }
      });
    // break;

    case "category":
      return _objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: categoryData
      });
    // break;

    case "owner":
      return _objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: OwnerData
      });
    // break;

    case "store":
      return _objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: storeData
      });
    // break;

    case "parent":
      return _objectSpread({}, dataChunk, {
        accepted: value => accept({
          value,
          name: formElement.id
        }),
        dropDownData: storeData
      });
    // break;

    default:
      return _objectSpread({}, dataChunk, {
        accepted: value => inputChangedHandler({
          value: value,
          name: formElement.id
        }),
        changed: e => {
          var _e$currentTarget, _e$currentTarget2, _e$currentTarget3;

          return inputChangedHandler({
            value: (_e$currentTarget = e.currentTarget) === null || _e$currentTarget === void 0 ? void 0 : _e$currentTarget.value,
            name: formElement.id,
            type: (_e$currentTarget2 = e.currentTarget) === null || _e$currentTarget2 === void 0 ? void 0 : _e$currentTarget2.type,
            files: (_e$currentTarget3 = e.currentTarget) === null || _e$currentTarget3 === void 0 ? void 0 : _e$currentTarget3.files
          });
        }
      });
  }
};

/* harmony default export */ __webpack_exports__["default"] = (FormManagement);

/***/ }),

/***/ "./panelAdmin/utils/operation/formTouchChange.js":
/*!*******************************************************!*\
  !*** ./panelAdmin/utils/operation/formTouchChange.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../.. */ "./panelAdmin/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const formTouchChange = ({
  data,
  setData
}) => {
  const updateObject = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.updateObject;
  const checkValidity = ___WEBPACK_IMPORTED_MODULE_0__["default"].utils.checkValidity;
  let changeValid,
      updatedForm,
      updatedFormElement = {};

  for (const key in data.Form) {
    let value = data.Form[key].value;
    let typeofData = typeof value;
    let isArray, isObject, isString;
    typeofData === "object" && value != undefined ? value && value.length >= 0 ? isArray = true : isObject = true : isString = true; // console.log({ data }, isArray, isObject, isString);

    updatedFormElement[key] = updateObject(data.Form[key], {
      touched: true,
      titleValidity: checkValidity(data.Form[key].value, data.Form[key].validation, isArray, data.Form[key].value).errorTitle
    });
  }

  updatedForm = updateObject(data.Form, _objectSpread({}, updatedFormElement));
  return setData({
    Form: updatedForm,
    formIsValid: false
  });
};

/* harmony default export */ __webpack_exports__["default"] = (formTouchChange);

/***/ }),

/***/ "./panelAdmin/utils/operation/index.js":
/*!*********************************************!*\
  !*** ./panelAdmin/utils/operation/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _submitted__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./submitted */ "./panelAdmin/utils/operation/submitted.js");
/* harmony import */ var _FormManagement__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FormManagement */ "./panelAdmin/utils/operation/FormManagement.js");
/* harmony import */ var _formTouchChange__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./formTouchChange */ "./panelAdmin/utils/operation/formTouchChange.js");



const operation = {
  submitted: _submitted__WEBPACK_IMPORTED_MODULE_0__["default"],
  FormManagement: _FormManagement__WEBPACK_IMPORTED_MODULE_1__["default"],
  formTouchChange: _formTouchChange__WEBPACK_IMPORTED_MODULE_2__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (operation);

/***/ }),

/***/ "./panelAdmin/utils/operation/submitted.js":
/*!*************************************************!*\
  !*** ./panelAdmin/utils/operation/submitted.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _updateObject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../updateObject */ "./panelAdmin/utils/updateObject.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const submitted = async props => {
  const {
    removeKey,
    editKey,
    setSubmitLoading,
    data,
    editData,
    put,
    post,
    setData,
    states,
    setEdit,
    propsHideModal,
    setCheckSubmitted,
    checkSubmitted
  } = props;
  setSubmitLoading(true);
  setCheckSubmitted(!checkSubmitted);
  const formData = {};

  for (let formElementIdentifier in data.Form) {
    if (formElementIdentifier === "isRequired") {
      formData[formElementIdentifier] = data.Form[formElementIdentifier].value === "true" ? true : false;
    } else if (formElementIdentifier === (editKey === null || editKey === void 0 ? void 0 : editKey.beforeKey)) formData[editKey === null || editKey === void 0 ? void 0 : editKey.afterKey] = data.Form[formElementIdentifier].value;else if (formElementIdentifier !== removeKey) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
  }

  console.log({
    formData
  });

  if (editData) {
    if (await put({
      id: editData._id,
      data: formData
    })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post(formData)) setData(_objectSpread({}, states));

  if (states.Form["phone"]) {
    states.Form["phone"].value = [];
  }

  if (states.Form["coordinate"]) {
    states.Form["coordinate"].value = {
      lat: "",
      lng: ""
    };
  }

  if (states.Form["location"]) {
    states.Form["location"].value = {
      lat: "",
      lng: ""
    };
  }

  if (states.Form["slides"]) {
    states.Form["slides"].value = [];
  }

  setSubmitLoading(false);
};

/* harmony default export */ __webpack_exports__["default"] = (submitted);

/***/ }),

/***/ "./panelAdmin/utils/toastify.js":
/*!**************************************!*\
  !*** ./panelAdmin/utils/toastify.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-toastify */ "react-toastify");
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_1__);



const toastify = async (text, type) => {
  react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].configure();

  if (type === "error") {
    // alert("error");
    await react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].error(text, {
      position: "top-right",
      autoClose: 5000
    });
  } else if (type === "success") {
    // alert("success");
    await react_toastify__WEBPACK_IMPORTED_MODULE_1__["toast"].success(text, {
      position: "top-right",
      autoClose: 5000
    });
  }
};

/* harmony default export */ __webpack_exports__["default"] = (toastify);

/***/ }),

/***/ "./panelAdmin/utils/updateObject.js":
/*!******************************************!*\
  !*** ./panelAdmin/utils/updateObject.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const updateObject = (oldObject, updatedProperties) => {
  return _objectSpread({}, oldObject, {}, updatedProperties);
};

/* harmony default export */ __webpack_exports__["default"] = (updateObject);

/***/ }),

/***/ "./panelAdmin/values/apiString.js":
/*!****************************************!*\
  !*** ./panelAdmin/values/apiString.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const ADMIN = "/admin";
const CATEGORY = ADMIN + "/category";
const PRODUCT = ADMIN + "/product";
const IMAGE = ADMIN + "/images";
const UPLOAD = ADMIN + "/upload";
const OWNERS = ADMIN + "/owner";
const STORE = ADMIN + "/store";
const SLIDER = ADMIN + "/slider";
const BANNER = ADMIN + "/banner";
const NOTIFICATION = ADMIN + "/notification";
const VERSION = ADMIN + "/version";
const LOGIN = ADMIN + "/login";
const USER = ADMIN + "/users";
const apiString = {
  CATEGORY,
  PRODUCT,
  IMAGE,
  UPLOAD,
  OWNERS,
  STORE,
  SLIDER,
  BANNER,
  NOTIFICATION,
  VERSION,
  LOGIN,
  USER
};
/* harmony default export */ __webpack_exports__["default"] = (apiString);

/***/ }),

/***/ "./panelAdmin/values/index.js":
/*!************************************!*\
  !*** ./panelAdmin/values/index.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _strings__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./strings */ "./panelAdmin/values/strings/index.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./routes */ "./panelAdmin/values/routes/index.js");
/* harmony import */ var _apiString__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./apiString */ "./panelAdmin/values/apiString.js");
/* harmony import */ var _strings_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./strings/constants */ "./panelAdmin/values/strings/constants.js");
/* harmony import */ var _strings_fa__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./strings/fa */ "./panelAdmin/values/strings/fa/index.js");
/* harmony import */ var _strings_en__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./strings/en */ "./panelAdmin/values/strings/en/index.js");






const values = {
  routes: _routes__WEBPACK_IMPORTED_MODULE_1__["default"],
  strings: _strings__WEBPACK_IMPORTED_MODULE_0__["default"],
  apiString: _apiString__WEBPACK_IMPORTED_MODULE_2__["default"],
  constants: _strings_constants__WEBPACK_IMPORTED_MODULE_3__["default"],
  fa: _strings_fa__WEBPACK_IMPORTED_MODULE_4__["default"],
  en: _strings_en__WEBPACK_IMPORTED_MODULE_5__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (values);

/***/ }),

/***/ "./panelAdmin/values/routes/index.js":
/*!*******************************************!*\
  !*** ./panelAdmin/values/routes/index.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const GS_PANEL_ADMIN_TITLE = "/panelAdmin";
const GS_ADMIN_DASHBOARD = GS_PANEL_ADMIN_TITLE + "/dashboard";
const GS_ADMIN_GALLERY = GS_PANEL_ADMIN_TITLE + "/gallery";
const GS_ADMIN_CATEGORY = GS_PANEL_ADMIN_TITLE + "/category";
const GS_ADMIN_ADD_CATEGORY = GS_PANEL_ADMIN_TITLE + "/addCategory";
const GS_ADMIN_PRODUCT = GS_PANEL_ADMIN_TITLE + "/product";
const GS_ADMIN_ADD_PRODUCT = GS_PANEL_ADMIN_TITLE + "/addProduct";
const GS_ADMIN_VARIABLE = GS_PANEL_ADMIN_TITLE + "/variable";
const GS_ADMIN_ADD_VARIABLE = GS_PANEL_ADMIN_TITLE + "/addVariable";
const GS_ADMIN_OWNER = GS_PANEL_ADMIN_TITLE + "/owner";
const GS_ADMIN_ADD_OWNER = GS_PANEL_ADMIN_TITLE + "/addOwner";
const GS_ADMIN_STORE = GS_PANEL_ADMIN_TITLE + "/store";
const GS_ADMIN_ADD_STORE = GS_PANEL_ADMIN_TITLE + "/addStore";
const GS_ADMIN_SLIDER = GS_PANEL_ADMIN_TITLE + "/slider";
const GS_ADMIN_ADD_SLIDER = GS_PANEL_ADMIN_TITLE + "/addSlider";
const GS_ADMIN_BANNER = GS_PANEL_ADMIN_TITLE + "/banner";
const GS_ADMIN_ADD_BANNER = GS_PANEL_ADMIN_TITLE + "/addBanner";
const GS_ADMIN_NOTIFICATION = GS_PANEL_ADMIN_TITLE + "/notification";
const GS_ADMIN_ADD_NOTIFICATION = GS_PANEL_ADMIN_TITLE + "/addNotification";
const GS_ADMIN_VERSION = GS_PANEL_ADMIN_TITLE + "/version";
const GS_ADMIN_ADD_VERSION = GS_PANEL_ADMIN_TITLE + "/addVersion";
const GS_ADMIN_USER = GS_PANEL_ADMIN_TITLE + "/user";
const routes = {
  GS_ADMIN_DASHBOARD,
  GS_PANEL_ADMIN_TITLE,
  GS_ADMIN_GALLERY,
  GS_ADMIN_VARIABLE,
  GS_ADMIN_ADD_VARIABLE,
  GS_ADMIN_CATEGORY,
  GS_ADMIN_ADD_CATEGORY,
  GS_ADMIN_PRODUCT,
  GS_ADMIN_ADD_PRODUCT,
  GS_ADMIN_OWNER,
  GS_ADMIN_ADD_OWNER,
  GS_ADMIN_STORE,
  GS_ADMIN_ADD_STORE,
  GS_ADMIN_SLIDER,
  GS_ADMIN_ADD_SLIDER,
  GS_ADMIN_BANNER,
  GS_ADMIN_ADD_BANNER,
  GS_ADMIN_NOTIFICATION,
  GS_ADMIN_ADD_NOTIFICATION,
  GS_ADMIN_VERSION,
  GS_ADMIN_ADD_VERSION,
  GS_ADMIN_USER
};
/* harmony default export */ __webpack_exports__["default"] = (routes);

/***/ }),

/***/ "./panelAdmin/values/strings/constants.js":
/*!************************************************!*\
  !*** ./panelAdmin/values/strings/constants.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const ALBUM_CONSTANTS = "album";
const PLAY_LIST_CONSTANTS = "playlist";
const SONG_CONSTANTS = "song";
const FLAG_CONSTANTS = "flag";
const ARTIST_CONSTANTS = "artist";
const INSTRUMENT_CONSTANTS = "instrument";
const MUSIC_VIDEO_CONSTANTS = "musicvideo";
const MOOD_CONSTANTS = "mood";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS
};
/* harmony default export */ __webpack_exports__["default"] = (constants);

/***/ }),

/***/ "./panelAdmin/values/strings/en/constants.js":
/*!***************************************************!*\
  !*** ./panelAdmin/values/strings/en/constants.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const ALBUM_CONSTANTS = "album";
const PLAY_LIST_CONSTANTS = "playlist";
const SONG_CONSTANTS = "song";
const FLAG_CONSTANTS = "flag";
const ARTIST_CONSTANTS = "artist";
const INSTRUMENT_CONSTANTS = "instrument";
const MUSIC_VIDEO_CONSTANTS = "musicvideo";
const MOOD_CONSTANTS = "mood";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS
};
/* harmony default export */ __webpack_exports__["default"] = (constants);

/***/ }),

/***/ "./panelAdmin/values/strings/en/global.js":
/*!************************************************!*\
  !*** ./panelAdmin/values/strings/en/global.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const TRACKS = "Tracks";
const NO_ENTRIES = "no entries";
const FOLLOWERS = "followers";
const global = {
  TRACKS,
  NO_ENTRIES: NO_ENTRIES,
  FOLLOWERS
};
/* harmony default export */ __webpack_exports__["default"] = (global);

/***/ }),

/***/ "./panelAdmin/values/strings/en/index.js":
/*!***********************************************!*\
  !*** ./panelAdmin/values/strings/en/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navbar_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navbar.js */ "./panelAdmin/values/strings/en/navbar.js");
/* harmony import */ var _sideMenu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sideMenu */ "./panelAdmin/values/strings/en/sideMenu.js");
/* harmony import */ var _global_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./global.js */ "./panelAdmin/values/strings/en/global.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./constants */ "./panelAdmin/values/strings/en/constants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const en = _objectSpread({}, _navbar_js__WEBPACK_IMPORTED_MODULE_0__["default"], {}, _sideMenu__WEBPACK_IMPORTED_MODULE_1__["default"], {}, _global_js__WEBPACK_IMPORTED_MODULE_2__["default"], {}, _constants__WEBPACK_IMPORTED_MODULE_3__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (en);

/***/ }),

/***/ "./panelAdmin/values/strings/en/navbar.js":
/*!************************************************!*\
  !*** ./panelAdmin/values/strings/en/navbar.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const APP_NAME = "Rimtal";
const EXPLORE = "Explore";
const TRACKS = "Tracks";
const PLAYLISTS = "Playlists";
const ALBUMS = "Albums";
const ARTISTS = "Artists";
const VIDEOS = "Videos";
const SIGN_IN = "Sign In";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ __webpack_exports__["default"] = (navbar);

/***/ }),

/***/ "./panelAdmin/values/strings/en/sideMenu.js":
/*!**************************************************!*\
  !*** ./panelAdmin/values/strings/en/sideMenu.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";
const CATEGORIES = "دسته بندی ها";
const SEE_ARTIST = "مشاهده هنرمندان";
const ARTIST = "هنرمند";
const ADD_ARTIST = "افزودن هنرمند";
const SEE_ALBUM = "مشاهده آلبوم ها";
const ALBUM = "آلبوم";
const ADD_ALBUM = "افزودن آلبوم";
const SEE_GENRES = "مشاهده ژانر ها";
const GENRES = "ژانر";
const ADD_GENRE = "افزودن ژانر";
const SEE_COUNTRY = "مشاهده کشور ها";
const COUNTRY = "کشور";
const ADD_COUNTRY = "افزودن کشور";
const SEE_INSTRUMENT = "مشاهده ساز ها";
const INSTRUMENT = "ساز";
const ADD_INSTRUMENT = "افزودن ساز";
const SEE_GALLERY = "مشاهده گالری ها";
const GALLERY = "گالری";
const SEE_SONG = "مشاهده آهنگ ها";
const SONG = "آهنگ";
const SEE_MOOD = "مشاهده حالت ها";
const MOOD = "حالت";
const sideMenu = {
  SEE_GALLERY,
  GALLERY,
  DASHBOARD,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  SETTING_WEB,
  CATEGORIES,
  SEE_ARTIST,
  ARTIST,
  ADD_ARTIST,
  ALBUM,
  SEE_ALBUM,
  ADD_ALBUM,
  SEE_GENRES,
  GENRES,
  ADD_GENRE,
  SEE_COUNTRY,
  COUNTRY,
  ADD_COUNTRY,
  SEE_INSTRUMENT,
  INSTRUMENT,
  ADD_INSTRUMENT,
  SEE_SONG,
  SONG,
  SEE_MOOD,
  MOOD
};
/* harmony default export */ __webpack_exports__["default"] = (sideMenu);

/***/ }),

/***/ "./panelAdmin/values/strings/fa/constants.js":
/*!***************************************************!*\
  !*** ./panelAdmin/values/strings/fa/constants.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const ALBUM_CONSTANTS = "آلبوم";
const PLAY_LIST_CONSTANTS = "لیست پخش";
const SONG_CONSTANTS = "موزیک";
const FLAG_CONSTANTS = "پرچم";
const ARTIST_CONSTANTS = "هنرمند";
const INSTRUMENT_CONSTANTS = "ساز";
const MUSIC_VIDEO_CONSTANTS = "موزیک ویدئو";
const MOOD_CONSTANTS = "حالت";
const constants = {
  ALBUM_CONSTANTS,
  PLAY_LIST_CONSTANTS,
  SONG_CONSTANTS,
  FLAG_CONSTANTS,
  ARTIST_CONSTANTS,
  INSTRUMENT_CONSTANTS,
  MUSIC_VIDEO_CONSTANTS,
  MOOD_CONSTANTS
};
/* harmony default export */ __webpack_exports__["default"] = (constants);

/***/ }),

/***/ "./panelAdmin/values/strings/fa/global.js":
/*!************************************************!*\
  !*** ./panelAdmin/values/strings/fa/global.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const TRACKS = "آهنگ ها ";
const NO_ENTRIES = "وارد نشده";
const FOLLOWERS = "دنبال کنندگان";
const global = {
  TRACKS,
  NO_ENTRIES: NO_ENTRIES,
  FOLLOWERS
};
/* harmony default export */ __webpack_exports__["default"] = (global);

/***/ }),

/***/ "./panelAdmin/values/strings/fa/index.js":
/*!***********************************************!*\
  !*** ./panelAdmin/values/strings/fa/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navbar_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navbar.js */ "./panelAdmin/values/strings/fa/navbar.js");
/* harmony import */ var _sideMenu_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sideMenu.js */ "./panelAdmin/values/strings/fa/sideMenu.js");
/* harmony import */ var _global_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./global.js */ "./panelAdmin/values/strings/fa/global.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./constants */ "./panelAdmin/values/strings/fa/constants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






const fa = _objectSpread({}, _navbar_js__WEBPACK_IMPORTED_MODULE_0__["default"], {}, _sideMenu_js__WEBPACK_IMPORTED_MODULE_1__["default"], {}, _global_js__WEBPACK_IMPORTED_MODULE_2__["default"], {}, _constants__WEBPACK_IMPORTED_MODULE_3__["default"]);

/* harmony default export */ __webpack_exports__["default"] = (fa);

/***/ }),

/***/ "./panelAdmin/values/strings/fa/navbar.js":
/*!************************************************!*\
  !*** ./panelAdmin/values/strings/fa/navbar.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const APP_NAME = "ریمتال";
const EXPLORE = "کاوش کردن";
const TRACKS = "آهنگ ها";
const PLAYLISTS = "لیست های پخش";
const ALBUMS = "آلبوم ها";
const ARTISTS = "هنرمندان";
const VIDEOS = "ویدیو ها";
const SIGN_IN = "ورود";
const navbar = {
  APP_NAME,
  EXPLORE,
  TRACKS,
  PLAYLISTS,
  ALBUMS,
  ARTISTS,
  VIDEOS,
  SIGN_IN
};
/* harmony default export */ __webpack_exports__["default"] = (navbar);

/***/ }),

/***/ "./panelAdmin/values/strings/fa/sideMenu.js":
/*!**************************************************!*\
  !*** ./panelAdmin/values/strings/fa/sideMenu.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";
const SEE_VARIABLE = "مشاهده متغییر";
const VARIABLE = "متغییر";
const ADD_VARIABLE = "افزودن متغییر";
const GALLERY = "گالری";
const GALLERIES = "گالری ها";
const ADD_GALLERY = "افزودن گالری";
const SEE_GALLERIES = "مشاهده گالری ها";
const CATEGORY = "دسته بندی";
const CATEGORIES = "دسته بندی ها";
const ADD_CATEGORY = "افزودن دسته بندی";
const SEE_CATEGORIES = "مشاهده دسته بندی ها";
const PRODUCT = "محصول ";
const ADD_PRODUCT = "افزودن محصول";
const SEE_PRODUCTS = "مشاهده محصولات ";
const OWNER = "فروشنده ";
const ADD_OWNER = "افزودن فروشنده";
const SEE_OWNERS = "مشاهده فروشندگان ";
const STORE = "فروشگاه ";
const ADD_STORE = "افزودن فروشگاه";
const SEE_STORES = "مشاهده فروشگاه ها ";
const SLIDER = "اسلایدر ";
const ADD_SLIDER = "افزودن اسلایدر";
const SEE_SLIDERS = "مشاهده اسلایدر ها ";
const BANNER = "بنر ";
const ADD_BANNER = "افزودن بنر";
const SEE_BANNERS = "مشاهده بنر ها ";
const NOTIFICATION = "اعلان ";
const ADD_NOTIFICATION = "افزودن اعلان";
const SEE_NOTIFICATIONS = "مشاهده اعلان ها ";
const VERSION = "ورژن ";
const ADD_VERSION = "افزودن ورژن";
const SEE_VERSIONS = "مشاهده ورژن ها ";
const USERS = "کاربران ";
const sideMenu = {
  DASHBOARD,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  SETTING_WEB,
  CATEGORIES,
  SEE_VARIABLE,
  VARIABLE,
  ADD_VARIABLE,
  GALLERY,
  GALLERIES,
  ADD_GALLERY,
  SEE_GALLERIES,
  CATEGORY,
  ADD_CATEGORY,
  SEE_CATEGORIES,
  PRODUCT,
  ADD_PRODUCT,
  SEE_PRODUCTS,
  OWNER,
  ADD_OWNER,
  SEE_OWNERS,
  STORE,
  ADD_STORE,
  SEE_STORES,
  STORE,
  ADD_STORE,
  SEE_STORES,
  SLIDER,
  ADD_SLIDER,
  SEE_SLIDERS,
  BANNER,
  ADD_BANNER,
  SEE_BANNERS,
  NOTIFICATION,
  ADD_NOTIFICATION,
  SEE_NOTIFICATIONS,
  VERSION,
  ADD_VERSION,
  SEE_VERSIONS,
  USERS
};
/* harmony default export */ __webpack_exports__["default"] = (sideMenu);

/***/ }),

/***/ "./panelAdmin/values/strings/index.js":
/*!********************************************!*\
  !*** ./panelAdmin/values/strings/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _en__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./en */ "./panelAdmin/values/strings/en/index.js");
/* harmony import */ var _fa__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fa */ "./panelAdmin/values/strings/fa/index.js");


const Lang = "rtl";
let strings;
if (Lang === "rtl") strings = _fa__WEBPACK_IMPORTED_MODULE_1__["default"];else strings = _en__WEBPACK_IMPORTED_MODULE_0__["default"];
/* harmony default export */ __webpack_exports__["default"] = (strings);

/***/ }),

/***/ 4:
/*!******************************************!*\
  !*** multi ./pages/panelAdmin/banner.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/mojtaba/dev/projects/safir/pages/panelAdmin/banner.js */"./pages/panelAdmin/banner.js");


/***/ }),

/***/ "@material-ui/core/styles":
/*!*******************************************!*\
  !*** external "@material-ui/core/styles" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/styles");

/***/ }),

/***/ "@material-ui/lab/Rating":
/*!******************************************!*\
  !*** external "@material-ui/lab/Rating" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/lab/Rating");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "js-cookie":
/*!****************************!*\
  !*** external "js-cookie" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("js-cookie");

/***/ }),

/***/ "leaflet":
/*!**************************!*\
  !*** external "leaflet" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("leaflet");

/***/ }),

/***/ "leaflet-geosearch":
/*!************************************!*\
  !*** external "leaflet-geosearch" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("leaflet-geosearch");

/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ "next/dynamic":
/*!*******************************!*\
  !*** external "next/dynamic" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/dynamic");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "prop-types-exact":
/*!***********************************!*\
  !*** external "prop-types-exact" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types-exact");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "react-dom":
/*!****************************!*\
  !*** external "react-dom" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-dom");

/***/ }),

/***/ "react-is":
/*!***************************!*\
  !*** external "react-is" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-is");

/***/ }),

/***/ "react-leaflet":
/*!********************************!*\
  !*** external "react-leaflet" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-leaflet");

/***/ }),

/***/ "react-perfect-scrollbar":
/*!******************************************!*\
  !*** external "react-perfect-scrollbar" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-perfect-scrollbar");

/***/ }),

/***/ "react-scrollbars-custom":
/*!******************************************!*\
  !*** external "react-scrollbars-custom" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-scrollbars-custom");

/***/ }),

/***/ "react-toastify":
/*!*********************************!*\
  !*** external "react-toastify" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-toastify");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),

/***/ "swr":
/*!**********************!*\
  !*** external "swr" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("swr");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=banner.js.map