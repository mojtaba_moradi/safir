exports.ids = [0];
exports.modules = {

/***/ "9K3d":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("faye");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var leaflet_geosearch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("6LTc");
/* harmony import */ var leaflet_geosearch__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet_geosearch__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("hgx0");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("AuoD");
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_leaflet__WEBPACK_IMPORTED_MODULE_4__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


 // import "./index.scss";


 // import "leaflet/dist/leaflet.css";
// import pin from "../../assets/Images/icons/pin.png";

const SimpleExample = props => {
  const {
    center,
    data,
    newPin,
    onChange
  } = props; // //console.log({ center, data, newPin });

  const pin = new leaflet__WEBPACK_IMPORTED_MODULE_3___default.a.Icon({
    iconUrl: "https://image.flaticon.com/icons/svg/64/64113.svg",
    iconRetinaUrl: "https://image.flaticon.com/icons/svg/64/64113.svg",
    iconSize: [41, 51],
    iconAnchor: [20, 51]
  }); // const createLeafletElement = () => {
  //   const searchEl = GeoSearchControl({
  //     provider: new OpenStreetMapProvider(),
  //     style: "bar",
  //     showMarker: true,
  //     showPopup: false,
  //     autoClose: true,
  //     retainZoomLevel: false,
  //     animateZoom: true,
  //     keepResult: false,
  //     searchLabel: "search",
  //   });
  //   return searchEl;
  // };
  // lat: "37.3003561", lon: "49.6029556"

  const {
    0: Position,
    1: setPosition
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(center);
  const {
    0: clicked,
    1: setClicked
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: pinLocation,
    1: setPinLocation
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(center); // //console.log({ center });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    setClicked(newPin ? true : false);
  });
  const mapRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    clicked: 0
  });

  const onClickCircle = () => {
    setState({
      clicked: state.clicked + 1
    });
  };

  const CLickedNewPin = e => {
    let latLang = {
      lat: e.latlng.lat.toString(),
      lng: e.latlng.lng.toString()
    };
    onChange(latLang); // setPosition(latLang);

    setPinLocation(latLang);
    setClicked(true); // //console.log(dataOnChange, e);
  };

  const outer = [[50.505, -29.09], [52.505, 29.09]];
  const inner = [[49.505, -2.09], [53.505, 2.09]];

  const clickMap = () => [];

  const mapMarker = data && data.map((hus, index) => {
    //console.log({ hus });
    return __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Marker"], {
      key: index,
      position: hus.location,
      icon: pin
    }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Popup"], null, __jsx("div", {
      className: "markerPopUp"
    }, __jsx("div", {
      className: "markerImage"
    }, __jsx("img", {
      src: hus.image
    })), __jsx("div", {
      className: "markerTitle"
    }, __jsx("span", null, "\u0622\u062F\u0631\u0633 :"), hus.address), __jsx("div", {
      className: "markerTitle"
    }, __jsx("span", null, "\u062A\u0644\u0641\u0646 :"), " ", hus.telephone.map((tel, index) => {
      return __jsx("strong", null, tel, hus.telephone.length - 1 > index ? " , " : " ");
    })), __jsx("div", {
      className: "markerTitle"
    }, " ", __jsx("span", null, "\u0645\u0648\u0628\u0627\u06CC\u0644 :"), hus.mobile))), __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Tooltip"], null, hus.title), __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Circle"], {
      center: hus.location,
      fillColor: "blue",
      onClick: onClickCircle,
      radius: 5
    }));
  });
  return __jsx("div", {
    className: "map"
  }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Map"], {
    draggable: true,
    animate: true,
    center: Position,
    zoom: 13,
    ref: mapRef,
    onClick: newPin ? CLickedNewPin : clickMap
  }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["TileLayer"], {
    attribution: "&copy <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors",
    url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
  }), __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Pane"], {
    name: "cyan-rectangle",
    style: {
      zIndex: 500
    }
  }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Rectangle"], {
    bounds: outer,
    color: "cyan"
  })), newPin ? clicked && __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Marker"], {
    position: pinLocation,
    icon: pin
  }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Circle"], {
    center: pinLocation,
    fillColor: "blue",
    onClick: onClickCircle,
    radius: 5
  })) : mapMarker));
};

/* harmony default export */ __webpack_exports__["default"] = (SimpleExample);

/***/ })

};;