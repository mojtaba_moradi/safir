exports.ids = [1];
exports.modules = {

/***/ "./panelAdmin/screen/Owner/AddOwner/FormMap/map.js":
/*!*********************************************************!*\
  !*** ./panelAdmin/screen/Owner/AddOwner/FormMap/map.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MyMap; });
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-leaflet */ "react-leaflet");
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_leaflet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var leaflet_geosearch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! leaflet-geosearch */ "leaflet-geosearch");
/* harmony import */ var leaflet_geosearch__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet_geosearch__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/screen/Owner/AddOwner/FormMap/map.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





class SearchBox extends react_leaflet__WEBPACK_IMPORTED_MODULE_2__["MapControl"] {
  constructor(props) {
    super(props);
    props.leaflet.map.on("geosearch/showlocation", e => props.updateMarker(e));
  }

  createLeafletElement() {
    const searchEl = Object(leaflet_geosearch__WEBPACK_IMPORTED_MODULE_3__["GeoSearchControl"])({
      provider: new leaflet_geosearch__WEBPACK_IMPORTED_MODULE_3__["OpenStreetMapProvider"](),
      style: "bar",
      showMarker: true,
      showPopup: false,
      autoClose: true,
      retainZoomLevel: false,
      animateZoom: true,
      keepResult: false,
      searchLabel: "search"
    });
    return searchEl;
  }

}

class MyMap extends react__WEBPACK_IMPORTED_MODULE_1__["Component"] {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      center: {
        lat: 31.698956,
        lng: 76.732407
      },
      marker: {
        lat: 31.698956,
        lng: 76.732407
      },
      zoom: 13,
      draggable: true
    });

    _defineProperty(this, "refmarker", Object(react__WEBPACK_IMPORTED_MODULE_1__["createRef"])(this.state.marker));

    _defineProperty(this, "toggleDraggable", () => {
      this.setState({
        draggable: !this.state.draggable
      });
    });

    _defineProperty(this, "updateMarker", e => {
      // const marker = e.marker;
      this.setState({
        marker: e.marker.getLatLng()
      }); //console.log(e.marker.getLatLng());
    });

    _defineProperty(this, "updatePosition", () => {
      const marker = this.refmarker.current;

      if (marker != null) {
        this.setState({
          marker: marker.leafletElement.getLatLng()
        });
      } //console.log(marker.leafletElement.getLatLng());

    });
  }

  render() {
    const position = [this.state.center.lat, this.state.center.lng];
    const markerPosition = [this.state.marker.lat, this.state.marker.lng];
    const SearchBar = Object(react_leaflet__WEBPACK_IMPORTED_MODULE_2__["withLeaflet"])(SearchBox);
    return __jsx("div", {
      className: "jsx-744391653" + " " + "map-root",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 71,
        columnNumber: 7
      }
    }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_2__["Map"], {
      center: position,
      zoom: this.state.zoom,
      style: {
        height: "700px"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 9
      }
    }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_2__["TileLayer"], {
      attribution: "&copy <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors",
      url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 11
      }
    }), __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_2__["Marker"], {
      draggable: true,
      onDragend: this.updatePosition,
      position: markerPosition,
      animate: true,
      ref: this.refmarker,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80,
        columnNumber: 11
      }
    }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_2__["Popup"], {
      minWidth: 90,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 81,
        columnNumber: 13
      }
    }, __jsx("span", {
      onClick: this.toggleDraggable,
      className: "jsx-744391653",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82,
        columnNumber: 15
      }
    }, this.state.draggable ? "DRAG MARKER" : "MARKER FIXED"))), __jsx(SearchBar, {
      updateMarker: this.updateMarker,
      className: "jsx-744391653",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 85,
        columnNumber: 11
      }
    })), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
      id: "744391653",
      __self: this
    }, ".map-root.jsx-744391653{height:100%;}.leaflet-container.jsx-744391653{height:400px !important;width:80%;margin:0 auto;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL21vanRhYmEvZGV2L3Byb2plY3RzL3NhZmlyL3BhbmVsQWRtaW4vc2NyZWVuL093bmVyL0FkZE93bmVyL0Zvcm1NYXAvbWFwLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXVGVyxBQUcyQixBQUdZLFlBRjFCLFlBR1ksVUFDSSxjQUNoQiIsImZpbGUiOiIvaG9tZS9tb2p0YWJhL2Rldi9wcm9qZWN0cy9zYWZpci9wYW5lbEFkbWluL3NjcmVlbi9Pd25lci9BZGRPd25lci9Gb3JtTWFwL21hcC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQsIGNyZWF0ZVJlZiB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgTWFwLCBUaWxlTGF5ZXIsIE1hcmtlciwgUG9wdXAsIE1hcENvbnRyb2wsIHdpdGhMZWFmbGV0IH0gZnJvbSBcInJlYWN0LWxlYWZsZXRcIjtcbmltcG9ydCB7IEdlb1NlYXJjaENvbnRyb2wsIE9wZW5TdHJlZXRNYXBQcm92aWRlciB9IGZyb20gXCJsZWFmbGV0LWdlb3NlYXJjaFwiO1xuXG5jbGFzcyBTZWFyY2hCb3ggZXh0ZW5kcyBNYXBDb250cm9sIHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgcHJvcHMubGVhZmxldC5tYXAub24oXCJnZW9zZWFyY2gvc2hvd2xvY2F0aW9uXCIsIChlKSA9PiBwcm9wcy51cGRhdGVNYXJrZXIoZSkpO1xuICB9XG5cbiAgY3JlYXRlTGVhZmxldEVsZW1lbnQoKSB7XG4gICAgY29uc3Qgc2VhcmNoRWwgPSBHZW9TZWFyY2hDb250cm9sKHtcbiAgICAgIHByb3ZpZGVyOiBuZXcgT3BlblN0cmVldE1hcFByb3ZpZGVyKCksXG4gICAgICBzdHlsZTogXCJiYXJcIixcbiAgICAgIHNob3dNYXJrZXI6IHRydWUsXG4gICAgICBzaG93UG9wdXA6IGZhbHNlLFxuICAgICAgYXV0b0Nsb3NlOiB0cnVlLFxuICAgICAgcmV0YWluWm9vbUxldmVsOiBmYWxzZSxcbiAgICAgIGFuaW1hdGVab29tOiB0cnVlLFxuICAgICAga2VlcFJlc3VsdDogZmFsc2UsXG4gICAgICBzZWFyY2hMYWJlbDogXCJzZWFyY2hcIixcbiAgICB9KTtcbiAgICByZXR1cm4gc2VhcmNoRWw7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTXlNYXAgZXh0ZW5kcyBDb21wb25lbnQge1xuICBzdGF0ZSA9IHtcbiAgICBjZW50ZXI6IHtcbiAgICAgIGxhdDogMzEuNjk4OTU2LFxuICAgICAgbG5nOiA3Ni43MzI0MDcsXG4gICAgfSxcbiAgICBtYXJrZXI6IHtcbiAgICAgIGxhdDogMzEuNjk4OTU2LFxuICAgICAgbG5nOiA3Ni43MzI0MDcsXG4gICAgfSxcbiAgICB6b29tOiAxMyxcbiAgICBkcmFnZ2FibGU6IHRydWUsXG4gIH07XG5cbiAgcmVmbWFya2VyID0gY3JlYXRlUmVmKHRoaXMuc3RhdGUubWFya2VyKTtcblxuICB0b2dnbGVEcmFnZ2FibGUgPSAoKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGRyYWdnYWJsZTogIXRoaXMuc3RhdGUuZHJhZ2dhYmxlIH0pO1xuICB9O1xuXG4gIHVwZGF0ZU1hcmtlciA9IChlKSA9PiB7XG4gICAgLy8gY29uc3QgbWFya2VyID0gZS5tYXJrZXI7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBtYXJrZXI6IGUubWFya2VyLmdldExhdExuZygpLFxuICAgIH0pO1xuICAgIC8vY29uc29sZS5sb2coZS5tYXJrZXIuZ2V0TGF0TG5nKCkpO1xuICB9O1xuXG4gIHVwZGF0ZVBvc2l0aW9uID0gKCkgPT4ge1xuICAgIGNvbnN0IG1hcmtlciA9IHRoaXMucmVmbWFya2VyLmN1cnJlbnQ7XG4gICAgaWYgKG1hcmtlciAhPSBudWxsKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgbWFya2VyOiBtYXJrZXIubGVhZmxldEVsZW1lbnQuZ2V0TGF0TG5nKCksXG4gICAgICB9KTtcbiAgICB9XG4gICAgLy9jb25zb2xlLmxvZyhtYXJrZXIubGVhZmxldEVsZW1lbnQuZ2V0TGF0TG5nKCkpO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBwb3NpdGlvbiA9IFt0aGlzLnN0YXRlLmNlbnRlci5sYXQsIHRoaXMuc3RhdGUuY2VudGVyLmxuZ107XG4gICAgY29uc3QgbWFya2VyUG9zaXRpb24gPSBbdGhpcy5zdGF0ZS5tYXJrZXIubGF0LCB0aGlzLnN0YXRlLm1hcmtlci5sbmddO1xuICAgIGNvbnN0IFNlYXJjaEJhciA9IHdpdGhMZWFmbGV0KFNlYXJjaEJveCk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJtYXAtcm9vdFwiPlxuICAgICAgICA8TWFwXG4gICAgICAgICAgY2VudGVyPXtwb3NpdGlvbn1cbiAgICAgICAgICB6b29tPXt0aGlzLnN0YXRlLnpvb219XG4gICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgIGhlaWdodDogXCI3MDBweFwiLFxuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICA8VGlsZUxheWVyIGF0dHJpYnV0aW9uPScmYW1wO2NvcHkgPGEgaHJlZj1cImh0dHA6Ly9vc20ub3JnL2NvcHlyaWdodFwiPk9wZW5TdHJlZXRNYXA8L2E+IGNvbnRyaWJ1dG9ycycgdXJsPVwiaHR0cHM6Ly97c30udGlsZS5vcGVuc3RyZWV0bWFwLm9yZy97en0ve3h9L3t5fS5wbmdcIiAvPlxuICAgICAgICAgIDxNYXJrZXIgZHJhZ2dhYmxlPXt0cnVlfSBvbkRyYWdlbmQ9e3RoaXMudXBkYXRlUG9zaXRpb259IHBvc2l0aW9uPXttYXJrZXJQb3NpdGlvbn0gYW5pbWF0ZT17dHJ1ZX0gcmVmPXt0aGlzLnJlZm1hcmtlcn0+XG4gICAgICAgICAgICA8UG9wdXAgbWluV2lkdGg9ezkwfT5cbiAgICAgICAgICAgICAgPHNwYW4gb25DbGljaz17dGhpcy50b2dnbGVEcmFnZ2FibGV9Pnt0aGlzLnN0YXRlLmRyYWdnYWJsZSA/IFwiRFJBRyBNQVJLRVJcIiA6IFwiTUFSS0VSIEZJWEVEXCJ9PC9zcGFuPlxuICAgICAgICAgICAgPC9Qb3B1cD5cbiAgICAgICAgICA8L01hcmtlcj5cbiAgICAgICAgICA8U2VhcmNoQmFyIHVwZGF0ZU1hcmtlcj17dGhpcy51cGRhdGVNYXJrZXJ9IC8+XG4gICAgICAgIDwvTWFwPlxuICAgICAgICA8c3R5bGUganN4PlxuICAgICAgICAgIHtgXG4gICAgICAgICAgICAubWFwLXJvb3Qge1xuICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAubGVhZmxldC1jb250YWluZXIge1xuICAgICAgICAgICAgICBoZWlnaHQ6IDQwMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICAgIHdpZHRoOiA4MCU7XG4gICAgICAgICAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIGB9XG4gICAgICAgIDwvc3R5bGU+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG4iXX0= */\n/*@ sourceURL=/home/mojtaba/dev/projects/safir/panelAdmin/screen/Owner/AddOwner/FormMap/map.js */"));
  }

}

/***/ })

};;
//# sourceMappingURL=1.js.map