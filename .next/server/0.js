exports.ids = [0];
exports.modules = {

/***/ "./panelAdmin/component/SimpleExample/index.js":
/*!*****************************************************!*\
  !*** ./panelAdmin/component/SimpleExample/index.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "react-dom");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var leaflet_geosearch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! leaflet-geosearch */ "leaflet-geosearch");
/* harmony import */ var leaflet_geosearch__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet_geosearch__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! leaflet */ "leaflet");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-leaflet */ "react-leaflet");
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_leaflet__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/home/mojtaba/dev/projects/safir/panelAdmin/component/SimpleExample/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


 // import "./index.scss";


 // import "leaflet/dist/leaflet.css";
// import pin from "../../assets/Images/icons/pin.png";

const SimpleExample = props => {
  const {
    center,
    data,
    newPin,
    onChange
  } = props; // //console.log({ center, data, newPin });

  const pin = new leaflet__WEBPACK_IMPORTED_MODULE_3___default.a.Icon({
    iconUrl: "https://image.flaticon.com/icons/svg/64/64113.svg",
    iconRetinaUrl: "https://image.flaticon.com/icons/svg/64/64113.svg",
    iconSize: [41, 51],
    iconAnchor: [20, 51]
  }); // const createLeafletElement = () => {
  //   const searchEl = GeoSearchControl({
  //     provider: new OpenStreetMapProvider(),
  //     style: "bar",
  //     showMarker: true,
  //     showPopup: false,
  //     autoClose: true,
  //     retainZoomLevel: false,
  //     animateZoom: true,
  //     keepResult: false,
  //     searchLabel: "search",
  //   });
  //   return searchEl;
  // };
  // lat: "37.3003561", lon: "49.6029556"

  const {
    0: Position,
    1: setPosition
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(center);
  const {
    0: clicked,
    1: setClicked
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: pinLocation,
    1: setPinLocation
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(center); // //console.log({ center });

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    setClicked(newPin ? true : false);
  });
  const mapRef = Object(react__WEBPACK_IMPORTED_MODULE_0__["useRef"])(null);
  const {
    0: state,
    1: setState
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({
    clicked: 0
  });

  const onClickCircle = () => {
    setState({
      clicked: state.clicked + 1
    });
  };

  const CLickedNewPin = e => {
    let latLang = {
      lat: e.latlng.lat.toString(),
      lng: e.latlng.lng.toString()
    };
    onChange(latLang); // setPosition(latLang);

    setPinLocation(latLang);
    setClicked(true); // //console.log(dataOnChange, e);
  };

  const outer = [[50.505, -29.09], [52.505, 29.09]];
  const inner = [[49.505, -2.09], [53.505, 2.09]];

  const clickMap = () => [];

  const mapMarker = data && data.map((hus, index) => {
    //console.log({ hus });
    return __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Marker"], {
      key: index,
      position: hus.location,
      icon: pin,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 76,
        columnNumber: 9
      }
    }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Popup"], {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 77,
        columnNumber: 11
      }
    }, __jsx("div", {
      className: "markerPopUp",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 78,
        columnNumber: 13
      }
    }, __jsx("div", {
      className: "markerImage",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 15
      }
    }, __jsx("img", {
      src: hus.image,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80,
        columnNumber: 17
      }
    })), __jsx("div", {
      className: "markerTitle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 82,
        columnNumber: 15
      }
    }, __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 17
      }
    }, "\u0622\u062F\u0631\u0633 :"), hus.address), __jsx("div", {
      className: "markerTitle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 15
      }
    }, __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 87,
        columnNumber: 17
      }
    }, "\u062A\u0644\u0641\u0646 :"), " ", hus.telephone.map((tel, index) => {
      return __jsx("strong", {
        __self: undefined,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90,
          columnNumber: 21
        }
      }, tel, hus.telephone.length - 1 > index ? " , " : " ");
    })), __jsx("div", {
      className: "markerTitle",
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 97,
        columnNumber: 15
      }
    }, " ", __jsx("span", {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 99,
        columnNumber: 17
      }
    }, "\u0645\u0648\u0628\u0627\u06CC\u0644 :"), hus.mobile))), __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Tooltip"], {
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 104,
        columnNumber: 11
      }
    }, hus.title), __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Circle"], {
      center: hus.location,
      fillColor: "blue",
      onClick: onClickCircle,
      radius: 5,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 105,
        columnNumber: 11
      }
    }));
  });
  return __jsx("div", {
    className: "map",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 110,
      columnNumber: 5
    }
  }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Map"], {
    draggable: true,
    animate: true,
    center: Position,
    zoom: 13,
    ref: mapRef,
    onClick: newPin ? CLickedNewPin : clickMap,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 111,
      columnNumber: 7
    }
  }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["TileLayer"], {
    attribution: "&copy <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors",
    url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 9
    }
  }), __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Pane"], {
    name: "cyan-rectangle",
    style: {
      zIndex: 500
    },
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 113,
      columnNumber: 9
    }
  }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Rectangle"], {
    bounds: outer,
    color: "cyan",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114,
      columnNumber: 11
    }
  })), newPin ? clicked && __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Marker"], {
    position: pinLocation,
    icon: pin,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118,
      columnNumber: 15
    }
  }, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_4__["Circle"], {
    center: pinLocation,
    fillColor: "blue",
    onClick: onClickCircle,
    radius: 5,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 119,
      columnNumber: 17
    }
  })) : mapMarker));
};

/* harmony default export */ __webpack_exports__["default"] = (SimpleExample);

/***/ })

};;
//# sourceMappingURL=0.js.map